$(document).ready(function() {
	layer.load(2, {shade: 0.3});
	var getGameID = 110;
    getLinkGame({gametype: getGameID}, function(data) {
    	layer.closeAll('loading');
        if (data['message'] !== '') {
            showErrorAlert(data['message'], 2000, function(){
            	window.close();
            });
        }else {
            window.location.href = data['linkGameForward'];
        }
    });
});

function toQueryPair(key, value) {
    if (typeof value === 'undefined') {
        return key;
    }
    return key + '=' + encodeURIComponent(value === null ? '' : String(value));
}

function toBodyString(obj) {
    var ret = [];
    for (var key in obj) {
        var values = obj[key];
        if (values && values.constructor === Array) {
            var queryValues = [];
            for (var i = 0, len = values.length, value; i < len; i++) {
                value = values[i];
                queryValues.push(toQueryPair(key + "[]", value));
            }
            ret = ret.concat(queryValues);
        } else {
            ret.push(toQueryPair(key, values));
        }
    }
    return ret.join('&');
}

function requestHTTP(data, url) {
    var data = data || {},
        urlAPI = url || '',
        promise;

    promise = new Promise(function(resolve, reject) {
        var bodyParam = toBodyString(data),
            xhr = new window.XMLHttpRequest();

        xhr.open('POST', urlAPI, true);
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr.onreadystatechange = function() {
            if ( xhr.readyState === 4 ) {
                if ( xhr.status === 200 ) {
                    resolve( JSON.parse(xhr.responseText) );
                }else {
                    reject( xhr );
                }
            }
        };
        xhr.send(bodyParam);
    });
    return promise;
}

function getBalance(funCallBack) {
    requestHTTP({flag: 'bancaCheckUserBalance'}, location.origin + "/UserService.aspx").then(function(data) {
        funCallBack && funCallBack(data);
    });
}

function getListGames(funCallBack) {
    requestHTTP({flag: 'bancaListGames'}, location.origin + "/UserService.aspx").then(function(data) {
        funCallBack && funCallBack(data);
    });
}

function getLinkGame(params, funCallBack) {
    params['flag'] = 'bancaForwardGame';
    requestHTTP(params, location.origin + "/UserService.aspx").then(function(data) {
        funCallBack && funCallBack(data);
    });
}