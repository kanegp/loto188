var $ = jQuery.noConflict(),
    _body = $("body");

var maxWidth = 1903,
    maxHeight = 680,
    ratio = maxWidth / 1920,
    FishSettings = [
        {
            "desc": "red",
            "cname": "fish1",
            "anim": 0,
            "speed": 7,
            "dir": "left",
            "left": [1, 0],
            "top": [.4, 2 / 7],
            "move": [
                [0, 400],
                [-30, 500],
                [0, 300],
                [-20, 300]
            ],
            "angle": 0,
            "lastdeg": -45
        },
        {
            "desc": "blue",
            "cname": "fish2",
            "anim": 2,
            "speed": 10,
            "dir": "right",
            "left": [0, 0],
            "top": [.6, 2 / 7],
            "move": [
                [180, 700]
            ],
            "angle": 0,
            "lastdeg": 170
        },
        {
            "desc": "gold",
            "cname": "fish3",
            "anim": 0,
            "speed": 5,
            "dir": "right",
            "left": [0, 0],
            "top": [.4, .2],
            "move": [
                [170, 250]
            ],
            "angle": 0,
            "lastdeg": 150
        },
        {
            "desc": "nemo",
            "cname": "fish4",
            "anim": 2,
            "speed": 15,
            "dir": "left",
            "left": [1, 0],
            "top": [.1, .2],
            "move": [
                [-360, 200],
                [-345, 500]
            ],
            "angle": 0,
            "lastdeg": -330
        },
        {
            "desc": "nemo2",
            "cname": "fish4",
            "anim": 2,
            "speed": 15,
            "dir": "left",
            "left": [1, 0],
            "top": [.4, 2 / 7],
            "move": [
                [0, 750],
                [-15, 500]
            ],
            "angle": 0,
            "lastdeg": -30
        },
        {
            "desc": "jelly left",
            "cname": "fish5",
            "anim": 18,
            "speed": 50,
            "dir": "left",
            "left": [1 / 8, 1 / 8],
            "top": [1, 0],
            "move": [
                [-270, 2e3],
                [-400, 1e4]
            ],
            "angle": -65,
            "lastdeg": -400
        },
        {
            "desc": "jelly right",
            "cname": "fish5",
            "anim": 18,
            "speed": 50,
            "dir": "left",
            "left": [5 / 8, 1 / 8],
            "top": [1, 0],
            "move": [
                [-260, 600],
                [-240, 600],
                [-230, 600],
                [-220, 600]
            ],
            "angle": -65,
            "lastdeg": -210
        }
    ],
    BubblesSettings = [
        {
            "top": [1, 0],
            "left": [.05, 1 / 15],
            "size": [10, 5]
        },
        {
            "top": [1, 0],
            "left": [1 / 8, 1 / 15],
            "size": [30, 10]
        },
        {
            "top": [1, 0],
            "left": [1 / 3, 1 / 15],
            "size": [10, 5]
        },
        {
            "top": [1, 0],
            "left": [.5, 1 / 15],
            "size": [20, 5]
        },
        {
            "top": [1, 0],
            "left": [2 / 3, 1 / 15],
            "size": [10, 5]
        },
        {
            "top": [1, 0],
            "left": [6 / 7, 1 / 15],
            "size": [30, 10]
        }
    ],
    imgSize = [260, 180, 200],
    direction = [];

direction[-1] = [1, 0, 1, 0], direction[0] = [-1, 1, -1, -1], direction[1] = [0, -1, 0, 1];
var loc = window.location.pathname;
var dir = loc.substring(0, loc.lastIndexOf('/'));
$(document).ready(function() {
    // animation fish
    $.get(dir + "/banca/images/BanCaBG.jpg", function() {
        $.fn.fish = function(t, d) {
            return setTimeout(function() {
                new Fishes(t)
            }, d), this
        };
        $.fn.bubbles = function(t, d) {
            for (var g = 0; g < d; g++) setTimeout(function() {
                new Bubbles(t)
            }, 750 * (g + 1));
            return this
        };

        $("img[data-width]").each(function() {
            var w = parseInt($(this).attr("data-width"));
            $(this).css({
                "width": w * ratio
            });
        });

        addFishes();
        addBubbles();
    });
    // end animation fish
});

function degToRad(deg) {
    if (deg >= 0) return deg * (Math.PI / 180);
    for (; deg * -1 / 360 > 0;) deg += 360;
    return (360 - Math.abs(deg)) * (Math.PI / 180)
}

function addFishes() {
    var fishesArr = [
        {
            t: 0,
            f: [10, 2500, 4500, 5000, 7500, 7800, 8000]
        },
        {
            t: 1,
            f: [10, 3000, 6000, 7500, 9500]
        },
        {
            t: 2,
            f: [1000, 1500, 2000, 2500, 2750, 3250, 3500, 3750, 4500]
        },
        {
            t: 4,
            f: [10, 5000, 8000]
        },
        {
            t: 5,
            f: [10, 3000, 1500]
        }
    ];

    for (var idx in fishesArr) {
        for (var i = 0, t = fishesArr[idx].t, count = fishesArr[idx].f.length; i < count; i++) {
            _body.fish(t, fishesArr[idx].f[i]);
        }
    }
}

function addBubbles() {
    _body.bubbles(0, 5).bubbles(1, 7).bubbles(2, 7).bubbles(3, 7).bubbles(4, 7).bubbles(5, 5);
}

function Fishes(num) {
    this.type = (0 === num ? 0 : num);
    var th = this;
    this.side = 1, this.currdeg = 0, this.lastdeg = 0, this.rotating = 0, this.distance = 0, this.turn = 1, this.curmove = 0, this.endmove = 0, this.random = Math.random(), this.opt = FishSettings[this.type], this.left = Math.floor(this.random * (maxWidth * this.opt.left[1])) + maxWidth * this.opt.left[0], this.top = Math.floor(this.random * (maxHeight * this.opt.top[1])) + maxHeight * this.opt.top[0], this.maxmove = this.opt.move.length - 1, this.proc, this.ele = document.createElement("span"), this.ele.className = "fish " + this.opt.dir + " " + this.opt.cname, this.ele.style.left = this.left + "px", this.ele.style.top = this.top + "px", this.ele.setAttribute("data-side", 1),
        document.getElementById("itemsGame").appendChild(this.ele),
        this.animate = function() {
            th.side = th.side == th.opt.anim ? 1 : th.side + 1, th.ele.setAttribute("data-side", th.side), setTimeout(function() {
                th.animate()
            }, 300)
        },
        this.rotateFish = function(s) {
            if (s) {
                th.curmove = 0, th.rotating = 0, th.endmove = 0, th.lastdeg = th.currdeg = th.opt.move[th.curmove][0], th.distance = th.opt.move[th.curmove][1], th.left = Math.floor(th.random * (maxWidth * th.opt.left[1])) + maxWidth * th.opt.left[0], th.top = Math.floor(th.random * (maxHeight * th.opt.top[1])) + maxHeight * th.opt.top[0];
                var angle = "left" == th.opt.dir ? th.lastdeg : th.lastdeg > 0 ? 180 - th.lastdeg : th.lastdeg - 180;
                $(th.ele).css({
                    "transform": "rotateZ(" + (angle + th.opt.angle) + "deg)",
                    "display": "inline-block"
                }), th.proc = setTimeout(function() {
                    th.rotateFish()
                }, 1)
            } else if (th.rotating)
                if (th.lastdeg !== th.currdeg) {
                    th.currdeg = th.currdeg > th.lastdeg ? th.currdeg - 1 : th.currdeg + 1;
                    var angle = "left" == th.opt.dir ? th.currdeg : th.currdeg > 0 ? 180 - th.currdeg : th.currdeg - 180;
                    $(th.ele).css({
                        "transform": "rotateZ(" + (angle + th.opt.angle) + "deg)"
                    }), th.proc = setTimeout(function() {
                        th.rotateFish()
                    }, th.turn)
                } else th.rotating = 0, th.proc = setTimeout(function() {
                    th.rotateFish()
                }, 1);
            else th.distance || th.endmove ? (th.distance = th.distance ? th.distance - 1 : 0, th.proc = setTimeout(function() {
                    th.rotateFish()
                }, 1)) : th.curmove == th.maxmove ? (th.rotating = 1, th.lastdeg = th.opt.lastdeg, th.distance = 1e3, th.endmove = 1, th.turn = th.distance / Math.abs(th.lastdeg - th.currdeg), th.proc = setTimeout(function() {
                    th.rotateFish()
                }, 1)) : (th.curmove = th.curmove + 1, th.lastdeg = th.opt.move[th.curmove][0], th.distance = th.opt.move[th.curmove][1], th.lastdeg == th.currdeg ? th.rotating = 0 : (th.rotating = 1, th.turn = (th.distance + Math.abs(th.lastdeg - th.currdeg) * th.opt.speed) / Math.abs(th.lastdeg - th.currdeg)), th.proc = setTimeout(function() {
                    th.rotateFish(0)
                }, 1))
        },
        this.moveFish = function() {
            if (th.top < -20 || th.top > maxHeight || th.left < -20 || th.left > maxWidth) th.random = Math.random(), clearTimeout(th.proc), th.rotateFish(1);
            else {
                var rad = degToRad(th.currdeg);
                th.top = th.top + Math.sin(rad), th.ele.style["top"] = th.top + "px", th.left = th.left + Math.cos(rad) * -1, th.ele.style["left"] = th.left + "px"
            }
            setTimeout(function() {
                th.moveFish()
            }, th.opt.speed)
        },
        this.startSwim = function() {
            th.opt.anim && th.animate(), th.rotateFish(1), th.moveFish()
        },
        this.startSwim()
}

function Bubbles(num) {
    this.type = void 0 === num ? 0 : num;
    var turnAngle = [250, 270, 290],
        th = this;
    this.random = Math.random(), this.opt = BubblesSettings[this.type], this.rotation = Math.floor(50 * this.random) + 70, this.proc, this.ele = document.createElement("img"), this.ele.src = dir + "/banca/images/bubble.png", this.ele.className = "bubble", document.getElementById("itemsGame").appendChild(this.ele), this.moveBubbles = function(s) {
        if (s) {
            th.turn = 100 * (Math.floor(5 * th.random) + 3.5), th.top = Math.floor(th.random * (maxHeight * th.opt.top[1])) + maxHeight * th.opt.top[0], th.left = Math.floor(th.random * (maxWidth * th.opt.left[1])) + maxWidth * th.opt.left[0];
            var wdth = Math.floor(th.random * th.opt.size[1]) + th.opt.size[0];
            th.speed = .25 * wdth, th.ele.style.width = wdth + "px", th.angle = turnAngle[Math.floor(3 * th.random)], th.proc = setTimeout(function() {
                th.moveBubbles()
            }, 10)
        } else if (th.turn)
            if (th.turn = th.turn - 1, th.turn % 50 == 0) th.random = Math.random(), th.angle = turnAngle[Math.floor(3 * th.random)], th.proc = setTimeout(function() {
                th.moveBubbles()
            }, 10);
            else {
                var rad = degToRad(th.angle);
                th.top = th.top + Math.sin(rad), th.ele.style["top"] = th.top + "px", th.left = th.left + Math.cos(rad), th.ele.style["left"] = th.left + "px", th.ele.style.display = "inline-block", th.proc = setTimeout(function() {
                    th.moveBubbles()
                }, th.speed)
            }
        else th.random = Math.random(), clearTimeout(th.proc), th.proc = setTimeout(function() {
                th.moveBubbles(1)
            }, 50)
    }, window.addEventListener ? th.ele.addEventListener("mouseover", function() {
        clearTimeout(th.proc), th.proc = setTimeout(function() {
            th.moveBubbles(1)
        }, 20)
    }) : window.attachEvent && th.ele.attachEvent("onmouseover", function() {
        clearTimeout(th.proc), th.proc = setTimeout(function() {
            th.moveBubbles(1)
        }, 20)
    }), this.moveBubbles(1)
}