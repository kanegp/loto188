import * as helper from './helper';
import { BAUCUA } from './service';

function DATA_SUBMIT_BETTING() { // for baucua1phut
    this.flag = 'baucua1mBet';
    this.gameid = 93;
    this.trace_if = 0;
    this.total_nums = 1;
    this.total_money = 0;
    this.betdata = {};
    this['buyitem[]'] = {'money': 0, 'nums': 1, 'times': '1'};
    this.issue_start = null;
}

function BAUCUA_1PHUT() {
    this.getServerTime = null;
    this.getEndTime = null;
    this.curIssue = null;
    this.els = {
        elTimeCountDown: document.getElementById('counttime'),
        elBtnConfirmBet: document.getElementById('btnConfirmBet'),
        elBtnClosePopupConfirmBet: document.getElementById('closePoupConfirm'),
        elBtnSubmitPopupBet: document.getElementById('submitBetBauCua1M'),
        elPopupConfirmBet: document.getElementById('confirmBet')
    };
    this.objBC = null;
    this.countMaxRequestCurIssue = 0;
    this.requestMaxCurIssue = 5;
    this.storeTimeCountDown = 0;
    this.storeBalanceAfterBet = 0;
    this.storeDataReBet1M = [];
    this.hasBetting = false;

    this.els.elBtnConfirmBet.addEventListener('click', this.showPopupConfirm.bind(this));
    this.els.elBtnClosePopupConfirmBet.addEventListener('click', this.closeConfirm);
    this.els.elBtnSubmitPopupBet.addEventListener('click', this.submitBetting1M.bind(this));

    document.addEventListener("storeDataBetting", event => {
        if (event.detail.message.length > 0) this.els.elBtnConfirmBet.classList.remove('btnBetDisabel');
        else this.els.elBtnConfirmBet.classList.add('btnBetDisabel');
    }, false);

    document.addEventListener("xocAction", event => {
        this.objBC.xocAnimationPlate(() => {
            this.openWinTimeout();
        });
    }, false);

    document.addEventListener("openPlateAction", event => {
        this.openWinTimeout();
    }, false);
}

BAUCUA_1PHUT.prototype.openWinTimeout = function() {
    this.objBC.preventTimeout().remove();
    if (this.storeTimeCountDown <= 1) {
        this.storeTimeCountDown = 10000; // for prevent continuity click
        const paramsOpenWin = {flag: 'baucua1mBetOpenWin', lotteryid: this.objBC.gameID, issue: this.curIssue};
        this.objBC.apiOpenWin(paramsOpenWin, dataResult => {
            this.objBC.storeDataReBet = this.storeDataReBet1M;
            const getCode = dataResult['code'].join('');
            this.objBC.updateHistory(getCode, dataResult['issue']);
            this.objBC.randerPositionDice(getCode);
            this.objBC.apiGetBalance(false, () => {
                const dataEffectOpenWin = {
                    balance: this.objBC.storeBalance,
                    result: getCode,
                    prize: 0
                };
                this.objBC.effectOpenWin(dataEffectOpenWin);
            });

            if (this.hasBetting) {
                this.hasBetting = false;
                // request balance
                let countRequest = 0;
                const intervalBalance = setInterval(() => {
                    this.objBC.apiGetBalance(false, () => {
                        countRequest += 1;
                        if (Number(this.objBC.storeBalance) > Number(this.storeBalanceAfterBet)) {
                            clearInterval(intervalBalance);
                            const winMoney = Number(this.objBC.storeBalance) - Number(this.storeBalanceAfterBet);
                            this.storeBalanceAfterBet = this.objBC.storeBalance;
                            setTimeout(() => {
                                this.objBC.els.elMoneyStatuBetEffect.querySelector('.moneyStatusBet').innerHTML = `+${helper.numberWithCommas(winMoney)}`;
                                this.objBC.els.elMoneyStatuBetEffect.classList.add('winMoney', 'showMoneyEffectBet');
                                // timeout finish effect: -webkit-animation: effectMoneyStatusWin 1.5s ease-out; => 1500
                                const timeoutEffectMoneyWin = 1500;
                                setTimeout(() => {
                                    this.objBC.els.elMoneyStatuBetEffect.classList.remove('lossMoney', 'winMoney', 'showMoneyEffectBet');
                                },timeoutEffectMoneyWin+20);
                            }, this.objBC.timeoutEffectMoneyWinLoss);
                        }
                        if (countRequest >= 10) {
                            clearInterval(intervalBalance);
                        }
                    });
                }, 500);
            }
        });
        this.apiGetCurIssue();
    }
};

BAUCUA_1PHUT.prototype.init = function() {
    this.objBC.apiGetBalance(null, () => {
        this.storeBalanceAfterBet = this.objBC.storeBalance;
    });

    // constructor open plate and show last issue result
    this.objBC.els.elPlateUp.classList.add('openPlate');

    this.apiGetHistoryIssue(data => {
        this.objBC.renderHistory(JSON.stringify(data));
    });

    this.apiInit(data => {
        this.curIssue = data['curissue']['issue'];
        this.getServerTime = data['servertime'];
        this.getEndTime = data['curissue']['endtime'];
        this.lastIssue = data['lastopenissue']['issue'];
        this.runningCurIssue();
    });
};

BAUCUA_1PHUT.prototype.runningCurIssue = function() {
    this.els.elTimeCountDown.querySelector('.issue').innerHTML = this.curIssue;
    this.els.elPopupConfirmBet.querySelector('.issueConfirm').innerHTML = this.curIssue;
    helper.timerCountDown(this.getServerTime, this.getEndTime, $('#counttime .timer'), () => { // do something countdown finish
        helper.fireEvent(this.objBC.els.elPlateUp, 'click');
    }, (obj) => { // call back time countdown
        this.storeTimeCountDown = Number(obj['s']);
    });
};

BAUCUA_1PHUT.prototype.apiInit = function(funCallBack) {
    const params = {flag: 'baucua1mInit'};
    helper.requestHTTP(params, this.objBC.BAUCUA_SERVICE).then(data => {
        if (data['msg'] === '') {
            if (Number(data['gameid']) !== Number(this.objBC.gameID)) {
                helper.showErrorAlert('GameID không hợp lệ!', 2000, function(){
                    window.location.href = '/';
                });
                return;
            }
            this.objBC.mapModel(data['model']);
            funCallBack && funCallBack(data);
        }else {
            helper.showErrorAlert(data['msg'], 4000, function(){
                window.location.href = '/';
            });
        }
    });
};

BAUCUA_1PHUT.prototype.showPopupConfirm = function(event) {
    event.preventDefault();
    if (event.target.classList.contains('btnBetDisabel')) return;
    if (this.objBC.storeDataBetting.length > 0) {
        let str = '';
        let totalMoneyBet = this.objBC.storeDataBetting.reduce(function(accumulator, currentValue) { return accumulator + currentValue.totalMoney;}, 0);
        if (totalMoneyBet*1000 > this.objBC.storeBalance) {
            helper.showErrorAlert('Số dư không đủ!', 2000, function(){});
            return;
        }
        this.objBC.storeDataBetting.forEach(item => {
            if (item['itemBetting'] === 'nai') {
                str += `<p>[Nai]: ${helper.numberWithCommas(item['totalMoney']*1000)}</p>`;
            }else if (item['itemBetting'] === 'cua') {
                str += `<p>[Cua]: ${helper.numberWithCommas(item['totalMoney']*1000)}</p>`;
            }else if (item['itemBetting'] === 'ga') {
                str += `<p>[Gà]: ${helper.numberWithCommas(item['totalMoney']*1000)}</p>`;
            }else if (item['itemBetting'] === 'ca') {
                str += `<p>[Cá]: ${helper.numberWithCommas(item['totalMoney']*1000)}</p>`;
            }else if (item['itemBetting'] === 'bau') {
                str += `<p>[Bầu]: ${helper.numberWithCommas(item['totalMoney']*1000)}</p>`;
            }else if (item['itemBetting'] === 'tom') {
                str += `<p>[Tôm]: ${helper.numberWithCommas(item['totalMoney']*1000)}</p>`;
            }
        });
        this.els.elPopupConfirmBet.querySelector('.detailConfirm').innerHTML = str;
        this.els.elPopupConfirmBet.querySelector('.totalMoneyBetNum').innerHTML = helper.numberWithCommas(totalMoneyBet*1000);
        layer.open({
            area: ['500px', '400px'],
            skin: 'confirmBetting',
            type: 1,
            content: $('#confirmBet')
        });
    }else {
        helper.showErrorAlert('Vui lòng đặt cược!', 2000, function(){});
    }
};

BAUCUA_1PHUT.prototype.closeConfirm = function(event) {
    event.preventDefault();
    layer.closeAll();
};

BAUCUA_1PHUT.prototype.submitBetting1M = function(event) {
    event.preventDefault();
    this.closeConfirm(event);
    const paramsBetting = new DATA_SUBMIT_BETTING();
    paramsBetting.issue_start = this.curIssue;
    this.objBC.storeDataBetting.forEach(item => {
        paramsBetting.total_money += item['totalMoney']*1000;
        paramsBetting.betdata[item['itemBetting']] = item['totalMoney']*1000;
    });
    paramsBetting['buyitem[]'].money = paramsBetting.total_money;
    paramsBetting['buyitem[]'] = JSON.stringify(paramsBetting['buyitem[]']);
    helper.requestHTTP(paramsBetting, this.objBC.BAUCUA_SERVICE).then(data => {
        if (data['msg'] === '') {
            try {
                let getData = JSON.parse(data['betresult']);
                if (getData['success'] === 1 && getData['msg'] !== '') {
                    layer.msg(getData['msg'], {
                        shade : 0.3,
                        closeBtn : true,
                        icon : 1, // 1: icon success, 2: icon error, 3: warning question, 4: lock, 5: face sad, 6: face funny, 7: warning
                        time : 2000
                    }, () => {
                        this.hasBetting = true;
                        this.objBC.storeDataReBet = this.storeDataReBet1M = this.objBC.storeDataBetting;

                        this.storeBalanceAfterBet = getData['data']['balance'];
                        this.objBC.els.elBalance.querySelector('.txtbalance').innerHTML = helper.numberWithCommas(this.storeBalanceAfterBet);

                        this.objBC.els.elMoneyStatuBetEffect.querySelector('.moneyStatusBet').innerHTML = `-${helper.numberWithCommas(paramsBetting.total_money)}`;
                        this.objBC.els.elMoneyStatuBetEffect.classList.add('lossMoney', 'showMoneyEffectBet');

                        this.objBC.cleanBetting();

                        // timeout finish effect loss money: animation: effectMoneyStatusLoss 1s  ease-out; => 1000
                        const timeoutEffectMoneyLoss = 1000;
                        setTimeout(() => {
                            this.objBC.els.elMoneyStatuBetEffect.classList.remove('lossMoney', 'showMoneyEffectBet');
                        }, timeoutEffectMoneyLoss+20);
                    });
                }
            }catch(error) {
                helper.showErrorAlert(`Lỗi JSON.parse <span style="color:red;">data['betresult']</span>: </br> ${error}`, 3000, () => {});
            }
        }else {
            helper.showErrorAlert(data['msg'], 2500, () => {});
        }
    });
};

BAUCUA_1PHUT.prototype.apiGetHistoryIssue = function(funCallBack) {
    const params = {flag: 'baucua1mHis', sid: this.objBC.gameID, num: 10};
    helper.requestHTTP(params, this.objBC.BAUCUA_SERVICE).then(data => {
        if (data['data'] !== '') {
            funCallBack && funCallBack(data['data']);
        }else {
            console.log('Not Found History Issues!');
        }
    });
};

BAUCUA_1PHUT.prototype.apiGetCurIssue = function() {
    const params = {flag: 'baucua1mCurIssue'};
    helper.requestHTTP(params, this.objBC.BAUCUA_SERVICE).then(data => {
        if (helper.formatToDate(data['endtime']).getTime() > helper.formatToDate(data['servertime']).getTime()) {
            this.curIssue = data['issue'];
            this.getServerTime = data['servertime'];
            this.getEndTime = data['endtime'];
            this.lastIssue = data['lastissue'];
            this.countMaxRequestCurIssue = 0;
            this.runningCurIssue();
        }else {
            console.log("Request CurIssue Again!");
            this.countMaxRequestCurIssue += 1;
            const timeoutRequest = setTimeout(() => {
                this.apiGetCurIssue();
            }, 1000);
            if (this.countMaxRequestCurIssue >= this.requestMaxCurIssue) {
                clearTimeout(timeoutRequest);
                helper.showErrorAlert("Lượt chơi tiếp theo (Issue) không hợp lệ. Vui lòng quay lại sau!", 2000, function(){
                    window.location.href = "/";
                });
            }
        }
    });
};

window.addEventListener ?
    window.addEventListener('load', function() {
        const gameID = 93;
        const bauCua = new BAUCUA(gameID);
        bauCua.loadingPage(() => {
            const bauCua1M = new BAUCUA_1PHUT();
            bauCua1M.objBC = bauCua;
            bauCua1M.init();
        });
    }, false) :
    window.attachEvents && window.attachEvents('onload', function() {});