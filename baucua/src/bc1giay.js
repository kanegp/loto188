import { BAUCUA } from './service';
import * as helper from "./helper";

function DATA_SUBMIT_BETTING() {
    this.flag = 'baucua1sBet';
    this.gameid = 90;
    this.money = 0;
    this.betdata = {};
}

function BAUCUA_1GIAY() {
    this.objBC = null;
}

BAUCUA_1GIAY.prototype.init = function() {
    this.objBC.apiGetBalance();
    this.apiInit();

    document.addEventListener("xocAction", event => {
        const paramsBetting = new DATA_SUBMIT_BETTING();
        paramsBetting.gameid = this.objBC.gameID;
        this.objBC.storeDataBetting.forEach(item => {
            paramsBetting.money += item['totalMoney']*1000;
            paramsBetting.betdata[item['itemBetting']] = item['totalMoney']*1000;
        });
        if (paramsBetting.money > this.objBC.storeBalance) {
            helper.showErrorAlert('Số dư không đủ!', 2000, function(){});
            return false;
        }
        if (this.objBC.storeDataBetting.length <= 0) {
            helper.showErrorAlert('Vui lòng đặt cược!', 2000, function(){});
            return false;
        }
        this.objBC.xocAnimationPlate(() => {
            this.objBC.apiOpenWin(paramsBetting, (dataResult) => {
                this.objBC.randerPositionDice(dataResult['result']);
                this.objBC.updateHistory(dataResult['result'], dataResult['betTime']);
                this.objBC.effectOpenWin(dataResult);
            });
        });
    }, false);
};

BAUCUA_1GIAY.prototype.apiInit = function() {
    const params = {flag: 'baucua1sInit'};
    helper.requestHTTP(params, this.objBC.BAUCUA_SERVICE).then(data => {
        if (data['msg'] === '') {
            if (Number(data['gameid']) !== Number(this.objBC.gameID)) {
                helper.showErrorAlert('GameID không hợp lệ!', 2000, function(){window.location.href = '/';});
                return;
            }
            this.objBC.renderHistory(data['history']);
            this.objBC.mapModel(data['model']);
        }else {
            helper.showErrorAlert(data['msg'], 4000, function(){window.location.href = '/';});
        }
    });
};

window.addEventListener ?
    window.addEventListener('load', function() {
        const gameID = 92;
        const bauCua = new BAUCUA(gameID);
        bauCua.loadingPage(() => {
            const bc1giay = new BAUCUA_1GIAY();
            bc1giay.objBC = bauCua;
            bc1giay.init();
        });
    }, false) :
    window.attachEvents && window.attachEvents('onload', function() {});