import * as helper from './helper';
import {configXD, requestHTTP} from "../../xocdia/src/services";

function DATA_ITEM_BETTING() {
    this.totalMoney = 0;
    this.itemBetting = '';
    this.chips = [];
}

export function BAUCUA(gameID) {
    this.storeBalance = 0;
    this.swiperChip = null;
    this.swiperHis = null;
    this.colHisShow = 5;
    this.chipCurrentData = null;
    this.objChipActive = {chipVal: "", idx: 0, chipPosi: {}};
    this.storeDataBetting = [];
    this.wChip = 100;
    this.hChip = 100;
    this.numChipShow = 5;
    this.objChips = [
        {chipVal: "5", status: 1},
        {chipVal: "10", status: 0},
        {chipVal: "25", status: 0},
        {chipVal: "50", status: 0},
        {chipVal: "100", status: 0},
        {chipVal: "200", status: 0},
        {chipVal: "300", status: 0},
        {chipVal: "400", status: 0},
        {chipVal: "500", status: 0}
    ];
    this.USER_SERVICE = "/UserService.aspx";
    this.MOBILE_SERVICE = "/MobileService.aspx";
    this.LOTTERY_SERVICE = "/LotteryService.aspx";
    this.BAUCUA_SERVICE = "/BCService.aspx";
    this.els = {
        elWrapDice: document.getElementById('items-dice'),
        elChipsBetting: document.getElementById('chipsBetting'),
        elMoneyBet: document.getElementById('money-bet'),
        elBalance: document.getElementById('balance'),
        elPlateUp: document.getElementById('plateup'),
        elPlateWrap: document.getElementById('plate'),
        elClearBet: document.getElementById('clearBet'),
        elRebet: document.getElementById('rebet'),
        elTblHistory: document.getElementById('tbl-history'),
        elInforQuestion: document.getElementById('infor-question'),
        elAreaPlayer: document.getElementById('area-player'),
        elReloadBalance: document.getElementById('reloadBalance'),
        elHistoryResult: document.getElementById('history-result'),
        elContentPopupHis: document.getElementById('historyBetUser'),
        elMoneyStatuBetEffect: document.getElementById('effectMoneyBet'),
        elTopPage: document.getElementById('top-page'),
        elBottomPage: document.getElementById('bottom-page')
    };
    this.dataTypeBet = new DATA_ITEM_BETTING();
    this.maxGroupChip = 5;
    this.callbackElChooseBetClick = null;
    this.callbackSlideRemove = false;
    this.gameID = gameID;
    this.model = [];
    this.storeDataReBet = [];
    this.statusOpenWin = {
        0: 'Chưa mở thưởng',
        1: 'Đang chờ mở thưởng',
        2: 'Đang chờ tính thưởng',
        3: 'Trượt',
        4: 'Trúng',
        5: 'Hủy nuôi cược',
        6: 'Cá nhân hủy cược',
        7: 'Hủy mở thưởng'
    };
    this.typeBetModel = {
        1: 'nai',
        2: 'cua',
        3: 'ga',
        4: 'ca',
        5: 'bau',
        6: 'tom'
    };
    this.intervalTimeCheckBalance = null;

    // valable check request timeout
    this.countRequestBalance = 0;
    this.maxCountRequestBalance = 5;
    this.countMaxRequestOpenWin = 0;
    this.requestMaxOpenWin = 20;

    this.objAudioPlay = new Audio('./baucua/src/assets/audio/xocdia.wav');
    this.objAudioOpen = new Audio('./baucua/src/assets/audio/xocdiaopen.mp3');
    this.objAudioChip = new Audio('./baucua/src/assets/audio/chip.wav');
    this.timeoutEffectMoneyWinLoss = 2500;
}

BAUCUA.prototype.loadingPage = function(loadingCallBack) {
    const arrImgLoad = ['./baucua/src/assets/images/BgTile.jpg', './baucua/src/assets/images/icons.png', './baucua/src/assets/images/BgDecor.png', './baucua/src/assets/images/plate.png'];
    let arrImg = [];
    let imagesloaded = 0;
    const _this = this;
    arrImgLoad.forEach(src => {
        const img = new Image();
        img.src = src;
        arrImg.push(img);
    });
    arrImg.forEach(function(img) {
        if (img.complete) {
            handleImageComplete();
        }else {
            img.onload = handleImageComplete;
        }
    });

    function handleImageComplete() {
        imagesloaded++;
        if (imagesloaded === arrImg.length) {
            _this.init();
            loadingCallBack && loadingCallBack();
        }
    }
};

BAUCUA.prototype.aniEls = function() {
    this.els.elTopPage.querySelector('.iconlogo').classList.add('animate');
    this.els.elBalance.classList.add('animate');
    this.els.elInforQuestion.classList.add('animate');
    this.els.elTblHistory.classList.add('animate');
    this.els.elRebet.classList.add('animate');
    this.els.elClearBet.classList.add('animate');
    this.els.elAreaPlayer.classList.add('animate');
    this.els.elBottomPage.classList.add('animate');
};

BAUCUA.prototype.init = function() {
    this.randerPositionDice();
    this.renderChips();
    this.chooseItemBetting();
    this.intervalTimeCheckBalance = setInterval(() => {
        this.apiGetBalance();
    }, 10000);
    this.aniEls();
    this.els.elPlateUp.addEventListener('click', this.plateActionClick.bind(this));
    this.els.elClearBet.addEventListener('click', this.cleanBetting.bind(this));
    this.els.elRebet.addEventListener('click', this.reBetting.bind(this));
    this.els.elTblHistory.addEventListener('click', this.showHistory.bind(this));
    this.els.elInforQuestion.addEventListener('click', this.inforQuestion.bind(this));
    this.els.elReloadBalance.addEventListener('click', this.apiGetBalance.bind(this));
};

BAUCUA.prototype.convertWinNumber = function(winNumber) {
    let strWin = winNumber || null;
    if (strWin && this.gameID === 92) {
        strWin = winNumber.split('|');
    }else if (strWin && this.gameID === 93) {
        strWin = winNumber.split('');
    }
    return strWin;
};

BAUCUA.prototype.mapModel = function(dataModel) {
    dataModel.forEach(model => {
        this.model.push({typebet: model['checkflag'], maxbet: model['maxbet'], levelnum: model['levelnum'], ratio: model['level1'] - model['levelnum']});
    });

    this.model.forEach(itemModel => {
        const elTypeBet = this.els.elAreaPlayer.querySelector(`[data-bet=${itemModel.typebet}] .typebet`);
        const elRatio = document.createElement('div');
        elRatio.classList.add('ratioTypeBet');
        elRatio.innerHTML = itemModel['levelnum'] + ' : ' + itemModel['ratio'];
        if (elTypeBet.querySelector('.ratioTypeBet')) elTypeBet.querySelector('.ratioTypeBet').remove();
        elTypeBet.appendChild(elRatio);
    });
};

BAUCUA.prototype.renderHistory = function(dataHis) {
    const getDataHis = JSON.parse(dataHis);
    let strHis = '';
    let strSlide = '';
    let idxTemp = 0;
    const lenHis = getDataHis.length;
    if (lenHis > 0) {
        strSlide = '<div class="swiper-slide">';
        for (let i = lenHis - 1; i >= 0; i--) {
            let item = getDataHis[i];
            let arrWin = this.convertWinNumber(item['winnumber']);
            let strVi = '';
            arrWin.forEach(itemWin => {
                strVi += `<span class="icon result-${this.typeBetModel[itemWin]}"></span>`;
            });

            let getTitleTime = '';
            if (this.gameID === 92) {
                getTitleTime = item['bettime'];
            }else if (this.gameID === 93) {
                if (i === 0) { // render last result Dice
                    this.randerPositionDice(item['winnumber']);
                }

                const openTime = new Date(item['opentime'].replace(/-/g, "/"));
                const getYear = openTime.getFullYear();
                let getMonth = openTime.getMonth()+1;
                let getDay = openTime.getDate();
                if (getMonth < 10) getMonth = "0" + getMonth.toString();
                if (getDay < 10) getDay = "0" + getDay.toString();
                getTitleTime = `${getYear}${getMonth}${getDay}-${item['issue']}`;
            }

            let strWrapVi = `<div class="typebet-results" title="${getTitleTime}"><div>${strVi}</div></div>`;
            idxTemp += 1;
            if (idxTemp < this.colHisShow) {
                strSlide += strWrapVi;
            } else {
                strSlide += strWrapVi;
                strHis += strSlide + '</div>';
                idxTemp = 0;
                strSlide = '<div class="swiper-slide">';
            }
            if (i === 0 && idxTemp > 0) {
                strHis += strSlide + '</div>';
            }
        }
    }else {
        strHis = '<div class="swiper-slide no-data"><p>Chưa có dữ liệu!</p></div>';
    }

    this.els.elHistoryResult.querySelector('.swiper-wrapper').innerHTML = strHis;

    this.swiperHis = new Swiper('#history-result .swiper-container', {
        slidesPerView: 1,
        preventClicks: true,
        navigation: {
            nextEl: '.history-button-next',
            prevEl: '.history-button-prev',
        },
    });
    this.swiperHis.slideTo(this.swiperHis.slides.length);
};


/**
 * @param strVi = 1|1|2
 * @param titleHover = issue OR time
 */
BAUCUA.prototype.updateHistory = function(strVi, titleHover) {
    let lenSlide = this.swiperHis.slides.length;
    let lastSlide = this.swiperHis.slides[lenSlide - 1];
    let elViResults = lastSlide !== undefined ? lastSlide.querySelectorAll('.typebet-results') : [];
    let arrVi = this.convertWinNumber(strVi);
    if (elViResults.length > 0 && elViResults.length < this.colHisShow) {
        let elViResult = document.createElement('div');
        elViResult.classList.add('typebet-results', 'appendEffect');
        elViResult.setAttribute('title', titleHover);
        for (let i = 0; i < arrVi.length; i++) {
            let item = arrVi[i];
            let elVi = document.createElement('span');
            elVi.classList.add('icon', `result-${this.typeBetModel[item]}`);
            elViResult.appendChild(elVi);
        }
        lastSlide.appendChild(elViResult);
    } else if (elViResults.length === this.colHisShow || elViResults.length === 0) {
        let str = `<div class="swiper-slide"><div class="typebet-results appendEffect" title="${titleHover}">`;
        let strVi = '';
        for (let i = 0; i < arrVi.length; i++) {
            let item = arrVi[i];
            strVi += `<span class="icon result-${this.typeBetModel[item]}"></span>`;
        }
        str += strVi + '</div></div>';
        if (lastSlide.classList.contains('no-data')) {
            this.swiperHis.removeSlide(0);
        }
        this.swiperHis.appendSlide(str);
        this.swiperHis.slideTo(this.swiperHis.slides.length);
    }
};

BAUCUA.prototype.plateActionClick = function(event) {
    event.preventDefault();
    if (this.els.elPlateUp.classList.contains('openPlate')) {
        this.customEventBauCua(document.querySelector('body'), 'xocAction', "");
    }else if (this.els.elPlateUp.classList.contains('statusOpenPlate')) {
        this.customEventBauCua(document.querySelector('body'), 'openPlateAction', "");
    }
};

BAUCUA.prototype.xocAnimationPlate = function(funCallBack) {
    this.preventTimeout().add();
    this.els.elPlateUp.classList.remove('openPlate');

    // timeout close plate: transition: all .2s ease-in-out; => 200
    const timeClosePlate = 150;
    setTimeout(() => {
        try {this.objAudioPlay.play();}catch(error) {console.log(error);}
        this.els.elPlateWrap.classList.add('xocPlate');
        // timeout finish xoc: animation: xocAction .5s cubic-bezier(0.55, 0.055, 0.675, 0.19); => 500
        const timeFinishXoc = 500;
        setTimeout(() => {
            this.els.elPlateWrap.classList.remove('xocPlate');
            this.els.elPlateUp.classList.add('statusOpenPlate');

            //remove dice to can't see
            this.els.elWrapDice.innerHTML = 'Chưa có kết quả!';

            setTimeout(() => {
                funCallBack && funCallBack();
            }, 500);
        }, timeFinishXoc+200);
    }, timeClosePlate+20);
};

BAUCUA.prototype.apiOpenWin = function(paramsBetting, funCallBack) {
    helper.loading(false, this.els.elPlateUp);
    helper.requestHTTP(paramsBetting, this.BAUCUA_SERVICE).then(data => {
        if ((data.hasOwnProperty('msg') && data['msg'] === '') || (data.hasOwnProperty('data') && data['data'].hasOwnProperty('msg') && data['data'].msg === '')) {
            helper.loading(true, this.els.elPlateUp);
            this.countMaxRequestOpenWin = 0;
            this.storeDataReBet = this.storeDataBetting;
            try {
                let dataResult = null;
                let checkSuccess = null;
                if (this.gameID === 92) {
                    const getResult = JSON.parse(data['betresult']);
                    checkSuccess = getResult['type'];
                    dataResult = getResult['data'];
                }else if (this.gameID === 93) {
                    checkSuccess = data['status'];
                    dataResult = data['data'];
                }

                if (checkSuccess === 'success') { // is success
                    if (this.gameID === 92) {
                        this.els.elMoneyStatuBetEffect.querySelector('.moneyStatusBet').innerHTML = `-${helper.numberWithCommas(paramsBetting.money)}`;
                        this.els.elMoneyStatuBetEffect.classList.add('lossMoney', 'showMoneyEffectBet');
                    }

                    // timeout finish effect loss money: animation: effectMoneyStatusLoss 1s  ease-out; => 1000
                    const timeoutEffectMoneyLoss = 1000;
                    setTimeout(() => {
                        try {this.objAudioOpen.play();}catch(error) {console.log(error);}
                        this.els.elPlateUp.classList.remove('statusOpenPlate');
                        this.els.elPlateUp.classList.add('openPlate');
                        funCallBack && funCallBack(dataResult);
                    }, timeoutEffectMoneyLoss+20);
                }else {
                    helper.showErrorAlert(data['msg'], 2000, () => {
                        window.location.reload();
                    });
                }
            }catch(error) {
                helper.showErrorAlert('Lỗi bất thường! Vui lòng liên hệ CSKH.', 2000, () => {
                    window.location.reload();
                });
            }
        }else if (data.hasOwnProperty('data') && data['data'].hasOwnProperty('msg') && data['data'].msg.trim().search('未开奖') >= 0) {
            this.countMaxRequestOpenWin += 1;
            const timeoutRequest = setTimeout(() => {
                this.apiOpenWin(paramsBetting, funCallBack);
            }, 1000);
            if (this.countMaxRequestOpenWin >= this.requestMaxOpenWin) {
                clearTimeout(timeoutRequest);
                helper.showErrorAlert("Open Win Error!", 2000, function(){
                    window.location.reload(false);
                });
            }
        }else {
            helper.showErrorAlert(data['msg'], 2000, () => {
                window.location.reload();
            });
        }
    });
};


/**
 * @param dataResult: {balance: "7910270", betId: "B190602072654100003", betTime: "2019-06-02 07:26:54", betcontent: "flag=baucua1sBet&gameid=...", codes: "[ga]Bet:5,000&&[tom]Bet:5,000&...", details:  ["", "cua", "bau"], orderStatus: 4, prize: "24700", result: "2|5|5"}
 */
BAUCUA.prototype.effectOpenWin = function(dataResult) {
    const result = this.convertWinNumber(dataResult['result']);
    this.storeBalance = Number(dataResult['balance']);

    this.els.elBalance.querySelector('.txtbalance').innerHTML = helper.numberWithCommas(this.storeBalance);

    result.forEach(typeBetKey => {
        if (typeBetKey === '') return false;
        const elTypeBet = this.els.elAreaPlayer.querySelector(`[data-bet=${this.typeBetModel[typeBetKey]}]`);
        elTypeBet.classList.add('effectOpenWin');
    });

    // start check effect result > 2
    let storeCountDupliItem = {item: null, count: 0};
    for(let key in this.typeBetModel) {
        let countDupliItem = 0;
        result.forEach(typeBetKey => {
            if (key === typeBetKey) {
                countDupliItem += 1;
            }
        });
        if (countDupliItem > storeCountDupliItem.count) {
            storeCountDupliItem = {item: this.typeBetModel[key], count: countDupliItem};
        }
    }
    if (storeCountDupliItem.count > 1) {
        const elTypeBetEffectNum = this.els.elAreaPlayer.querySelector(`[data-bet=${storeCountDupliItem.item}] .typebet`);
        const elNum = document.createElement('div');
        elNum.classList.add('effectNumWinMore');
        elNum.innerHTML = `<span>${storeCountDupliItem.count.toString()}</span>`;
        elTypeBetEffectNum.appendChild(elNum);
    }
    // end check effect result > 2

    setTimeout(() => {
        const elsTypeBet = this.els.elAreaPlayer.querySelectorAll('[data-bet]');
        helper.removeClassesAllEls(elsTypeBet, 'effectOpenWin');

        if (Number(dataResult['prize']) > 0) {
            this.els.elMoneyStatuBetEffect.querySelector('.moneyStatusBet').innerHTML = `+${helper.numberWithCommas(dataResult['prize'])}`;
            this.els.elMoneyStatuBetEffect.classList.add('winMoney', 'showMoneyEffectBet');
            // timeout finish effect: -webkit-animation: effectMoneyStatusWin 1.5s ease-out; => 1500
            const timeoutEffectMoneyWin = 1500;
            setTimeout(() => {
                this.els.elMoneyStatuBetEffect.classList.remove('lossMoney', 'winMoney', 'showMoneyEffectBet');
            },timeoutEffectMoneyWin+20);
        }else {
            this.els.elMoneyStatuBetEffect.classList.remove('lossMoney', 'showMoneyEffectBet');
        }

        if (this.gameID === 92) {
            this.cleanBetting();
        }
        this.preventTimeout().remove();

        const elNumEffect = this.els.elAreaPlayer.querySelector('.effectNumWinMore');
        if (elNumEffect) elNumEffect.remove();
    }, this.timeoutEffectMoneyWinLoss);
};


/**
 * @param valPosi: "1|3|6"
 */
BAUCUA.prototype.randerPositionDice = function(valPosi) {
    let valPosiArr = this.convertWinNumber(valPosi);
    const posiItem = valPosiArr ? helper.randomPosiOverlapping(valPosiArr) : helper.randomPosiOverlapping();
    let str = '';
    posiItem.itemRandom.forEach((item, idx) => {
        str += `<span style="top: ${posiItem.posiRandom[idx].y}px;left: ${posiItem.posiRandom[idx].x}px" class="icon dice-${this.typeBetModel[item]}"></span>`;
    });
    this.els.elWrapDice.innerHTML = str;
};

BAUCUA.prototype.renderChips = function() {
    let strEl = '';
    for (let i = 0; i < this.objChips.length; i++) {
        let objChip = this.objChips[i];
        let statusClass = '';
        if (Number(objChip.status)) {
            statusClass = 'active';
            this.objChipActive.idx = i;
            this.objChipActive.chipVal = objChip.chipVal;
        }
        strEl += `<div class="swiper-slide"><a data-chipkey="chip-${objChip.chipVal}" href="javascript:void(0);" class="${statusClass} chip"><span class="icon item chip-${objChip.chipVal}"><span class="light-around icon"></span></span><span class="numchip">${objChip.chipVal}</span></a></div>`;
    }
    this.els.elChipsBetting.innerHTML = strEl;

    this.swiperChip = new Swiper('#select-chip .swiper-container', {
        slidesPerView: this.numChipShow,
        preventClicks: false,
    });

    this.swiperChip.on('slideChangeTransitionEnd', () => {
        let elChipCur = this.els.elChipsBetting.querySelector('.chip.active');
        this.objChipActive.chipPosi = helper.getPositionXYEl(elChipCur);
        if (this.callbackElChooseBetClick) {
            this.addChipToTypeBetting(this.callbackElChooseBetClick);
            this.callbackElChooseBetClick = null;
        }else if (this.callbackSlideRemove) {
            this.posiChipCheckRemove(this.callbackSlideRemove);
            this.callbackSlideRemove = null;
        }
    });

    // start event click in chip
    let elsChip = this.els.elChipsBetting.querySelectorAll('.chip');
    this.objChipActive.chipPosi = helper.getPositionXYEl(elsChip[this.objChipActive.idx]);
    helper.addEventEls('click', elsChip, 'chip', (targetEl, idxEl) => {
        if (targetEl.classList.contains('active')) return;
        let getSwiperSlide = targetEl.parentElement;
        let getSwiperItems = getSwiperSlide.parentElement.querySelectorAll('.swiper-slide');
        this.objChipActive.idx = [...getSwiperItems].indexOf(getSwiperSlide);

        // start update status chip
        this.objChips = this.objChips.map((item, idx) => {
            item['status'] = 0;
            if (idx === this.objChipActive.idx) {
                this.objChipActive.chipVal = item['chipVal'];
                item['status'] = 1;
            }
            return item;
        });
        // end update status chip

        helper.removeClassesEffectTimeout(elsChip, 'active', 'hideEffect', 1000);
        targetEl.classList.add('active');
        this.objChipActive.chipPosi = helper.getPositionXYEl(targetEl);
    });
    // end event click in chip
};

BAUCUA.prototype.apiGetBalance = function(event, funCallBack) {
    if (event) event.target.classList.add('loaddingBalance');
    const params = {flag: 'balance'};
    helper.requestHTTP(params, this.LOTTERY_SERVICE).then(data => {
        if (data['msg'] === '') {
            this.storeBalance = Number(data['balance']);
            this.countRequestBalance = 0;
            funCallBack && funCallBack();
        }else {
            this.storeBalance = 0;
            this.countRequestBalance += 1;
            if (this.countRequestBalance >= this.maxCountRequestBalance) {
                helper.showErrorAlert('Lỗi đường truyền!', 2000, function() {
                    window.location.href = '/';
                });
            }
        }
        this.els.elBalance.querySelector('.txtbalance').innerHTML = helper.numberWithCommas(this.storeBalance);
        if (event) event.target.classList.remove('loaddingBalance');
    });
};

BAUCUA.prototype.chooseItemBetting = function() {
    const elsChoose = document.querySelectorAll('[data-bet]');
    helper.addEventEls('click', elsChoose, 'wraptype', (el) => {
        let curChipActive = null;
        for (let i = 0; i < this.swiperChip.slides.length; i++) {
            let item = this.swiperChip.slides[i];
            if (item.querySelector('.chip.active')) {
                curChipActive = i;
                break;
            }
        }
        let countCurShowChip = this.swiperChip.activeIndex + this.numChipShow;
        if (countCurShowChip <= curChipActive) { // slide to left
            this.swiperChip.slideTo(curChipActive);
            this.callbackElChooseBetClick = el;
        } else if (countCurShowChip - curChipActive > this.numChipShow) { // slide to right
            this.swiperChip.slideTo(curChipActive);
            this.callbackElChooseBetClick = el;
        } else { // slide normal
            this.addChipToTypeBetting(el);
        }
    });

    helper.addEventEls('contextmenu', elsChoose, 'wraptype', targetEl => {
        const elChipEmbed = targetEl.querySelectorAll('.chipEmbed');
        if (elChipEmbed.length <= 0) return;
        this.removeChip(targetEl);
    });
};


/**
 * @param elTypeBetting: element <div data-bet="ga" class="wraptype">...
 */
BAUCUA.prototype.addChipToTypeBetting = function(elTypeBetting) {
    const getDataChoose = elTypeBetting.dataset.bet;
    const checkTypeBetMaxBet = this.storeDataBetting.find(itemMaxBet => itemMaxBet['itemBetting'] === getDataChoose);
    const getTypeBetModel = this.model.find(model => model['typebet'] === getDataChoose);
    if (checkTypeBetMaxBet && checkTypeBetMaxBet.totalMoney*1000 >= getTypeBetModel.maxbet) {
        helper.showErrorAlert(`Vượt mức giới hạn cược ${helper.numberWithCommas(getTypeBetModel.maxbet)}`, 2000, function(){});
        return;
    }
    this.preventTimeout().add();
    const elTypeBetW = elTypeBetting.clientWidth / 2;
    const elTypeBetH = elTypeBetting.clientHeight / 2;
    const elTypeBetX = elTypeBetting.getBoundingClientRect().left;
    const elTypeBetY = elTypeBetting.getBoundingClientRect().top;

    // start create chip embed to type betting
    let elChipAdd = document.createElement('a');
    elChipAdd.style.position = "absolute";
    elChipAdd.setAttribute('data-chipkey', `chip-${this.objChipActive.chipVal}`);
    elChipAdd.style.left = this.objChipActive.chipPosi.x + 'px';
    elChipAdd.style.top = this.objChipActive.chipPosi.y + 'px';
    elChipAdd.setAttribute('href', 'javascript:void(0);');
    elChipAdd.classList.add('chip', 'chipEmbed');
    let elTypeBet = document.querySelector(`[data-bet=${getDataChoose}]`);
    let elChipEmbed = elTypeBet.querySelectorAll('.chipEmbed');
    let elChipLast = undefined;
    let zIndexChip = "1";
    if (elChipEmbed.length > 0) {
        elChipLast = elChipEmbed[elChipEmbed.length - 1];
        zIndexChip = (Number(elChipLast.style.zIndex)+1).toString();
    }
    elChipAdd.style.zIndex = zIndexChip;
    elChipAdd.innerHTML = `<span class="icon item chip-${this.objChipActive.chipVal}"><span class="light-around icon"></span></span><span class="numchip">${this.objChipActive.chipVal}</span>`;
    elTypeBetting.appendChild(elChipAdd);
    // end create chip embed to type betting

    // start waiting for animation chip
    setTimeout(() => {
        let topChipMove = (elTypeBetY + elTypeBetH);
        let leftChipMove = (elTypeBetX + elTypeBetW - this.wChip / 2);

        elChipAdd.style.left = leftChipMove + 'px';
        let elsChipEmbedNew = elTypeBet.querySelectorAll('.chipEmbed');
        if (elsChipEmbedNew.length > this.maxGroupChip) { // group chip and calculate position top for chip next hidden
            topChipMove = elChipLast.style.top.replace('px','');
            elChipAdd.classList.add('chipEmbedHideGroup');
            this.updateTotalMoneyGroupChip(elTypeBetting, this.objChipActive.chipVal);
        }else if(elChipLast) { // position top for chip next
            topChipMove = Number(elChipLast.style.top.replace('px','')) - 10;
        }

        elChipAdd.style.top = topChipMove + 'px';

        this.objAudioChip.play();

        // start store chip position move
        const objStorePosiMove = {
            chipVal: this.objChipActive.chipVal,
            leftChipMove: elTypeBetX + elTypeBetW - this.wChip / 2,
            topChipMove: Number(topChipMove),
            zIndex: zIndexChip
        };
        // end store chip position move

        // start check store item betting
        this.dataBetting(1, getDataChoose, null, objStorePosiMove);
        // end check store item betting

        this.preventTimeout().remove();
    }, 0);
    // end waiting for animation chip
};


/**
 * @param elTypeBet: element <div data-bet="ga" class="wraptype">...
 * @param chipValGroup: [undefined] || string="100"
 */
BAUCUA.prototype.updateTotalMoneyGroupChip = function(elTypeBet, chipValGroup) {
    const elChipEmbed = elTypeBet.querySelectorAll('.chipEmbed');
    let getChipValGroup = chipValGroup || 0;
    if (elChipEmbed.length > this.maxGroupChip) {
        const getDataChoose = elTypeBet.dataset.bet;
        const elChipShowMoney = elChipEmbed[this.maxGroupChip - 1];
        const elNumChipShow = elChipShowMoney.querySelector('.numchip');
        const getItemMap = this.storeDataBetting.find(item => item['itemBetting'] === getDataChoose);
        if (getItemMap) {
            elNumChipShow.innerHTML = getItemMap.totalMoney + Number(getChipValGroup);
        }
    }
};


/**
 * @param elTypeBetting: element <div data-bet="ga" class="wraptype">...
 */
BAUCUA.prototype.removeChip = function(elTypeBetting) {
    this.preventTimeout().add();
    let curChipActive = null;
    for (let i = 0; i < this.swiperChip.slides.length; i++) {
        let item = this.swiperChip.slides[i];
        if (item.querySelector('.chip.active')) {
            curChipActive = i;
            break;
        }
    }
    let countCurShowChip = this.swiperChip.activeIndex + this.numChipShow;
    if (countCurShowChip <= curChipActive) { // slide to left
        this.swiperChip.slideTo(curChipActive);
        this.callbackSlideRemove = elTypeBetting;
    } else if (countCurShowChip - curChipActive > this.numChipShow) { // slide to right
        this.swiperChip.slideTo(curChipActive);
        this.callbackSlideRemove = elTypeBetting;
    }else {
        this.posiChipCheckRemove(elTypeBetting);
    }
};


/**
 * @param elTypeBetting: element <div data-bet="ga" class="wraptype">...
 */
BAUCUA.prototype.posiChipCheckRemove = function(elTypeBetting) {
    const elChipEmbed = elTypeBetting.querySelectorAll('.chipEmbed');
    const elLastChip = elChipEmbed[elChipEmbed.length-1];
    const getDataChipKey = elLastChip.dataset.chipkey;
    const chipValRemove = getDataChipKey.split('-')[1];
    const elChipKeySlide = this.els.elChipsBetting.querySelector(`[data-chipkey=${getDataChipKey}]`);
    const getPosiXY = helper.getPositionXYEl(elChipKeySlide);
    elLastChip.classList.remove('chipEmbedHideGroup');
    elLastChip.style.left = getPosiXY.x+'px';
    elLastChip.style.top = getPosiXY.y+'px';
    setTimeout(() => {
        this.preventTimeout().remove();
        elLastChip.remove();
        this.dataBetting(0, elTypeBetting.dataset.bet, chipValRemove);
        this.updateTotalMoneyGroupChip(elTypeBetting);
    },320);
};

/**
 * store data each item of type betting selected
 * @param flag => 1: add, 0: remove
 * @param typeBetChoose => string = bau or cua or tom or ca or nai or ga
 * @param chipValRemove => 5,10,25,50
 * @param objStorePosiMove => [null] || [object] {chipVal: 5, leftChipMove: 123, topChipMove: 234, zIndex: 2}
 */
BAUCUA.prototype.dataBetting = function(flag, typeBetChoose, chipValRemove, objStorePosiMove) {
    if (this.storeDataBetting.length <= 0 && flag === 1) { // constructor store data betting
        this.dataTypeBet.itemBetting = typeBetChoose;
        this.dataTypeBet.totalMoney += Number(this.objChipActive.chipVal);
        this.dataTypeBet.chips.push(objStorePosiMove);
        this.storeDataBetting.push(this.dataTypeBet);
    }else if (flag === 0 && chipValRemove) { // remove data store
        this.storeDataBetting = this.storeDataBetting.map((item, idx) => {
            if (item['itemBetting'] === typeBetChoose) { // exist
                item['totalMoney'] -= Number(chipValRemove);
            }
            return item;
        }).filter(itemStore => itemStore['totalMoney'] > 0);
    }else { // update data betting
        let countCheckExist = 0;
        this.storeDataBetting = this.storeDataBetting.map((item, idx) => {
            if (item['itemBetting'] === typeBetChoose) { // exist
                const getMaxBetTypeBet = this.model.find(itemMaxBet => itemMaxBet['typebet'] === item['itemBetting']);
                if (item['totalMoney'] <= getMaxBetTypeBet['maxbet']) {
                    item['totalMoney'] += Number(this.objChipActive.chipVal);
                    item.chips.push(objStorePosiMove);
                }
            }else { // not exist
                countCheckExist += 1;
            }
            return item;
        });

        // constructor store new data betting
        if (countCheckExist === this.storeDataBetting.length) {
            this.dataTypeBet = new DATA_ITEM_BETTING();
            this.dataTypeBet.itemBetting = typeBetChoose;
            this.dataTypeBet.totalMoney += Number(this.objChipActive.chipVal);
            this.dataTypeBet.chips.push(objStorePosiMove);
            this.storeDataBetting.push(this.dataTypeBet);
        }
    }

    // update total money bet
    const totalMoneyBet = this.storeDataBetting.reduce(function(accumulator, currentValue) { return accumulator + currentValue.totalMoney;}, 0);
    this.els.elMoneyBet.innerHTML = helper.numberWithCommas(totalMoneyBet*1000);
    // end update total money bet

    this.customEventBauCua(document.querySelector('body'), 'storeDataBetting', this.storeDataBetting);
};

BAUCUA.prototype.customEventBauCua = function(el, nameEvent, msg) {
    const event = new CustomEvent(nameEvent, {
        detail: {
            message: msg,
            time: new Date(),
        },
        bubbles: true,
        cancelable: true
    });
    el.dispatchEvent(event);
};

BAUCUA.prototype.preventTimeout = function() {
    return {
        add: () => {
            let elOverlayBG = document.createElement('span');
            elOverlayBG.classList.add('bgoverlay');
            document.querySelector('body').appendChild(elOverlayBG);
            elOverlayBG.addEventListener('contextmenu', e => e.preventDefault());
        },
        remove: () => {
            if (document.querySelector('.bgoverlay')) document.querySelector('.bgoverlay').remove();
        }
    };
};

BAUCUA.prototype.cleanBetting = function(flagWinLoss) {
    this.storeDataBetting.forEach(itemStore => {
        const elDataBet = this.els.elAreaPlayer.querySelector(`[data-bet=${itemStore.itemBetting}]`);
        const elsChipEmbed = elDataBet.querySelectorAll('.chipEmbed');
        helper.removeAllEls(elsChipEmbed);
    });

    this.storeDataBetting = [];
    this.els.elMoneyBet.innerHTML = '0';
    this.dataTypeBet = new DATA_ITEM_BETTING();

    this.customEventBauCua(document.querySelector('body'), 'storeDataBetting', this.storeDataBetting);
};

BAUCUA.prototype.reBetting = function(event) {
    event.preventDefault();
    if (this.storeDataReBet.length <= 0) {
        helper.showErrorAlert('Không tồn tại cược trước đó!', 2000, function(){});
        return false;
    }

    const _this = this;

    // start remove all chips added before
    this.storeDataBetting.forEach(itemStore => {
        const elDataBet = this.els.elAreaPlayer.querySelector(`[data-bet=${itemStore.itemBetting}]`);
        const elsChipEmbed = elDataBet.querySelectorAll('.chipEmbed');
        helper.removeAllEls(elsChipEmbed);
    });
    // end remove all chips added before

    this.storeDataBetting = this.storeDataReBet;

    this.customEventBauCua(document.querySelector('body'), 'storeDataBetting', this.storeDataBetting);

    let totalMoneyBet = 0;
    this.storeDataReBet.forEach(itemsChipUI => {
        const chipUIData = itemsChipUI.chips;
        const elDataBet = this.els.elAreaPlayer.querySelector(`[data-bet=${itemsChipUI.itemBetting}]`);
        const elsChipEmbed = elDataBet.querySelectorAll('.chipEmbed');
        totalMoneyBet += itemsChipUI.totalMoney;
        helper.removeAllEls(elsChipEmbed);
        chipUIData.forEach((itemChipUI, idx) => {
            if (idx+1 <= this.maxGroupChip) {
                renderChipReBetting(elDataBet, itemChipUI, false);
            }else { // group chip value
                renderChipReBetting(elDataBet, itemChipUI, true);
            }
        });
    });

    this.els.elMoneyBet.innerHTML = helper.numberWithCommas(totalMoneyBet*1000);

    function renderChipReBetting(elTypeBetting, dataChipUI, flagGroupChipVal) {
        let elChipAdd = document.createElement('a');
        elChipAdd.style.position = "absolute";
        elChipAdd.setAttribute('data-chipkey', `chip-${dataChipUI.chipVal}`);
        elChipAdd.style.left = dataChipUI.leftChipMove + 'px';
        elChipAdd.style.top = dataChipUI.topChipMove + 'px';
        elChipAdd.style.zIndex = dataChipUI.zIndex;
        elChipAdd.setAttribute('href', 'javascript:void(0);');
        elChipAdd.classList.add('chip', 'chipEmbed');
        elChipAdd.innerHTML = `<span class="icon item chip-${dataChipUI.chipVal}"><span class="light-around icon"></span></span><span class="numchip">${dataChipUI.chipVal}</span>`;
        elTypeBetting.appendChild(elChipAdd);
        if (flagGroupChipVal) {
            elChipAdd.classList.add('chipEmbedHideGroup');
            _this.updateTotalMoneyGroupChip(elTypeBetting, 0);
        }
    }
};

BAUCUA.prototype.showHistory = function(event) {
    if (event) event.preventDefault();
    if (this.gameID === 92) {
        const params = {flag: 'baucua1sInit'};
        helper.requestHTTP(params, this.BAUCUA_SERVICE).then(data => {
            if (data['msg'] === '') {
                this.renderHisPopup(data['history']);
            }else {
                helper.showErrorAlert(data['msg'], 2000, function(){});
            }
        });
    }else if (this.gameID === 93) {
        const params = {flag: 'baucua1mHisByUser', sid: this.gameID};
        helper.requestHTTP(params, this.BAUCUA_SERVICE).then(data => {
            if (data['status'] === 'success') {
                const getHis = data['data']['reslist'];
                const getObjHis = getHis.map(item => {
                    return {
                        betcontent: item['betcontent'],
                        bettime: item['issue'],
                        codes: item['codes'],
                        status: item['status'],
                        winnumber: item['winnumber'],
                        id: item['id'],
                        amount: item['amount'],
                        bonus: item['bonus']
                    };
                });
                this.renderHisPopup(JSON.stringify(getObjHis));
            }else {
                helper.showErrorAlert("Get History By User Error!", 2000, function(){});
            }
        });
    }
};


/**
 * @param dataHis: [{betcontent: "flag=baucua1sBet&gameid=92...", bettime: "2019-06-02 07:18:22", codes: "[ga]Bet:5,000&&[tom]Bet:5,000...", status: 4, winnumber: "6|4|6", id: "B190605165553100035"}]
 */
BAUCUA.prototype.renderHisPopup = function(dataHis) {
    let convertJson = JSON.parse(dataHis);
    let lenHisApi = convertJson.length;
    let strHis = '<table>';
    if (lenHisApi > 0) {
        for (let i = 0; i < lenHisApi; i++) {
            let item = convertJson[i];
            let arrWin = [];
            let strVi = '';
            arrWin = this.convertWinNumber(item['winnumber']);
            if (arrWin && arrWin.length > 0) {
                for (let j = 0; j < arrWin.length; j++) {
                    strVi += `<span class="icon result-${this.typeBetModel[arrWin[j]]}"></span>`;
                }
            }else {
                strVi = '?';
            }
            if (this.gameID === 92) {
                strHis += `<tr><td>${item["id"]}</td><td>${item["bettime"]}</td><td>${this.formatBetContent(item['codes'], item['betcontent'])}</td><td><div class="typebet-results">${strVi}</div></td><td class="status_${item["status"]}">${this.statusOpenWin[item["status"]]}</td></tr>`;
            }else if (this.gameID === 93) {
                let btnCancelBet = '';
                if (item['status'] === 0) {
                    btnCancelBet = '<a class="cancelBetItem" href="javascript:void(0);" data-id="' + item["id"] + '">Hủy</a>';
                }
                strHis += `<tr>
                                <td>${item["id"]}</td>
                                <td>${item["bettime"]}</td>
                                <td>${this.formatBetContent(item['codes'], item['betcontent'])}</td>
                                <td>${helper.numberWithCommas(item["amount"])}</td>
                                <td>${helper.numberWithCommas(item["bonus"])}</td>
                                <td><div class="typebet-results">${strVi}</div></td>
                                <td class="status_${item["status"]}">${this.statusOpenWin[item["status"]]}</td>
                                <td>${btnCancelBet}</td>
                           </tr>`;
            }
        }
    }else {
        strHis += '<tr><td style="text-align:center;">Không có dữ liệu!</td></tr></table>';
    }
    this.els.elContentPopupHis.querySelector('.content').innerHTML = strHis;

    let arrBtnCancelBet = this.els.elContentPopupHis.querySelectorAll('.cancelBetItem');
    helper.addEventEls('click', arrBtnCancelBet, 'cancelBetItem', (targetEl, i) => {
        this.cancelBetItem(targetEl.dataset.id);
    });

    if (document.getElementById('hisUserBet')) return false;

    layer.open({
        area: ['929px', '450px'],
        skin: 'hisUserBet',
        type: 1,
        content: $('#historyBetUser'),
        shade: 0.7,
        shadeClose: true,
        scrollbar: true,
        id: "hisUserBet"
    });
};

BAUCUA.prototype.cancelBetItem = function(id) {
    let params = {flag: 'baucua1mCancel',sid: id};
    requestHTTP(params, this.BAUCUA_SERVICE).then( data => {
        if (data['status'] === 'success') {
            layer.msg(data['data'], {
                shade : 0.3,
                closeBtn : true,
                icon : 1, // 1: icon success, 2: icon error, 3: warning question, 4: lock, 5: face sad, 6: face funny, 7: warning
                time : 2000
            }, () => {
                this.showHistory();
            });
        }
    }).catch(function (error) {

    });
};


/**
 * @param codes: "[ga]Bet:5,000&&[tom]Bet:5,000,Prize:14,800&&[bau]Bet:5,000&&"
 * @param betContent: "flag=baucua1sBet&gameid=92&money=15000&...."
 * @returns {string}
 */
BAUCUA.prototype.formatBetContent = function(codes, betContent) {
    let arrCodes = codes.split('&').clean("");
    let strBetContent = betContent.split('&');
    let str = '';
    for (let j = 0; j < arrCodes.length; j++) {
        let itemCode = arrCodes[j].toLowerCase();
        let getPrize = itemCode.split(':');
        let prize = '';
        if (this.gameID === 92) {
            prize = 0;
        }
        if (itemCode.search('prize') >= 0) prize = getPrize[getPrize.length - 1];
        if (itemCode.indexOf('[nai]') >= 0) {
            str += `<span class="detailBetUser"><span>Nai: ${helper.numberWithCommas(getVal(strBetContent, 'nai'))}</span><span class="winMoney">${prize}</span></span>`;
        }else if (itemCode.indexOf('[cua]') >= 0) {
            str += `<span class="detailBetUser"><span>Cua: ${helper.numberWithCommas(getVal(strBetContent, 'cua'))}</span><span class="winMoney">${prize}</span></span>`;
        }else if (itemCode.indexOf('[ga]') >= 0) {
            str += `<span class="detailBetUser"><span>Gà: ${helper.numberWithCommas(getVal(strBetContent, 'ga'))}</span><span class="winMoney">${prize}</span></span>`;
        }else if(itemCode.indexOf('[ca]') >= 0) {
            str += `<span class="detailBetUser"><span>Cá: ${helper.numberWithCommas(getVal(strBetContent, 'ca'))}</span><span class="winMoney">${prize}</span></span>`;
        }else if(itemCode.indexOf('[bau]') >= 0) {
            str += `<span class="detailBetUser"><span>Bầu: ${helper.numberWithCommas(getVal(strBetContent, 'bau'))}</span><span class="winMoney">${prize}</span></span>`;
        }else if(itemCode.indexOf('[tom]') >= 0) {
            str += `<span class="detailBetUser"><span>Tôm: ${helper.numberWithCommas(getVal(strBetContent, 'tom'))}</span><span class="winMoney">${prize}</span></span>`;
        }
    }
    return str;

    function getVal(strBetContent, type) {
        let value = 0;
        for (let i = 0; i < strBetContent.length; i++) {
            let valItem = strBetContent[i];
            if (valItem.indexOf('betdata['+type+']') >= 0) {
                value = valItem.split('=')[1];
                break;
            }
        }
        return value;
    }
};

BAUCUA.prototype.inforQuestion = function(event) {
    event.preventDefault();
    layer.open({
        area: ['929px', '450px'],
        skin: 'inforQuestion',
        type: 1,
        content: $('#guiderBauCua'),
        shade: 0.7,
        shadeClose: true,
        scrollbar: true
    });
};

Array.prototype.clean = function(deleteValue) {
    for (let i = 0; i < this.length; i++) {
        if (this[i] === deleteValue) {
            this.splice(i, 1);
            i--;
        }
    }
    return this;
};