const path = require('path');
const webpack = require('webpack');
module.exports = {
    entry: {
        bc1giay: './src/bc1giay.js',
        bc1phut: './src/baucua1phut.js'
    },
    output: {
        filename: '[name].min.js',
        path: path.resolve(__dirname, 'dist')
    },
    watch: true
};