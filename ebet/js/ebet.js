$(document).ready(function() {
    loginEBET();
});

function loginEBET() {
    requestHTTP({flag: 'ebetLogin'}, location.origin + "/UserService.aspx").then(function(data) {
        if (data.message === "") {
            var linkGameEbet = "http://loto188.drfs.live/h5/3df1a6&username="+data.username+"&accessToken="+data.accessToken;
            window.location.href = linkGameEbet;
        }else {
            showErrorAlert(data.message, 3000, function() {
                window.close();
            });
        }
    });
}

function toQueryPair(key, value) {
    if (typeof value === 'undefined') {
        return key;
    }
    return key + '=' + encodeURIComponent(value === null ? '' : String(value));
}
function toBodyString(obj) {
    var ret = [];
    for (var key in obj) {
        var values = obj[key];
        if (values && values.constructor === Array) {
            var queryValues = [];
            for (var i = 0, len = values.length, value; i < len; i++) {
                value = values[i];
                queryValues.push(toQueryPair(key + "[]", value));
            }
            ret = ret.concat(queryValues);
        } else {
            ret.push(toQueryPair(key, values));
        }
    }
    return ret.join('&');
}

function requestHTTP(data, url) {
    var data = data || {},
        urlAPI = url || '',
        promise;

    promise = new Promise(function(resolve, reject) {
        var bodyParam = toBodyString(data),
            xhr = new window.XMLHttpRequest();

        xhr.open('POST', urlAPI, true);
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr.onreadystatechange = function() {
            if ( xhr.readyState === 4 ) {
                if ( xhr.status === 200 ) {
                    resolve( JSON.parse(xhr.responseText) );
                }else {
                    reject( xhr );
                }
            }
        };
        xhr.send(bodyParam);
    });
    return promise;
}