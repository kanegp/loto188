var getBalancePromise = 0;
var getTokenUser = '';
var interLoadBalance;

$(document).ready(function() {
	$("#goTop").on('click', function(e) {
		e.preventDefault();
		$("html,body").animate({
            scrollTop: 0
        }, 200);
	});

	$('#moneyHaba').text(numberWithCommas(parseFloat($('#moneyHaba').text())));

    $('#refresh').on('click', function(e) {
    	e.preventDefault();
    	var _this = $(this);
        _this.addClass('reloadBalanceHaba');
        getBalanceHaba(function() {
            $('#moneyHaba').text(numberWithCommas(parseFloat(getBalancePromise)));
            _this.removeClass('reloadBalanceHaba');
        });
	});

    getBalanceHaba(function() {
        $('#moneyHaba').text(numberWithCommas(parseFloat(getBalancePromise)));
    });

    if ($('#gamePlay').length) {
        interLoadBalance = setInterval(function() {
            getBalanceHaba(function() {
                $('#moneyHaba').text(numberWithCommas(parseFloat(getBalancePromise)));
            });
        }, 10000);
    }

    // check game play valid
    $('.itemsGame .itemsInner a').on('click', function(e) {
        e.preventDefault();
        var _this = $(this),
            getURL = _this.attr('href');
        if ($('#userMode').val() === 'true') {
            window.location.href = getURL;
            return;
        }
        layer.load(2, {shade: 0.3});
        getBalanceHaba(function() {
            layer.closeAll('loading');
            window.location.href = getURL;
        });
    });
});

function getBalanceHaba(funCallBack) {
    if ($('#userMode').val() === 'true' || $('.errorShow').length) return;
    requestHTTP({flag: 'habaCheckUserBalance'}, location.origin + "/UserService.aspx").then(function(data) {
        layer.closeAll('loading');
        getBalancePromise = data.balance;
        getTokenUser = data.sessionToken;
        if (data.msg && data.msg !== '') {
            location.href = location.origin;
            return;
        }
        if (data.message !== '' && getTokenUser === '') {
            clearInterval(interLoadBalance);
            showErrorAlert("Không kết nối đươc máy chủ!", 2500, function() {
                window.close();
            });
            return;
        }
        if (getTokenUser === '') {
            clearInterval(interLoadBalance);
            showAlert('Phiên chơi game của bạn đã hết. Chọn "OK" để đăng nhập lại.', 3, function() {
                window.location.reload();
            });
            return;
        }
        funCallBack && funCallBack();
    });
}

function toQueryPair(key, value) {
    if (typeof value === 'undefined') {
        return key;
    }
    return key + '=' + encodeURIComponent(value === null ? '' : String(value));
}

function toBodyString(obj) {
    var ret = [];
    for (var key in obj) {
        var values = obj[key];
        if (values && values.constructor === Array) {
            var queryValues = [];
            for (var i = 0, len = values.length, value; i < len; i++) {
                value = values[i];
                queryValues.push(toQueryPair(key + "[]", value));
            }
            ret = ret.concat(queryValues);
        } else {
            ret.push(toQueryPair(key, values));
        }
    }
    return ret.join('&');
}

function requestHTTP(data, url) {
    var data = data || {},
        urlAPI = url || '',
        promise;

    promise = new Promise(function(resolve, reject) {
        var bodyParam = toBodyString(data),
            xhr = new window.XMLHttpRequest();

        xhr.open('POST', urlAPI, true);
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr.onreadystatechange = function() {
            if ( xhr.readyState === 4 ) {
                if ( xhr.status === 200 ) {
                    resolve( JSON.parse(xhr.responseText) );
                }else {
                    reject( xhr );
                }
            }
        };
        xhr.send(bodyParam);
    });
    return promise;
}

function confirmLogoutHaba() {
    showConfirm("Bạn có muốn rời khỏi trò chơi?", userLogOutHaba);
}

function logoutComplete(data) {
    if (data !== "") {
	    window.close();
    }
}