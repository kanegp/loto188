module.exports = function (grunt) {
    grunt.initConfig({
        watch: {
            scripts: {
                files: ['./common.js', './game/interface-game.js', './game/lt-result.js', './game/lottery-game.js', './game/trace.js'],
                tasks: ['uglify']
            }
        },
        uglify: {
            js: {
                files: {
                    'build/lib.min.js': ['./lib/jquery-1.11.1.js', './lib/bootstrap-3.3.2.js', './lib/layer.js'],
                    'build/common.min.js': ['./common.js', './header.js'],
                    'build/lt.min.js': ['./game/interface-game.js', './game/lt-result.js', './game/lottery-game.js'],
                    'build/lt-trace.min.js': ['./game/trace.js'],
                    'build/mdice.min.js': ['mdice.js'],
                    'build/game-dice-all.min.js': ['./../buildjs/game-dice-all.js'],
                    'build/lt-sports.min.js': ['./lt-sports.js'],
                    'build/haba.min.js': ['../haba/js/haba.js'],
                    'build/ebet.min.js': ['../ebet/js/ebet.js']
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.registerTask('default', ['uglify', 'watch']);
};