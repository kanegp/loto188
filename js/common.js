var curBalance = 0;
var countLoadBalance = 0;
redirectWap();
$(window).on('load', function () {
    $('.feedbackLink').on('click', function (e) {
        e.preventDefault();
        var getUsernamePlayer = $('[name="j_id5:usernamePlayer"]').val(),
            googleSurveLink = 'https://docs.google.com/forms/d/e/1FAIpQLSchQFyeFXRnc9t1RnYasSV1le9SvzSmHxWLoWDrkd4W83FwOg/viewform?entry.1171301224=Tài+khoản:+' + getUsernamePlayer + '%0A%0A%0A%0A';
        window.open(googleSurveLink, '_blank');
    });

    // Start active menu left in dashboard
    var arrLinkActive = [
        {
            name: 'account',
            subLink: ['PersonPwd', 'PersonMsg', 'PersonBank', 'TransferMoney']
        },
        {
            name: 'agency',
            subLink: ['AgentReg', 'AgentUser', 'AgentAutoReg', 'AgentBalance']
        },
        {
            name: 'history',
            subLink: ['ReportZb', 'BonusReport', 'BetRecord', 'TraceRecord']
        },
        {
            name: 'notification',
            subLink: ['Index']
        },
        {
            name: 'report',
            subLink: ['ReportWL', 'ReportTD', 'ReportDay']
        },
        {
            name: 'quickview',
            subLink: ['DWDeposit', 'DWDepositRecord', 'DWWithDrawl', 'DWWithDrawlRecord', 'RechargeInstructions']
        },
        {
            name: 'question',
            subLink: ['question']
        }
    ],
        getPathName = location.pathname.toLowerCase().split('/'),
        getNameLink = getPathName[getPathName.length - 1].toString().split('.')[0],
        $panel = $('.dashboard__menu .panel__item');

    if ($panel.length) {
        for (var i = 0, len = arrLinkActive.length; i < len; i++) {
            var arrSubLink = arrLinkActive[i].subLink,
                flag = false;

            for (var j = 0, lenSubLink = arrSubLink.length; j < lenSubLink; j++) {
                var toLowerTxt = arrSubLink[j].toLowerCase();
                if (getNameLink === toLowerTxt) {
                    var $panelTitle = $panel.eq(i).find('.panel__title'),
                        $panelCollapse = $panel.eq(i).find('.panel-collapse'),
                        $link = $panelCollapse.find('a');

                    $panelTitle.removeClass('collapsed');
                    $panelTitle.attr('aria-expanded', true);
                    $panelCollapse.addClass('in');

                    $link.each(function () {
                        $link.removeClass('active');
                        if (typeof ($(this).attr('href')) !== 'undefined' && $(this).attr('href').toLowerCase().search(toLowerTxt) > 0) {
                            $(this).addClass('active');
                            return false;
                        }
                    });

                    flag = true;

                    break;
                }
            }

            if (flag) break;
        }
    }
    // End active menu left in dashboard

    setTimeout(function () {
        $('.letter__alert').addClass('alert__tip--show');
    }, 800);

    // hover result
    hoverResult();

    // reload money
    if (document.getElementById('flagLogin')) {
        var valCheckUserLogin = document.getElementById('flagLogin').value;
        if (!Number(valCheckUserLogin)) {
            mbInterval = window.setInterval('loadbalance();', 8000);
        }
    }

    // cookie toggle money
    if (getCookie('toggle_money') === null || !getCookie('toggle_money')) {
        $('.cash__real .line').removeClass('showHideMoney');
        $('.toggle__cash').attr('title', 'Ẩn số tiền');
    }

    $('.check__link').on('click', function () {
        if (parseInt($('#flagLogin').val())) {
            showErrorAlert("Vui lòng đăng nhập.");
            return false;
        }
        return true;
    });

    var elHomepage = $('.homepage:not(.promotionsPage)');
    if (elHomepage && elHomepage.length) {
        setTimeout(function () {
            if (getCookie('support_alert') === '1') return;
            $('.live__chat--link').addClass('showHover');
            document.getElementById('audio__noti').play();
        }, 500);
        $('#close__chat').on('click', function (e) {
            e.stopPropagation();
            $('.live__chat--link').removeClass('showHover');
            setCookie('support_alert', 1, 2);
        });
    }
});

$(window).on('scroll', function () {
    var val = $(this).scrollTop();
    scrollTopPage(val);
});

function hoverResult() {
    var $tblResult = $('#lotteryResult'),
        $tblFirstLast = $('#firstLast tr:not(.result__title)');

    $tblFirstLast.off('mouseover').on('mouseover', function (e) {
        e.preventDefault();
        var _this = $(this),
            arrItem = typeof (_this.data('win')) !== 'undefined' ? _this.data('win').toString().split(',') : [];

        $('[data-numwin]').each(function (index) {
            var _this = $(this),
                $numHightLight = _this.find('.num__hightlight'),
                numHightLight = $numHightLight.text(),
                txt = _this.text();
            for (var i = 0, len = arrItem.length; i < len; i++) {
                if (numHightLight.length === 2 && numHightLight === arrItem[i]) {
                    var html = '<span class="active">' + numHightLight + '</span>';
                    $numHightLight.html(html);
                } else if (numHightLight.length === 3 && numHightLight.substr(-2) === arrItem[i]) {
                    var html = numHightLight.substring(0, 1) + '<span class="active">' + arrItem[i] + '</span>';
                    $numHightLight.html(html);
                } else if (numHightLight.length === 1 && _this.data('row') === 'dacbiet' && (numHightLight === arrItem[i].split('')[0]) && (parseInt($.lt_method_data.methodid) === 9 || parseInt($.lt_method_data.methodid) === 20)) {
                    var arrNum = txt.split(''),
                        idxNum = $.inArray(numHightLight, arrNum),
                        strSlice = '';
                    if (idxNum > -1) {
                        arrNum[idxNum] = '<span class="num__hightlight">' + numHightLight + '</span>';
                    }
                    strSlice = arrNum.slice(arrNum.length - 2, arrNum.length).join('');
                    strSlice = txt.substring(0, txt.length - 2) + '<span class="active">' + strSlice + '</span>';
                    _this.html(strSlice);
                } else if (numHightLight.length === 1 && _this.data('row') === 'dacbiet' && (numHightLight === arrItem[i].split('')[1]) && (parseInt($.lt_method_data.methodid) === 10 || parseInt($.lt_method_data.methodid) === 22)) {
                    var arrNum = txt.split(''),
                        idxNum = $.inArray(numHightLight, arrNum),
                        strSlice = '';
                    if (idxNum > -1) {
                        arrNum[idxNum] = '<span class="num__hightlight">' + numHightLight + '</span>';
                    }
                    strSlice = arrNum.slice(arrNum.length - 2, arrNum.length).join('');
                    strSlice = txt.substring(0, txt.length - 2) + '<span class="active">' + strSlice + '</span>';
                    _this.html(strSlice);
                } else if (numHightLight.length === 4 && numHightLight.substr(-2) === arrItem[i]) {
                    var html = numHightLight.substring(0, 2) + '<span class="active">' + arrItem[i] + '</span>';
                    $numHightLight.html(html);
                } else {
                    if (txt.substr(-2) === arrItem[i]) {
                        var html = txt.length > 2 ? (txt.substring(0, txt.length - 2) + '<span class="active">' + arrItem[i] + '</span>') : '<span class="active">' + arrItem[i] + '</span>';
                        _this.html(html);
                    }
                }
            }
        });
    });

    $tblFirstLast.off('mouseleave').on('mouseleave', function (e) {
        e.preventDefault();
        var _this = $(this);
        $('[data-numwin]').each(function (index) {
            var _this = $(this),
                txt = _this.text(),
                $numHightLight = _this.find('.num__hightlight'),
                numHightLight = $numHightLight.text();
            $numHightLight.html(numHightLight);
            if (numHightLight === '') {
                if ($.lt_method_data.methodid === '20036' && _this.data('row') === 'dacbiet') { // restore highlight 2 digit first
                    var getIdxRanger = txt.substr(0, 2);
                    var getRecentDigit = txt.substr(2);
                    txt = '<span class="num__hightlight">' + getIdxRanger + '</span>' + getRecentDigit;
                }
                _this.html(txt);
            } else if (_this.data('row') === 'dacbiet' && numHightLight.length === 1) {
                var arrNum = txt.split(''),
                    idxNum = $.inArray(numHightLight, arrNum);
                if (idxNum > -1) {
                    arrNum[idxNum] = '<span class="num__hightlight">' + numHightLight + '</span>';
                }
                _this.html(arrNum.join(''));
            }
        });
    });
}

function scrollTopPage(valScroll) {
    var $scroll = $('.back__top'),
        $body = $('body, html');
    if (valScroll >= 200) {
        $scroll.addClass('active');
        $scroll.parent().addClass('showScrollTop');
        $scroll.off('click');
        $scroll.on('click', function (e) {
            e.preventDefault();
            $body.stop().animate({ scrollTop: 0 }, '30000');
        });
    } else {
        $scroll.removeClass('active');
        $scroll.parent().removeClass('showScrollTop');
    }
}

function showSuccessAlert(data, timeHide, funCallback) {
    var timeHide = timeHide || 1500,
        funCallback = funCallback || [];
    if (data) {
        parent.layer.msg(data, {
            shade: 0.3,
            closeBtn: true,
            icon: 1, // 1: icon success, 2: icon error, 3: warning question, 4: lock, 5: face sad, 6: face funny, 7: warning
            time: timeHide
        }, function () {
            for (var i = 0, len = funCallback.length; i < len; i++) {
                funCallback[i] && funCallback[i]();
            }
            //funCallback && funCallback();
        });
    }
}

function showErrorAlert(data, timeHide, funCallback) {
    var timeHide = timeHide || 1500;
    if (data) {
        parent.layer.msg(data, {
            shade: 0.3,
            closeBtn: true,
            icon: 2, // 1: icon success, 2: icon error, 3: warning question, 4: lock, 5: face sad, 6: face funny, 7: warning
            time: timeHide
        }, function () {
            if (typeof funCallback === "function") funCallback();
        });
    }
}

function showAlert(data, typeAlert, callBackOk) { // typeAlert is icon
    if (data) {
        parent.layer.alert(data, {
            btn: ['OK'],
            area: ['420px'],
            icon: typeAlert,
            shade: 0.3,
            yes: function () {
                callBackOk();
            }
        });
    }
}

function showConfirm(msg, successCallBack) {
    parent.layer.confirm(msg, {
        btn: ['OK', 'Cancel'],
        area: ['350px'],
        icon: 3,
        yes: function () {
            parent.layer.closeAll();
            successCallBack && successCallBack();
        }
    });
}

function closePopup() {
    var index = parent.layer.getFrameIndex(window.name);
    parent.layer.close(index);
}

function forInputNumberFloorType(elInput, funCallBack) {
    elInput.off().on('keyup', function (e) {
        var _this = $(this),
            val = _this.val(),
            status = 0; // 0: error, 1: success
        if (val !== '' && val !== null && !isNaN(val)) {
            _this.val(Math.floor(val));
            val = Math.floor(val);
            status = 1;
        } else if (isNaN(val)) {
            _this.val('');
        }
        funCallBack && funCallBack(status, val);
    });
}

$.fn.updownNumber = function (options) {
    return this.each(function () {
        var defaults = {
            upItem: '.num__up',
            downItem: '.num__down',
            iPut: 'input[type=text]',
            eventKeyup: function () { },
            maxNumber: 1000000,
            minNumber: 0
        },
            settings,
            el = $(this);

        if (el.length == 0) return el;

        settings = $.extend({}, defaults, options);

        el.find(settings.iPut).val(settings.minNumber);

        el.find(settings.upItem).off().on('click', function (e) {
            e.preventDefault();
            var valIput = checkNumberInteger(el.find(settings.iPut).val(), settings.minNumber);
            valIput += 1;

            el.find(settings.iPut).val(valIput);

            el.find(settings.iPut).trigger('keyup');
        });

        el.find(settings.downItem).off().on('click', function (e) {
            e.preventDefault();
            var valIput = checkNumberInteger(el.find(settings.iPut).val(), settings.minNumber);
            if (valIput <= settings.minNumber) {
                valIput = settings.minNumber;
            } else {
                valIput -= 1;
            }

            el.find(settings.iPut).val(valIput);

            el.find(settings.iPut).trigger('keyup');
        });

        forInputNumberFloorType(el.find(settings.iPut), function (status, val) {
            if (status && val > 0) { // valid
                if (val >= settings.maxNumber) {
                    el.find(settings.iPut).val(settings.maxNumber);
                }
            } else { // unvalid
                //el.find(settings.iPut).val(1);
            }
            settings.eventKeyup(el.find(settings.iPut));
        });

        function checkNumberInteger(value, defau) {
            var value = value.replace(/[^0-9]/g, "").substring(0, 7);

            if (value == "") {
                value = defau;
            } else {
                value = parseInt(value, 10);
            }

            return value;
        }
    });
}

$.fn.loading = function (options) {

    return this.each(function () {
        var defaults = {
            inside_left: false,
            left: 10,
            inside_right: false,
            right: 10,
            full: true,
            elLoading: '<div id="loading__emu"><div class="loading__emu--inner"><div class="dot"></div><div class="dot"></div><div class="dot"></div></div></div>',
            remove: false,
            widthHeight: '15px',
            borderWidth: '2px',
            borderColor: '#f49e1d',
            color: '#009688',
            zIndexLoading: 10,
            done: function () { }
        },
            sc = {},
            settings,
            el = $(this),
            $loading;

        if (el.length == 0) return el;

        settings = $.extend({}, defaults, options);

        if (settings.remove) {
            $loading = el.find('#loading__emu');
            if ($loading.length) {
                $loading.remove();
            }
            return;
        }

        el.append($(settings.elLoading));
        $loading = el.find('#loading__emu');

        el.css({
            position: 'relative'
        });

        $loading.find('.dot').css({
            width: settings.widthHeight,
            height: settings.widthHeight,
            'border-width': settings.borderWidth,
            'border-color': settings.borderColor,
            color: settings.color,
            padding: 0
        });

        if (settings.inside_left) {
            el.find('.loading').css({
                left: settings.left + 'px',
            });
        } else if (settings.inside_right) {
            el.find('.loading').css({
                right: settings.right + 'px',
            });
        } else { //default settings.full === true
            if (el.is('body')) {
                $loading.css({
                    position: 'fixed'
                });
            }
            $loading.css({
                top: 0,
                bottom: 0,
                right: 0,
                left: 0,
                position: 'absolute',
                'background-color': 'rgba(0, 0, 0, 0.7)',
                'z-index': settings.zIndexLoading
            });

			/*$loading.css({
				position: 'absolute',
				left: '50%',
				top: '50%',
				'margin-top': '-'+parseInt(settings.widthHeight)/2+'px',
				'margin-left': '-'+parseInt(settings.widthHeight)/2+'px'
			});*/
        }
    });
}

// format number 3 digits
function numberWithCommas(x) {
    return x.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

// remove last 3 '0' of a string
function removeLastZero(strNum) {
    if (typeof (strNum) !== 'string') {
        alert('Not a string');
        return;
    }
    return strNum.substring(0, strNum.length - 3);
}

// remove item array duplicate
function removeItemDupliArr(arr) {
    if (typeof (arr) !== 'array' && arr.length === 0) return;
    var i,
        len = arr.length,
        result = [],
        obj = {},
        flag = false;

    // mẹo
    for (i = 0; i < len; i++) {
        if (obj[arr[i]] === undefined) {
            obj[arr[i]] = 0;
        } else {
            flag = true;
        }
    }

    for (i in obj) {
        result.push(i);
    }

    return {
        flag: flag,
        result: result
    };
}

Date.prototype.addHours = function (milliseconds) {
    this.setTime(this.getTime() + milliseconds);
    return this;
}

function toggleIputDropdown() {
    $('.iput__sub input').on('focus', function (e) {
        var _this = $(this),
            $parent = _this.closest('.iput__sub');
        $parent.find('.iput__subitems').show();
    }).on('keyup', function (e) {
        var _this = $(this),
            $parent = _this.closest('.iput__sub');

        $parent.find('.iput__subitems').hide();
    });

    $('.iput__subitems a').on('click', function (e) {
        e.preventDefault();

        var _this = $(this),
            getTxt = Number(_this.text()),
            $rootParent = _this.closest('.iput__sub'),
            $parent = _this.parent('.iput__subitems');

        $rootParent.find('input').val(getTxt).change();
        $parent.hide();
    });

}

hideElOutSite('.iput__sub', function () {
    $('.iput__sub .iput__subitems').hide();
});

//document click
function hideElOutSite(el, callBackItem) {
    $(document).on('click', function (e) {
        var container = $(el);
        if (!container.is(e.target) && container.has(e.target).length === 0) {
            callBackItem();
        }
    });
}

function separateNum(methodID) {
    methodID = Number(methodID) || 1;

    $('[data-numwin]').each(function (index) {
        var _this = $(this),
            getText = _this.text().trim(),
            html = '';

        _this.html(getText);

        if (methodID === 1 || methodID === 14 || methodID === 3 || methodID === 4 || methodID === 5 || methodID === 16 || methodID === 17 || methodID === 18
            || methodID === 27
            || methodID === 28
            || methodID === 29
            || methodID === 30
            || methodID === 31
            || methodID === 32) { // 2 digits
            appendDigits(2, getText, _this);
        } else if (methodID === 2 || methodID === 15) { // 3 digits
            if (getText.length > 2) {
                appendDigits(3, getText, _this);
            }
        } else if (methodID === 7) { // de dau
            if (_this.data('row') === 'giaitam') {
                appendDigits(2, getText, _this);
            }
        } else if (methodID === 6 || methodID === 1000) { // de dac biet
            if (_this.data('row') === 'dacbiet') {
                appendDigits(2, getText, _this);
            }
        } else if (methodID === 20036) { // de dac biet đầu
            if (_this.data('row') === 'dacbiet') {
                appendDigits(2, getText, _this, undefined, [0, 2]);
            }
        } else if (methodID === 20034) { // de giai nhat
            if (_this.data('row') === 'giainhat') {
                appendDigits(2, getText, _this);
            }
        } else if (methodID === 20035) { // de giai 7
            if (_this.data('row') === 'giaibay') {
                appendDigits(2, getText, _this);
            }
        } else if (methodID === 8) { // de dau duoi
            if (_this.data('row') === 'dacbiet' || _this.data('row') === 'giaitam') {
                appendDigits(2, getText, _this);
            }
        } else if (methodID === 9 || methodID === 20) { // dau
            if (_this.data('row') === 'dacbiet') {
                appendDigits(0, getText, _this, 2);
            }
        } else if (methodID === 10 || methodID === 22) { // duoi
            if (_this.data('row') === 'dacbiet') {
                appendDigits(0, getText, _this, 1);
            }
        } else if (methodID === 11) { // 3 cang dau
            if (_this.data('row') === 'giaibay') {
                appendDigits(3, getText, _this);
            }
        } else if (methodID === 12 || methodID === 21) { // 3 cang dac biet
            if (_this.data('row') === 'dacbiet') {
                appendDigits(3, getText, _this);
            }
        } else if (methodID === 13) { // 3 cang dau duoi
            if (_this.data('row') === 'dacbiet' || _this.data('row') === 'giaibay') {
                appendDigits(3, getText, _this);
            }
        } else if (methodID === 25 || methodID === 26) {
            if (getText.length > 3) {
                appendDigits(4, getText, _this);
            }
        } else if (methodID === 23 || methodID === 24) {
            if (_this.data('row') === 'dacbiet') {
                appendDigits(4, getText, _this);
            }
        }
    });

    /**
     * @param digits = how many digits get
     * @param str = string number
     * @param item = element container number
     * @param index = position of digits
     */
    function appendDigits(digits, str, item, index, idxRanger) {
        if (typeof (index) !== 'undefined') {
            var arr = str.split(''),
                getIdxVal = arr[arr.length - index],
                html = '';
            html = '<span class="num__hightlight">' + getIdxVal + '</span>';
            arr[arr.length - index] = html;
            item.html(arr.join(''));
            return;
        } else if (typeof (idxRanger) !== 'undefined') {
            var getIdxRanger = str.substr(idxRanger[0], idxRanger[1]);
            var getRecentDigit = str.substr(idxRanger[1]);
            var html = '<span class="num__hightlight">' + getIdxRanger + '</span>' + getRecentDigit;
            item.html(html);
            return;
        }
        var getDigit = str.substr(-digits),
            getRecentDigit = str.substring(0, str.length - digits),
            html = '';
        html = getRecentDigit + '<span class="num__hightlight">' + getDigit + '</span>';
        item.html(html);
    }
}

function timerCountDown(start, end, item, finished, funCountDown) {
    var timeLeave = 0, timerno;

    if (start == "" || end == "") {
        timeLeave = 0;
    } else {
        timeLeave = (format(end).getTime() - format(start).getTime()) / 1000;
    }

    timerno = setInterval(function () {
        if (timeLeave <= 0) {
            clearInterval(timerno);
            finished && finished();
        }
        var oDate = diff(timeLeave--),
            getDate = oDate.day > 0 ? (oDate.day + ' ngày ') : '',
            getHour = (fftime(oDate.hour) + "").substr(0, 1) + (fftime(oDate.hour) + "").substr(1),
            getMinute = (fftime(oDate.minute) + "").substr(0, 1) + (fftime(oDate.minute) + "").substr(1),
            getSecond = (fftime(oDate.second) + "").substr(0, 1) + (fftime(oDate.second) + "").substr(1),
            totalHour = oDate.day > 0 ? (Number(oDate.day) * 24 + Number(getHour)) : getHour;
        funCountDown && funCountDown({ h: totalHour, m: getMinute, s: getSecond });
        item.html(totalHour + ":" + getMinute + ":" + getSecond);
    }, 1000);

    function fftime(n) {
        return Number(n) < 10 ? "" + 0 + Number(n) : Number(n);
    }
    function format(dateStr) {
        return new Date(dateStr.replace(/-/g, "/"));
    }
    function diff(t) {
        return t > 0 ? {
            day: Math.floor(t / 86400),
            hour: Math.floor(t % 86400 / 3600),
            minute: Math.floor(t % 3600 / 60),
            second: Math.floor(t % 60)
        } : {
                day: 0,
                hour: 0,
                minute: 0,
                second: 0
            }
    }
}

function readFileContent(iput, itemSet, funCallBack) {
    var file = document.getElementById(iput).files[0];
    if (file) {
        var getExtensions = (/[.]/.exec(file.name)) ? /[^.]+$/.exec(file.name) : undefined;
        if ((getExtensions[0] === 'txt' || getExtensions[0] === 'csv') && typeof (getExtensions) !== 'undefined') {
            var reader = new FileReader();
            reader.readAsText(file, "UTF-8");
            layer.load(2, {
                shade: 0.3
            });
            reader.onload = function (evt) {
                document.getElementById(itemSet).value = evt.target.result;
                layer.closeAll('loading');
                funCallBack();
            };
            reader.onerror = function (evt) {
                alert('Error !!!');
            }
        } else {
            showErrorAlert("Định dạng file không hợp lệ.");
        }
    }
    document.getElementById(iput).value = '';
}

function FireEventJavascript(ElementId, EventName) {
    if (document.getElementById(ElementId) != null) {
        if (document.getElementById(ElementId).fireEvent) {
            document.getElementById(ElementId).fireEvent('on' + EventName);
        } else {
            var evObj = document.createEvent('Events');
            evObj.initEvent(EventName, true, false);
            document.getElementById(ElementId).dispatchEvent(evObj);
        }
    }
}

function toggleCash(el) {
    var _this = $(el),
        $parent = _this.parent();
    $parent.toggleClass('showHideMoney');
    if ($parent.hasClass('showHideMoney')) {
        _this.attr('title', 'Hiện số tiền');
        setCookie('toggle_money', 1, 30);
    } else {
        _this.attr('title', 'Ẩn số tiền');
        setCookie('toggle_money', 0, 30);
    }
}

function loadbalance() {
    jQuery(function ($) {
        $('.cash__real .line').addClass('reloadMoney');
        $.ajax({
            type: 'POST',
            url: '/LotteryService.aspx',
            data: 'flag=balance',
            success: function (data) {
                var getData = JSON.parse(data);
                var elCheckUser = document.getElementById('checkuser');
                if (getData.msg !== '' && elCheckUser.value !== '-1') { // -1: new user
                    window.location.href = '/';
                    return;
                }
                setTimeout(function () {
                    $('.cash__real .line').removeClass('reloadMoney');
                }, 1000);
                var balance = Number(getData["balance"]);
                balance = Math.round(balance);
                curBalance = balance;
                //getBalanceTransfer(balance);
                $("#ebalance").html(numberWithCommas(balance));
            },
            error: function (xhr, status, error) {
                console.log(error);
            }
        });
    });
}

function setCookie(name, value, days) {
    var expires = "";
    if (days) {
        var date = new Date();
        //console.log(days*24*60*60*1000);
        date.setTime(date.getTime() + Math.round(days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "") + expires + "; path=/";
}
function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

function redirectWap() {
    if (location.href.search('reg.html') > -1) {
        var newLink = location.href.replace(/reg.html/g, 'reg.shtml');
        location.href = newLink;
        return;
    }
    if (location.href.search('reg.shtml') > -1) return;
    var isMobile = {
        Android: function () {
            return navigator.userAgent.match(/Android/i);
        },
        BlackBerry: function () {
            return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS: function () {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        Opera: function () {
            return navigator.userAgent.match(/Opera Mini/i);
        },
        Windows: function () {
            return navigator.userAgent.match(/IEMobile/i);
        },
        any: function () {
            return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
        }
    };
    if (isMobile.any()) {
        location.href = 'https://m.loto188.com/';
    }
}

/**
 * @param arrDualNum = [{number: string, multiple: number, issue: string}]
 * @returns {Array}
 */
function groupMultipleDualNum(arrDualNum) {
    var flag = true;
    var i = 0;
    var j = 1;
    var countDualNum = 0;
    var arrTempDualNum = [];

    if (arrDualNum.length <= 0) {
        return arrTempDualNum;
    }

    while (flag) {
        if (arrDualNum.length > 1 && arrDualNum[i]['number'] === arrDualNum[j]['number'] && arrDualNum[i]['issue'] === arrDualNum[j]['issue'] && arrDualNum[i]['methodID'] === arrDualNum[j]['methodID']) {
            countDualNum += arrDualNum[j]['multiple'];
            arrDualNum.splice(j, 1);
            j -= 1;
        }
        j += 1;
        if (j >= arrDualNum.length) {
            countDualNum += arrDualNum[i]['multiple'];
            arrTempDualNum.push({ number: arrDualNum[i]['number'], multiple: countDualNum, issue: arrDualNum[i]['issue'], methodID: arrDualNum[i]['methodID'] });
            arrDualNum.splice(i, 1);
            j = 1;
            countDualNum = 0;
            if (arrDualNum.length === 0) {
                flag = false;
            }
        }
        if (arrDualNum.length === 1) {
            arrTempDualNum.push(arrDualNum[i]);
            flag = false;
        }
    }

    return arrTempDualNum;
}

/**
 * @param lotteryID = 200 || "200"
 * @param methodID = 6 || "6"
 * @returns {boolean}
 */
function flagCheckDualNumLottery(lotteryID, methodID) {
    var flag = false;
    if ((Number(lotteryID) === 200 || Number(lotteryID) === 201) && (Number(methodID) === 6 || Number(methodID) === 21 || Number(methodID) === 1000 || Number(methodID) === 23)) {
        flag = true;
    }
    return flag;
}

/**
 * @param codes = "5&3|5" || "55, 35"
 * @returns {any[55]}
 */
function createGroupDualNum(codes) {
    var strTempOutCome = [];
    if (codes.search(/[|]/g) >= 0) {
        var checkSymbol = codes.split('|');
        var arrTemp = [];
        checkSymbol.forEach(function (itemCheck) {
            arrTemp.push(itemCheck.split('&'));
        });
        strTempOutCome = getDualNumber(allPossibleCases(arrTemp));
    } else {
        strTempOutCome = getDualNumber(codes.split(','));
    }
    return strTempOutCome;
}

/**
 * check so co ton tai kep bang
 * @param arrNumber = ["00", "12", "11", "122", "455", "10", "14",...]
 * @returns {any["00", "11", "122", "455"]}
 */
function getDualNumber(arrNumber) {
    var amountCheck = [];
    arrNumber.forEach(function (number) {
        var strNum = number.toString().trim();
        var numberTwoDigits = strNum.substring(strNum.length - 2); // get last digit number
        var splitNum = numberTwoDigits.split('');
        var getFirstNum = splitNum[0];
        var countCheck = 0;
        for (var i = 1; i < splitNum.length; i++) {
            if (getFirstNum === splitNum[i]) {
                countCheck += 1;
            }
        }

        if (countCheck + 1 === splitNum.length) {
            amountCheck.push(number);
        }
    });
    return amountCheck;
}

/**
 * @param arr = [["1", "3", "5"], ["8", "0"],...]
 * @returns {any} = ["18", "10", "38", "30", "58", "50"]
 */
function allPossibleCases(arr) {
    if (arr.length === 1) {
        return arr[0];
    } else {
        var result = [];
        var allCasesOfRest = allPossibleCases(arr.slice(1));  // recur with the rest of array
        for (var i = 0; i < allCasesOfRest.length; i++) {
            for (var j = 0; j < arr[0].length; j++) {
                result.push(arr[0][j] + allCasesOfRest[i]);
            }
        }
        return result;
    }
}