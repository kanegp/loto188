
var faceXTMNMT = [
    {
        isrx: "0",
        isdefault: "1",
        title: "Bao Lô",
        status: 0,
        showInTab: ['digital', 'f-digital'],
        label: [
            {
                gtitle: "Lô 2 Số",
                selectarea: {
                    type: "digital",
                    layout: [
                        {
                            title: "Chục",
                            no: "0|1|2|3|4|5|6|7|8|9",
                            place: 0,
                            btn: [
                                {
                                    title: "Tất cả",
                                    attr_name: 'all'
                                },
                                {
                                    title: "Tài",
                                    attr_name: 'big'
                                },
                                {
                                    title: "Xỉu",
                                    attr_name: 'small'
                                },
                                {
                                    title: "Lẻ",
                                    attr_name: 'odd'
                                },
                                {
                                    title: "Chẵn",
                                    attr_name: 'even'
                                },
                                {
                                    title: "Xóa",
                                    attr_name: 'clean'
                                }
                            ]
                        },
                        {
                            title: "Đơn vị",
                            no: "0|1|2|3|4|5|6|7|8|9",
                            place: 1,
                            btn: [
                                {
                                    title: "Tất cả",
                                    attr_name: 'all'
                                },
                                {
                                    title: "Tài",
                                    attr_name: 'big'
                                },
                                {
                                    title: "Xỉu",
                                    attr_name: 'small'
                                },
                                {
                                    title: "Lẻ",
                                    attr_name: 'odd'
                                },
                                {
                                    title: "Chẵn",
                                    attr_name: 'even'
                                },
                                {
                                    title: "Xóa",
                                    attr_name: 'clean'
                                }
                            ]
                        }
                    ],
                    layoutfastslect: [
                        {
                            start: '00',
                            end: '99'
                        }
                    ]
                },
                methodid: 1,
                methodexample: "Đánh 2 chữ số cuối trong các giải.",
                methoddesc: '',
                methodhelp: '',
                dyprize: [],
                //amountNumber: 80000 // số tiền thắng trên 1 con số trúng của lô 2 số
                unitMoney: 1,
                show_str: '-,-,X,X',
                payprizenum: 0,
                prize: {
                    1: "80.0"
                }
            },
            {
                gtitle: "Lô 3 Số",
                selectarea: {
                    type: "digital",
                    layout: [
                        {
                            title: "Trăm",
                            no: "0|1|2|3|4|5|6|7|8|9",
                            place: 0,
                            btn: [
                                {
                                    title: "Tất cả",
                                    attr_name: 'all'
                                },
                                {
                                    title: "Tài",
                                    attr_name: 'big'
                                },
                                {
                                    title: "Xỉu",
                                    attr_name: 'small'
                                },
                                {
                                    title: "Lẻ",
                                    attr_name: 'odd'
                                },
                                {
                                    title: "Chẵn",
                                    attr_name: 'even'
                                },
                                {
                                    title: "Xóa",
                                    attr_name: 'clean'
                                }
                            ]
                        },
                        {
                            title: "Chục",
                            no: "0|1|2|3|4|5|6|7|8|9",
                            place: 1,
                            btn: [
                                {
                                    title: "Tất cả",
                                    attr_name: 'all'
                                },
                                {
                                    title: "Tài",
                                    attr_name: 'big'
                                },
                                {
                                    title: "Xỉu",
                                    attr_name: 'small'
                                },
                                {
                                    title: "Lẻ",
                                    attr_name: 'odd'
                                },
                                {
                                    title: "Chẵn",
                                    attr_name: 'even'
                                },
                                {
                                    title: "Xóa",
                                    attr_name: 'clean'
                                }
                            ]
                        },
                        {
                            title: "Đơn vị",
                            no: "0|1|2|3|4|5|6|7|8|9",
                            place: 2,
                            btn: [
                                {
                                    title: "Tất cả",
                                    attr_name: 'all'
                                },
                                {
                                    title: "Tài",
                                    attr_name: 'big'
                                },
                                {
                                    title: "Xỉu",
                                    attr_name: 'small'
                                },
                                {
                                    title: "Lẻ",
                                    attr_name: 'odd'
                                },
                                {
                                    title: "Chẵn",
                                    attr_name: 'even'
                                },
                                {
                                    title: "Xóa",
                                    attr_name: 'clean'
                                }
                            ]
                        }
                    ],
                    layoutfastslect: [
                        {
                            start: '000',
                            end: '099'
                        },
                        {
                            start: '100',
                            end: '199'
                        },
                        {
                            start: '200',
                            end: '299'
                        },
                        {
                            start: '300',
                            end: '399'
                        },
                        {
                            start: '400',
                            end: '499'
                        },
                        {
                            start: '500',
                            end: '599'
                        },
                        {
                            start: '600',
                            end: '699'
                        },
                        {
                            start: '700',
                            end: '799'
                        },
                        {
                            start: '800',
                            end: '899'
                        },
                        {
                            start: '900',
                            end: '999'
                        }
                    ]
                },
                methodid: 2,
                methodexample: "Đánh 3 chữ số cuối trong các giải có 3 chữ số trở lên.",
                methoddesc: '',
                methodhelp: '',
                dyprize: [],
                unitMoney: 1,
                show_str: '-,X,X,X',
                payprizenum: 0,
                prize: {
                    1: "80.0"
                }
            },
            {
                gtitle: "Lô 4 Số",
                selectarea: {
                    type: "digital",
                    layout: [
                        {
                            title: "Nghìn",
                            no: "0|1|2|3|4|5|6|7|8|9",
                            place: 0,
                            btn: [
                                {
                                    title: "Tất cả",
                                    attr_name: 'all'
                                },
                                {
                                    title: "Tài",
                                    attr_name: 'big'
                                },
                                {
                                    title: "Xỉu",
                                    attr_name: 'small'
                                },
                                {
                                    title: "Lẻ",
                                    attr_name: 'odd'
                                },
                                {
                                    title: "Chẵn",
                                    attr_name: 'even'
                                },
                                {
                                    title: "Xóa",
                                    attr_name: 'clean'
                                }
                            ]
                        },
                        {
                            title: "Trăm",
                            no: "0|1|2|3|4|5|6|7|8|9",
                            place: 1,
                            btn: [
                                {
                                    title: "Tất cả",
                                    attr_name: 'all'
                                },
                                {
                                    title: "Tài",
                                    attr_name: 'big'
                                },
                                {
                                    title: "Xỉu",
                                    attr_name: 'small'
                                },
                                {
                                    title: "Lẻ",
                                    attr_name: 'odd'
                                },
                                {
                                    title: "Chẵn",
                                    attr_name: 'even'
                                },
                                {
                                    title: "Xóa",
                                    attr_name: 'clean'
                                }
                            ]
                        },
                        {
                            title: "Chục",
                            no: "0|1|2|3|4|5|6|7|8|9",
                            place: 2,
                            btn: [
                                {
                                    title: "Tất cả",
                                    attr_name: 'all'
                                },
                                {
                                    title: "Tài",
                                    attr_name: 'big'
                                },
                                {
                                    title: "Xỉu",
                                    attr_name: 'small'
                                },
                                {
                                    title: "Lẻ",
                                    attr_name: 'odd'
                                },
                                {
                                    title: "Chẵn",
                                    attr_name: 'even'
                                },
                                {
                                    title: "Xóa",
                                    attr_name: 'clean'
                                }
                            ]
                        },
                        {
                            title: "Đơn vị",
                            no: "0|1|2|3|4|5|6|7|8|9",
                            place: 3,
                            btn: [
                                {
                                    title: "Tất cả",
                                    attr_name: 'all'
                                },
                                {
                                    title: "Tài",
                                    attr_name: 'big'
                                },
                                {
                                    title: "Xỉu",
                                    attr_name: 'small'
                                },
                                {
                                    title: "Lẻ",
                                    attr_name: 'odd'
                                },
                                {
                                    title: "Chẵn",
                                    attr_name: 'even'
                                },
                                {
                                    title: "Xóa",
                                    attr_name: 'clean'
                                }
                            ]
                        }
                    ],
                    layoutfastslect: []
                },
                methodid: 25,
                methodexample: "Đánh 4 chữ số cuối trong các giải có 4 chữ số trở lên.",
                methoddesc: '',
                methodhelp: '',
                dyprize: [],
                unitMoney: 1,
                show_str: '-,X,X,X',
                payprizenum: 0,
                prize: {
                    1: "80.0"
                }
            }
        ]
    },

    {
        isrx: "0",
        isdefault: "0",
        title: "Lô Xiên",
        status: 0,
        showInTab: ['digital', 'f-digital'],
        label: [
            {
                gtitle: "Xiên 2",
                selectarea: {
                    type: "input",
                    layout: [],
                    layoutfastslect: [
                        {
                            start: '00',
                            end: '99'
                        }
                    ]
                },
                methodid: 3,
                methodexample: "Xiên 2 của 2 chữ số cuối trong các giải.",
                methoddesc: '',
                methodhelp: '',
                dyprize: [],
                //amountNumber: 80000 // số tiền thắng trên 1 con số trúng của lô 2 số
                unitMoney: 1,
                show_str: 'X',
                payprizenum: 0,
                prize: {
                    1: "80.0"
                }
            },
            {
                gtitle: "Xiên 3",
                selectarea: {
                    type: "input",
                    layout: [],
                    layoutfastslect: [
                        {
                            start: '00',
                            end: '99'
                        }
                    ]
                },
                methodid: 4,
                methodexample: "Xiên 3 của 2 chữ số cuối trong các giải.",
                methoddesc: '',
                methodhelp: '',
                dyprize: [],
                //amountNumber: 80000 // số tiền thắng trên 1 con số trúng của lô 2 số
                unitMoney: 1,
                show_str: 'X',
                payprizenum: 0,
                prize: {
                    1: "80.0"
                }
            },
            {
                gtitle: "Xiên 4",
                selectarea: {
                    type: "input",
                    layout: [],
                    layoutfastslect: [
                        {
                            start: '00',
                            end: '99'
                        }
                    ]
                },
                methodid: 5,
                methodexample: "Xiên 4 của 2 chữ số cuối trong các giải.",
                methoddesc: '',
                methodhelp: '',
                dyprize: [],
                //amountNumber: 80000 // số tiền thắng trên 1 con số trúng của lô 2 số
                unitMoney: 1,
                show_str: 'X',
                payprizenum: 0,
                prize: {
                    1: "80.0"
                }
            }
        ]
    },

    {
        isrx: "0",
        isdefault: "0",
        title: "Đánh Đề",
        status: 0,
        showInTab: ['digital', 'f-digital'],
        label: [
            {
                gtitle: "Đề đầu",
                selectarea: {
                    type: "digital",
                    layout: [
                        {
                            title: "Chục",
                            no: "0|1|2|3|4|5|6|7|8|9",
                            place: 0,
                            btn: [
                                {
                                    title: "Tất cả",
                                    attr_name: 'all'
                                },
                                {
                                    title: "Tài",
                                    attr_name: 'big'
                                },
                                {
                                    title: "Xỉu",
                                    attr_name: 'small'
                                },
                                {
                                    title: "Lẻ",
                                    attr_name: 'odd'
                                },
                                {
                                    title: "Chẵn",
                                    attr_name: 'even'
                                },
                                {
                                    title: "Xóa",
                                    attr_name: 'clean'
                                }
                            ]
                        },
                        {
                            title: "Đơn vị",
                            no: "0|1|2|3|4|5|6|7|8|9",
                            place: 1,
                            btn: [
                                {
                                    title: "Tất cả",
                                    attr_name: 'all'
                                },
                                {
                                    title: "Tài",
                                    attr_name: 'big'
                                },
                                {
                                    title: "Xỉu",
                                    attr_name: 'small'
                                },
                                {
                                    title: "Lẻ",
                                    attr_name: 'odd'
                                },
                                {
                                    title: "Chẵn",
                                    attr_name: 'even'
                                },
                                {
                                    title: "Xóa",
                                    attr_name: 'clean'
                                }
                            ]
                        }
                    ],
                    layoutfastslect: [
                        {
                            start: '00',
                            end: '99'
                        }
                    ]
                },
                methodid: 7,
                methodexample: "Đánh giải 8.",
                methoddesc: '',
                methodhelp: '',
                dyprize: [],
                //amountNumber: 80000 // số tiền thắng trên 1 con số trúng của lô 2 số
                unitMoney: 1,
                show_str: '-,-,X,X',
                payprizenum: 0,
                prize: {
                    1: "80.0"
                }
            },
            {
                gtitle: "Đề đặc biệt",
                selectarea: {
                    type: "digital",
                    layout: [
                        {
                            title: "Chục",
                            no: "0|1|2|3|4|5|6|7|8|9",
                            place: 0,
                            btn: [
                                {
                                    title: "Tất cả",
                                    attr_name: 'all'
                                },
                                {
                                    title: "Tài",
                                    attr_name: 'big'
                                },
                                {
                                    title: "Xỉu",
                                    attr_name: 'small'
                                },
                                {
                                    title: "Lẻ",
                                    attr_name: 'odd'
                                },
                                {
                                    title: "Chẵn",
                                    attr_name: 'even'
                                },
                                {
                                    title: "Xóa",
                                    attr_name: 'clean'
                                }
                            ]
                        },
                        {
                            title: "Đơn vị",
                            no: "0|1|2|3|4|5|6|7|8|9",
                            place: 1,
                            btn: [
                                {
                                    title: "Tất cả",
                                    attr_name: 'all'
                                },
                                {
                                    title: "Tài",
                                    attr_name: 'big'
                                },
                                {
                                    title: "Xỉu",
                                    attr_name: 'small'
                                },
                                {
                                    title: "Lẻ",
                                    attr_name: 'odd'
                                },
                                {
                                    title: "Chẵn",
                                    attr_name: 'even'
                                },
                                {
                                    title: "Xóa",
                                    attr_name: 'clean'
                                }
                            ]
                        }
                    ],
                    layoutfastslect: [
                        {
                            start: '00',
                            end: '99'
                        }
                    ]
                },
                methodid: 6,
                methodexample: "Đánh 2 chữ số cuối trong giải đặc biệt.",
                methoddesc: '',
                methodhelp: '',
                dyprize: [],
                //amountNumber: 80000 // số tiền thắng trên 1 con số trúng của lô 2 số
                unitMoney: 1,
                show_str: '-,-,X,X',
                payprizenum: 0,
                prize: {
                    1: "80.0"
                }
            },
            {
                gtitle: "Đề đầu đuôi",
                selectarea: {
                    type: "digital",
                    layout: [
                        {
                            title: "Chục",
                            no: "0|1|2|3|4|5|6|7|8|9",
                            place: 0,
                            btn: [
                                {
                                    title: "Tất cả",
                                    attr_name: 'all'
                                },
                                {
                                    title: "Tài",
                                    attr_name: 'big'
                                },
                                {
                                    title: "Xỉu",
                                    attr_name: 'small'
                                },
                                {
                                    title: "Lẻ",
                                    attr_name: 'odd'
                                },
                                {
                                    title: "Chẵn",
                                    attr_name: 'even'
                                },
                                {
                                    title: "Xóa",
                                    attr_name: 'clean'
                                }
                            ]
                        },
                        {
                            title: "Đơn vị",
                            no: "0|1|2|3|4|5|6|7|8|9",
                            place: 1,
                            btn: [
                                {
                                    title: "Tất cả",
                                    attr_name: 'all'
                                },
                                {
                                    title: "Tài",
                                    attr_name: 'big'
                                },
                                {
                                    title: "Xỉu",
                                    attr_name: 'small'
                                },
                                {
                                    title: "Lẻ",
                                    attr_name: 'odd'
                                },
                                {
                                    title: "Chẵn",
                                    attr_name: 'even'
                                },
                                {
                                    title: "Xóa",
                                    attr_name: 'clean'
                                }
                            ]
                        }
                    ],
                    layoutfastslect: [
                        {
                            start: '00',
                            end: '99'
                        }
                    ]
                },
                methodid: 8,
                methodexample: "Đánh 2 chữ số giải đặc biệt và giải 8.",
                methoddesc: '',
                methodhelp: '',
                dyprize: [],
                //amountNumber: 80000 // số tiền thắng trên 1 con số trúng của lô 2 số
                unitMoney: 1,
                show_str: '-,-,X,X',
                payprizenum: 0,
                prize: {
                    1: "80.0"
                }
            },
        ]
    },

    {
        isrx: "0",
        isdefault: "0",
        title: "Đầu Đuôi",
        status: 0,
        showInTab: ['digital'],
        label: [
            {
                gtitle: "Đầu",
                selectarea: {
                    type: "digital",
                    layout: [
                        {
                            title: "Chục",
                            no: "0|1|2|3|4|5|6|7|8|9",
                            place: 0,
                            btn: [
                                {
                                    title: "Tất cả",
                                    attr_name: 'all'
                                },
                                {
                                    title: "Tài",
                                    attr_name: 'big'
                                },
                                {
                                    title: "Xỉu",
                                    attr_name: 'small'
                                },
                                {
                                    title: "Lẻ",
                                    attr_name: 'odd'
                                },
                                {
                                    title: "Chẵn",
                                    attr_name: 'even'
                                },
                                {
                                    title: "Xóa",
                                    attr_name: 'clean'
                                }
                            ]
                        }
                    ]
                },
                methodid: 9,
                methodexample: "Đánh 1 chữ số hàng chục của giải đặc biệt.",
                methoddesc: '',
                methodhelp: '',
                dyprize: [],
                //amountNumber: 80000 // số tiền thắng trên 1 con số trúng của lô 2 số
                unitMoney: 1,
                show_str: '-,-,-,X',
                payprizenum: 0,
                prize: {
                    1: "80.0"
                }
            },
            {
                gtitle: "Đuôi",
                selectarea: {
                    type: "digital",
                    layout: [
                        {
                            title: "Đơn vị",
                            no: "0|1|2|3|4|5|6|7|8|9",
                            place: 0,
                            btn: [
                                {
                                    title: "Tất cả",
                                    attr_name: 'all'
                                },
                                {
                                    title: "Tài",
                                    attr_name: 'big'
                                },
                                {
                                    title: "Xỉu",
                                    attr_name: 'small'
                                },
                                {
                                    title: "Lẻ",
                                    attr_name: 'odd'
                                },
                                {
                                    title: "Chẵn",
                                    attr_name: 'even'
                                },
                                {
                                    title: "Xóa",
                                    attr_name: 'clean'
                                }
                            ]
                        }
                    ]
                },
                methodid: 10,
                methodexample: "Đánh 1 chữ số cuối của giải đặc biệt.",
                methoddesc: '',
                methodhelp: '',
                dyprize: [],
                //amountNumber: 80000 // số tiền thắng trên 1 con số trúng của lô 2 số
                unitMoney: 1,
                show_str: '-,-,-,X',
                payprizenum: 0,
                prize: {
                    1: "80.0"
                }
            },
        ]
    },

    {
        isrx: "0",
        isdefault: "0",
        title: "3 Càng",
        status: 0,
        showInTab: ['digital', 'f-digital'],
        label: [
            {
                gtitle: "3 Càng Đầu",
                selectarea: {
                    type: "digital",
                    layout: [
                        {
                            title: "Trăm",
                            no: "0|1|2|3|4|5|6|7|8|9",
                            place: 0,
                            btn: [
                                {
                                    title: "Tất cả",
                                    attr_name: 'all'
                                },
                                {
                                    title: "Tài",
                                    attr_name: 'big'
                                },
                                {
                                    title: "Xỉu",
                                    attr_name: 'small'
                                },
                                {
                                    title: "Lẻ",
                                    attr_name: 'odd'
                                },
                                {
                                    title: "Chẵn",
                                    attr_name: 'even'
                                },
                                {
                                    title: "Xóa",
                                    attr_name: 'clean'
                                }
                            ]
                        },
                        {
                            title: "Chục",
                            no: "0|1|2|3|4|5|6|7|8|9",
                            place: 1,
                            btn: [
                                {
                                    title: "Tất cả",
                                    attr_name: 'all'
                                },
                                {
                                    title: "Tài",
                                    attr_name: 'big'
                                },
                                {
                                    title: "Xỉu",
                                    attr_name: 'small'
                                },
                                {
                                    title: "Lẻ",
                                    attr_name: 'odd'
                                },
                                {
                                    title: "Chẵn",
                                    attr_name: 'even'
                                },
                                {
                                    title: "Xóa",
                                    attr_name: 'clean'
                                }
                            ]
                        },
                        {
                            title: "Đơn vị",
                            no: "0|1|2|3|4|5|6|7|8|9",
                            place: 2,
                            btn: [
                                {
                                    title: "Tất cả",
                                    attr_name: 'all'
                                },
                                {
                                    title: "Tài",
                                    attr_name: 'big'
                                },
                                {
                                    title: "Xỉu",
                                    attr_name: 'small'
                                },
                                {
                                    title: "Lẻ",
                                    attr_name: 'odd'
                                },
                                {
                                    title: "Chẵn",
                                    attr_name: 'even'
                                },
                                {
                                    title: "Xóa",
                                    attr_name: 'clean'
                                }
                            ]
                        }
                    ],
                    layoutfastslect: [
                        {
                            start: '000',
                            end: '099'
                        },
                        {
                            start: '100',
                            end: '199'
                        },
                        {
                            start: '200',
                            end: '299'
                        },
                        {
                            start: '300',
                            end: '399'
                        },
                        {
                            start: '400',
                            end: '499'
                        },
                        {
                            start: '500',
                            end: '599'
                        },
                        {
                            start: '600',
                            end: '699'
                        },
                        {
                            start: '700',
                            end: '799'
                        },
                        {
                            start: '800',
                            end: '899'
                        },
                        {
                            start: '900',
                            end: '999'
                        }
                    ]
                },
                methodid: 11,
                methodexample: "Đánh giải 7 (giải 7 có 3 chữ số).",
                methoddesc: '',
                methodhelp: '',
                dyprize: [],
                unitMoney: 1,
                show_str: '-,X,X,X',
                payprizenum: 0,
                prize: {
                    1: "80.0"
                }
            },
            {
                gtitle: "3 Càng Đặc Biệt",
                selectarea: {
                    type: "digital",
                    layout: [
                        {
                            title: "Trăm",
                            no: "0|1|2|3|4|5|6|7|8|9",
                            place: 0,
                            btn: [
                                {
                                    title: "Tất cả",
                                    attr_name: 'all'
                                },
                                {
                                    title: "Tài",
                                    attr_name: 'big'
                                },
                                {
                                    title: "Xỉu",
                                    attr_name: 'small'
                                },
                                {
                                    title: "Lẻ",
                                    attr_name: 'odd'
                                },
                                {
                                    title: "Chẵn",
                                    attr_name: 'even'
                                },
                                {
                                    title: "Xóa",
                                    attr_name: 'clean'
                                }
                            ]
                        },
                        {
                            title: "Chục",
                            no: "0|1|2|3|4|5|6|7|8|9",
                            place: 1,
                            btn: [
                                {
                                    title: "Tất cả",
                                    attr_name: 'all'
                                },
                                {
                                    title: "Tài",
                                    attr_name: 'big'
                                },
                                {
                                    title: "Xỉu",
                                    attr_name: 'small'
                                },
                                {
                                    title: "Lẻ",
                                    attr_name: 'odd'
                                },
                                {
                                    title: "Chẵn",
                                    attr_name: 'even'
                                },
                                {
                                    title: "Xóa",
                                    attr_name: 'clean'
                                }
                            ]
                        },
                        {
                            title: "Đơn vị",
                            no: "0|1|2|3|4|5|6|7|8|9",
                            place: 2,
                            btn: [
                                {
                                    title: "Tất cả",
                                    attr_name: 'all'
                                },
                                {
                                    title: "Tài",
                                    attr_name: 'big'
                                },
                                {
                                    title: "Xỉu",
                                    attr_name: 'small'
                                },
                                {
                                    title: "Lẻ",
                                    attr_name: 'odd'
                                },
                                {
                                    title: "Chẵn",
                                    attr_name: 'even'
                                },
                                {
                                    title: "Xóa",
                                    attr_name: 'clean'
                                }
                            ]
                        }
                    ],
                    layoutfastslect: [
                        {
                            start: '000',
                            end: '099'
                        },
                        {
                            start: '100',
                            end: '199'
                        },
                        {
                            start: '200',
                            end: '299'
                        },
                        {
                            start: '300',
                            end: '399'
                        },
                        {
                            start: '400',
                            end: '499'
                        },
                        {
                            start: '500',
                            end: '599'
                        },
                        {
                            start: '600',
                            end: '699'
                        },
                        {
                            start: '700',
                            end: '799'
                        },
                        {
                            start: '800',
                            end: '899'
                        },
                        {
                            start: '900',
                            end: '999'
                        }
                    ]
                },
                methodid: 12,
                methodexample: "Đánh 3 chữ số cuối của giải đặc biệt.",
                methoddesc: '',
                methodhelp: '',
                dyprize: [],
                unitMoney: 1,
                show_str: '-,X,X,X',
                payprizenum: 0,
                prize: {
                    1: "80.0"
                }
            },
            {
                gtitle: "3 Càng Đầu Đuôi",
                selectarea: {
                    type: "digital",
                    layout: [
                        {
                            title: "Trăm",
                            no: "0|1|2|3|4|5|6|7|8|9",
                            place: 0,
                            btn: [
                                {
                                    title: "Tất cả",
                                    attr_name: 'all'
                                },
                                {
                                    title: "Tài",
                                    attr_name: 'big'
                                },
                                {
                                    title: "Xỉu",
                                    attr_name: 'small'
                                },
                                {
                                    title: "Lẻ",
                                    attr_name: 'odd'
                                },
                                {
                                    title: "Chẵn",
                                    attr_name: 'even'
                                },
                                {
                                    title: "Xóa",
                                    attr_name: 'clean'
                                }
                            ]
                        },
                        {
                            title: "Chục",
                            no: "0|1|2|3|4|5|6|7|8|9",
                            place: 1,
                            btn: [
                                {
                                    title: "Tất cả",
                                    attr_name: 'all'
                                },
                                {
                                    title: "Tài",
                                    attr_name: 'big'
                                },
                                {
                                    title: "Xỉu",
                                    attr_name: 'small'
                                },
                                {
                                    title: "Lẻ",
                                    attr_name: 'odd'
                                },
                                {
                                    title: "Chẵn",
                                    attr_name: 'even'
                                },
                                {
                                    title: "Xóa",
                                    attr_name: 'clean'
                                }
                            ]
                        },
                        {
                            title: "Đơn vị",
                            no: "0|1|2|3|4|5|6|7|8|9",
                            place: 2,
                            btn: [
                                {
                                    title: "Tất cả",
                                    attr_name: 'all'
                                },
                                {
                                    title: "Tài",
                                    attr_name: 'big'
                                },
                                {
                                    title: "Xỉu",
                                    attr_name: 'small'
                                },
                                {
                                    title: "Lẻ",
                                    attr_name: 'odd'
                                },
                                {
                                    title: "Chẵn",
                                    attr_name: 'even'
                                },
                                {
                                    title: "Xóa",
                                    attr_name: 'clean'
                                }
                            ]
                        }
                    ],
                    layoutfastslect: [
                        {
                            start: '000',
                            end: '099'
                        },
                        {
                            start: '100',
                            end: '199'
                        },
                        {
                            start: '200',
                            end: '299'
                        },
                        {
                            start: '300',
                            end: '399'
                        },
                        {
                            start: '400',
                            end: '499'
                        },
                        {
                            start: '500',
                            end: '599'
                        },
                        {
                            start: '600',
                            end: '699'
                        },
                        {
                            start: '700',
                            end: '799'
                        },
                        {
                            start: '800',
                            end: '899'
                        },
                        {
                            start: '900',
                            end: '999'
                        }
                    ]
                },
                methodid: 13,
                methodexample: "Đánh 3 chữ số cuối của giải đặc biệt và giải 7.",
                methoddesc: '',
                methodhelp: '',
                dyprize: [],
                unitMoney: 1,
                show_str: '-,X,X,X',
                payprizenum: 0,
                prize: {
                    1: "80.0"
                }
            }
        ]
    },

    {
        isrx: "0",
        isdefault: "0",
        title: "4 Càng",
        status: 0,
        showInTab: ['digital', 'f-digital'],
        label: [
            {
                gtitle: "4 Càng Đặc Biệt",
                selectarea: {
                    type: "digital",
                    layout: [
                        {
                            title: "Nghìn",
                            no: "0|1|2|3|4|5|6|7|8|9",
                            place: 0,
                            btn: [
                                {
                                    title: "Tất cả",
                                    attr_name: 'all'
                                },
                                {
                                    title: "Tài",
                                    attr_name: 'big'
                                },
                                {
                                    title: "Xỉu",
                                    attr_name: 'small'
                                },
                                {
                                    title: "Lẻ",
                                    attr_name: 'odd'
                                },
                                {
                                    title: "Chẵn",
                                    attr_name: 'even'
                                },
                                {
                                    title: "Xóa",
                                    attr_name: 'clean'
                                }
                            ]
                        },
                        {
                            title: "Trăm",
                            no: "0|1|2|3|4|5|6|7|8|9",
                            place: 1,
                            btn: [
                                {
                                    title: "Tất cả",
                                    attr_name: 'all'
                                },
                                {
                                    title: "Tài",
                                    attr_name: 'big'
                                },
                                {
                                    title: "Xỉu",
                                    attr_name: 'small'
                                },
                                {
                                    title: "Lẻ",
                                    attr_name: 'odd'
                                },
                                {
                                    title: "Chẵn",
                                    attr_name: 'even'
                                },
                                {
                                    title: "Xóa",
                                    attr_name: 'clean'
                                }
                            ]
                        },
                        {
                            title: "Chục",
                            no: "0|1|2|3|4|5|6|7|8|9",
                            place: 2,
                            btn: [
                                {
                                    title: "Tất cả",
                                    attr_name: 'all'
                                },
                                {
                                    title: "Tài",
                                    attr_name: 'big'
                                },
                                {
                                    title: "Xỉu",
                                    attr_name: 'small'
                                },
                                {
                                    title: "Lẻ",
                                    attr_name: 'odd'
                                },
                                {
                                    title: "Chẵn",
                                    attr_name: 'even'
                                },
                                {
                                    title: "Xóa",
                                    attr_name: 'clean'
                                }
                            ]
                        },
                        {
                            title: "Đơn vị",
                            no: "0|1|2|3|4|5|6|7|8|9",
                            place: 3,
                            btn: [
                                {
                                    title: "Tất cả",
                                    attr_name: 'all'
                                },
                                {
                                    title: "Tài",
                                    attr_name: 'big'
                                },
                                {
                                    title: "Xỉu",
                                    attr_name: 'small'
                                },
                                {
                                    title: "Lẻ",
                                    attr_name: 'odd'
                                },
                                {
                                    title: "Chẵn",
                                    attr_name: 'even'
                                },
                                {
                                    title: "Xóa",
                                    attr_name: 'clean'
                                }
                            ]
                        }
                    ],
                    layoutfastslect: []
                },
                methodid: 24,
                methodexample: "So với 4 số cuổi của giải đặc biệt.",
                methoddesc: '',
                methodhelp: '',
                dyprize: [],
                unitMoney: 1,
                show_str: '-,X,X,X',
                payprizenum: 0,
                prize: {
                    1: "80.0"
                }
            }
        ]
    },

    {
        isrx: "0",
        isdefault: "0",
        title: "Lô Trượt",
        status: 0,
        showInTab: ['digital', 'f-digital'],
        label: [
            {
                gtitle: "Trượt Xiên 4",
                selectarea: {
                    type: "input",
                    layout: [],
                    layoutfastslect: [
                        {
                            start: '00',
                            end: '99'
                        }
                    ]
                },
                methodid: 27,
                methodexample: "Trượt Xiên 4 của 2 chữ số cuối trong các giải.",
                methoddesc: '',
                methodhelp: '',
                dyprize: [],
                unitMoney: 1,
                show_str: '-,X,X,X',
                payprizenum: 0,
                prize: {
                    1: "80.0"
                }
            },
            {
                gtitle: "Trượt Xiên 8",
                selectarea: {
                    type: "input",
                    layout: [],
                    layoutfastslect: [
                        {
                            start: '00',
                            end: '99'
                        }
                    ]
                },
                methodid: 29,
                methodexample: "Trượt Xiên 8 của 2 chữ số cuối trong các giải.",
                methoddesc: '',
                methodhelp: '',
                dyprize: [],
                unitMoney: 1,
                show_str: '-,X,X,X',
                payprizenum: 0,
                prize: {
                    1: "80.0"
                }
            },
            {
                gtitle: "Trượt Xiên 10",
                selectarea: {
                    type: "input",
                    layout: [],
                    layoutfastslect: [
                        {
                            start: '00',
                            end: '99'
                        }
                    ]
                },
                methodid: 31,
                methodexample: "Trượt Xiên 10 của 2 chữ số cuối trong các giải.",
                methoddesc: '',
                methodhelp: '',
                dyprize: [],
                unitMoney: 1,
                show_str: '-,X,X,X',
                payprizenum: 0,
                prize: {
                    1: "80.0"
                }
            }
        ]
    }
];

var faceMB = [
    {
        isrx: "0",
        isdefault: "1",
        title: "Bao Lô",
        status: 0,
        showInTab: ['digital', 'f-digital'],
        label: [
            {
                gtitle: "Lô 2 Số",
                selectarea: {
                    type: "digital",
                    layout: [
                        {
                            title: "Chục",
                            no: "0|1|2|3|4|5|6|7|8|9",
                            place: 0,
                            btn: [
                                {
                                    title: "Tất cả",
                                    attr_name: 'all'
                                },
                                {
                                    title: "Tài",
                                    attr_name: 'big'
                                },
                                {
                                    title: "Xỉu",
                                    attr_name: 'small'
                                },
                                {
                                    title: "Lẻ",
                                    attr_name: 'odd'
                                },
                                {
                                    title: "Chẵn",
                                    attr_name: 'even'
                                },
                                {
                                    title: "Xóa",
                                    attr_name: 'clean'
                                }
                            ]
                        },
                        {
                            title: "Đơn vị",
                            no: "0|1|2|3|4|5|6|7|8|9",
                            place: 1,
                            btn: [
                                {
                                    title: "Tất cả",
                                    attr_name: 'all'
                                },
                                {
                                    title: "Tài",
                                    attr_name: 'big'
                                },
                                {
                                    title: "Xỉu",
                                    attr_name: 'small'
                                },
                                {
                                    title: "Lẻ",
                                    attr_name: 'odd'
                                },
                                {
                                    title: "Chẵn",
                                    attr_name: 'even'
                                },
                                {
                                    title: "Xóa",
                                    attr_name: 'clean'
                                }
                            ]
                        }
                    ],
                    layoutfastslect: [
                        {
                            start: '00',
                            end: '99'
                        }
                    ]
                },
                methodid: 14,
                methodexample: "Đánh 2 chữ số cuối trong các giải.",
                methoddesc: '',
                methodhelp: '',
                dyprize: [],
                //amountNumber: 80000 // số tiền thắng trên 1 con số trúng của lô 2 số
                unitMoney: 1,
                show_str: '-,-,X,X',
                payprizenum: 0,
                prize: {
                    1: "80.0"
                }
            },
            {
                gtitle: "Lô 3 Số",
                selectarea: {
                    type: "digital",
                    layout: [
                        {
                            title: "Trăm",
                            no: "0|1|2|3|4|5|6|7|8|9",
                            place: 0,
                            btn: [
                                {
                                    title: "Tất cả",
                                    attr_name: 'all'
                                },
                                {
                                    title: "Tài",
                                    attr_name: 'big'
                                },
                                {
                                    title: "Xỉu",
                                    attr_name: 'small'
                                },
                                {
                                    title: "Lẻ",
                                    attr_name: 'odd'
                                },
                                {
                                    title: "Chẵn",
                                    attr_name: 'even'
                                },
                                {
                                    title: "Xóa",
                                    attr_name: 'clean'
                                }
                            ]
                        },
                        {
                            title: "Chục",
                            no: "0|1|2|3|4|5|6|7|8|9",
                            place: 1,
                            btn: [
                                {
                                    title: "Tất cả",
                                    attr_name: 'all'
                                },
                                {
                                    title: "Tài",
                                    attr_name: 'big'
                                },
                                {
                                    title: "Xỉu",
                                    attr_name: 'small'
                                },
                                {
                                    title: "Lẻ",
                                    attr_name: 'odd'
                                },
                                {
                                    title: "Chẵn",
                                    attr_name: 'even'
                                },
                                {
                                    title: "Xóa",
                                    attr_name: 'clean'
                                }
                            ]
                        },
                        {
                            title: "Đơn vị",
                            no: "0|1|2|3|4|5|6|7|8|9",
                            place: 2,
                            btn: [
                                {
                                    title: "Tất cả",
                                    attr_name: 'all'
                                },
                                {
                                    title: "Tài",
                                    attr_name: 'big'
                                },
                                {
                                    title: "Xỉu",
                                    attr_name: 'small'
                                },
                                {
                                    title: "Lẻ",
                                    attr_name: 'odd'
                                },
                                {
                                    title: "Chẵn",
                                    attr_name: 'even'
                                },
                                {
                                    title: "Xóa",
                                    attr_name: 'clean'
                                }
                            ]
                        }
                    ],
                    layoutfastslect: [
                        {
                            start: '000',
                            end: '099'
                        },
                        {
                            start: '100',
                            end: '199'
                        },
                        {
                            start: '200',
                            end: '299'
                        },
                        {
                            start: '300',
                            end: '399'
                        },
                        {
                            start: '400',
                            end: '499'
                        },
                        {
                            start: '500',
                            end: '599'
                        },
                        {
                            start: '600',
                            end: '699'
                        },
                        {
                            start: '700',
                            end: '799'
                        },
                        {
                            start: '800',
                            end: '899'
                        },
                        {
                            start: '900',
                            end: '999'
                        }
                    ]
                },
                methodid: 15,
                methodexample: "Đánh 3 chữ số cuối trong các giải có 3 chữ số trở lên.",
                methoddesc: '',
                methodhelp: '',
                dyprize: [],
                unitMoney: 1,
                show_str: '-,X,X,X',
                payprizenum: 0,
                prize: {
                    1: "80.0"
                }
            },
            {
                gtitle: "Lô 4 Số",
                selectarea: {
                    type: "digital",
                    layout: [
                        {
                            title: "Nghìn",
                            no: "0|1|2|3|4|5|6|7|8|9",
                            place: 0,
                            btn: [
                                {
                                    title: "Tất cả",
                                    attr_name: 'all'
                                },
                                {
                                    title: "Tài",
                                    attr_name: 'big'
                                },
                                {
                                    title: "Xỉu",
                                    attr_name: 'small'
                                },
                                {
                                    title: "Lẻ",
                                    attr_name: 'odd'
                                },
                                {
                                    title: "Chẵn",
                                    attr_name: 'even'
                                },
                                {
                                    title: "Xóa",
                                    attr_name: 'clean'
                                }
                            ]
                        },
                        {
                            title: "Trăm",
                            no: "0|1|2|3|4|5|6|7|8|9",
                            place: 1,
                            btn: [
                                {
                                    title: "Tất cả",
                                    attr_name: 'all'
                                },
                                {
                                    title: "Tài",
                                    attr_name: 'big'
                                },
                                {
                                    title: "Xỉu",
                                    attr_name: 'small'
                                },
                                {
                                    title: "Lẻ",
                                    attr_name: 'odd'
                                },
                                {
                                    title: "Chẵn",
                                    attr_name: 'even'
                                },
                                {
                                    title: "Xóa",
                                    attr_name: 'clean'
                                }
                            ]
                        },
                        {
                            title: "Chục",
                            no: "0|1|2|3|4|5|6|7|8|9",
                            place: 2,
                            btn: [
                                {
                                    title: "Tất cả",
                                    attr_name: 'all'
                                },
                                {
                                    title: "Tài",
                                    attr_name: 'big'
                                },
                                {
                                    title: "Xỉu",
                                    attr_name: 'small'
                                },
                                {
                                    title: "Lẻ",
                                    attr_name: 'odd'
                                },
                                {
                                    title: "Chẵn",
                                    attr_name: 'even'
                                },
                                {
                                    title: "Xóa",
                                    attr_name: 'clean'
                                }
                            ]
                        },
                        {
                            title: "Đơn vị",
                            no: "0|1|2|3|4|5|6|7|8|9",
                            place: 3,
                            btn: [
                                {
                                    title: "Tất cả",
                                    attr_name: 'all'
                                },
                                {
                                    title: "Tài",
                                    attr_name: 'big'
                                },
                                {
                                    title: "Xỉu",
                                    attr_name: 'small'
                                },
                                {
                                    title: "Lẻ",
                                    attr_name: 'odd'
                                },
                                {
                                    title: "Chẵn",
                                    attr_name: 'even'
                                },
                                {
                                    title: "Xóa",
                                    attr_name: 'clean'
                                }
                            ]
                        }
                    ],
                    layoutfastslect: []
                },
                methodid: 26,
                methodexample: "Đánh 4 chữ số cuối trong các giải có 4 chữ số trở lên.",
                methoddesc: '',
                methodhelp: '',
                dyprize: [],
                unitMoney: 1,
                show_str: '-,X,X,X',
                payprizenum: 0,
                prize: {
                    1: "80.0"
                }
            }
        ]
    },

    {
        isrx: "0",
        isdefault: "0",
        title: "Lô Xiên",
        status: 0,
        showInTab: ['digital', 'f-digital'],
        label: [
            {
                gtitle: "Xiên 2",
                selectarea: {
                    type: "input",
                    layout: [],
                    layoutfastslect: [
                        {
                            start: '00',
                            end: '99'
                        }
                    ]
                },
                methodid: 16,
                methodexample: "Xiên 2 của 2 chữ số cuối trong các giải.",
                methoddesc: '',
                methodhelp: '',
                dyprize: [],
                //amountNumber: 80000 // số tiền thắng trên 1 con số trúng của lô 2 số
                unitMoney: 1,
                show_str: 'X',
                payprizenum: 0,
                prize: {
                    1: "80.0"
                }
            },
            {
                gtitle: "Xiên 3",
                selectarea: {
                    type: "input",
                    layout: [],
                    layoutfastslect: [
                        {
                            start: '00',
                            end: '99'
                        }
                    ]
                },
                methodid: 17,
                methodexample: "Xiên 3 của 2 chữ số cuối trong các giải.",
                methoddesc: '',
                methodhelp: '',
                dyprize: [],
                //amountNumber: 80000 // số tiền thắng trên 1 con số trúng của lô 2 số
                unitMoney: 1,
                show_str: 'X',
                payprizenum: 0,
                prize: {
                    1: "80.0"
                }
            },
            {
                gtitle: "Xiên 4",
                selectarea: {
                    type: "input",
                    layout: [],
                    layoutfastslect: [
                        {
                            start: '00',
                            end: '99'
                        }
                    ]
                },
                methodid: 18,
                methodexample: "Xiên 4 của 2 chữ số cuối trong các giải.",
                methoddesc: '',
                methodhelp: '',
                dyprize: [],
                //amountNumber: 80000 // số tiền thắng trên 1 con số trúng của lô 2 số
                unitMoney: 1,
                show_str: 'X',
                payprizenum: 0,
                prize: {
                    1: "80.0"
                }
            }
        ]
    },

    {
        isrx: "0",
        isdefault: "0",
        title: "Đánh Đề",
        status: 1,
        showInTab: ['digital', 'f-digital'],
        label: [
            {
                gtitle: "Đề đặc biệt",
                selectarea: {
                    type: "digital",
                    layout: [
                        {
                            title: "Chục",
                            no: "0|1|2|3|4|5|6|7|8|9",
                            place: 0,
                            btn: [
                                {
                                    title: "Tất cả",
                                    attr_name: 'all'
                                },
                                {
                                    title: "Tài",
                                    attr_name: 'big'
                                },
                                {
                                    title: "Xỉu",
                                    attr_name: 'small'
                                },
                                {
                                    title: "Lẻ",
                                    attr_name: 'odd'
                                },
                                {
                                    title: "Chẵn",
                                    attr_name: 'even'
                                },
                                {
                                    title: "Xóa",
                                    attr_name: 'clean'
                                }
                            ]
                        },
                        {
                            title: "Đơn vị",
                            no: "0|1|2|3|4|5|6|7|8|9",
                            place: 1,
                            btn: [
                                {
                                    title: "Tất cả",
                                    attr_name: 'all'
                                },
                                {
                                    title: "Tài",
                                    attr_name: 'big'
                                },
                                {
                                    title: "Xỉu",
                                    attr_name: 'small'
                                },
                                {
                                    title: "Lẻ",
                                    attr_name: 'odd'
                                },
                                {
                                    title: "Chẵn",
                                    attr_name: 'even'
                                },
                                {
                                    title: "Xóa",
                                    attr_name: 'clean'
                                }
                            ]
                        }
                    ],
                    layoutfastslect: [
                        {
                            start: '00',
                            end: '99'
                        }
                    ]
                },
                methodid: 6,
                methodexample: "Đánh 2 chữ số cuối trong giải đặc biệt.",
                methoddesc: '',
                methodhelp: '',
                dyprize: [],
                //amountNumber: 80000 // số tiền thắng trên 1 con số trúng của lô 2 số
                unitMoney: 1,
                show_str: '-,-,X,X',
                payprizenum: 0,
                prize: {
                    1: "80.0"
                }
            },
            {
                gtitle: "Đề đầu đặc biệt",
                selectarea: {
                    type: "digital",
                    layout: [
                        {
                            title: "Chục",
                            no: "0|1|2|3|4|5|6|7|8|9",
                            place: 0,
                            btn: [
                                {
                                    title: "Tất cả",
                                    attr_name: 'all'
                                },
                                {
                                    title: "Tài",
                                    attr_name: 'big'
                                },
                                {
                                    title: "Xỉu",
                                    attr_name: 'small'
                                },
                                {
                                    title: "Lẻ",
                                    attr_name: 'odd'
                                },
                                {
                                    title: "Chẵn",
                                    attr_name: 'even'
                                },
                                {
                                    title: "Xóa",
                                    attr_name: 'clean'
                                }
                            ]
                        },
                        {
                            title: "Đơn vị",
                            no: "0|1|2|3|4|5|6|7|8|9",
                            place: 1,
                            btn: [
                                {
                                    title: "Tất cả",
                                    attr_name: 'all'
                                },
                                {
                                    title: "Tài",
                                    attr_name: 'big'
                                },
                                {
                                    title: "Xỉu",
                                    attr_name: 'small'
                                },
                                {
                                    title: "Lẻ",
                                    attr_name: 'odd'
                                },
                                {
                                    title: "Chẵn",
                                    attr_name: 'even'
                                },
                                {
                                    title: "Xóa",
                                    attr_name: 'clean'
                                }
                            ]
                        }
                    ],
                    layoutfastslect: [
                        {
                            start: '00',
                            end: '99'
                        }
                    ]
                },
                methodid: 20036,
                methodexample: "Đánh 2 chữ số đầu trong giải đặc biệt.",
                methoddesc: '',
                methodhelp: '',
                dyprize: [],
                //amountNumber: 80000 // số tiền thắng trên 1 con số trúng của lô 2 số
                unitMoney: 1,
                show_str: '-,-,X,X',
                payprizenum: 0,
                prize: {
                    1: "80.0"
                }
            },
            {
                gtitle: "Đề giải 7",
                selectarea: {
                    type: "digital",
                    layout: [
                        {
                            title: "Chục",
                            no: "0|1|2|3|4|5|6|7|8|9",
                            place: 0,
                            btn: [
                                {
                                    title: "Tất cả",
                                    attr_name: 'all'
                                },
                                {
                                    title: "Tài",
                                    attr_name: 'big'
                                },
                                {
                                    title: "Xỉu",
                                    attr_name: 'small'
                                },
                                {
                                    title: "Lẻ",
                                    attr_name: 'odd'
                                },
                                {
                                    title: "Chẵn",
                                    attr_name: 'even'
                                },
                                {
                                    title: "Xóa",
                                    attr_name: 'clean'
                                }
                            ]
                        },
                        {
                            title: "Đơn vị",
                            no: "0|1|2|3|4|5|6|7|8|9",
                            place: 1,
                            btn: [
                                {
                                    title: "Tất cả",
                                    attr_name: 'all'
                                },
                                {
                                    title: "Tài",
                                    attr_name: 'big'
                                },
                                {
                                    title: "Xỉu",
                                    attr_name: 'small'
                                },
                                {
                                    title: "Lẻ",
                                    attr_name: 'odd'
                                },
                                {
                                    title: "Chẵn",
                                    attr_name: 'even'
                                },
                                {
                                    title: "Xóa",
                                    attr_name: 'clean'
                                }
                            ]
                        }
                    ],
                    layoutfastslect: [
                        {
                            start: '00',
                            end: '99'
                        }
                    ]
                },
                methodid: 20035,
                methodexample: "Đánh 2 chữ số cuối trong 4 giải của Giải 7.",
                methoddesc: '',
                methodhelp: '',
                dyprize: [],
                //amountNumber: 80000 // số tiền thắng trên 1 con số trúng của lô 2 số
                unitMoney: 1,
                show_str: '-,-,X,X',
                payprizenum: 0,
                prize: {
                    1: "80.0"
                }
            },
            {
                gtitle: "Đề giải nhất",
                selectarea: {
                    type: "digital",
                    layout: [
                        {
                            title: "Chục",
                            no: "0|1|2|3|4|5|6|7|8|9",
                            place: 0,
                            btn: [
                                {
                                    title: "Tất cả",
                                    attr_name: 'all'
                                },
                                {
                                    title: "Tài",
                                    attr_name: 'big'
                                },
                                {
                                    title: "Xỉu",
                                    attr_name: 'small'
                                },
                                {
                                    title: "Lẻ",
                                    attr_name: 'odd'
                                },
                                {
                                    title: "Chẵn",
                                    attr_name: 'even'
                                },
                                {
                                    title: "Xóa",
                                    attr_name: 'clean'
                                }
                            ]
                        },
                        {
                            title: "Đơn vị",
                            no: "0|1|2|3|4|5|6|7|8|9",
                            place: 1,
                            btn: [
                                {
                                    title: "Tất cả",
                                    attr_name: 'all'
                                },
                                {
                                    title: "Tài",
                                    attr_name: 'big'
                                },
                                {
                                    title: "Xỉu",
                                    attr_name: 'small'
                                },
                                {
                                    title: "Lẻ",
                                    attr_name: 'odd'
                                },
                                {
                                    title: "Chẵn",
                                    attr_name: 'even'
                                },
                                {
                                    title: "Xóa",
                                    attr_name: 'clean'
                                }
                            ]
                        }
                    ],
                    layoutfastslect: [
                        {
                            start: '00',
                            end: '99'
                        }
                    ]
                },
                methodid: 20034,
                methodexample: "Đánh 2 chữ số cuối trong Giải Nhất.",
                methoddesc: '',
                methodhelp: '',
                dyprize: [],
                //amountNumber: 80000 // số tiền thắng trên 1 con số trúng của lô 2 số
                unitMoney: 1,
                show_str: '-,-,X,X',
                payprizenum: 0,
                prize: {
                    1: "80.0"
                }
            }
        ]
    },

    {
        isrx: "0",
        isdefault: "0",
        title: "Đầu Đuôi",
        status: 0,
        showInTab: ['digital'],
        label: [
            {
                gtitle: "Đầu",
                selectarea: {
                    type: "digital",
                    layout: [
                        {
                            title: "Chục",
                            no: "0|1|2|3|4|5|6|7|8|9",
                            place: 0,
                            btn: [
                                {
                                    title: "Tất cả",
                                    attr_name: 'all'
                                },
                                {
                                    title: "Tài",
                                    attr_name: 'big'
                                },
                                {
                                    title: "Xỉu",
                                    attr_name: 'small'
                                },
                                {
                                    title: "Lẻ",
                                    attr_name: 'odd'
                                },
                                {
                                    title: "Chẵn",
                                    attr_name: 'even'
                                },
                                {
                                    title: "Xóa",
                                    attr_name: 'clean'
                                }
                            ]
                        }
                    ]
                },
                methodid: 20,
                methodexample: "Đánh 1 chữ số hàng chục của giải đặc biệt.",
                methoddesc: '',
                methodhelp: '',
                dyprize: [],
                //amountNumber: 80000 // số tiền thắng trên 1 con số trúng của lô 2 số
                unitMoney: 1,
                show_str: '-,-,-,X',
                payprizenum: 0,
                prize: {
                    1: "80.0"
                }
            },
            {
                gtitle: "Đuôi",
                selectarea: {
                    type: "digital",
                    layout: [
                        {
                            title: "Đơn vị",
                            no: "0|1|2|3|4|5|6|7|8|9",
                            place: 0,
                            btn: [
                                {
                                    title: "Tất cả",
                                    attr_name: 'all'
                                },
                                {
                                    title: "Tài",
                                    attr_name: 'big'
                                },
                                {
                                    title: "Xỉu",
                                    attr_name: 'small'
                                },
                                {
                                    title: "Lẻ",
                                    attr_name: 'odd'
                                },
                                {
                                    title: "Chẵn",
                                    attr_name: 'even'
                                },
                                {
                                    title: "Xóa",
                                    attr_name: 'clean'
                                }
                            ]
                        }
                    ]
                },
                methodid: 22,
                methodexample: "Đánh 1 chữ số cuối của giải đặc biệt.",
                methoddesc: '',
                methodhelp: '',
                dyprize: [],
                //amountNumber: 80000 // số tiền thắng trên 1 con số trúng của lô 2 số
                unitMoney: 1,
                show_str: '-,-,-,X',
                payprizenum: 0,
                prize: {
                    1: "80.0"
                }
            },
        ]
    },

    {
        isrx: "0",
        isdefault: "0",
        title: "3 Càng",
        status: 0,
        showInTab: ['digital', 'f-digital'],
        label: [
            {
                gtitle: "3 Càng Đặc Biệt",
                selectarea: {
                    type: "digital",
                    layout: [
                        {
                            title: "Trăm",
                            no: "0|1|2|3|4|5|6|7|8|9",
                            place: 0,
                            btn: [
                                {
                                    title: "Tất cả",
                                    attr_name: 'all'
                                },
                                {
                                    title: "Tài",
                                    attr_name: 'big'
                                },
                                {
                                    title: "Xỉu",
                                    attr_name: 'small'
                                },
                                {
                                    title: "Lẻ",
                                    attr_name: 'odd'
                                },
                                {
                                    title: "Chẵn",
                                    attr_name: 'even'
                                },
                                {
                                    title: "Xóa",
                                    attr_name: 'clean'
                                }
                            ]
                        },
                        {
                            title: "Chục",
                            no: "0|1|2|3|4|5|6|7|8|9",
                            place: 1,
                            btn: [
                                {
                                    title: "Tất cả",
                                    attr_name: 'all'
                                },
                                {
                                    title: "Tài",
                                    attr_name: 'big'
                                },
                                {
                                    title: "Xỉu",
                                    attr_name: 'small'
                                },
                                {
                                    title: "Lẻ",
                                    attr_name: 'odd'
                                },
                                {
                                    title: "Chẵn",
                                    attr_name: 'even'
                                },
                                {
                                    title: "Xóa",
                                    attr_name: 'clean'
                                }
                            ]
                        },
                        {
                            title: "Đơn vị",
                            no: "0|1|2|3|4|5|6|7|8|9",
                            place: 2,
                            btn: [
                                {
                                    title: "Tất cả",
                                    attr_name: 'all'
                                },
                                {
                                    title: "Tài",
                                    attr_name: 'big'
                                },
                                {
                                    title: "Xỉu",
                                    attr_name: 'small'
                                },
                                {
                                    title: "Lẻ",
                                    attr_name: 'odd'
                                },
                                {
                                    title: "Chẵn",
                                    attr_name: 'even'
                                },
                                {
                                    title: "Xóa",
                                    attr_name: 'clean'
                                }
                            ]
                        }
                    ],
                    layoutfastslect: [
                        {
                            start: '000',
                            end: '099'
                        },
                        {
                            start: '100',
                            end: '199'
                        },
                        {
                            start: '200',
                            end: '299'
                        },
                        {
                            start: '300',
                            end: '399'
                        },
                        {
                            start: '400',
                            end: '499'
                        },
                        {
                            start: '500',
                            end: '599'
                        },
                        {
                            start: '600',
                            end: '699'
                        },
                        {
                            start: '700',
                            end: '799'
                        },
                        {
                            start: '800',
                            end: '899'
                        },
                        {
                            start: '900',
                            end: '999'
                        }
                    ]
                },
                methodid: 21,
                methodexample: "Đánh 3 chữ số cuối của giải đặc biệt.",
                methoddesc: '',
                methodhelp: '',
                dyprize: [],
                unitMoney: 1,
                show_str: '-,X,X,X',
                payprizenum: 0,
                prize: {
                    1: "80.0"
                }
            }
        ]
    },

    {
        isrx: "0",
        isdefault: "0",
        title: "4 Càng",
        status: 0,
        showInTab: ['digital', 'f-digital'],
        label: [
            {
                gtitle: "4 Càng Đặc Biệt",
                selectarea: {
                    type: "digital",
                    layout: [
                        {
                            title: "Nghìn",
                            no: "0|1|2|3|4|5|6|7|8|9",
                            place: 0,
                            btn: [
                                {
                                    title: "Tất cả",
                                    attr_name: 'all'
                                },
                                {
                                    title: "Tài",
                                    attr_name: 'big'
                                },
                                {
                                    title: "Xỉu",
                                    attr_name: 'small'
                                },
                                {
                                    title: "Lẻ",
                                    attr_name: 'odd'
                                },
                                {
                                    title: "Chẵn",
                                    attr_name: 'even'
                                },
                                {
                                    title: "Xóa",
                                    attr_name: 'clean'
                                }
                            ]
                        },
                        {
                            title: "Trăm",
                            no: "0|1|2|3|4|5|6|7|8|9",
                            place: 1,
                            btn: [
                                {
                                    title: "Tất cả",
                                    attr_name: 'all'
                                },
                                {
                                    title: "Tài",
                                    attr_name: 'big'
                                },
                                {
                                    title: "Xỉu",
                                    attr_name: 'small'
                                },
                                {
                                    title: "Lẻ",
                                    attr_name: 'odd'
                                },
                                {
                                    title: "Chẵn",
                                    attr_name: 'even'
                                },
                                {
                                    title: "Xóa",
                                    attr_name: 'clean'
                                }
                            ]
                        },
                        {
                            title: "Chục",
                            no: "0|1|2|3|4|5|6|7|8|9",
                            place: 2,
                            btn: [
                                {
                                    title: "Tất cả",
                                    attr_name: 'all'
                                },
                                {
                                    title: "Tài",
                                    attr_name: 'big'
                                },
                                {
                                    title: "Xỉu",
                                    attr_name: 'small'
                                },
                                {
                                    title: "Lẻ",
                                    attr_name: 'odd'
                                },
                                {
                                    title: "Chẵn",
                                    attr_name: 'even'
                                },
                                {
                                    title: "Xóa",
                                    attr_name: 'clean'
                                }
                            ]
                        },
                        {
                            title: "Đơn vị",
                            no: "0|1|2|3|4|5|6|7|8|9",
                            place: 3,
                            btn: [
                                {
                                    title: "Tất cả",
                                    attr_name: 'all'
                                },
                                {
                                    title: "Tài",
                                    attr_name: 'big'
                                },
                                {
                                    title: "Xỉu",
                                    attr_name: 'small'
                                },
                                {
                                    title: "Lẻ",
                                    attr_name: 'odd'
                                },
                                {
                                    title: "Chẵn",
                                    attr_name: 'even'
                                },
                                {
                                    title: "Xóa",
                                    attr_name: 'clean'
                                }
                            ]
                        }
                    ],
                    layoutfastslect: []
                },
                methodid: 23,
                methodexample: "So với 4 số cuổi của giải đặc biệt.",
                methoddesc: '',
                methodhelp: '',
                dyprize: [],
                unitMoney: 1,
                show_str: '-,X,X,X',
                payprizenum: 0,
                prize: {
                    1: "80.0"
                }
            }
        ]
    },

    {
        isrx: "0",
        isdefault: "0",
        title: "Lô Trượt",
        status: 0,
        showInTab: ['digital', 'f-digital'],
        label: [
            {
                gtitle: "Trượt Xiên 4",
                selectarea: {
                    type: "input",
                    layout: [],
                    layoutfastslect: [
                        {
                            start: '00',
                            end: '99'
                        }
                    ]
                },
                methodid: 28,
                methodexample: "Trượt Xiên 4 của 2 chữ số cuối trong các giải.",
                methoddesc: '',
                methodhelp: '',
                dyprize: [],
                unitMoney: 1,
                show_str: '-,X,X,X',
                payprizenum: 0,
                prize: {
                    1: "80.0"
                }
            },
            {
                gtitle: "Trượt Xiên 8",
                selectarea: {
                    type: "input",
                    layout: [],
                    layoutfastslect: [
                        {
                            start: '00',
                            end: '99'
                        }
                    ]
                },
                methodid: 30,
                methodexample: "Trượt Xiên 8 của 2 chữ số cuối trong các giải.",
                methoddesc: '',
                methodhelp: '',
                dyprize: [],
                unitMoney: 1,
                show_str: '-,X,X,X',
                payprizenum: 0,
                prize: {
                    1: "80.0"
                }
            },
            {
                gtitle: "Trượt Xiên 10",
                selectarea: {
                    type: "input",
                    layout: [],
                    layoutfastslect: [
                        {
                            start: '00',
                            end: '99'
                        }
                    ]
                },
                methodid: 32,
                methodexample: "Trượt Xiên 10 của 2 chữ số cuối trong các giải.",
                methoddesc: '',
                methodhelp: '',
                dyprize: [],
                unitMoney: 1,
                show_str: '-,X,X,X',
                payprizenum: 0,
                prize: {
                    1: "80.0"
                }
            }
        ]
    }
];

var faceMBDB = [
    {
        isrx: "0",
        isdefault: "1",
        title: "Đặc Biệt 18h25",
        status: 0,
        showInTab: ['digital', 'f-digital'],
        label: [
            {
                gtitle: "Đề đặc biệt 18h25",
                selectarea: {
                    type: "digital",
                    layout: [
                        {
                            title: "Chục",
                            no: "0|1|2|3|4|5|6|7|8|9",
                            place: 0,
                            btn: [
                                {
                                    title: "Tất cả",
                                    attr_name: 'all'
                                },
                                {
                                    title: "Tài",
                                    attr_name: 'big'
                                },
                                {
                                    title: "Xỉu",
                                    attr_name: 'small'
                                },
                                {
                                    title: "Lẻ",
                                    attr_name: 'odd'
                                },
                                {
                                    title: "Chẵn",
                                    attr_name: 'even'
                                },
                                {
                                    title: "Xóa",
                                    attr_name: 'clean'
                                }
                            ]
                        },
                        {
                            title: "Đơn vị",
                            no: "0|1|2|3|4|5|6|7|8|9",
                            place: 1,
                            btn: [
                                {
                                    title: "Tất cả",
                                    attr_name: 'all'
                                },
                                {
                                    title: "Tài",
                                    attr_name: 'big'
                                },
                                {
                                    title: "Xỉu",
                                    attr_name: 'small'
                                },
                                {
                                    title: "Lẻ",
                                    attr_name: 'odd'
                                },
                                {
                                    title: "Chẵn",
                                    attr_name: 'even'
                                },
                                {
                                    title: "Xóa",
                                    attr_name: 'clean'
                                }
                            ]
                        }
                    ],
                    layoutfastslect: [
                        {
                            start: '00',
                            end: '99'
                        }
                    ]
                },
                methodid: 1000,
                methodexample: "Đánh 2 chữ số cuối trong giải đặc biệt.",
                methoddesc: '',
                methodhelp: '',
                dyprize: [],
                //amountNumber: 80000 // số tiền thắng trên 1 con số trúng của lô 2 số
                unitMoney: 1,
                show_str: '-,-,X,X',
                payprizenum: 0,
                prize: {
                    1: "80.0"
                }
            }
        ]
    }
];