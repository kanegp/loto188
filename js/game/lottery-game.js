var objTemp = {};
(function($) {
    var hrefLocation = location.href,
        getLink = hrefLocation.split('?')[0],
        getParam = hrefLocation.split('?')[1],
        FaceLottery = '',
        FaceLotteryById = '';

    var data_id = { // list id element render
            id_navgame: "#nav__game",
            id_subnavgame: "#sub__navgame",
            id_betexam: "#bet__exam",
            id_lotteryselector: "#lottery__selector",
            id_ratiolottery: "#ratio__lottery",
            id_multiple: "#num__multiple",
            id_countcombination: "#count__number--combination",
            id_amounttotalbet: "#amount__total",
            id_btnbetnow: "#bet__now",
            id_frmBet: "#frmBet",
            id_totalNum: "#lt_total_nums",
            id_totalMoney: "#lt_total_money",
            id_comfirmBet: "#popup__comfirmBet",
            id_betreview: "#tbl__betreview",
            id_totalMoneyBet: "#total_moneyBet",
            id_totalMoneyBetPopup: "#total_moneyBetPopup",
            id_btnAddTicket: "#btnAddTicket",
            id_btnBetAll: "#btnBetAll",
            id_btnDeleteAll: "#deleteAll",
            id_buyContinuously: "#buy__continuously",
            id_timeTitle: "#time__title",
            id_openIssue: "#open__issue",
            id_timeCountContentBet: "#timeCount_contentbet",
            id_loadingCountdownResult: "#loadingcountdown__result",
            id_timeWait: "#timeLoadingCountDown",
            id_issueResult: "#issueResult",
            id_iputLoxien: "#iput__loxien",
            id_iputFileLoXien: "file__loxien",
            id_iputAreaLoXien: "iput__loxien",
            id_lotterResult: "#lotteryResult",
            id_lotteryIssue: "#lotteryIssue",
            id_selectTabFastNum: "#selectTabFastNum",
            id_clearnLoXien: "#clearn__loxien",
            id_reloadHistory: "#reloadHistory",
            id_inputDigital: "iput__digital"
        },
        data_class = {
            cl_timeCountBet: ".timeCountBet",
            cl_tblTime: ".tbl__time",
            cl_tblResult: ".tbl__result",
            cl_superSpeed: ".nav__result",
            cl_slectNumFast: ".slect__numfast",
            cl_slectFromTo: ".slect__fromto",
            cl_numFastItem: ".numfast__item",
            cl_btnNuoiSo: ".btn__nuoiso"
        },
        lotteryID = 50,
        urlLotteryService = "/LotteryService.aspx",
        urlMobileService = "/MobileService.aspx",
        pri_user_data = "",
        listIDValid = [50, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 200, 201],
        curissue = '',
        servertime = '',
        objIssueStart = [],
        historyissue = '',
        betList = [],
        typeArea = '',
        dataTypeTabSlect = '',
        arrMethodIdHasIn = [],
        menuTop = [
            {
                name: 'Xổ số siêu tốc',
                keyTypeLotter: 'xsst',
                index: 1,
                ids: [50, 100, 101, 102, 103, 104]
            },
            {
                name: 'Xổ số miền nam',
                index: 2,
                keyTypeLotter: 'xsmn',
                ids: [105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124]
            },
            {
                name: 'Xổ số miền trung',
                index: 3,
                keyTypeLotter: 'xsmt',
                ids: [126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139]
            },
            {
                name: 'Xổ số miền bắc',
                index: 4,
                keyTypeLotter: 'xsmb',
                ids: [200, 201]
            },
            {
                name: 'Tài Xỉu',
                index: 5,
                keyTypeLotter: 'taixiu',
                ids: ["/dice"]
            },
            {
                name: 'Thể Thao',
                index: 6,
                keyTypeLotter: 'thethao',
                ids: ["/sport2"]
            },
            {
                name: 'Đá Gà',
                index: 7,
                keyTypeLotter: 'cock-fight',
                ids: ["/cock-fight"]
            },
            {
                name: 'Khuyến mãi',
                index: 11,
                keyTypeLotter: 'promotions',
                ids: ["/promotions"]
            }
        ];

    $(document).ready(function() {
        // menu super speed
        selectLtSuperSpeed();
    });

    if ( hrefLocation.search('home.shtml') > -1 ) return;

    activeTopMenuOther();

    if ( hrefLocation.search('main.shtml') > -1 ) {
        if ( typeof(getParam) === 'undefined' || getParam === '' ) {
            location.href = getLink + '?xoso=' + lotteryID;
        }else {
            if ( hrefLocation.search('/?xoso=') > -1 ) {
                var getHashID = Number(getParam.split('=')[1]);

                if ( !isNaN(getHashID) ) { // if is a number
                    if ( $.inArray( getHashID, listIDValid ) <= -1 ) {
                        showErrorAlert("ID không hợp lệ");
                        setTimeout(function() {
                            location.href = "/main.shtml";
                        }, 1600);
                        return;
                    }

                    lotteryID = getHashID;
                    $("input[name='lotteryid']").val(lotteryID);

                    // check current lottery id and active top menu
                    activeTopMenu();

                    // get data by id
                    getDataByID( lotteryID );

                    // render data by id
                    renderDataInit( lotteryID );

                    if ( lotteryID !== 50 ) {
                        // get history top bet record
                        getOtherLottery( lotteryID );

                        $(data_id.id_issueResult).html(historyissue);

                    }else if ( lotteryID === 50 ) {
                        // get history top bet record
                        getMmcWNum( lotteryID );

                        $(data_class.cl_btnNuoiSo).addClass('hide');
                    }

                    renderContinuouslyBuy( lotteryID );
                }else { // if is a string
                    showErrorAlert("ID không hợp lệ");
                    setTimeout(function() {
                        location.href = location.origin + "/main.shtml?xoso=50";
                    }, 1600);
                }
            }
        }

        switchFaceRegion();

        $(document).ready(function() {
            $.playInit({
                data_label: FaceLottery, // get interface in file interface-game.js
                lotteryid: lotteryID,
            });

            // get bet history
            getHistoryBet( lotteryID );
        });
    }

    $.playInit = function(opts) {
        var ps = {
            data_label: [],
            data_prize: [],
            lotteryid: 1
        };

        opts = $.extend({}, ps, opts || {});

        $.extend({
            lt_id: opts.lotteryid, // game id
            lt_method_data: {},
            lt_total_nums: 0,
            lt_total_money: 0,
            lt_same_code: [],
            lt_position_sel: [],
            lt_data_prize: pri_user_data, // get from server
        });

        mapTitleToDataGame();

        $.lt_method_data = {
            payprizenum: 0,
            methodid: null,
            methodidInDB: null,
            title: '',
            str: '',
            unitMoney: 0,
            name: ''
        };

        FaceLotteryById = opts;

        // select tab 'chon so' and 'chon so nhanh'
        slectTabFastNum();
    };

    /**
     * set title to each method id from serve
     */
    function mapTitleToDataGame() {
        for (var i = 0, len = FaceLottery.length; i < len; i++) {
            var arrMethodGame = FaceLottery[i].label;
            for (var k = 0, lenK = $.lt_data_prize.length; k < lenK; k++) {
                for (var j = 0, lenJ = arrMethodGame.length; j < lenJ; j++) {
                    if (Number($.lt_data_prize[k].methodid) === arrMethodGame[j].methodid) {
                        $.lt_data_prize[k].title = arrMethodGame[j].gtitle;
                    }
                }
            }
        }
    }

    function switchFaceRegion() {
        if (lotteryID === 200) { // is region MB
            FaceLottery = faceMB;
        }else if (lotteryID === 201) {
            FaceLottery = faceMBDB;
        }else { // is another regions
            FaceLottery = faceXTMNMT;
        }
    }

    function activeTopMenu() {
        var idx = null;
        for ( var i = 0, len = menuTop.length; i < len; i++ ) {
            if ( $.inArray( lotteryID, menuTop[i].ids ) > -1 ) {
                idx = menuTop[i].index;
                break;
            }
        }
        if ( idx !== null ) {
            $(data_class.cl_superSpeed).find('.parent__menu > li').eq(idx).find('>a').addClass('active');
        }
    }

    function activeTopMenuOther() {
        var getNamePage = location.pathname.split('.')[0].trim();
        var idx = null;
        for ( var i = 0, len = menuTop.length; i < len; i++ ) {
            if ( $.inArray( getNamePage, menuTop[i].ids ) > -1 ) {
                idx = menuTop[i].index;
                break;
            }
        }
        if ( idx !== null ) {
            $(data_class.cl_superSpeed).find('.parent__menu > li').eq(idx).find('>a').addClass('active');
        }
    }

    function slectTabFastNum() {
        dataTypeTabSlect = $(data_id.id_selectTabFastNum).find('a.active').data('tab');

        $(data_id.id_selectTabFastNum).find('a').off().on('click', function(e) {
            e.preventDefault();
            var _this = $(this),
                dataType = _this.data('tab');

            if ( _this.hasClass('active') ) return;

            $(data_id.id_selectTabFastNum).find('a').removeClass('active');
            _this.addClass('active');

            dataTypeTabSlect = dataType;

            renderNavGame();
        });

        renderNavGame();
    }

    function getDataPrize(arrSubNav) {
        var dyprizeGet = [],
            flag = false;

        if ( typeof($.lt_data_prize) === 'undefined' ) {
            showErrorAlert("Không lấy được dữ liệu data prize.", 4000);
            return;
        }

        if ( typeof( arrSubNav.length ) === 'undefined' ) {
            arrSubNav = [arrSubNav];
        }

        // reset
        arrMethodIdHasIn = [];

        for ( var k = 0, lenK = arrSubNav.length; k < lenK; k++ ) {
            var methodIDLocal = arrSubNav[k].methodid;

            for ( var i = 0, len = $.lt_data_prize.length; i < len; i++ ) {

                if ( k === 0 ) arrMethodIdHasIn.push( +($.lt_data_prize[i].methodid) );

                if ( methodIDLocal === +($.lt_data_prize[i].methodid) && !flag ) {
                    dyprizeGet = $.lt_data_prize[i].dyprize;

                    $.lt_method_data.payprizenum = $.lt_data_prize[i].payprizenum; // get from server
                    $.lt_method_data.methodid = $.lt_data_prize[i].methodid; // get from server
                    $.lt_method_data.methodidInDB = $.lt_data_prize[i].methodidInDB; // get from server

                    renderRatioLottery(dyprizeGet[0]);

                    flag = true;
                }
            }
        }
    }

    function renderNavGame() {
        var htmlNavGame = '',
            $htmlNavGame = '',
            position = 0,
            have_rx = 0,
            opts = FaceLotteryById;

        if ( $.lt_method_data.methodid !== null ) {
            for ( var i = 0, len = opts.data_label.length; i < len; i++ ) {
                var clsStatus = '';
                if (opts.data_label[i].status === 1) {
                    clsStatus = 'hotnewIcon';
                }
                var arrSubTab = opts.data_label[i].label,
                    tabActive = null,
                    tabSubActive = null;
                for ( var k = 0, lenK = arrSubTab.length; k < lenK; k++ ) {
                    if ( parseInt($.lt_method_data.methodid) === arrSubTab[k].methodid ) {
                        tabActive = i;
                        tabSubActive = k;
                        break;
                    }
                }
                if ( tabActive !== null ) {
                    // remove class active
                    htmlNavGame.replace('class="active"', '');
                    // active sub nav game have isdefault = 1
                    htmlNavGame += '<li class="active '+clsStatus+'" value="' + tabActive + '" tag="' + opts.data_label[tabSubActive].isrx + '" default="' + opts.data_label[tabActive].isdefault + '"><a  href="javascript:void(0)">' + opts.data_label[tabActive].title + "</a></li>";

                    // render sub nav game first
                    renderSubNavGame({
                        title: opts.data_label[tabActive].title,
                        label: opts.data_label[tabActive].label,
                        tabSubActive: tabSubActive
                    });
                }else {
                    htmlNavGame += '<li class="'+clsStatus+'" value="' + i + '" tag="' + opts.data_label[i].isrx + '" default="' + opts.data_label[i].isdefault + '"><a  href="javascript:void(0)">' + opts.data_label[i].title + "</a></li>";
                }
            }
        }else {
            for ( var i = 0, len = opts.data_label.length; i < len; i++ ) {
                var clsStatus = '';
                if (opts.data_label[i].status === 1) {
                    clsStatus = 'hotnewIcon';
                }
                if ( Number(opts.data_label[i].isdefault) === 1 ) { // get first active tab
                    // remove class active
                    htmlNavGame.replace('class="active"', '');

                    // active sub nav game have isdefault = 1
                    htmlNavGame += '<li class="active '+clsStatus+'" value="' + i + '" tag="' + opts.data_label[i].isrx + '" default="' + opts.data_label[i].isdefault + '"><a  href="javascript:void(0)">' + opts.data_label[i].title + "</a></li>";

                    // render sub nav game first
                    renderSubNavGame({
                        title: opts.data_label[i].title,
                        label: opts.data_label[i].label
                    });

                }else {
                    htmlNavGame += '<li class="'+clsStatus+'" value="' + i + '" tag="' + opts.data_label[i].isrx + '" default="' + opts.data_label[i].isdefault + '"><a  href="javascript:void(0)">' + opts.data_label[i].title + "</a></li>";
                }
            }
        }

        $htmlNavGame = $(htmlNavGame);

        if ( $(data_id.id_navgame).length ) {
            $(data_id.id_navgame).empty();
            // append html nav game
            $(data_id.id_navgame).append($htmlNavGame);

            // click change tab [Bao lo, Lo Xien, Dac Biet...]
            $(data_id.id_navgame).find('a').on('click', function(e) {
                e.preventDefault();
                var _this = $(this),
                    $parent = _this.parent('li'),
                    idxFace = parseInt( $parent.attr('value') );

                if ( $parent.hasClass('active') ) return;

                $(data_id.id_navgame).find('li').removeClass('active');

                $parent.addClass('active');

                // render sub nav game
                renderSubNavGame({
                    title: opts.data_label[idxFace].title,
                    label: opts.data_label[idxFace].label
                });
            });
        }
    }

    function renderSubNavGame(opts) {
        var htmlSubNavGame = '',
            titleParentTab = opts.title,
            tabSubActive = opts.tabSubActive || null,
            opts = opts.label,
            temp = true;

        $.lt_method_data.title = titleParentTab; // get from server

        $(data_id.id_subnavgame).empty();

        if ( tabSubActive !== null ) {
            for ( var i = 0, len = opts.length; i < len; i++ ) {
                if ( $.inArray( Number(opts[i].methodid), arrMethodIdHasIn ) > -1 ) {
                    if ( tabSubActive === i ) {
                        htmlSubNavGame += '<li value="'+ i +'"><a href="javascript:void(0)" class="active">'+ opts[i].gtitle +'</a></li>';

                        // render lottery selectarea
                        renderLotterySelector(opts[i]);

                        $.lt_method_data.str = opts[i].show_str;
                        $.lt_method_data.unitMoney = opts[i].unitMoney;
                        $.lt_method_data.name = opts[i].gtitle;
                    }else {
                        htmlSubNavGame += '<li value="'+ i +'"><a href="javascript:void(0)">'+ opts[i].gtitle +'</a></li>';
                    }
                }
            }
        }else {
            getDataPrize(opts);
            for ( var i = 0, len = opts.length; i < len; i++ ) {
                if ( $.inArray( Number(opts[i].methodid), arrMethodIdHasIn ) > -1 ) {
                    if ( temp ) { //get first active sub tab
                        htmlSubNavGame += '<li value="'+ i +'"><a href="javascript:void(0)" class="active">'+ opts[i].gtitle +'</a></li>';

                        // render lottery selectarea
                        renderLotterySelector(opts[i]);

                        $.lt_method_data.str = opts[i].show_str;
                        $.lt_method_data.unitMoney = opts[i].unitMoney;
                        $.lt_method_data.name = opts[i].gtitle;
                    }else {
                        htmlSubNavGame += '<li value="'+ i +'"><a href="javascript:void(0)">'+ opts[i].gtitle +'</a></li>';
                    }
                    temp = false;
                }
            }
        }

        if ( $(data_id.id_subnavgame).length && htmlSubNavGame !== '' ) {
            // append html sub nav game
            $(data_id.id_subnavgame).append($(htmlSubNavGame));

            // click sub nav game
            $(data_id.id_subnavgame).find('a').on('click', function(e) {
                e.preventDefault();
                var _this = $(this),
                    $parent = _this.parent('li'),
                    idxFace = parseInt( $parent.attr('value') );

                if ( _this.hasClass('active') ) return;

                $(data_id.id_subnavgame).find('a').removeClass('active');

                _this.addClass('active');

                $.lt_method_data.str = opts[idxFace].show_str;
                $.lt_method_data.unitMoney = opts[idxFace].unitMoney;
                $.lt_method_data.name = opts[idxFace].gtitle;

                getDataPrize(opts[idxFace]);

                // render lottery selectarea
                renderLotterySelector(opts[idxFace]);
            });
        }else {
            $(data_id.id_subnavgame).empty();
            $(data_id.id_betexam).addClass('hide');
        }
    }

    /**
     * check if had tab hidden
     * @param tabIsHide = 'f-digital' or 'digital' or 'input-digital'
     * @param tabIsShow = 'f-digital' or 'digital' or 'input-digital'
     */
    function switchActiveTabTypeGame(tabIsHide, tabIsShow) {
        var $getCurrentActive = $(data_id.id_selectTabFastNum).find('a.active'),
            getNameTabActive = $getCurrentActive.data('tab');
        if (tabIsHide === getNameTabActive) {
            $(data_id.id_selectTabFastNum).find('a').removeClass('active');
            $(data_id.id_selectTabFastNum).find('[data-tab='+tabIsShow+']').addClass('active');
            dataTypeTabSlect = tabIsShow;
        }
        tabIsHide === '' || tabIsHide === undefined ? $(data_id.id_selectTabFastNum).find('[data-tab]').parent().show() : $(data_id.id_selectTabFastNum).find('[data-tab='+tabIsHide+']').parent().hide();
    }

    function renderLotterySelector(opts) {
        if ( opts.length === 0 ) {
            $(data_id.id_lotteryselector).empty();
            return;
        }

        separateNum( $.lt_method_data.methodid );

        $(data_id.id_amounttotalbet).html(0);
        $(data_id.id_countcombination).html(0);

        var htmlSelector = '',
            layout = opts.selectarea.layout,
            layoutFast = opts.selectarea.layoutfastslect, // layoutFast = undefined, layoutFast = [], layoutFast = [....]
            noBigIndex = 5,
            max_place = 0,
            data_sel = [], // save temp data selector
            data_get = {};

        typeArea = opts.selectarea.type;

        $(data_id.id_lotteryselector).empty();

        // get text example for game
        if ( opts.methodexample !== '' ) {
            var htmlBetExam = '<span class="icon-help"></span><div class="inner"><p>'+ opts.methodexample +'</p></div>';
            $(data_id.id_betexam).html(htmlBetExam);
            $(data_id.id_betexam).removeClass('hide');
        }else {
            $(data_id.id_betexam).addClass('hide');
        }

        if ( typeof(layoutFast) === 'undefined' ) { // hide options view input game tab
            $(data_id.id_selectTabFastNum).hide();
        }else if (layoutFast.length === 0) { // hide options "Chon so nhanh"
            switchActiveTabTypeGame('f-digital', 'digital');
        }else {
            $(data_id.id_selectTabFastNum).show();
            switchActiveTabTypeGame();
        }

        if ( typeArea === 'digital' ) {
            $(data_id.id_selectTabFastNum).find('[data-tab=digital]').parent().show();
            if ( dataTypeTabSlect === 'digital' || typeof(layoutFast) === 'undefined' ) {
                for ( var i = 0, len = layout.length; i < len; i++  ) {
                    var no = layout[i].no.split('|'),
                        btn = layout[i].btn;

                    // count place row
                    max_place = layout[i].place > max_place ? layout[i].place : max_place;

                    //set data number selector
                    data_sel[layout[i].place] = [];

                    htmlSelector += '<div class="unit__items"><div class="clearfix"><div class="pull-left unit__item"><span class="d-ib mgR-25 fs-12 fs-15 ver-c w--50">'+ layout[i].title +'</span>';
                    // render number selector
                    for ( var j = 0; j < no.length; j++ ) {
                        htmlSelector += '<a href="" name="lottery_place_'+layout[i].place+'"><span>'+ no[j] +'</span></a>';
                    }
                    htmlSelector += '</div>';

                    // render button behavior
                    htmlSelector += '<div class="pull-right options__unit">';
                    for ( var j = 0; j < btn.length; j++ ) {
                        htmlSelector += '<a href="" name="'+ btn[j].attr_name +'"><span>'+ btn[j].title +'</span></a>';
                    }

                    htmlSelector += '</div></div></div>';
                }
            }else if ( dataTypeTabSlect === 'f-digital' ) {
                if ( layoutFast.length === 0 ) return;
                data_sel[0] = [];
                htmlSelector = renderNumFast();
            }else if ( dataTypeTabSlect === 'input-digital' ) {
                data_sel[0] = [];
                htmlSelector = renderNumInput();
            }
        }else if ( typeArea === 'input' ) {

            switchActiveTabTypeGame('digital', 'input-digital');

            if ( dataTypeTabSlect === 'digital' || dataTypeTabSlect === 'input-digital' ) {
                var htmlNoteEx = '';
                if ( +$.lt_method_data.methodid === 3 || +$.lt_method_data.methodid === 16 ) { // xien 2
                    htmlNoteEx = '12&34<span class="symbol__separate">;</span>68&23 <span class="txtOr">hoặc</span> 12&34<span class="symbol__separate">,</span>68&23 <span class="txtOr">hoặc</span> 12&34 68&23';
                }else if ( +$.lt_method_data.methodid === 4 || +$.lt_method_data.methodid === 17 ) { // xien 3
                    htmlNoteEx = '12&34&20<span class="symbol__separate">;</span>68&23&11 <span class="txtOr">hoặc</span> 12&34&20<span class="symbol__separate">,</span>68&23&11 <span class="txtOr">hoặc</span> 12&34&20 68&23&11';
                }else if ( +$.lt_method_data.methodid === 5 || +$.lt_method_data.methodid === 18 || +$.lt_method_data.methodid === 27 || +$.lt_method_data.methodid === 28 ) { // xien 4, truot xien 4
                    htmlNoteEx = '12&34&20&15<span class="symbol__separate">;</span>68&23&11&34 <span class="txtOr">hoặc</span> 12&34&20&15<span class="symbol__separate">,</span>68&23&11&34 <span class="txtOr">hoặc</span> 12&34&20&15 68&23&11&34';
                }else if ( +$.lt_method_data.methodid === 29 || +$.lt_method_data.methodid === 30) { // truot xien 8
                    htmlNoteEx = '12&34&20&15&11&01&21&33<span class="symbol__separate">;</span>68&23&11&34&88&81&12&90 <span class="txtOr">hoặc</span> 12&34&20&15&11&01&21&33<span class="symbol__separate">,</span>68&23&11&34&88&81&12&90 <span class="txtOr">hoặc</span> 12&34&20&15&11&01&21&33 68&23&11&34&88&81&12&90';
                }else if ( +$.lt_method_data.methodid === 31 || +$.lt_method_data.methodid === 32) { // truot xien 10
                    htmlNoteEx = '12&34&20&15&11&01&21&33&77&83<span class="symbol__separate">;</span>68&23&11&34&88&81&12&90&80&72 <span class="txtOr">hoặc</span> 12&34&20&15&11&01&21&33&77&83<span class="symbol__separate">,</span>68&23&11&34&88&81&12&90&80&72 <span class="txtOr">hoặc</span> 12&34&20&15&11&01&21&33&77&83 68&23&11&34&88&81&12&90&80&72';
                }
                htmlSelector += '<div class="row">' +
                    '<div class="col-sm-7">' +
                    '<div class="frm__item loxien__area">' +
                    '<textarea name="" id="iput__loxien" class="w-100 h-150"></textarea>' +
                    '<div class="note">' +
                    '<p>Cách chơi:</p>' +
                    '<p>Giữa mỗi xiên cần phân cách bởi dấu chấm phẩy " ; " hoặc dấu phẩy " , " hoặc khoảng trắng " "</p>' +
                    '<p>Ví dụ:'+ htmlNoteEx +'</p>' +
                    '</div>' +
                    '</div>' +
                    '</div>'+
                    '<div class="col-sm-3">' +
                    '<div class="mgB-20">' +
                    '<div class="notification noti__small noti__center--top d-ib">' +
                    '<div class="iput__file btn__primary w--110">' +
                    '<input type="file" id="file__loxien" />' +
                    '<span class="btn__iputfile"><span class="icon-add"></span> Tải tập tin</span>' +
                    '</div>' +
                    '<div class="wrap__noti"><div class="wrap__noti--inner"><p class="text-overflow">Định dạng .txt hoặc .csv</p></div></div>' +
                    '</div>' +
                    '</div>' +
                    '<button type="button" class="btn__cancel pdT-10 pdB-10 d-b w--110" id="clearn__loxien">Hủy</button>' +
                    '</div>' +
                    '</div>';
            }else if ( dataTypeTabSlect === 'f-digital' ) {
                if ( layoutFast.length === 0 ) return;
                data_sel[0] = [];
                htmlSelector = renderNumFast();
            }
        }

        if ( $(data_id.id_lotteryselector).length && htmlSelector !== '' ) {
            // append html number selector
            $(data_id.id_lotteryselector).append($(htmlSelector));
        }

        //upload file loxien
        $('#'+data_id.id_iputFileLoXien).on('change', function(e) {
            e.preventDefault();
            if (dataTypeTabSlect === 'input-digital') {
                readFileContent(data_id.id_iputFileLoXien, data_id.id_inputDigital, function() {
                    $('#'+data_id.id_inputDigital).trigger('focus');
                    $('#'+data_id.id_inputDigital).trigger('keyup');
                });
            }else {
                readFileContent(data_id.id_iputFileLoXien, data_id.id_iputAreaLoXien, function() {
                    $(data_id.id_iputLoxien).trigger('focus');
                    $(data_id.id_iputLoxien).trigger('keyup');
                });
            }
        });

        //clear file loxien
        $(data_id.id_clearnLoXien).on('click', function(e) {
            e.preventDefault();
            if (dataTypeTabSlect === 'input-digital') {
                $('#'+data_id.id_inputDigital).val('');
                $('#'+data_id.id_inputDigital).trigger('blur');
                $('#'+data_id.id_inputDigital).trigger('keyup');
            }else {
                $(data_id.id_iputLoxien).val('');
                $(data_id.id_iputLoxien).trigger('blur');
                $(data_id.id_iputLoxien).trigger('keyup');
            }
        });

        // click number lottery
        $(data_id.id_lotteryselector).find("[name^='lottery_place_']").off().on('click', function(e) {
            e.preventDefault();
            var _this = $(this);

            // remove class in btn behavior
            _this.closest(".unit__items").find('.options__unit a').removeClass('active');

            if ( _this.hasClass('active') ) {
                unSelectNum(this, false);
            } else {
                selectNum(this, false);
            }
        });

        // click behavior lottery
        $(data_id.id_lotteryselector).find(".options__unit a").off().on('click', function(e) {
            e.preventDefault();
            var _this = $(this),
                getName = _this.attr('name');

            _this.parent(".options__unit").find('a').removeClass('active');

            _this.addClass('active');

            switch( getName ) {
                case 'all': // chon tất cả
                    _this.closest('.unit__items').find("[name^='lottery_place_']").each(function(i, n) {
                        selectNum(n, true);
                    });
                    break;
                case 'big': // chọn Tài
                    _this.closest('.unit__items').find("[name^='lottery_place_']").each(function(i, n) {
                        selectBig(i, n);
                    });
                    break;
                case 'small': // chọn Xỉu
                    _this.closest('.unit__items').find("[name^='lottery_place_']").each(function(i, n) {
                        selectSmall(i, n);
                    });
                    break;
                case 'odd': // chọn Lẻ
                    _this.closest('.unit__items').find("[name^='lottery_place_']").each(function(i, n) {
                        selectOdd(n);
                    });
                    break;
                case 'even': // chọn chẵn
                    _this.closest('.unit__items').find("[name^='lottery_place_']").each(function(i, n) {
                        selectEven(n);
                    });
                    break;
                case 'clean': // chọn Xóa
                    _this.closest('.unit__items').find("[name^='lottery_place_']").each(function(i, n) {
                        unSelectNum(n, true);
                    });
                    _this.removeClass('active');
                    break;
                default:
                    break;
            }

            checkValLottery();
        });

        // change value multiple
        $(data_id.id_multiple).updownNumber({
            minNumber: 1,
            eventKeyup: function(item) {
                var valNumMultiple = parseInt(item.val()),
                    nums = parseInt($(data_id.id_countcombination).html(), 10),
                    moneyGet = Math.round(valNumMultiple * nums * opts.unitMoney * $.lt_method_data.payprizenum);

                moneyGet = isNaN(moneyGet) ? 0 : moneyGet;

                data_get.moneyGet = moneyGet;
                data_get.numMultiple = valNumMultiple;

                $(data_id.id_amounttotalbet).html( numberWithCommas(moneyGet) );
            }
        });

        // btn bet now
        $(data_id.id_btnbetnow).off().on('click', function(e) {
            e.preventDefault();
            betNow();
        });

        // btn bet now in popup
        $(data_id.id_comfirmBet).find('#btnsubmit__fastbet').off().on('click', function(e) {
            e.preventDefault();

            checkMB_DeDacBiet_3CangDacBiet(function(arrCode) {
                var arrGroupDualNumOld = [];
                var arrGroupDualNumLast = [];
                arrCode.forEach(function(itemCode) {
                    var codes = itemCode['codes'];
                    var getMultiple = itemCode['multiple'];
                    var getIssue = itemCode['issue'];
                    var getDualNums = createGroupDualNum(codes);
                    getDualNums.forEach(function(dualNum) {
                        arrGroupDualNumOld.push({number: dualNum, multiple: getMultiple, issue: getIssue, methodID: itemCode['methodid']});
                    });
                });

                $('input[name="lt_project[]"]').each(function(){
                    var _this = $(this), val = _this.val();
                    val = JSON.parse( val.replace(/'/g, '"') );
                    if (flagCheckDualNumLottery(lotteryID, val['methodid'])) {
                        var getMultiple = val['times'];
                        var codes = val['codes'];
                        var getDualNums = createGroupDualNum(codes);
                        getDualNums.forEach(function(dualNum) {
                            arrGroupDualNumLast.push({number: dualNum, multiple: getMultiple, issue: curissue["issue"], methodID: val['methodid']});
                        });
                    }
                });

                var groupMultiOld = groupMultipleDualNum(arrGroupDualNumOld);
                var groupMultiLast = groupMultipleDualNum(arrGroupDualNumLast);
                var arrTempMulti = groupMultipleDualNum(groupMultiOld.concat(groupMultiLast));

                var getMultiDB = arrTempMulti.filter(function(itemMulti) {return itemMulti['methodID'] === 6 || itemMulti['methodID'] === 1000;});
                var getMaxMultiDB = 0;
                if (getMultiDB.length > 0) getMaxMultiDB = Math.max.apply(Math, getMultiDB.map(function(o) {return o.multiple;}));

                var getMulti3Cang = arrTempMulti.filter(function(itemMulti) {return itemMulti['methodID'] === 21;});
                var getMaxMulti3Cang = 0;
                if (getMulti3Cang.length > 0) getMaxMulti3Cang = Math.max.apply(Math, getMulti3Cang.map(function(o) {return o.multiple;}));

                var getMulti4CangDB = arrTempMulti.filter(function(itemMulti) {return itemMulti['methodID'] === 23;});
                var getMaxMulti4CangDB = 0;
                if (getMulti4CangDB.length > 0) getMaxMulti4CangDB = Math.max.apply(Math, getMulti4CangDB.map(function(o) {return o.multiple;}));

                if (getMaxMultiDB > 2000 || getMaxMulti3Cang > 200 || getMaxMulti4CangDB > 20) { // 2000 là số tối đa cho đánh số kép bằng cho mỗi số
                    arrTempMulti.sort(function(a, b){ return (b['multiple']-a['multiple']);}); // sort z-a
                    var str = '<ul>';
                    arrTempMulti.forEach(function(item) {
                        var clsRed = '';
                        if ((item['multiple'] > 2000 && (item['methodID'] === 6 || item['methodID'] === 1000)) || (item['multiple'] > 200 && item['methodID'] === 21) || (item['multiple'] > 20 && item['methodID'] === 23)) {
                            clsRed = 'color-red invalid';
                        }
                        var pointFirst = 0;
                        var pointLast = 0;
                        var findExistOld = groupMultiOld.find(function(dualNumOld) { return (dualNumOld['number'] === item['number'] && dualNumOld['issue'] === item['issue']); });
                        var findExistLast = groupMultiLast.find(function(dualNumLast) { return (dualNumLast['number'] === item['number'] && dualNumLast['issue'] === item['issue']); });
                        if (findExistOld) {
                            pointFirst = findExistOld['multiple'];
                        }
                        if (findExistLast) {
                            pointLast = findExistLast['multiple'];
                        }
                        var pointRecent = 0;
                        var maxDualNum = 0;
                        if (item['methodID'] === 6 || item['methodID'] === 1000) {
                            pointRecent = 2000 - pointFirst;
                            maxDualNum = '2000';
                        } else if (item['methodID'] === 21) {
                            pointRecent = 200 - pointFirst;
                            maxDualNum = '200';
                        } else if (item['methodID'] === 23) {
                            pointRecent = 20 - pointFirst;
                            maxDualNum = '20';
                        }
                        str += "<li><div><p>Đã cược <strong>"+pointFirst+"/"+maxDualNum+"</strong> điểm</p><p>Cược thêm tối đa <strong>"+pointRecent+"</strong> điểm</p><p>Đang cược <span class='color-red'><strong>"+pointLast+"</strong></span> điểm</p></div><div class='"+clsRed+" numberBet'>["+item['number']+"]</div><div class='issue'>lượt xổ "+item['issue']+"</div></li>";
                    });
                    str += '</ul>';

                    parent.layer.alert('Bạn vượt quá số điểm cho phép đánh <span class="font-700">mỗi số kép bằng</span> <i>(số kép bằng là những số có 2 chữ số cuối giống nhau VD: 00, 11, 22, 155, 066, 1699, 1300...). Chỉ áp dụng cho giải:</i><ul class="applyDualNum"><li>Đề Miền Bắc ĐB, Đề Miền Bắc ĐB 18h25 - <span class="color-red font-700">2000</span> điểm</li><li>3 Càng Miền Bắc - <span class="color-red font-700">200</span> điểm</li><li>4 Càng Miền Bắc - <span class="color-red font-700">20</span> điểm</li></ul>'+str, {
                        id: "DualNumber",
                        btn: ['OK'],
                        area: ['700px'],
                        icon: 2,
                        shade: 0.3,
                        yes: function(index) {
                            parent.layer.close(index);
                        }
                    });
                } else {
                    layer.closeAll();
                    var curtimes = (new Date()).getTime();
                    $('input[name="lt_project[]"]').each(function(){
                        var _this = $(this),
                            val = _this.val();
                        val = JSON.parse( val.replace(/'/g, '"') );
                        val.curtimes = curtimes;
                        _this.val(JSON.stringify(val));
                    });

                    ajaxSubmit($(data_id.id_frmBet));

                    //reset value when submit bet
                    resetData();
                }
            });
        });

        function checkMB_DeDacBiet_3CangDacBiet(funCallBack) {
            var issueArr = ["'" + curissue["issue"] + "'"];
            $.ajax({
                url: urlMobileService,
                dataType: 'json',
                type: 'POST',
                async: false,
                data: {id: lotteryID, issue: issueArr.join(','), flag: 'betRecordAllDay'}
            }).done(function(data) {
                try {
                    var arrHis = data['reslist'];
                    var arrCode = arrHis.filter(function(item) {
                        if (flagCheckDualNumLottery(item['lotteryid'], item['methodid'])) {
                            return item;
                        }
                    });
                    funCallBack && funCallBack(arrCode);
                }catch ( error ) {
                    showErrorAlert("Có lỗi xảy ra.<br>"+ error +"", 4000, function() {
                        //window.location.href = '/home.shtml';
                    });
                }
            }).error(function(jqXHR, status, error) {
                console.log(error);
            });
        }

        // btn bet all
        $(data_id.id_btnBetAll).off().on('click', function(e) {
            e.preventDefault();
            betAll();
        });

        // btn cancel bet now in popup
        $(data_id.id_comfirmBet).find('.btn__cancel').off().on('click', function(e) {
            e.preventDefault();
            layer.closeAll();
        });

        // btn add ticket bet
        $(data_id.id_btnAddTicket).off().on('click', function(e) {
            e.preventDefault();

            //get data
            if ( !betLottery() ) return;

            //render data to view
            addBetDetail();

            // reset checkValLottery
            checkValLottery();
        });

        // btn devare all
        $(data_id.id_btnDeleteAll).off().on('click', function(e) {
            e.preventDefault();
            showConfirm("Xác nhận xóa tất cả cược ?", resetData);
        });

        // keyup and change -> detect change value for type=input
        if ( typeArea === 'input' || dataTypeTabSlect === 'input-digital' ) {
            $(data_id.id_iputLoxien).on('focus', function(e) {
                e.preventDefault();
                $(this).parent().find('.note').addClass('hide');
            }).on('blur', function(e) {
                e.preventDefault();
                if ( $(this).val() === '' ) {
                    $(this).parent().find('.note').removeClass('hide');
                    return;
                }
                $(this).parent().find('.note').addClass('hide');
            }).on('keyup', function(e) {
                e.preventDefault();

                var _this = $(this),
                    val = _this.val();

                formatLoXien( val );

                checkValLottery();
            });

            $('#'+data_id.id_inputDigital).on('focus', function(e) {
                e.preventDefault();
                $(this).parent().find('.note').addClass('hide');
            }).on('blur', function(e) {
                e.preventDefault();
                if ( $(this).val() === '' ) {
                    $(this).parent().find('.note').removeClass('hide');
                    return;
                }
                $(this).parent().find('.note').addClass('hide');
            }).on('keyup', function(e) {
                e.preventDefault();
                var _this = $(this),
                    val = _this.val();
                formatInputDigital( val );
                checkValLottery();
            });
        }

        // click tab select from to number
        $(data_class.cl_slectFromTo).find('a').off().on('click', function(e) {
            e.preventDefault();
            var _this = $(this),
                idx = _this.parent().index();

            if ( _this.hasClass('active') ) return;

            $(data_class.cl_slectFromTo).find('a').removeClass('active');

            _this.addClass('active');

            $(data_class.cl_numFastItem).remove();

            $(data_class.cl_slectNumFast).append( rendernumFastItem( layoutFast[idx], idx ) );
        });

        // click select fast number
        $(data_class.cl_slectNumFast).on('click', data_class.cl_numFastItem + ' a:not(".disable")', actionNumFast);

        // click trace button
        $(data_class.cl_btnNuoiSo).off().on('click', function(e) {
            e.preventDefault();
            popupNuoiSo();
        });

        function popupNuoiSo() {
            if ( $.lt_total_nums === 0 ) {
                showErrorAlert("Thêm nội dung cá cược.", 2000);
                return;
            }

            localStorage.setItem('traceobj', JSON.stringify( $(data_id.id_frmBet).serializeArray() ));

            layer.open({
                type: 2,
                area: ['970px', '635px'],
                title: "Nuôi Số",
                id: "traceIframe",
                content: '/page/Trace.xhtml',
                end: function() {
                    var flag = localStorage.getItem('flag') || false;
                    localStorage.removeItem('traceobj');
                    localStorage.removeItem('flag');
                    if ( flag ) {
                        resetData();
                        setTimeout(function() {
                            getHistoryBet( lotteryID );
                        }, 2200);
                    }
                }
            });
        }

        function actionNumFast( e ) {
            e.preventDefault();

            var _this = $(this),
                getVal = _this.data('value').toString(),
                getTabOf = _this.data('tabOf'),
                $tabParent = $(data_class.cl_slectFromTo).find('li').eq( +getTabOf ),
                $countSlected = $tabParent.find('.num__slected'),
                getCountNum = Number( $countSlected.html() ),
                objSlected = [],
                idxSplice;

            typeof($tabParent.data('nums')) === 'undefined' ? $tabParent.data('nums', []) : objSlected = $tabParent.data('nums');

            if ( _this.hasClass('active') ) {
                getCountNum -= 1;
                if ( typeArea === 'digital' ) {
                    unSelectNum(this, false);
                }
            }else {
                getCountNum += 1;
                $countSlected.addClass('active');
                if ( typeArea === 'digital' ) {
                    selectNum(this, false);
                }
            }

            $countSlected.html(getCountNum);

            if ( getCountNum === 0 ) {
                $countSlected.removeClass('active__effect active');
                objSlected = [];
            }else {
                $countSlected.addClass('active__effect');

                idxSplice = $.inArray( getVal, objSlected );

                if ( idxSplice > -1 ) {
                    objSlected.splice( idxSplice, 1 );
                }else {
                    objSlected.push(getVal);
                }

                setTimeout(function() {
                    $countSlected.removeClass('active__effect');
                }, 200);
            }

            objSlected = objSlected.sort(_SortNum);

            $tabParent.data('nums', objSlected);

            if ( typeArea === 'input' ) {
                var strXien = '',
                    flag = false;

                _this.toggleClass('active');

                for ( var i = 0, len = objSlected.length; i < len; i++ ) {
                    strXien += objSlected[i];
                    if ( parseInt($.lt_method_data.methodid) === 3 || parseInt($.lt_method_data.methodid) === 16 ) { // xien 2
                        if ( (i+1)%2 === 0 ) {
                            strXien += ';';
                        }else {
                            strXien += '&';
                        }
                        if ( len === 2 ) flag = true;
                    }else if ( parseInt($.lt_method_data.methodid) === 4 || parseInt($.lt_method_data.methodid) === 17 ) { // xien 3
                        if ( (i+1)%3 === 0 ) {
                            strXien += ';';
                        }else {
                            strXien += '&';
                        }
                        if ( len === 3 ) flag = true;
                    }else if ( parseInt($.lt_method_data.methodid) === 5 || parseInt($.lt_method_data.methodid) === 18
                        || parseInt($.lt_method_data.methodid) === 27
                        || parseInt($.lt_method_data.methodid) === 28) { // xien 4, truot xien 4
                        if ( (i+1)%4 === 0 ) {
                            strXien += ';';
                        }else {
                            strXien += '&';
                        }
                        if ( len === 4 ) flag = true;
                    }else if ( parseInt($.lt_method_data.methodid) === 29 || parseInt($.lt_method_data.methodid) === 30 ) { // truot xien 8
                        if ( (i+1)%8 === 0 ) {
                            strXien += ';';
                        }else {
                            strXien += '&';
                        }
                        if ( len === 8 ) flag = true;
                    }else if ( parseInt($.lt_method_data.methodid) === 31 || parseInt($.lt_method_data.methodid) === 32 ) { // truot xien 10
                        if ( (i+1)%10 === 0 ) {
                            strXien += ';';
                        }else {
                            strXien += '&';
                        }
                        if ( len === 10 ) flag = true;
                    }
                }
                formatLoXien(strXien);
                checkValLottery();
                if ( flag ) {
                    $(data_class.cl_slectNumFast).find(data_class.cl_numFastItem + ' a:not(.active)').addClass('disable');
                }else {
                    $(data_class.cl_slectNumFast).find(data_class.cl_numFastItem + ' a').removeClass('disable');
                }
            }
        }

        function renderNumFast() {
            var html = '<div class="slect__numfast"><ul class="slect__fromto clearfix">',
                htmlFromTo = '';

            // reset num count fast item
            countSlectedNumFastItem = 0;

            for ( var i = 0, len = layoutFast.length; i < len; i++ ) {
                var numFrom = layoutFast[i].start,
                    numTo = layoutFast[i].end;

                if ( i === 0 ) { // get first active and render
                    htmlFromTo += '<li><a href="javascript:;" class="active">'+ numFrom + ' - ' + numTo +'<span class="num__slected">0</span></a></li>';
                }else {
                    htmlFromTo += '<li><a href="javascript:;">'+ numFrom + ' - ' + numTo +'<span class="num__slected">0</span></a></li>';
                }
            }

            html += htmlFromTo + '</ul>';

            // render fist active item
            html += rendernumFastItem( layoutFast[0], 0 ) + '</div>';

            return html;
        }

        function renderNumInput() {
            var htmlNoteEx = '10<span class="symbol__separate">;</span>20<span class="symbol__separate">;</span>30';
            if ( +$.lt_method_data.methodid === 2
                || +$.lt_method_data.methodid === 11
                || +$.lt_method_data.methodid === 12
                || +$.lt_method_data.methodid === 13
                || +$.lt_method_data.methodid === 15
                || +$.lt_method_data.methodid === 21) {
                htmlNoteEx = '123<span class="symbol__separate">;</span>231<span class="symbol__separate">;</span>105';
            }else if (+$.lt_method_data.methodid === 23
                || +$.lt_method_data.methodid === 24
                || +$.lt_method_data.methodid === 25
                || +$.lt_method_data.methodid === 26) {
                htmlNoteEx = '1234<span class="symbol__separate">;</span>2310<span class="symbol__separate">;</span>1052';
            }
            var htmlSelector =  '<div class="row">' +
                '<div class="col-sm-7">' +
                '<div class="frm__item loxien__area">' +
                '<textarea name="" id="iput__digital" class="w-100 h-150"></textarea>' +
                '<div class="note">' +
                '<p>Cách chơi:</p>' +
                '<p>Giữa mỗi cược cần phân cách bởi dấu chấm phẩy ";" hoặc dấu phẩy "," hoặc khoảng trắng " "</p>' +
                '<p>Ví dụ:'+ htmlNoteEx +'</p>' +
                '</div>' +
                '</div>' +
                '</div>'+
                '<div class="col-sm-3">' +
                '<div class="mgB-20">' +
                '<div class="notification noti__small noti__center--top d-ib">' +
                '<div class="iput__file btn__primary w--110">' +
                '<input type="file" id="file__loxien" />' +
                '<span class="btn__iputfile"><span class="icon-add"></span> Tải tập tin</span>' +
                '</div>' +
                '<div class="wrap__noti"><div class="wrap__noti--inner"><p class="text-overflow">Định dạng .txt hoặc .csv</p></div></div>' +
                '</div>' +
                '</div>' +
                '<button type="button" class="btn__cancel pdT-10 pdB-10 d-b w--110" id="clearn__loxien">Hủy</button>' +
                '</div>' +
                '</div>';
            return htmlSelector;
        }

        function rendernumFastItem( arr, tabOf ) {
            var numFrom = arr.start,
                numTo = arr.end,
                html = '',
                htmlNumFastItem = '',
                $dataSlected = $(data_class.cl_slectFromTo).find('li').eq(tabOf),
                arrDataSlected = $dataSlected.data('nums') || [],
                reAddClassActive = '';

            html += '<ul class="numfast__item clearfix">';

            for ( var i = Number(numFrom); i <= Number(numTo); i++ ) {
                if ( i.toString().length < numTo.length ) {
                    var addZero = numTo.length - i.toString().length, txtZeroAdd = '';
                    for (var j = 0; j < addZero; j++) {
                        txtZeroAdd += '0';
                    }
                    i = txtZeroAdd + i;
                }

                for ( var j = 0, lenJ = arrDataSlected.length; j < lenJ; j++ ) {
                    if ( arrDataSlected[j] === i.toString() ) {
                        reAddClassActive = 'active';
                        break;
                    }else {
                        reAddClassActive = '';
                    }
                }

                htmlNumFastItem += '<li><a href="javascript:;" data-value="'+i+'" data-tab-of="'+ tabOf +'" class="'+reAddClassActive+'">'+ i +'</a></li>';
            }

            html += htmlNumFastItem + '</ul>';

            return html;
        }

        function formatLoXien( val ) {
            var val = formatWhiteSpace(val).replace(/[\s,]/g,';'),
                formatVal = val.split(';');

            for ( var i = 0, len = formatVal.length; i < len; i++ ) {
                if ( formatVal[i] === "" ) {
                    formatVal.splice( i, 1 );
                }
            }

            data_sel = [];

            for ( var i = 0, len = formatVal.length; i < len; i++ ) {
                var item = formatVal[i],
                    getNumItem = '',
                    idxNull,
                    flag = false;

                if ( item.search('&') > -1 ) {
                    getNumItem = item.split('&');
                    idxNull = $.inArray('', getNumItem);

                    if ( idxNull > -1 ) {
                        getNumItem.splice( idxNull, 1 );
                    }

                    for ( var j = 0, lenJ = getNumItem.length; j < lenJ; j++ ) {
                        if (  parseInt( $.lt_method_data.methodid ) === 3 || parseInt( $.lt_method_data.methodid ) === 16 ) { // xien 2
                            if ( !checkNumLen( 2, getNumItem[j] ) ) {
                                flag = true;
                            }
                            if ( (j+1) === 2 && !flag && lenJ === 2 ) { // when get 2 num of xien
                                if ( $.inArray( item, data_sel ) <= -1 ) {
                                    if (!checkNumDuplicateLX(item.split('&'))) {
                                        data_sel.push(item.split('&').sort(_SortNum).join('&'));
                                    }
                                }
                            }
                        }else if ( parseInt( $.lt_method_data.methodid ) === 4 || parseInt( $.lt_method_data.methodid ) === 17 ) { // xien 3
                            if ( !checkNumLen( 2, getNumItem[j] ) ) {
                                flag = true;
                            }
                            if ( (j+1) === 3 && !flag && lenJ === 3 ) { // when get 2 num of xien
                                if ( $.inArray( item, data_sel ) <= -1 ) {
                                    if (!checkNumDuplicateLX(item.split('&'))) {
                                        data_sel.push(item.split('&').sort(_SortNum).join('&'));
                                    }
                                }
                            }
                        }else if ( parseInt( $.lt_method_data.methodid ) === 5 || parseInt( $.lt_method_data.methodid ) === 18
                            || parseInt( $.lt_method_data.methodid ) === 27 || parseInt( $.lt_method_data.methodid ) === 28 ) { // xien 4, truot xien 4
                            if ( !checkNumLen( 2, getNumItem[j] ) ) {
                                flag = true;
                            }
                            if ( (j+1) === 4 && !flag && lenJ === 4 ) { // when get 2 num of xien
                                if ( $.inArray( item, data_sel ) <= -1 ) {
                                    if (!checkNumDuplicateLX(item.split('&'))) {
                                        data_sel.push( item.split('&').sort(_SortNum).join('&') );
                                    }
                                }
                            }
                        }else if ( parseInt( $.lt_method_data.methodid ) === 29 || parseInt( $.lt_method_data.methodid ) === 30 ) { // truot xien 8
                            if ( !checkNumLen( 2, getNumItem[j] ) ) {
                                flag = true;
                            }
                            if ( (j+1) === 8 && !flag && lenJ === 8 ) { // when get 2 num of xien
                                if ( $.inArray( item, data_sel ) <= -1 ) {
                                    if (!checkNumDuplicateLX(item.split('&'))) {
                                        data_sel.push( item.split('&').sort(_SortNum).join('&') );
                                    }
                                }
                            }
                        }else if ( parseInt( $.lt_method_data.methodid ) === 31 || parseInt( $.lt_method_data.methodid ) === 32 ) { // truot xien 10
                            if ( !checkNumLen( 2, getNumItem[j] ) ) {
                                flag = true;
                            }
                            if ( (j+1) === 10 && !flag && lenJ === 10 ) { // when get 2 num of xien
                                if ( $.inArray( item, data_sel ) <= -1 ) {
                                    if (!checkNumDuplicateLX(item.split('&'))) {
                                        data_sel.push( item.split('&').sort(_SortNum).join('&') );
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if ( data_sel.length > 0 ) {
                var checkDupli = removeItemDupliArr(data_sel);
                if ( checkDupli.flag ) {
                    data_sel = checkDupli.result;
                    //reset textarea
                    $(data_id.id_iputLoxien).val( data_sel.join(';') );

                    showSuccessAlert("Cược bị trùng. Đã tự động loại bỏ.");
                }
            }
        }

        function formatWhiteSpace( val ) {
            val = val.replace(/[\n\r,]/g, ';');
            var temp = [];
            if (val.search(';') > 0) temp = val.split(';');
            if (temp.length) {
                for (var i = 0, len = temp.length; i < len; i++) {
                    temp[i] = temp[i].trim().replace(/\s/g,'&');
                }
                val = temp.join(';');
            }

            return val;
        }

        function checkNumDuplicateLX(arr) {
            var flagCheck = false, count = 0, valTemp = arr[count], flag = true, len = arr.length, i = 1;
            while(flag) {
                if (valTemp === arr[i] && i !== count) {
                    flagCheck = true;
                    break;
                }
                if (i === len) {
                    count += 1;
                    i = count;
                    valTemp = arr[count];
                }
                i += 1;
                if (!flag || i > len) break;
            }
            return flagCheck;
        }

        function formatInputDigital( val ) {
            var checkStr = /[^0-9&;,\s]/g.test(val),
                checkWrongNum = false,
                dataSelTemp = [];
            if (!checkStr) {
                var val = val.replace(/[,\s]/g,';'),
                    formatVal = val.split(';');

                for ( var i = 0, len = formatVal.length; i < len; i++ ) {
                    if ( formatVal[i] === "" ) {
                        formatVal.splice( i, 1 );
                    }
                }

                for ( var i = 0, len = formatVal.length; i < len; i++ ) {
                    var item = formatVal[i],
                        flag = false;

                    if (parseInt( $.lt_method_data.methodid ) === 1
                        || parseInt( $.lt_method_data.methodid ) === 7
                        || parseInt( $.lt_method_data.methodid ) === 6 // dedacbiet
                        || parseInt( $.lt_method_data.methodid ) === 20036 // dedacbietdau
                        || parseInt( $.lt_method_data.methodid ) === 20034 // degiainhat
                        || parseInt( $.lt_method_data.methodid ) === 20035 // degiai7
                        || parseInt( $.lt_method_data.methodid ) === 8
                        || parseInt( $.lt_method_data.methodid ) === 1000
                        || parseInt( $.lt_method_data.methodid ) === 14) { // lo2so, dedau, dedacbiet, dedauduoi
                        if ( !checkNumLen( 2, item ) ) {
                            flag = true;
                            checkWrongNum = true;
                        }
                        if ( !flag ) {
                            dataSelTemp.push( item );
                        }
                    }else if (parseInt( $.lt_method_data.methodid ) === 2
                        || parseInt( $.lt_method_data.methodid ) === 11
                        || parseInt( $.lt_method_data.methodid ) === 12
                        || parseInt( $.lt_method_data.methodid ) === 13
                        || parseInt( $.lt_method_data.methodid ) === 15
                        || parseInt( $.lt_method_data.methodid ) === 21) {
                        if ( !checkNumLen( 3, item ) ) {
                            flag = true;
                            checkWrongNum = true;
                        }
                        if ( !flag ) {
                            dataSelTemp.push( item );
                        }
                    }else if (parseInt( $.lt_method_data.methodid ) === 26
                        || parseInt( $.lt_method_data.methodid ) === 25
                        || parseInt( $.lt_method_data.methodid ) === 23
                        || parseInt( $.lt_method_data.methodid ) === 24) {
                        if ( !checkNumLen( 4, item ) ) {
                            flag = true;
                            checkWrongNum = true;
                        }
                        if ( !flag ) {
                            dataSelTemp.push( item );
                        }
                    }
                }

                if ( dataSelTemp.length > 0 ) {
                    var checkDupli = removeItemDupliArr(dataSelTemp);
                    if ( checkDupli.flag ) {
                        dataSelTemp = checkDupli.result;
                        //reset textarea
                        $('#'+data_id.id_inputDigital).val( dataSelTemp.join(';') );
                        showSuccessAlert("Cược bị trùng. Đã tự động loại bỏ.");
                    }
                }
            }else {
                checkWrongNum = true;
            }

            if (checkWrongNum) {
                dataSelTemp = [];
            }
            data_sel[0] = dataSelTemp;
        }

        function checkNumLen( manyNum, num ) {
            var partn = "";

            manyNum = parseInt(manyNum, 10);

            switch ( manyNum ) {
                case 2:
                    partn = /^[0-9]{2}$/;
                    break;
                case 3:
                    partn = /^[0-9]{3}$/;
                    break;
                case 4:
                    partn = /^[0-9]{4}$/;
                    break;
                default:
                    break;
            }

            return partn.test(num);
        }

        function betNow() {
            if ( !betLottery() ) return;

            // render html for bet detail popup
            renderPopupComfirm();

            addBetDetail();

            // reset checkValLottery
            checkValLottery();
        }

        function renderPopupComfirm() {
            var htmlBetDetailPopup = "";
            $(data_id.id_comfirmBet).find('.gid__tbl--inner .tbl').empty();
            for ( var i = 0, len = betList.length; i < len; i++ ) {
                htmlBetDetailPopup = '<div class="tbl__row">' +
                    '<div class="w-18 tbl__col">'+ betList[i].nameBet +'</div>' +
                    '<div class="w-25 tbl__col">'+ betList[i].numBetGet +'</div>' +
                    '<div class="w-15 tbl__col">'+ betList[i].nums +'</div>' +
                    '<div class="w-13 tbl__col">'+ betList[i].numMultiple +'</div>' +
                    '<div class="w-15 tbl__col">'+ numberWithCommas(betList[i].moneyGet) +'</div>' +
                    '<div class="w-19 tbl__col">'+ numberWithCommas(betList[i].calcuMoneyWin) +'</div>' +
                    '</div>';

                $(data_id.id_comfirmBet).find('.gid__tbl--inner .tbl').prepend($(htmlBetDetailPopup));
                htmlBetDetailPopup = '';
            }

            $(data_id.id_totalMoneyBetPopup).html( numberWithCommas($.lt_total_money) );

            layer.open({
                type: 1,
                title: 'Xác nhận',
                area: ['900px', '380px'],
                content: $(data_id.id_comfirmBet)
            });
        }

        function resetData() {
            betList = [];
            data_get = {};
            $(data_id.id_betreview).find('.tbl__empty').removeClass('hide');
            $(data_id.id_betreview).find('.gid__tbl--inner .tbl__row:not(.tbl__empty)').remove();
            $.lt_total_nums = 0;
            $.lt_total_money = 0;
            $.lt_same_code = [];
            $(data_id.id_totalMoneyBet).html(0);
            $(data_id.id_totalMoneyBetPopup).html(0);
            $(data_id.id_buyContinuously).find('select[name="lt_issue_start"]').val(1);

            $(data_id.id_lotteryselector).find(".options__unit a[name='clean']").trigger('click');

            // reset f-digital
            $(data_class.cl_slectFromTo).find('li').data('nums', []);
            $(data_class.cl_slectFromTo).find('li').find('.num__slected').removeClass('active__effect active').html(0);
            $(data_class.cl_slectNumFast).find(data_class.cl_numFastItem + ' a').removeClass('active');

            // reset loxien
            $(data_id.id_clearnLoXien).trigger('click');
        }

        function betAll() {
            if ( $.lt_total_nums === 0 ) {
                showErrorAlert("Thêm nội dung cá cược.", 2500);
                return;
            }

            renderPopupComfirm();
        }

        function addBetDetail() {
            var $wrap = $('<div class="tbl__row effect__row"></div>'), // init $valiable to set data
                htmlBetDetail = "";

            htmlBetDetail = '<div class="w-7 tbl__col">'+ data_get.stt +'</div>' +
                '<div class="w-16 tbl__col">'+ data_get.nameBet +'</div>' +
                '<div class="w-20 tbl__col" title="'+ data_get.fullNumBetGet +'">'+ data_get.numBetGet +'</div>' +
                '<div class="w-15 tbl__col">'+ data_get.nums +'</div>' +
                '<div class="w-12 tbl__col">'+ data_get.numMultiple +'</div>' +
                '<div class="w-15 tbl__col">'+ numberWithCommas(data_get.moneyGet) +'</div>' +
                '<div class="w-15 tbl__col">'+ numberWithCommas(data_get.calcuMoneyWin) +'</div>' +
                '<div class="w-5 tbl__col"><a class="color-f49e1d devareBetReview"><span class="icon-bin"></span></a></div>' +
                '<input type="hidden" name="lt_project[]" value="' + data_get.serverData + '" />';

            $(data_id.id_betreview).find('.tbl__empty').addClass('hide');

            // make sure total number row bet exists
            if ( $.lt_total_nums === 0 ) {
                $(data_id.id_betreview).find('.gid__tbl--inner .tbl__row:not(.tbl__empty)').remove();
            }

            // add data attribute for know get value of row bet
            $wrap.data("data", {
                methodid: $.lt_method_data.methodid,
                methodname: data_get.nameBet,
                nums: data_get.nums,
                money: data_get.moneyGet,
                position: data_get.cur_position,
                prize: data_get.prize,
                point: data_get.point,
                code: data_get.fullNumBetGet
            });

            $wrap.html(htmlBetDetail);

            $wrap.find('.devareBetReview').on('click', function(e) {
                e.preventDefault();
                var _this = $(this),
                    $parentRow = _this.closest('.tbl__row');

                devareBet({
                    parentRow: $parentRow,
                    betList: betList
                });
            });

            $wrap.prependTo($(data_id.id_betreview).find('.gid__tbl--inner .tbl'));

            //remove class effect__row
            setTimeout(function() {
                $(data_id.id_betreview).find('.gid__tbl--inner .tbl__row:not(.tbl__empty)').removeClass('effect__row');
            },300);

            $(data_id.id_betreview).find('.gid__tbl--inner .tbl__row:not(.tbl__empty)').each(function(i) {
                var _this = $(this),
                    $firstCol = _this.find('.tbl__col').eq(0);
                $firstCol.html(i+1);
            });

            $(data_id.id_totalMoneyBet).html( numberWithCommas($.lt_total_money) );

            data_get = {};

            $(data_id.id_iputLoxien).val('').blur();
            $('#'+data_id.id_inputDigital).val('').blur();
        }

        function betLottery() {
            var typePostTab = dataTypeTabSlect;
            if (dataTypeTabSlect === 'input-digital') {
                typePostTab = 'f-digital';
            }
            var milliseconds = (new Date).getTime(),
                numStr = $.lt_method_data.str,
                methodid = parseInt($.lt_method_data.methodid),
                typePost = typePostTab === 'f-digital' ? typePostTab : typeArea,
                serverData = '',
                temp = [],
                strCut,
                descStr,
                cur_position = 0,
                numBetGet = "",
                //getValRatioSelected = $(data_id.id_ratiolottery).find('select').val(),
                getValRatioSelected = $(data_id.id_ratiolottery).find('#valRatio').html(),
                calcuMoneyWin = 0,
                flagValidate = true,
                totalMoneyBet = 0,
                getCodes = '',
                flagNotHaveFastDigital = false;

            // check Đầu Đuôi methodid
            if ( $.lt_method_data.methodid === '9' || $.lt_method_data.methodid === '20' || $.lt_method_data.methodid === '10' || $.lt_method_data.methodid === '22' ) {
                typePost = 'digital';
                flagNotHaveFastDigital = true;
            }
            serverData = "{'type':'" + typePost + "','methodid':" + $.lt_method_data.methodidInDB + ",'codes':'";

            data_get.curtimes = milliseconds;
            data_get.cur_position = cur_position;

            if ( getValRatioSelected === '' || typeof(getValRatioSelected) === 'undefined' ) {
                showErrorAlert('Không lấy được giá trị của tỉ lệ cược.', 2500);
                return;
            }

            /*data_get.point = getValRatioSelected.split("|")[0];
            data_get.prize = getValRatioSelected.split("|")[1];*/
            data_get.point = '0';
            data_get.prize = getValRatioSelected;

            if (isNaN(data_get.nums) || isNaN(data_get.numMultiple) || isNaN(data_get.moneyGet) || data_get.moneyGet <= 0) {
                showErrorAlert('Chọn số không hợp lệ. Vui lòng chọn lại !!!');
                return;
            }

            if ( typeArea === 'digital' ) {
                if ( typePostTab === 'f-digital' && !flagNotHaveFastDigital ) {
                    for (var i = 0; i < data_sel.length; i++) {
                        //numStr = numStr.replace("X", data_sel[i].sort(_SortNum).join(""));
                        temp.push( data_sel[i].sort(_SortNum).join(",") );
                    }
                    getCodes = temp;
                    data_get.fullNumBetGet = temp.join('');
                }else {
                    for (var i = 0; i < data_sel.length; i++) {
                        //numStr = numStr.replace("X", data_sel[i].sort(_SortNum).join(""));
                        temp.push( data_sel[i].sort(_SortNum).join("&") );
                    }
                    data_get.fullNumBetGet = temp.join("|").replace(/&/g, ",");
                    getCodes = temp.join("|");
                }
            }else if ( typeArea === 'input' ) {
                data_get.fullNumBetGet = data_sel.join(";");
                temp.push( data_sel.join(";") );
                getCodes = temp;
                numStr = temp;
            }

            // compare bet next is there a duplication?
            if ($.lt_same_code[methodid] !== undefined && $.lt_same_code[methodid][cur_position] !== undefined && $.lt_same_code[methodid][cur_position].length > 0) {
                if ( $.inArray( data_get.fullNumBetGet, $.lt_same_code[methodid][cur_position] ) != -1 ) {
                    showErrorAlert('Đã tồn tại cược này trong <span class="font-500">Chi tiết cược</span>.<br>Vui Lòng chọn lại.', 2500);

                    flagValidate = false;

                    return false;
                }
            }

            // function push bet each bet to $.lt_same_code
            checkBetExist({
                arr: temp,
                cur_position: cur_position
            });

            if (numStr.length > 40) {
                strCut = numStr.substring(0, 35) + "...";
            } else {
                strCut = numStr;
            }

            // reset data_sel
            for (i = 0; i < data_sel.length; i++) {
                if ( typeArea === 'input' ) {
                    data_sel = [];
                }else if ( typeArea === 'digital' ) {
                    data_sel[i] = [];
                }
            }

            // reset class active of number and btn behavior
            $(data_id.id_lotteryselector).find('.unit__item a').removeClass('active');
            $(data_id.id_lotteryselector).find('.options__unit a').removeClass('active');

            // reset f-digital selected
            $(data_class.cl_slectFromTo).find('li').data('nums', []);
            $(data_class.cl_slectFromTo).find('li').find('.num__slected').removeClass('active__effect active').html(0);
            $(data_class.cl_slectNumFast).find(data_class.cl_numFastItem + ' a').removeClass('active disable');

            // reset multiple
            $(data_id.id_multiple).find('input').val(1);

            descStr = "[" + $.lt_method_data.title + " - " + $.lt_method_data.name + "] " + strCut;

            serverData += getCodes + "','zip':" + 0 + ",'point':" + data_get.point + ",'mode':" + 1 + ",'nums':" + data_get.nums + ",'times':" + data_get.numMultiple + ",'money':" + data_get.moneyGet + ",'desc':'" + descStr + "'}";

            data_get.serverData = serverData;

            // conver array temp to view
            numBetGet = cutStrDot(temp);

            calcuMoneyWin = (Number(data_get.prize)*data_get.numMultiple).toFixed(2)*1000;

            data_get.numBetGet = numBetGet; // show view string number bet
            data_get.nameBet = '[ '+ $.lt_method_data.title + " - " + $.lt_method_data.name +' ]';
            data_get.calcuMoneyWin = calcuMoneyWin;

            // push data with each bet
            betList.push(data_get);

            $.lt_total_nums += data_get.nums;
            $.lt_total_money += parseInt(data_get.moneyGet);

            // set value to input hidden
            $(data_id.id_totalNum).val($.lt_total_nums);
            $(data_id.id_totalMoney).val($.lt_total_money);

            return flagValidate;
        }

        // sort number
        function _SortNum(a, b) {
            a = parseInt(a, 10);
            b = parseInt(b, 10);
            if (isNaN(a) || isNaN(b)) {
                return true
            }
            return (a - b);
        }

        function unSelectNum(item, isBtnBehavior) {
            var getPlace = typeof($(item).attr("name")) === 'undefined' ? 0 : Number($(item).attr("name").replace("lottery_place_", "")),
                getValNum = Number($.trim($(item).text()));

            $(item).removeClass('active');

            data_sel[getPlace] = $.grep(data_sel[getPlace], function(n, i) {
                return n == getValNum;
            }, true);

            if ( !isBtnBehavior ) { // check if btn behavior click, false: not click, true: clicked
                checkValLottery();
            }
        }

        function selectNum(item, isBtnBehavior) {
            // not get number has class active and not push to array data_sel
            if ( $(item).hasClass('active') ) return;

            var getPlace = typeof($(item).attr("name")) === 'undefined' ? 0 : Number($(item).attr("name").replace("lottery_place_", "")),
                getValNum = $.trim($(item).text());

            $(item).addClass('active');

            data_sel[getPlace].push(getValNum);

            if ( !isBtnBehavior ) { // check if btn behavior click, false: not click, true: clicked
                checkValLottery();
            }
        }

        function selectBig(i, obj) { // Tài
            if ( i >= noBigIndex ) {
                selectNum(obj, true);
            } else {
                unSelectNum(obj, true);
            }
        }

        function selectSmall(i, obj) { // Xỉu
            if ( i < noBigIndex ) {
                selectNum(obj, true);
            }else {
                unSelectNum(obj, true);
            }
        }

        function selectOdd(obj) { // lẻ
            if ( Number($(obj).text()) % 2 === 1 ) {
                selectNum(obj, true);
            } else {
                unSelectNum(obj, true);
            }
        }

        function selectEven(obj) { // chẵn
            if ( Number($(obj).text()) % 2 === 0 ) {
                selectNum(obj, true);
            } else {
                unSelectNum(obj, true);
            }
        }

        function checkValLottery() {
            var tmp_nums = 1,
                nums = 0;

            if ( typeArea === 'input' ) {
                nums = data_sel.length;
            }else if ( typeArea === 'digital') {
                for (i = 0; i <= max_place; i++) {
                    if (data_sel[i].length == 0) { // calculator Combination
                        tmp_nums = 0;
                        break;
                    }
                    tmp_nums *= data_sel[i].length
                }
                nums = tmp_nums; // return how many number
            }

            // get value multiple
            var valNumMultiple = parseInt($(data_id.id_multiple).find('input').val(), 10),
                moneyGet = Math.round(valNumMultiple * nums * opts.unitMoney * parseInt($.lt_method_data.payprizenum));

            moneyGet = isNaN(moneyGet) ? 0 : moneyGet;

            data_get.nums = nums;
            data_get.moneyGet = moneyGet,
                data_get.numMultiple = valNumMultiple;

            $(data_id.id_countcombination).html(nums);
            $(data_id.id_amounttotalbet).html( numberWithCommas(moneyGet) );
        }

        function devareBet(obj) {
            var $item = obj.parentRow,
                betList = obj.betList,
                methodid = parseInt($item.data('data').methodid),
                nums = $item.data('data').nums,
                methodname = $item.data('data').methodname,
                money = $item.data('data').money,
                position = $item.data('data').position,
                prize = $item.data('data').prize,
                point = $item.data('data').point,
                numBetGet = $item.data('data').code,
                idxSameCode = null,
                idxBetList = null;

            $.each($.lt_same_code[methodid][position], function(i, code) {
                if (code === numBetGet) {
                    idxSameCode = i;
                }
            });

            if (idxSameCode !== null) {
                $.lt_same_code[methodid][position].splice(idxSameCode, 1);
            }

            $.each(betList, function(index, item) {
                if ( item.fullNumBetGet === numBetGet ) {
                    idxBetList = index;
                }
            });

            if ( idxBetList !== null ) {
                betList.splice(idxBetList, 1);
            }

            // update total value
            $.lt_total_nums -= nums;
            $.lt_total_money -= money;

            // set value to input hidden
            $(data_id.id_totalNum).val($.lt_total_nums);
            $(data_id.id_totalMoney).val($.lt_total_money);

            // update view
            $(data_id.id_totalMoneyBet).html( numberWithCommas($.lt_total_money) );

            $item.remove();

            $(data_id.id_betreview).find('.gid__tbl--inner .tbl__row:not(.tbl__empty)').each(function(i) {
                var _this = $(this),
                    $firstCol = _this.find('.tbl__col').eq(0);
                $firstCol.html(i+1);
            });

            if ( $.lt_total_nums === 0 ) {
                $(data_id.id_betreview).find('.tbl__empty').removeClass('hide');
            }
        }
    }

    function cutStrDot(str, type) {
        var lenTemp = 0,
            strDot = '',
            typeGet = '';

        typeGet = typeof(type) !== "undefined" ? type : typeArea;

        if ( typeGet === 'digital' || typeGet === 'f-digital' ) {
            str = typeof(str) === 'string' ? str.split('|') : str;
            if ( str.length === 1 ) {
                strDot = str[0];
                if ( strDot.length > 15 ) {
                    strDot = strDot.substring(0, 15) + "...";
                }
                return strDot.replace(/&|,/g, '<span class="line__num"></span>');
            }
            for ( var i = 0; i < str.length; i++ ) {
                var getStr = str[i],
                    strReplace = getStr.replace(/&/g, ",");
                str[i] = strReplace;
                lenTemp += strReplace.length;
                if ( lenTemp > 20 ) {
                    str[i] = str[i].substring(0, 5) + "...";
                }
            }
            strDot = str.join('<span class="line__num"></span>');
        }else if ( typeGet === 'input' ) {
            str = typeof(str) === 'string' ? str : str.toString();
            if ( str.length > 16 ) {
                strDot = str.substring(0, 16).replace(/&/g, '<span class="line__num"></span>').replace(/;/g, " - ") + "...";
            }else {
                strDot = str.replace(/&/g, '<span class="line__num"></span>').replace(/;/g, " - ");
            }
        }

        return strDot;
    }

    function renderRatioLottery(dyprizeGet) {
        // update only ratio
        var htmlRatio = '<p class="lh-35">Tỉ lệ cược <span class="color-red">1</span> ăn ',
            prize = dyprizeGet.prize[0];
        htmlRatio += '<span id="valRatio" class="color-red">'+(parseFloat(prize.prize) % 1 != 0 ? prize.prize : parseFloat(prize.prize).toFixed(0))+'</span></p>';
        if ( $(data_id.id_ratiolottery).length ) {
            $(data_id.id_ratiolottery).empty();
            // append html ratio lottery
            $(data_id.id_ratiolottery).append($(htmlRatio));
        }
    }

    function renderContinuouslyBuy( id ) {
        if ( objIssueStart.length === 0 ) return;

        var html = '';

        if ( id === 50 ) {
            html = '<span class="d-ib ver-c mgR-5">Mua liên tục:</span><div class="slect__box ver-c"><select name="lt_issue_start">';
            for ( var i = 0, len = objIssueStart.length; i < len; i++ ) {
                html += '<option value="'+ objIssueStart[i].value +'">'+ objIssueStart[i].text +'</option>'
            }
            html += '</select><span class="icon-keyboard_arrow_down"></span></div>';
            $(data_id.id_buyContinuously).removeClass('hide');
        }else {
            html += '<input value="'+ objIssueStart[0].value +'" name="lt_issue_start" type="hidden" />';
            $(data_id.id_buyContinuously).addClass('hide');
        }

        if ( $(data_id.id_buyContinuously).length ) {
            $(data_id.id_buyContinuously).empty();
            $(data_id.id_buyContinuously).append($(html));
        }
    }

    function ajaxSubmit(frm) {
        layer.load(2, {
            shade: 0.3
        });

        $.ajax({
            type: "POST",
            url: urlLotteryService,
            timeout: 30000,
            async: false,
            contentType: "application/x-www-form-urlencoded;charset=utf-8",
            data: frm.serialize(),
            success: function(data) {
                layer.closeAll('loading');

                try {
                    var jsonParse = JSON.parse(data);

                    checkLogin(jsonParse.msg);

                    if ( jsonParse.msg !== '' ) {
                        showErrorAlert(jsonParse.msg);
                    }else {
                        showSuccessAlert('Đặt cược thành công.', 2000);
                    }

                    setTimeout(function() {
                        getHistoryBet( lotteryID );
                        if ( lotteryID === 50 ) {
                            getMmcWNum();
                        }
                    }, 2000);
                }catch( error ) {
                    showErrorAlert("Có lỗi xảy ra.<br>"+ error +"", 4000, function() {
                        window.location.href = '/home.shtml';
                    });
                    return;
                }
            }
        });
    }

    function checkBetExist(objOneBet) {
        var arr = objOneBet.arr,
            cur_position = objOneBet.cur_position,
            methodid = parseInt($.lt_method_data.methodid);

        if ($.lt_same_code[methodid] == undefined) {
            $.lt_same_code[methodid] = []
        }

        if ($.lt_same_code[methodid][cur_position] == undefined) {
            $.lt_same_code[methodid][cur_position] = []
        }

        if ( typeArea === 'digital' ) {
            $.lt_same_code[methodid][cur_position].push(arr.join("|").replace(/&/g,','));
        }else if ( typeArea === 'input' ) {
            $.lt_same_code[methodid][cur_position].push(arr.toString());
        }
    }

    function selectLtSuperSpeed() {
        $(data_class.cl_superSpeed).find('a[data-url-id]').on('click', function(e) {
            e.preventDefault();
            var _this = $(this),
                getID = +_this.data('urlId');

            if ( parseInt($('#flagLogin').val()) ) {
                showErrorAlert("Vui lòng đăng nhập.");
                return;
            }

            location.href = location.origin + '/main.shtml?xoso=' + getID;
        });

        $(data_class.cl_superSpeed).find('.linkRedirectGame').on('click', function(e) {
            if ( parseInt($('#flagLogin').val()) ) {
                e.preventDefault();
                showErrorAlert("Vui lòng đăng nhập.");
                return;
            }
        });
    }

    function renderDataInit( getID ) {
        $(data_class.cl_tblTime).removeClass('hide');
        $(data_class.cl_tblResult).removeClass('full');
        var htmlTitvarime = '';
        switch( getID ) {
            case 50:
                htmlTitvarime = '<p>Xổ số siêu tốc 1 giây</p>';
                $(data_class.cl_tblTime).addClass('hide');
                $(data_class.cl_tblResult).addClass('full');

                // set issue for 1 giây
                objIssueStart = [
                    {
                        value: 1,
                        text: '1 lần'
                    },
                    {
                        value: 2,
                        text: '2 lần'
                    },
                    {
                        value: 5,
                        text: '5 lần'
                    },
                    {
                        value: 8,
                        text: '8 lần'
                    },
                    {
                        value: 10,
                        text: '10 lần'
                    },
                    {
                        value: 15,
                        text: '15 lần'
                    },
                    {
                        value: 20,
                        text: '20 lần'
                    },
                    {
                        value: 45,
                        text: '45 lần'
                    },
                    {
                        value: 100,
                        text: '100 lần'
                    }
                ];

                break;
            case 100:
                htmlTitvarime = '<p>Xổ số siêu tốc 45 giây</p>';
                break;
            case 101:
                htmlTitvarime = '<p>Xổ số siêu tốc 1 phút</p>';
                break;
            case 102:
                htmlTitvarime = '<p>Xổ số siêu tốc 1.5 phút</p>';
                break;
            case 103:
                htmlTitvarime = '<p>Xổ số siêu tốc 2 phút</p>';
                break;
            case 104:
                htmlTitvarime = '<p>Xổ số siêu tốc 5 phút</p>';
                break;
            case 105:
                htmlTitvarime = '<p>Xổ số Hồ Chí Minh</p>';
                break;
            case 106:
                htmlTitvarime = '<p>Xổ số Đồng Tháp</p>';
                break;
            case 107:
                htmlTitvarime = '<p>Xổ số Cà Mau</p>';
                break;
            case 108:
                htmlTitvarime = '<p>Xổ số Bến Tre</p>';
                break;
            case 109:
                htmlTitvarime = '<p>Xổ số Vũng Tàu</p>';
                break;
            case 110:
                htmlTitvarime = '<p>Xổ số Bạc Liêu</p>';
                break;
            case 111:
                htmlTitvarime = '<p>Xổ số Đồng Nai</p>';
                break;
            case 112:
                htmlTitvarime = '<p>Xổ số Cần Thơ</p>';
                break;
            case 113:
                htmlTitvarime = '<p>Xổ số Sóc Trăng</p>';
                break;
            case 114:
                htmlTitvarime = '<p>Xổ số Tây Ninh</p>';
                break;
            case 115:
                htmlTitvarime = '<p>Xổ số An Giang</p>';
                break;
            case 116:
                htmlTitvarime = '<p>Xổ số Bình Thuận</p>';
                break;
            case 117:
                htmlTitvarime = '<p>Xổ số Vĩnh Long</p>';
                break;
            case 118:
                htmlTitvarime = '<p>Xổ số Bình Dương</p>';
                break;
            case 119:
                htmlTitvarime = '<p>Xổ số Trà Vinh</p>';
                break;
            case 120:
                htmlTitvarime = '<p>Xổ số Long An</p>';
                break;
            case 121:
                htmlTitvarime = '<p>Xổ số Bình Phước</p>';
                break;
            case 122:
                htmlTitvarime = '<p>Xổ số Hậu Giang</p>';
                break;
            case 123:
                htmlTitvarime = '<p>Xổ số Tiền Giang</p>';
                break;
            case 124:
                htmlTitvarime = '<p>Xổ số Kiên Giang</p>';
                break;
            case 125:
                htmlTitvarime = '<p>Xổ số Đà Lạt</p>';
                break;
            case 126:
                htmlTitvarime = '<p>Xổ số Thừa Thiên Huế</p>';
                break;
            case 127:
                htmlTitvarime = '<p>Xổ số Phú Yên</p>';
                break;
            case 128:
                htmlTitvarime = '<p>Xổ số Đắk Lắk</p>';
                break;
            case 129:
                htmlTitvarime = '<p>Xổ số Quảng Nam</p>';
                break;
            case 130:
                htmlTitvarime = '<p>Xổ số Đà Nẵng</p>';
                break;
            case 131:
                htmlTitvarime = '<p>Xổ số Khánh Hòa</p>';
                break;
            case 132:
                htmlTitvarime = '<p>Xổ số Bình Định</p>';
                break;
            case 133:
                htmlTitvarime = '<p>Xổ số Quảng Trị</p>';
                break;
            case 134:
                htmlTitvarime = '<p>Xổ số Quảng Bình</p>';
                break;
            case 135:
                htmlTitvarime = '<p>Xổ số Gia Lai</p>';
                break;
            case 136:
                htmlTitvarime = '<p>Xổ số Ninh Thuận</p>';
                break;
            case 137:
                htmlTitvarime = '<p>Xổ số Quảng Ngãi</p>';
                break;
            case 138:
                htmlTitvarime = '<p>Xổ số Đắc Nông</p>';
                break;
            case 139:
                htmlTitvarime = '<p>Xổ số Kon Tum</p>';
                break;
            case 200:
                htmlTitvarime = '<p>Xổ số Miền bắc</p>';
                break;
            case 201:
                htmlTitvarime = '<p>Đặc biệt 18h25</p>';
                break;
            default:
                showErrorAlert("Lottery ID không hợp lệ", 1500);
                setTimeout(function() {
                    location.href = location.origin + '/main.shtml?xoso=50';
                }, 1600);
                break;
        }

        lotteryID = getID;

        $("input[name='lotteryid']").val(lotteryID);

        $(data_id.id_timeTitle).html(htmlTitvarime);

        document.title = $(htmlTitvarime).text();
    }

    function coutdownTime() {
        if ( typeof(curissue) === 'undefined' || curissue === '' ) return;

        var txtIssue = curissue.issue;

        $(data_id.id_openIssue).html(convertDateFormat(lotteryID, txtIssue));

        objIssueStart = [
            {
                value: txtIssue,
                text: '45 giây'
            }
        ];

        // update value name="lt_issue_start"
        $(data_id.id_buyContinuously).find('input[name="lt_issue_start"]').val(txtIssue);

        $(data_id.id_timeCountContentBet).removeClass('hide');

        timerCountDown(servertime, curissue.endtime, $(data_class.cl_timeCountBet), countTimeWait);
    }

    function countTimeWait() {
        $(data_id.id_timeWait).addClass('show__timewait');

        timerCountDown(curissue.endtime, curissue.opentime, $(data_id.id_timeWait), function() {
            $(data_id.id_timeWait).removeClass('show__timewait');
            getHistoryBet( lotteryID );
            getOtherLottery( lotteryID );
        });

        setTimeout(function() {
            getDataByID( lotteryID );
        }, 1000);
    }

    /**
     * get history bet
     * @param id = lotteryID
     */
    function getHistoryBet( id ) {
        $('#history__detail .grid__tbl').loading();
        $.ajax({
            url: urlMobileService,
            dataType: 'json',
            type: 'POST',
            async: false,
            data: {id: id,flag: 'betrecord'}
        }).done(function(data) {
            // just keep loading a little milisecond
            setTimeout(function() {
                $('#history__detail .grid__tbl').loading({
                    remove: true
                });
            }, 500);

            try {
                var responseList = data.reslist,
                    $historyBet = $('#history__bet--record'),
                    html = '',
                    arrLotteryIDTrim = [50, 100, 101, 102, 103, 104];

                $historyBet.find('.tbl__empty').addClass('hide');
                if ( responseList.length === 0 ) {
                    $historyBet.find('.tbl__row:not(.tbl__empty)').remove();
                    $historyBet.find('.tbl__empty').removeClass('hide');
                    return;
                }

                $historyBet.find('.tbl__row:not(.tbl__empty)').remove();

                for ( var i = 0, len = responseList.length; i < len; i++ ) {
                    // replace & = ,
                    var getStrCodesFormat = '',
                        getLotteryID = Number(responseList[i].lotteryid);

                    if ( responseList[i].methodid === 3 || responseList[i].methodid === 4 || responseList[i].methodid === 5 ) {
                        responseList[i].codes = responseList[i].codes.replace(/;/g, ' - ');
                    }
                    getStrCodesFormat = cutStrDot( responseList[i].codes, responseList[i].bettype );

                    if ( responseList[i].issue.search('-') > -1 && ($.inArray( getLotteryID, arrLotteryIDTrim ) > -1) ) {
                        responseList[i].issue = responseList[i].issue.split('-')[1];
                    }

                    var color_class = '';
                    switch(responseList[i].status) {
                        case 0:
                            color_class = 'color-waitopenbonus';
                            break;
                        case 1:
                            color_class = 'color-waitopenbonus';
                            break;
                        case 2:
                            color_class = 'color-waitcalcubonus';
                            break;
                        case 3:
                            color_class = 'color-truot';
                            break;
                        case 4:
                            color_class = 'color-trung';
                            break;
                        case 5:
                            color_class = 'color-huy';
                            break;
                        case 6:
                            color_class = 'color-huy';
                            break;
                        case 7:
                            color_class = 'color-huy';
                            break;
                        default:
                            break;
                    }

                    html += "<div class='tbl__row' onclick=\"getDetailBet('" + responseList[i].id + "')\">" +
                        '<div class="tbl__col w-9">'+ responseList[i].issue +'</div>' +
                        '<div class="tbl__col w-10">'+ responseList[i].lotteryname +'</div>' +
                        '<div class="tbl__col w-10">'+ responseList[i].methodname +'</div>' +
                        '<div class="tbl__col w-14">'+ convertDateFormat(id, responseList[i].bettime, 1) +'</div>' +
                        '<div class="tbl__col w-14">'+ getStrCodesFormat +'</div>' +
                        '<div class="tbl__col w-8">'+ responseList[i].betnumber +'</div>' +
                        '<div class="tbl__col w-12">'+ responseList[i].multiple +'</div>' +
                        '<div class="tbl__col w-12">'+ numberWithCommas(responseList[i].amount) +'</div>' +
                        '<div class="tbl__col w-11 ' + color_class + '">'+ responseList[i].statusname +'</div>' + '</div>';
                    $historyBet.append($(html));
                    html = '';
                }
            }catch ( error ) {
                debugger;
                showErrorAlert("Có lỗi xảy ra.<br>"+ error +"", 4000, function() {
                    window.location.href = '/home.shtml';
                });
                return;
            }
        }).error(function(jqXHR, status, error) {
            console.log(error);
        });
    }

    function getDataByID( id ) {
        $.ajax({
            url: urlMobileService,
            dataType: 'json',
            type: 'POST',
            async: false,
            data: {
                lotteryid: id,
                flag: 'initlottery'
            },
        }).done(function(data) {
            try {
                // data prize
                pri_user_data = data.userprize;

                if ( typeof(data.curissue) !== 'undefined' && typeof(data.servertime) !== 'undefined' ) {
                    // data current issue
                    curissue = data.curissue;
                    //curissue.issue = convertDateFormat(id, curissue.issue); //dd-mm-yyyy

                    // data server time
                    servertime = data.servertime;

                    // history historyissue
                    //historyissue = convertDateFormat(id, data.cursitem.historyissue);
                    historyissue = data.cursitem.historyissue;

                    $(data_id.id_issueResult).html(convertDateFormat(id, data.cursitem.historyissue));
                }

                coutdownTime();
            }catch( error ) {
                debugger;
                showErrorAlert("Có lỗi xảy ra.<br>"+ error +"", 4000, function() {
                    window.location.href = '/home.shtml';
                });
                return;
            }
        }).error(function(jqXHR, status, error) {
            console.log(error);
        });
    }

    function checkLogin(msg) {
        var flag = true;
        if ( msg === "请重新登录" ) {
            window.location.href = '/home.shtml';
            flag = false;
        }
        return flag;
    }

    $(data_id.id_reloadHistory).on('click', function(e){
        e.preventDefault();
        getHistoryBet(lotteryID);
    });

    objTemp.getHistoryByID = getHistoryBet;
    objTemp.lotteryID = lotteryID;

    function checkMB_DeDacBiet_3CangDacBiet(funCallBack) {
        $.ajax({
            url: urlMobileService,
            dataType: 'json',
            type: 'POST',
            async: false,
            data: {id: lotteryID, issue: curissue['issue'], flag: 'betRecordAllDay'}
        }).done(function(data) {
            try {
                var arrHis = data['reslist'];
                var arrCode = arrHis.filter(function(item) {
                    if ((item['lotteryid'] === 200 || item['lotteryid'] === 201) && (item['methodid'] === 6 || item['methodid'] === 21 || item['methodid'] === 1000)) {
                        return item;
                    }
                });
                funCallBack && funCallBack(arrCode);
            }catch ( error ) {
                showErrorAlert("Có lỗi xảy ra.<br>"+ error +"", 4000, function() {
                    window.location.href = '/home.shtml';
                });
            }
        }).error(function(jqXHR, status, error) {
            console.log(error);
        });
    }

    function allPossibleCases(arr) {
        if (arr.length == 1) {
            return arr[0];
        } else {
            var result = [];
            var allCasesOfRest = allPossibleCases(arr.slice(1));  // recur with the rest of array
            for (var i = 0; i < allCasesOfRest.length; i++) {
                for (var j = 0; j < arr[0].length; j++) {
                    result.push(arr[0][j] + allCasesOfRest[i]);
                }
            }
            return result;
        }
    }

    function getDualNumber(arrNumber) {
        var amountCheck = [];
        arrNumber.forEach(function(number) {
            var strNum = number.toString().trim();
            var numberTwoDigits = strNum.substring(strNum.length-2);
            var splitNum = numberTwoDigits.split('');
            var getFirstNum = splitNum[0];
            var countCheck = 0;
            for (var i = 1; i < splitNum.length; i++) {
                if (getFirstNum === splitNum[i]) {
                    countCheck += 1;
                }
            }

            if (countCheck+1 === splitNum.length) {
                amountCheck.push(number);
            }
        });
        return amountCheck;
    }
})(jQuery);