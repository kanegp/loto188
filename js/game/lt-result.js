var DataLottery;
function getMmcWNum() {
    $('.tbl__result--inner').loading({
        zIndexLoading: 999
    });
    $.ajax({
        url: '/LotteryService.aspx',
        dataType: 'json',
        type: 'POST',
        async: false,
        data: {
            flag: 'mmcwnum'
        },
    }).done(function(data) {
        setTimeout(function() {
            $('.tbl__result--inner').loading({
                remove: true
            });
        }, 300);

        try {
            var resListData = data.reslist;
            
            DataLottery = data.reslist;

            if (resListData.length != 0) {
                showDetailLotteryRes(0, 50)
            }
        } catch (error) {
            showErrorAlert("Có lỗi xảy ra.<br>" + error + "", 4000, function() {
                window.location.href = '/home.shtml';
            });
            return;
        }
    });
}

function getOtherLottery(id) {
    var id = id || 1;
    $('.tbl__result--inner').loading({
        zIndexLoading: 999
    });
    $.ajax({
        url: '/UserService.aspx',
        dataType: 'json',
        type: 'POST',
        async: false,
        data: {
            flag: 'UIWinOpenNumberBean',
            id: id,
            num: 50,
        }
    }).done(function(data) {
        setTimeout(function() {
            $('.tbl__result--inner').loading({
                remove: true
            });
        }, 300);

        try {
            var resListData = data.reslist;
            DataLottery = data.reslist;

            /*DataLottery = [JSON.parse("{\"dacbiet\":\"02867\",\"nhat\":\"96545\",\"nhi\":\"34696 - 58670\",\"ba\":\"96019 - 92635 - 81797 - 49057 - 81140 - 60633\",\"tu\":\"1182 - 0483 - 1342 - 5126\",\"nam\":\"3926 - 2100 - 2630 - 4813 - 3914 - 5214\",\"sau\":\"777 - 921 - 454\",\"bay\":\"71 - 54 - 39 - 84\",\"tam\":\"\",\"winnumber\":\"02867,96545,34696,58670,96019,92635,81797,49057,81140,60633,1182,0483,1342,5126,3926,2100,2630,4813,3914,5214,777,921,454,71,54,39,84,\",\"namelottery\":\"Miền Bắc\",\"timebet\":\"2019-04-14\"}")];
            showDetailLotteryRes(0, id);*/

            if (resListData.length !== 0) {
                showDetailLotteryRes(0, id)
            }
        } catch (error) {
            showErrorAlert("Có lỗi xảy ra.<br>" + error + "", 4000, function() {
                window.location.href = '/home.shtml';
            });
            return;
        }
    });
}

function showDetailLotteryRes(index, lotteryId) {

    var displayNameLottery = "";

    if (lotteryId >= 100 && lotteryId <= 104) {
        displayNameLottery = DataLottery[0].namelottery;
    }else if( lotteryId == 50 ){
    	displayNameLottery = "Siêu Tốc 1 Giây "
    } else {
    	displayNameLottery = "Xổ Số " + DataLottery[0].namelottery;
    }

    $("#issueResult").html(DataLottery[index].timebet);
    
    displayTableLottery(DataLottery[index],lotteryId);
    
    var elem = "<ul>";
    var activeItem = '';
    
    for (var i = 0; i < DataLottery.length; i++) {
        if (i == index) {
            activeItem = 'class="active"';
        } else {
            activeItem = '';
        }
        elem += '<li><a href="javascript:;" ' + activeItem + 'onclick="showDetailLotteryRes(' + i + ',' + lotteryId + ')">' + displayNameLottery + ' - ' + (convertDateFormat(lotteryId, DataLottery[i].timebet)) + '<span class="icon-check"></span></a></li>';
    }

    elem += "</ul>"
    
    $("#lotteryIssue ul").remove();
    
    $("#lotteryIssue").append(elem);

    if ( typeof($.lt_method_data) !== 'undefined' ) {
        separateNum( $.lt_method_data.methodid );
    }
}

function displayTableLottery(dataLottery, lotteryId) {
    if (dataLottery != null) {
        var htmlDacBiet = '', htmlNhat = '', htmlNhi = '', htmlBa = '', htmlTu = '', htmlNam = '', htmlSau = '', htmlBay = '', htmlTam = '',
            arrDB = dataLottery.dacbiet.split('-'),
            arrNhat = dataLottery.nhat.split('-'),
            arrNhi = dataLottery.nhi.split('-'),
            arrBa = dataLottery.ba.split('-'),
            arrTu = dataLottery.tu.split('-'),
            arrNam = dataLottery.nam.split('-'),
            arrSau = dataLottery.sau.split('-'),
            arrBay = dataLottery.bay.split('-'),
            arrTam = dataLottery.tam.split('-');

        for ( var i = 0, len = arrDB.length; i < len; i++ ) {
            htmlDacBiet += '<div data-row="dacbiet" data-numwin="'+arrDB[i]+'">'+arrDB[i]+'</div>';
            if ( i+1 < len ) {
                htmlDacBiet += '<span class="line"></span>';
            }
        }
        for ( var i = 0, len = arrNhat.length; i < len; i++ ) {
            htmlNhat += '<div data-row="giainhat" data-numwin="'+arrNhat[i]+'">'+arrNhat[i]+'</div>';
            if ( i+1 < len ) {
                htmlNhat += '<span class="line"></span>';
            }
        }
        for ( var i = 0, len = arrNhi.length; i < len; i++ ) {
            htmlNhi += '<div data-row="giainhi" data-numwin="'+arrNhi[i]+'">'+arrNhi[i]+'</div>';
            if ( i+1 < len ) {
                htmlNhi += '<span class="line"></span>';
            }
        }
        for ( var i = 0, len = arrBa.length; i < len; i++ ) {
            htmlBa += '<div data-row="giaiba" data-numwin="'+arrBa[i]+'">'+arrBa[i]+'</div>';
            if ( i+1 < len ) {
                htmlBa += '<span class="line"></span>';
            }
        }
        for ( var i = 0, len = arrTu.length; i < len; i++ ) {
            htmlTu += '<div data-row="giaitu" data-numwin="'+arrTu[i]+'">'+arrTu[i]+'</div>';
            if ( i+1 < len ) {
                htmlTu += '<span class="line"></span>';
            }
        }
        for ( var i = 0, len = arrNam.length; i < len; i++ ) {
            htmlNam += '<div data-row="giainam" data-numwin="'+arrNam[i]+'">'+arrNam[i]+'</div>';
            if ( i+1 < len ) {
                htmlNam += '<span class="line"></span>';
            }
        }
        for ( var i = 0, len = arrSau.length; i < len; i++ ) {
            htmlSau += '<div data-row="giaisau" data-numwin="'+arrSau[i]+'">'+arrSau[i]+'</div>';
            if ( i+1 < len ) {
                htmlSau += '<span class="line"></span>';
            }
        }
        for ( var i = 0, len = arrBay.length; i < len; i++ ) {
            htmlBay += '<div data-row="giaibay" data-numwin="'+arrBay[i]+'">'+arrBay[i]+'</div>';
            if ( i+1 < len ) {
                htmlBay += '<span class="line"></span>';
            }
        }
        for ( var i = 0, len = arrTam.length; i < len; i++ ) {
            htmlTam += '<div data-row="giaitam" data-numwin="'+arrTam[i]+'">'+arrTam[i]+'</div>';
            if ( i+1 < len ) {
                htmlTam += '<span class="line"></span>';
            }
        }
        var table = '<tr><td class="w-15">Giải ĐB</td><td>' + htmlDacBiet + '</td></tr>' +
                    '<tr><td>Giải nhất</td><td>' + htmlNhat + '</td></tr>' +
                    '<tr><td>Giải nhì</td><td>' + htmlNhi + '</td></tr>' +
                    '<tr><td>Giải ba</td><td>' + htmlBa + '</td></tr>' +
                    '<tr><td>Giải tư</td><td>' + htmlTu + '</td></tr>' +
                    '<tr><td>Giải năm</td><td>' + htmlNam + '</td></tr>' +
                    '<tr><td>Giải sáu</td><td>' + htmlSau + '</td></tr>' +
                    '<tr><td>Giải bảy</td><td>' + htmlBay + '</td></tr>';
        			if(lotteryId!=200 && lotteryId!=201){
        				table += '<tr><td>Giải tám</td><td>' + htmlTam + '</td></tr>';
        			}

        $("#lotteryResult tr:not(.result__title)").remove();

        $("#lotteryResult").append(table);

        var resfl = dataLottery.winnumber;

        displayFirstLastNum(resfl, 'firstLast');

    }
    return false;
}

function displayFirstLastNum(resfl, idElement) {
    var regExp = /..(?=,)/g;
    var matches = resfl.match(regExp);
    for (var i = 0; i < 11; i++) {
        $('#' + idElement + ' td:eq(' + (i * 2 + 3) + ')').text('');
    }
    if (matches != null) {
        for (var i = 0; i < matches.length; i++) {
            var first = matches[i].charAt(0);
            var last = matches[i].charAt(1);
            var pointInex = parseInt(first) * 2 + 3;
            var contentOld = $('#' + idElement + ' td:eq(' + pointInex + ')').text();

            if (contentOld == "") {
                $('#' + idElement + ' td:eq(' + pointInex + ')').text(last);
            } else {
                $('#' + idElement + ' td:eq(' + pointInex + ')').text(contentOld + "," + last);
            }
        }
        
        $('#'+idElement).find('tr:not(.result__title)').each(function() {
            var _this = $(this),
                txtFirst = _this.find('.first__num').text(),
                arrLast = _this.find('.last__num').text() !== '' ? _this.find('.last__num').text().split(',') : [],
                arrNum = [];
            
            if ( arrLast.length ) {
                for ( var i = 0, len = arrLast.length; i < len; i++ ) {
                    var mergeNum = txtFirst + arrLast[i];
                    arrNum.push( mergeNum );
                }
                //_this.attr('data-win', arrNum.join(','));
                _this.data("win", arrNum.join(','));
            }
        });
        hoverResult();
    }
}
function convertDateFormat(id, dateStr, type) {
    if (!id || dateStr == '') {
        return;
    }
    var arrLotteryIDTrim = [100, 101, 102, 103, 104],
        result = '';
    if (id == 50 && type == undefined) {
    	result = dateStr;
    } else if ($.inArray(id, arrLotteryIDTrim) > -1 && type == undefined) {
        result = dateStr.substr(6,2) + dateStr.substr(4,2) + dateStr.substr(0,4) + dateStr.substr(8,dateStr.length);
    } else {
        result = dateStr.substr(8,2) + dateStr.substr(4,4) + dateStr.substr(0,4) + dateStr.substr(10,dateStr.length);;
    }
    return result;
}