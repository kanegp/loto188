(function($){
    var post = function( url, params ) {
        return new Promise(function( resolve, reject ) {
            var xhr = new window.XMLHttpRequest();
            xhr.open('POST', url, false);
            xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
            xhr.onreadystatechange = function() {
                if ( xhr.readyState === 4 ) {
                    if ( xhr.status === 200 ) {
                        resolve( JSON.parse( xhr.responseText ) );
                    }else {
                        reject( xhr );
                    }
                }
            };

            xhr.onerror = function() {
                reject("Network Error !!!");
            };

            xhr.send( params );
        });
    };

    var set = function() {

    };

    // model
    var model = {
        urlLotteryService: "/LotteryService.aspx",
        urlMobileService: "/MobileService.aspx",
        urlUserService: "/UserService.aspx",

        getHistoryByID: function( lotteryID ) { // default lottery ID 50 of xo so 1 giay
            var lotteryID = lotteryID || 50;
            return post( this.urlMobileService, 'id='+lotteryID+'&flag=betrecord' );
        },

        getDataByID: function( lotteryID ) { // default lottery ID 50 of xo so 1 giay
            var lotteryID = lotteryID || 50;
            return post( this.urlMobileService, 'lotteryid='+lotteryID+'&flag=initlottery' );
        },

        getMMCRecord: function() {
            return post( this.urlLotteryService, 'flag=mmcwnum' );
        },

        getRecord: function( lotteryID ) {
            return post( this.urlUserService, 'id='+lotteryID+'&flag=UIWinOpenNumberBean&num=10' );
        }
    };

    // controller
    var controller = {

        init: function() {
            model.getDataByID()
                .then(function( response ) {
                    if ( response.msg !== '' ) {
                        showErrorAlert("Không lấy được dữ liệu khởi tạo.\nMessage: "+ response.msg +"", 3000);
                        return;
                    }
                    view.listUserPrize = response.userprize;
                    return model.getHistoryByID();
                })
                .then(function( response ) {
                    if ( response.msg !== '' ) {
                        showErrorAlert("Không lấy được dữ liệu lịch sử cược.\nMessage: "+ response.msg +"", 3000);
                        return;
                    }
                    view.listHistoryBet = response;
                    view.init();
                })
                .catch(function( error ) {
                    console.log(error);
                });
        }
    };

    // view
    var view = {
        listUserPrize: '',
        listHistoryBet: '',
        data_label: face,

        renderHistoryBet: function() {

        },

        isMMCGame: function() {

        },

        init: function() {
            this.renderHistoryBet();
            console.log(this.data_label);
        }
    };

    controller.init();
})(jQuery);