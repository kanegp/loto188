(function($){

	// model
	var model = {
		urlLotteryService: "/LotteryService.aspx",
        urlMobileService: "/MobileService.aspx",
        $gridTbl: $('#trace .grid__tbl'),

		getDataIssueList: function() {
			var _this = this;
			this.$gridTbl.loading({ zIndexLoading: 99 });

			$.ajax({
	            url: this.urlMobileService,
	            dataType: 'json',
	            type: 'POST',
                async: false,
	            data: {
	                lotteryid: octopus.lotteryid,
	                flag: 'searchIssueList'
	            },
	        }).done(function(data) {
	        	setTimeout(function(){ _this.$gridTbl.loading({ remove: true }); }, 1500);
	            try {
	                octopus.getIssueModel(data);
	            }catch ( error ) {
	                showErrorAlert("Có lỗi xảy ra.<br>"+ error +"", 4000);
	                return;
	            }
	        }).error(function(jqXHR, status, error) {
	            console.log(error);
	        });
		},

		getDataInit: function() {
			var __this = this;
			$.ajax({
	            url: this.urlMobileService,
	            dataType: 'json',
	            type: 'POST',
                async: false,
	            data: {
	                lotteryid: octopus.lotteryid,
	                flag: 'initlottery'
	            }
	        }).done(function(data) {
	            try {
	                if ( typeof(data.curissue) !== 'undefined' && typeof(data.servertime) !== 'undefined' ) {
	                    // data current issue
	                    octopus.openTime = data.curissue.opentime;
	                    octopus.endTime = data.curissue.endtime;
	                    // data server time
	                    octopus.serverTime = data.servertime;

	                    // get current issue
	                    octopus.lt_issue_start  = data.curissue.issue;

	                    view.countDown();

	                    __this.getDataIssueList();
					}
	            }catch( error ) {
	                showErrorAlert("Có lỗi xảy ra.<br>"+ error +"", 4000);
	                return;
	            }
	        }).error(function(jqXHR, status, error) {
	            console.log(error);
	        });
		},

		submitTrace: function( frm ) {
			parent.layer.load(2, {
	            shade: 0.3
	        });
			$.ajax({
	            type: "POST",
	            url: this.urlLotteryService,
	            timeout: 30000,
                async: false,
	            contentType: "application/x-www-form-urlencoded;charset=utf-8",
	            data: frm.serialize(),
	            success: function(data) {
	                parent.layer.closeAll();
	                try {
	                    var jsonParse = JSON.parse(data);
	                    if ( jsonParse.msg !== '' ) {
	                        showErrorAlert(jsonParse.msg, 2000);
	                    }else {
	                        showSuccessAlert('Đặt cược thành công.', 2000);
	                        localStorage.setItem('flag', true);
	                    }
	                }catch( error ) {
	                    showErrorAlert("Có lỗi xảy ra.<br>"+ error +"", 3000);
	                    return;
	                }
	            }
	        });
		},
	};

	// trace
	var octopus = {
		lotteryid: 0,
		flag: '',
		lotteryname: '',
		lt_sel_modes: 0,
		lt_sel_times: 0,
		lt_total_nums: 0,
		lt_total_money: 0,
		lt_issue_start: '',
		lt_project: [],
		lt_trace_times_margin: 1,
		lt_trace_margin: 50,
		lt_trace_times_same: 1,
		lt_trace_diff: 1,
		lt_trace_times_diff: 1,
		lt_trace_if: 'yes',
		lt_trace_count_input: 0, // count input checkbox is checked
		lt_trace_stop: 'yes', // yes: Ngung nuoi, no: bo ngung nuoi
		lt_trace_money: 0, // total money trace
		/*
		* array name and value trace issue value
		* ex:
			lt_trace_times[
				{
					name: lt_trace_times_20171130-043,
					value: 1
				}
			]
		*/
		lt_trace_times: [],
		/*
		* array trace issue name
		* ex:
			lt_trace_issues[]['20171130-043', '20171130-044',...]
		*/
		lt_trace_issues: [],
		objDataForm: {},

		dataIssue: '',
		openTime: '',
		endTime: '',
		serverTime: '',

		buildDataForm: function() {
			this.objDataForm.lotteryid = this.lotteryid;
			this.objDataForm.flag = this.flag;
			this.objDataForm.lt_sel_modes = Number(this.lt_sel_modes);
			this.objDataForm.lt_sel_times = Number(this.lt_sel_times);
			this.objDataForm.lt_total_nums = Number(this.lt_total_nums);
			this.objDataForm.lt_total_money = Number(this.lt_total_money);
			this.objDataForm.lt_issue_start = this.lt_issue_start;
			this.objDataForm.lt_project = this.lt_project;
			this.objDataForm.lt_trace_times_margin = this.lt_trace_times_margin;
			this.objDataForm.lt_trace_margin = this.lt_trace_margin;
			this.objDataForm.lt_trace_times_same = this.lt_trace_times_same;
			this.objDataForm.lt_trace_diff = this.lt_trace_diff;
			this.objDataForm.lt_trace_times_diff = this.lt_trace_times_diff;
			this.objDataForm.lt_trace_if = this.lt_trace_if;
			this.objDataForm.lt_trace_count_input = this.lt_trace_count_input;
			this.objDataForm.lt_trace_stop = this.lt_trace_stop;
			this.objDataForm.lt_trace_money = this.lt_trace_money;
			this.objDataForm.lt_trace_times = this.lt_trace_times;
			this.objDataForm.lt_trace_issues = this.lt_trace_issues;
		},

		setValObj: function() {
			var getTraceObj =  JSON.parse( localStorage.getItem('traceobj') );
			if ( getTraceObj === null ) return;
			for ( var i = 0, len = getTraceObj.length; i < len; i++ ) {
				if ( getTraceObj[i].name === 'lotteryid' ) {
					this.lotteryid = Number(getTraceObj[i].value);
				}else if ( getTraceObj[i].name === 'flag' ) {
					this.flag = getTraceObj[i].value;
				}else if ( getTraceObj[i].name === 'lotteryname' ) {
					this.lotteryname = getTraceObj[i].value;
				}else if ( getTraceObj[i].name === 'lt_sel_modes' ) {
					this.lt_sel_modes = getTraceObj[i].value;
				}else if ( getTraceObj[i].name === 'lt_sel_times' ) {
					this.lt_sel_times = getTraceObj[i].value;
				}else if ( getTraceObj[i].name === 'lt_total_nums' ) {
					this.lt_total_nums = getTraceObj[i].value;
				}else if ( getTraceObj[i].name === 'lt_total_money' ) {
					this.lt_total_money = getTraceObj[i].value;
				}else if ( getTraceObj[i].name === 'lt_issue_start' ) {
					this.lt_issue_start = getTraceObj[i].value;
				}else if ( getTraceObj[i].name === 'lt_project[]' ) {
					this.lt_project.push( JSON.parse( getTraceObj[i].value.replace(/'/g, '"') ) );
				}
			}
		},

		getIssueModel: function(data) {
			this.dataIssue = data;
			view.renderRecord();
		},

		init: function() {
			this.setValObj();
			model.getDataInit();
			this.buildDataForm();
		}
	};

	// view
	var view = {
		$tblRecordTrace: $('#record__trace'),
		$countTime: $('#countime'),
		$totalTurn: $('#totalTurn'),
		$totalBetMoney: $('#totalBetMoney'),
		totalTurn: 0,
		totalBetMoney: 0,
		$createPlanMutiple: $('.createPlanMutiple'),
		$filterDashboard: $('.filter__dashboard'),
		$toggleTrace: $('#toggle__trace'),
		$btnSaveTrace: $('#btnSaveTrace'),
		$frmTrace: $('#frmTrace'),
		$destiIssue: $('.desti__issue'),
		$mutipleXxx: $('.mutiple__xxx'),
		$traceMultiple: $('#traceMultiple'),

		toggleTrace: function() {
			this.$toggleTrace.on('click', function(e) {
				$(this).is(':checked') ? octopus.lt_trace_stop = 'yes' : octopus.lt_trace_stop = 'no';
				octopus.buildDataForm();
			});
		},

		renderRecord: function() {
			if ( octopus.dataIssue !== '' && octopus.dataIssue.issuelist.length > 0 ) {
				//reset
				this.totalTurn = 0;
				this.totalBetMoney = 0;

				this.$tblRecordTrace.find('.tbl__empty').addClass('hide');
				this.$tblRecordTrace.find('.tbl__row:not(.tbl__empty)').remove();

				var issueList = octopus.dataIssue.issuelist,
					html = '';

				for ( var i = 0, len = issueList.length; i < len; i++ ) {
					if ( i === 50 ) break;
					html += '<div class="tbl__row">' +
								'<div class="w-10 tbl__col">'+ (i+1) +'</div>' +
								'<div class="text-center w-10 tbl__col"><label class="r__c--emu r__c--emu13x13 r__c--single"><div class="r__c--ui"><input type="checkbox" /><span class="check"></span><span class="checked"></span></div></label></div>' +
								'<div class="text-center w-20 tbl__col issue__item">'+ issueList[i].issue +'</div>' +
								'<div class="text-center w-15 tbl__col"><div class="frm__item"><input type="text" class="w--50 h-25 text-center d-ib ver-c mgR-5" value="0" readonly="readonly" /><span class="d-ib ver-c fs-13">Lần</span></div></div>' +
								'<div class="text-center w-25 tbl__col"><span class="money__bet">0</span> VNĐ</div>' +
								'<div class="text-center w-20 tbl__col">'+ issueList[i].opentime +'</div>' +
							'</div>';
				}
				this.$tblRecordTrace.append( $(html) );
				//reset event checkbox and input multiple
				this.checkboxIssue();
				this.iputMutiple();
				//reset total num and total money
				this.updateTotal();
				//reset trace times issue
				octopus.lt_trace_times = [];
				octopus.lt_trace_issues = [];
			}else {
				this.$tblRecordTrace.find('.tbl__empty').removeClass('hide');
			}
		},

		countDown: function() {
			timerCountDown(octopus.serverTime, octopus.endTime, this.$countTime, function() {
				setTimeout(function() {
                    model.getDataInit();
				}, 1000);
			});
		},

		checkboxIssue: function() {
			var __this = this;
			this.$tblRecordTrace.find('.r__c--emu input[type=checkbox]').off().on('click', function(e) {
				var _this = $(this),
					$row = _this.closest('.tbl__row'),
					getTxtIssue = $row.find('.issue__item').text(),
					issueName = 'lt_trace_times_' + getTxtIssue;

				__this.$createPlanMutiple.removeClass('active');

				if ( _this.is(':checked') ) {
					__this.totalTurn += 1;
					$row.find('input').removeAttr('readonly').val(1);
					$row.find('.money__bet').html( numberWithCommas(octopus.lt_total_money) );
					// add trace time issue
					octopus.lt_trace_times.push({
						name: issueName,
						value: 1
					});

					// add trace issue
					octopus.lt_trace_issues.push(getTxtIssue);
				}else {
					__this.totalTurn -= 1;
					$row.find('input').attr('readonly', 'readonly').val(0);
					$row.find('.money__bet').html( 0 );
					// remove trace time issue
					for ( var i = 0, len = octopus.lt_trace_times.length; i < len; i++ ) {
						if ( octopus.lt_trace_times[i].name === issueName ) {
							octopus.lt_trace_times.splice(i, 1);
							octopus.lt_trace_issues.splice(i, 1);
							break;
						}
					}
				}

				__this.updateTotal();
			});
		},

		iputMutiple: function() {
			var __this = this;
			this.$tblRecordTrace.find('input[type=text]').off().on('keyup', function(e) {
				e.preventDefault();
				var _this = $(this),
					val = Number(_this.val()),
					$row = _this.closest('.tbl__row');

				val = __this.numMinimum(val);

				_this.val(val);

				$row.find('.money__bet').html( numberWithCommas( Number(octopus.lt_total_money)*val ) );
				
				__this.updateTotal();

				// update trace times issue
				var getTxtIssue = $row.find('.issue__item').text(),
					issueUpdate = 'lt_trace_times_' + getTxtIssue;
				for ( var i = 0, len = octopus.lt_trace_times.length; i < len; i++ ) {
					if ( octopus.lt_trace_times[i].name === issueUpdate ) {
						octopus.lt_trace_times[i].value = val;
						break;
					}
				}
			});
		},

		updateTotal: function() {
			var __this = this;
			this.$totalTurn.html( this.totalTurn );

			this.totalBetMoney = 0;
			this.$tblRecordTrace.find('.money__bet').each(function() {
				var numTxt = $(this).text().split(',').join('');
				__this.totalBetMoney += Number(numTxt);
			});
			this.$totalBetMoney.html( numberWithCommas( this.totalBetMoney ) );

			octopus.lt_trace_count_input = this.totalTurn;
			octopus.lt_trace_money = this.totalBetMoney;
		},

		createPlanMulti: function() {
			var __this = this;
			this.$createPlanMutiple.on('click', function(e) {
				e.preventDefault();
				var _this = $(this),
					$parent = _this.closest('.filter__dashboard'),
					valNumMuti = Number($parent.find('.num__mutiple').val()) || 1,
					valSlectNumTurn = Number($parent.find('.slect__numturn').val()) || 1,
					valDesti = Number($parent.find('.desti__issue').val()) || 1,
					valMultipleXXX = Number($parent.find('.mutiple__xxx').val()) || 1,
					valSet = 0;

				if ( _this.hasClass('active') ) return;

				_this.addClass('active');

				__this.$tblRecordTrace.find('input[type=checkbox]:checked').trigger('click');
				for ( var i = 0; i < (valSlectNumTurn*valDesti); i++ ) {
					if ( i % valDesti === 0 ) {
						valSet = valMultipleXXX*valNumMuti;
						__this.$tblRecordTrace.find('input[type=checkbox]').eq(i).trigger('click');
						__this.$tblRecordTrace.find('input[type=text]').eq(i).val( valSet ).trigger('keyup');
						valMultipleXXX = valMultipleXXX > 1 ? valMultipleXXX*2 : 1;
					}
				}
			});

			this.$filterDashboard.find('.num__mutiple, .slect__numturn').on('keyup change', function(e) {
				e.preventDefault();
				var _this = $(this),
					val = _this.val();
				val = __this.numMinimum(val);
				_this.val( val );
				if ( _this.hasClass('slect__numturn') ) {
					__this.$filterDashboard.find('.slect__numturn').val(val);
				}
				__this.$createPlanMutiple.removeClass('active');
			});
		},

		numMinimum: function( val ) {
			if ( val <= 1 || isNaN(val) ) {
				val = 1;
			}
			return val;
		},

		submitTrace: function() {
			var __this = this;
			this.$btnSaveTrace.on('click', function(e) {
				e.preventDefault();
				if (octopus.lt_trace_issues.length <= 0) {
                    showErrorAlert("Vui lòng chọn lượt nuôi!", 2000, function() {});
                    return;
				}

                __this.checkMB_DeDacBiet_3CangDacBiet(function(arrCode) {
                    var arrGroupDualNumOld = [];
                    var arrGroupDualNumLast = [];
                    arrCode.forEach(function(itemCode) {
                        var codes = itemCode['codes'];
                        var getMultiple = itemCode['multiple'];
                        var getIssue = itemCode['issue'];
                        var getDualNums = createGroupDualNum(codes);
                        getDualNums.forEach(function(dualNum) {
                            arrGroupDualNumOld.push({number: dualNum, multiple: getMultiple, issue: getIssue, methodID: itemCode['methodid']});
                        });
                    });
                    octopus.lt_project.forEach(function(itemProject) {
                        if (flagCheckDualNumLottery(octopus.lotteryid, itemProject['methodid'])) {
                            var getMultiple = itemProject['times'];
                            var codes = itemProject['codes'];
                            var getDualNums = createGroupDualNum(codes);
                            var getMethodID = itemProject['methodid'];
                            getDualNums.forEach(function(dualNum) {
                                arrGroupDualNumLast.push({number: dualNum, multiple: getMultiple, issue: octopus.lt_issue_start, methodID: getMethodID});
                            });
                        }
                    });

                    if (octopus.lt_trace_times.length > 0) {
                        var arrTempLast = [];
                        octopus.lt_trace_times.forEach(function(item) {
                            arrGroupDualNumLast.forEach(function(itemDualNum) {
                                arrTempLast.push({number: itemDualNum['number'], multiple: item['value']*itemDualNum['multiple'], issue: item['name'].split('lt_trace_times_')[1], methodID: itemDualNum['methodID']});
                            });
                        });
                        arrGroupDualNumLast = arrTempLast;
                    }

                    var groupMultiOld = groupMultipleDualNum(arrGroupDualNumOld);
                    var groupMultiLast = groupMultipleDualNum(arrGroupDualNumLast);
                    var arrTempMulti = groupMultipleDualNum(groupMultiOld.concat(groupMultiLast));

                    var getMultiDB = arrTempMulti.filter(function(itemMulti) {return itemMulti['methodID'] === 6 || itemMulti['methodID'] === 1000;});
                    var getMaxMultiDB = 0;
                    if (getMultiDB.length > 0) getMaxMultiDB = Math.max.apply(Math, getMultiDB.map(function(o) {return o.multiple;}));

                    var getMulti3Cang = arrTempMulti.filter(function(itemMulti) {return itemMulti['methodID'] === 21;});
                    var getMaxMulti3Cang = 0;
                    if (getMulti3Cang.length > 0) getMaxMulti3Cang = Math.max.apply(Math, getMulti3Cang.map(function(o) {return o.multiple;}));

                    var getMulti4CangDB = arrTempMulti.filter(function(itemMulti) {return itemMulti['methodID'] === 23;});
                    var getMaxMulti4CangDB = 0;
                    if (getMulti4CangDB.length > 0) getMaxMulti4CangDB = Math.max.apply(Math, getMulti4CangDB.map(function(o) {return o.multiple;}));

                    if (getMaxMultiDB > 2000 || getMaxMulti3Cang > 200 || getMaxMulti4CangDB > 20) { // 2000 là số tối đa cho đánh số kép bằng cho mỗi số
                        arrTempMulti.sort(function(a, b){ return (b['multiple']-a['multiple']);}); // sort z-a
                        var str = '<ul>';
                        arrTempMulti.forEach(function(item) {
                            var clsRed = '';
                            if ((item['multiple'] > 2000 && (item['methodID'] === 6 || item['methodID'] === 1000)) || (item['multiple'] > 200 && item['methodID'] === 21) || (item['multiple'] > 20 && item['methodID'] === 23)) {
                                clsRed = 'color-red invalid';
                            }
                            var pointFirst = 0;
                            var pointLast = 0;
                            var findExistOld = groupMultiOld.find(function(dualNumOld) { return (dualNumOld['number'] === item['number'] && dualNumOld['issue'] === item['issue']); });
                            var findExistLast = groupMultiLast.find(function(dualNumLast) { return (dualNumLast['number'] === item['number'] && dualNumLast['issue'] === item['issue']); });
                            if (findExistOld) {
                                pointFirst = findExistOld['multiple'];
                            }
                            if (findExistLast) {
                                pointLast = findExistLast['multiple'];
                            }
                            var pointRecent = 0;
                            var maxDualNum = 0;
                            if (item['methodID'] === 6 || item['methodID'] === 1000) {
                                pointRecent = 2000 - pointFirst;
                                maxDualNum = '2000';
                            } else if (item['methodID'] === 21) {
                                pointRecent = 200 - pointFirst;
                                maxDualNum = '200';
                            } else if (item['methodID'] === 23) {
                                pointRecent = 20 - pointFirst;
                                maxDualNum = '20';
                            }
                            str += "<li><div><p>Đã cược <strong>"+pointFirst+"/"+maxDualNum+"</strong> điểm</p><p>Cược thêm tối đa <strong>"+pointRecent+"</strong> điểm</p><p>Đang cược <span class='color-red'><strong>"+pointLast+"</strong></span> điểm</p></div><div class='"+clsRed+" numberBet'>["+item['number']+"]</div><div class='issue'>lượt xổ "+item['issue']+"</div></li>";
                        });
                        str += '</ul>';

                        parent.layer.alert('Bạn vượt quá số điểm <span class="color-red font-700">2000</span> điểm cho phép đánh <span class="font-700">mỗi số kép bằng</span> <i>(số kép bằng là những số có 2 chữ số cuối giống nhau VD: 00, 11, 22, 155, 066...). Chỉ áp dụng cho giải Đề Miền Bắc ĐB, Đề Miền Bắc ĐB 18h25.</i><br/>'+str, {
                            id: "DualNumber",
                            btn: ['OK'],
                            area: ['600px', '500px'],
                            icon: 2,
                            shade: 0.3,
                            yes: function(index) {
                                parent.layer.close(index);
                            }
                        });
                    } else {
                        var formData = new FormData(), html = '';
                        octopus.buildDataForm();
                        html += '<input type="hidden" name="lotteryid" value="'+ octopus.lotteryid +'" />' +
                            '<input type="hidden" name="flag" value="'+ octopus.flag +'" />' +
                            '<input type="hidden" name="lt_sel_times" value="'+ octopus.lt_sel_times +'" />' +
                            '<input type="hidden" name="lt_sel_modes" value="'+ octopus.lt_sel_modes +'" />' +
                            '<input type="hidden" name="lt_issue_start" value="'+ octopus.lt_issue_start +'" />' +
                            '<input type="hidden" name="lt_total_nums" value="'+ octopus.lt_total_nums +'" />' +
                            '<input type="hidden" name="lt_total_money" value="'+ octopus.lt_total_money +'" />' +
                            '<input type="hidden" name="lt_trace_times_margin" value="'+ octopus.lt_trace_times_margin +'" />' +
                            '<input type="hidden" name="lt_trace_margin" value="'+ octopus.lt_trace_margin +'" />' +
                            '<input type="hidden" name="lt_trace_times_same" value="'+ octopus.lt_trace_times_same +'" />' +
                            '<input type="hidden" name="lt_trace_diff" value="'+ octopus.lt_trace_diff +'" />' +
                            '<input type="hidden" name="lt_trace_times_diff" value="'+ octopus.lt_trace_times_diff +'" />' +
                            '<input type="hidden" name="lt_trace_if" value="'+ octopus.lt_trace_if +'" />' +
                            '<input type="hidden" name="lt_trace_count_input" value="'+ octopus.lt_trace_count_input +'" />' +
                            '<input type="hidden" name="lt_trace_stop" value="'+ octopus.lt_trace_stop +'" />' +
                            '<input type="hidden" name="lt_trace_money" value="'+ octopus.lt_trace_money +'" />';

                        for ( var i = 0, len = octopus.lt_trace_issues.length; i < len; i++ ) {
                            html += '<input type="hidden" name="lt_trace_issues[]" value="'+ octopus.lt_trace_issues[i] +'" />';
                        }
                        for ( var i = 0, len = octopus.lt_trace_times.length; i < len; i++ ) {
                            html += '<input type="hidden" name="'+ octopus.lt_trace_times[i].name +'" value="'+ octopus.lt_trace_times[i].value +'" />';
                        }
                        for ( var i = 0, len = octopus.lt_project.length; i < len; i++ ) {
                            html += '<input type="hidden" name="lt_project[]" value="'+ JSON.stringify(octopus.lt_project[i]).replace(/"/g, "'") +'" />';
                        }
                        var doc = new DOMParser().parseFromString(html, 'text/html'),
                            result = new XMLSerializer().serializeToString(doc);

                        __this.$frmTrace.append( $($(result).find('body').html()) );

                        model.submitTrace( __this.$frmTrace );
                    }
                });
			});
		},

        checkMB_DeDacBiet_3CangDacBiet: function(funCallBack) {
            var issueArr = [];
            octopus.lt_trace_issues.forEach(function(item) {
            	issueArr.push("'" + item + "'");
			});
            $.ajax({
                url: model.urlMobileService,
                dataType: 'json',
                type: 'POST',
                async: false,
                data: {id: octopus.lotteryid, issue: issueArr.join(','), flag: 'betRecordAllDay'}
            }).done(function(data) {
                try {
                    var arrHis = data['reslist'];
                    var arrCode = arrHis.filter(function(item) {
                        if (flagCheckDualNumLottery(item['lotteryid'], item['methodid'])) {
                            return item;
                        }
                    });
                    funCallBack && funCallBack(arrCode);
                }catch ( error ) {
                    showErrorAlert("Có lỗi xảy ra.<br>"+ error +"", 4000, function() {
                        //window.location.href = '/home.shtml';
                    });
                }
            }).error(function(jqXHR, status, error) {
                console.log(error);
            });
        },

		init: function() {
			this.createPlanMulti();
			this.toggleTrace();
			this.submitTrace();
			var __this = this;
            forInputNumberFloorType(this.$traceMultiple.find('.num__mutiple'), function(status, val) {
                /*if (!(status && val > 0)) { // unvalid
                    __this.$traceMultiple.find('.num__mutiple').val(1);
                }*/
            });
            forInputNumberFloorType(this.$traceMultiple.find('.slect__numturn'), function(status, val) {
                /*if (!(status && val > 0)) { // unvalid
                    __this.$traceMultiple.find('.slect__numturn').val(1);
                }*/
            });
		}
	};

    octopus.init();

	$(document).ready(function() {
		view.init();
	});

})(jQuery);