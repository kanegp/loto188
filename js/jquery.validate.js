function validate(object) {
	if (object.username !== undefined) {
		if (object.username === '') {
			showErrorAlert("Tên đăng nhập không được để trống");
			return false;
		} else if (object.username.length < 5) {
			showErrorAlert("Tên đăng nhập không được ít hơn 5 ký tự");
			return false;
		}
	}
	if (object.password !== undefined) {
		if (object.password === '') {
			showErrorAlert("Mật khẩu không được để trống");
			return false;
		} else if (object.password.length < 6) {
			showErrorAlert("Mật khẩu không được ít hơn 6 ký tự");
			return false;
		}
	}
	if (object.checkPassword !== undefined) {
		if (object.checkPassword === '') {
			showErrorAlert("Mật khẩu xác thanh toán không được để trống");
			return false;
		} else if (object.password === object.checkPassword ) {
			showErrorAlert("Mật khẩu đăng nhập và mật khẩu thanh toán không được trùng nhau");
			return false;
		}
	}
	if (object.newPassowrd1 !== undefined && object.newPassowrd2 !== undefined) {
		if (object.newPassowrd1 === '') {
			showErrorAlert("Mật khẩu không được để trống");
			return false;
		} else if (object.newPassowrd1 !== object.newPassowrd2) {
			showErrorAlert("Nhập lại mật khẩu không khớp");
			return false;
		}
	}
	if (object.email !== undefined) {
		if (object.email === '') {
			showErrorAlert("Hòm thư không được để trống");
			return false;
		} else if (!isEmail(object.email)) {
			showErrorAlert("Hòm thư không đúng định dạng");
			return false;
		}
	}
	if (object.anwser !== undefined) {
		if (object.anwser === '') {
			showErrorAlert("Câu trả lời không được để trống");
			return false;
		}
	}
	if (object.newAnwser !== undefined) {
		if (object.newAnwser === '') {
			showErrorAlert("Câu trả lời mới không được để trống");
			return false;
		}
	}
	if (object.nickname !== undefined) {
		if (object.nickname === '') {
			showErrorAlert("Biệt danh không được để trống");
			return false;
		}
	}
	if (object.phone !== undefined) {
		if (object.phone === '') {
			showErrorAlert("Số điện thoại không được để trống");
			return false;
		}else if (object.phone.trim().length <= 6) {
            showErrorAlert("Số điện thoại không hợp lệ");
            return false;
		}else if (!isPhoneNumber(object.phone)) {
			showErrorAlert("Số điện thoại không đúng định dạng");
			return false;
		}
	}
	return true;
}
function isPhoneNumber(phone) {
	 var phoneNo = /^[0-9]{3,20}$/;
	 return phone.match(phoneNo);  
}
function isEmail(email) {
	var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;0
	return regex.test(email);
}

function abc (obj) {
    alert('Hello ' + obj.first + ' ' + obj.last);
}
