var apiConfig = {
    USER_SERVICE: location.origin + "/UserService.aspx",
    LOTTERY_SERVICE: location.origin + "/LotteryService.aspx"
};
// only wait document body
$(document).ready(function () {
    var valCheckUserLogin = document.getElementById('flagLogin').value;
    toggleBtnTryPlay(valCheckUserLogin);
});

// wait load all [document, iframe, video....]
window.addEventListener ?
    window.addEventListener('load', function () {
        var valCheckUserLogin = document.getElementById('flagLogin').value;
        if (Number(valCheckUserLogin) || document.querySelectorAll('#transferMoney').length === 0) return;
        objBalanceAllGame.getAllBalace();
    }, false) :
    window.attachEvents && window.attachEvents('onload', function () { });

function toQueryPair(key, value) {
    if (typeof value === 'undefined') {
        return key;
    }
    return key + '=' + encodeURIComponent(value === null ? '' : String(value));
}

function toBodyString(obj) {
    var ret = [];
    for (var key in obj) {
        var values = obj[key];
        if (values && values.constructor === Array) {
            var queryValues = [];
            for (var i = 0, len = values.length, value; i < len; i++) {
                value = values[i];
                queryValues.push(toQueryPair(key + "[]", value));
            }
            ret = ret.concat(queryValues);
        } else {
            ret.push(toQueryPair(key, values));
        }
    }
    return ret.join('&');
}

function requestHTTP(data, url) {
    var data = data || {},
        urlAPI = url || '',
        promise;

    promise = new Promise(function (resolve, reject) {
        var bodyParam = toBodyString(data),
            xhr = new window.XMLHttpRequest();

        xhr.open('POST', urlAPI, true);
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4) {
                if (xhr.status === 200) {
                    resolve(JSON.parse(xhr.responseText));
                } else {
                    reject(xhr);
                }
            }
        };
        xhr.send(bodyParam);
    });
    return promise;
}

function transferToFormAccount() {
    var elFromAccount = document.getElementById('fromAccount');
    var elToAccount = document.getElementById('toAccount');
    var elTxtNote = document.querySelector('.noteTransfer');
    var elTypeGameBet = document.getElementById('typeGameBet');
    var elMinMoneySend = document.getElementById('minMoneySend');
    var elInputValMoney = document.getElementById('valTransferMoney');
    var txtMinMaxSport = "Ít nhất 100,000 VND, Nhiều nhất 100,000,000 VND";
    var txtMinMaxGame = "Ít nhất là 1,000 VND";

    elToAccount.addEventListener('change', function (e) {
        e.preventDefault();
        // var disableSport = '';
        var disableSport2 = '';
        var disableHaba = '';
        var disableCasino = '';
        var disableBanCa = '';
        var disableDaGa = '';
        objBalanceAllGame.dataCheckInvalid.forEach(function (item) {
            elToAccount.querySelector('[data-option="' + item + '"]').setAttribute('disabled', 'disabled');
            // if (item === 'thethao') disableSport = 'disabled';
            if (item === 'thethao2') disableSport2 = 'disabled';
            else if (item === 'haba') disableHaba = 'disabled';
            else if (item === 'ebet') disableCasino = 'disabled';
            else if (item === 'banca') disableBanCa = 'disabled';
            else if (item === 'daga') disableDaGa = 'disabled';
        });
        if (Number(e.target.value) === 1) { // Deposit to thirtparty game
            elFromAccount.innerHTML = '<option value="5" ' + disableSport2 + '>Thể Thao 2</option><option value="2" ' + disableHaba + '>Trò Chơi</option><option value="3" ' + disableCasino + '>Live Casino</option><option value="4" ' + disableBanCa + '>Bắn Cá</option><option value="6" ' + disableDaGa + '>Đá Gà</option>';
            elTypeGameBet.innerHTML = "Thể Thao";
            elMinMoneySend.innerHTML = "1,000";
            elTxtNote.style.display = 'block';
            elInputValMoney.setAttribute('placeholder', txtMinMaxSport);
        } else if (Number(e.target.value) === 0) {
            elFromAccount.innerHTML = '<option value="0">Tài khoản chính</option>';
            elTxtNote.style.display = 'none';
            elInputValMoney.setAttribute('placeholder', txtMinMaxSport);
        } else if (Number(e.target.value) === 6) { // IBC2
            elFromAccount.innerHTML = '<option value="0">Tài khoản chính</option>';
            elTxtNote.style.display = 'none';
            elInputValMoney.setAttribute('placeholder', txtMinMaxSport);
        } else if (Number(e.target.value) === 7) { // Đá Gà
            elFromAccount.innerHTML = '<option value="0">Tài khoản chính</option>';
            elTxtNote.style.display = 'none';
            elInputValMoney.setAttribute('placeholder', txtMinMaxGame);
        } else { // Withdraw from Sport
            elFromAccount.innerHTML = '<option value="0">Tài khoản chính</option>';
            elTxtNote.style.display = 'none';
            elInputValMoney.setAttribute('placeholder', txtMinMaxGame);
        }

        var getCurrentValueToFund = elFromAccount.value;
        if (getCurrentValueToFund) {
            if (Number(getCurrentValueToFund) === 2) { // Deposit to HABA
                elTxtNote.style.display = 'none';
                elInputValMoney.setAttribute('placeholder', txtMinMaxGame);
            }
            // else if (Number(getCurrentValueToFund) === 1) { // Deposit to Sport
            //     elTxtNote.style.display = 'block';
            //     elMinMoneySend.innerHTML = "1,000";
            //     elTypeGameBet.innerHTML = "Thể Thao";
            //     elInputValMoney.setAttribute('placeholder', txtMinMaxSport);
            // } 
            else if (Number(getCurrentValueToFund) === 5) { // Deposit to Sport 2
                elTxtNote.style.display = 'block';
                elMinMoneySend.innerHTML = "1,000";
                elTypeGameBet.innerHTML = "Thể Thao 2";
                elInputValMoney.setAttribute('placeholder', txtMinMaxSport);
            } else if (Number(getCurrentValueToFund) === 3) { // Deposit to Live Casino
                elTxtNote.style.display = 'block';
                elMinMoneySend.innerHTML = "1,000";
                elTypeGameBet.innerHTML = "Live Casino";
                elInputValMoney.setAttribute('placeholder', txtMinMaxGame);
            } else if (Number(getCurrentValueToFund) === 4) { // Deposit to BanCa
                elTxtNote.style.display = 'block';
                elMinMoneySend.innerHTML = "3,000";
                elTypeGameBet.innerHTML = "Bắn Cá";
                elInputValMoney.setAttribute('placeholder', txtMinMaxSport);
            } else if (Number(getCurrentValueToFund) === 6) { // Deposit to BanCa
                elTxtNote.style.display = 'block';
                elMinMoneySend.innerHTML = "1,000";
                elTypeGameBet.innerHTML = "Đá Gà";
                elInputValMoney.setAttribute('placeholder', txtMinMaxGame);
            }
        }
    });

    elFromAccount.addEventListener('change', function (e) {
        if (Number(e.target.value) === 2) { // Deposit to HABA
            elTxtNote.style.display = 'none';
            elInputValMoney.setAttribute('placeholder', txtMinMaxGame);
        }
        // else if (Number(e.target.value) === 1) { // Deposit to Sport
        //     elTxtNote.style.display = 'block';
        //     elMinMoneySend.innerHTML = "1,000";
        //     elTypeGameBet.innerHTML = "Thể Thao";
        //     elInputValMoney.setAttribute('placeholder', txtMinMaxSport);
        // }
        else if (Number(e.target.value) === 5) { // Deposit to Sport 2
            elTxtNote.style.display = 'block';
            elMinMoneySend.innerHTML = "1,000";
            elTypeGameBet.innerHTML = "Thể Thao 2";
            elInputValMoney.setAttribute('placeholder', txtMinMaxSport);
        } else if (Number(e.target.value) === 3) { // Deposit to Live Casino
            elTxtNote.style.display = 'block';
            elMinMoneySend.innerHTML = "1,000";
            elTypeGameBet.innerHTML = "Live Casino";
            elInputValMoney.setAttribute('placeholder', txtMinMaxGame);
        } else if (Number(e.target.value) === 4) { // Deposit to BanCa
            elTxtNote.style.display = 'block';
            elMinMoneySend.innerHTML = "3,000";
            elTypeGameBet.innerHTML = "Bắn Cá";
            elInputValMoney.setAttribute('placeholder', txtMinMaxSport);
        } else if (Number(e.target.value) === 6) { // Deposit to Đá Gà
            elTxtNote.style.display = 'block';
            elMinMoneySend.innerHTML = "1,000";
            elTypeGameBet.innerHTML = "Đá Gà";
            elInputValMoney.setAttribute('placeholder', txtMinMaxGame);
        }
    });

    // trigger event change
    var evObj = document.createEvent('Events');
    evObj.initEvent('change', true, false);
    document.getElementById('toAccount').dispatchEvent(evObj);

    elInputValMoney.addEventListener('keyup', function (e) {
        e.preventDefault();
        var valIput = e.target.value.replace(/[,]/g, '');
        e.target.value = numberWithCommas(Number(valIput));
    });

    document.getElementById('btnSubmitTransferMoney').removeEventListener('click', transferSubmit);
    document.getElementById('btnSubmitTransferMoney').addEventListener('click', transferSubmit);
}

function transferSubmit(e) {
    e.preventDefault();
    var elFromAccount = document.getElementById('fromAccount'),
        elToAccount = document.getElementById('toAccount'),
        valTransferMoney = document.getElementById('valTransferMoney').value;
    if (valTransferMoney === '' || /[^0-9,]/g.test(valTransferMoney)) {
        showErrorAlert('Số tiền không hợp lệ !!!', 2000);
        return;
    }

    if (e.target.classList.contains('activeSubmit')) return;
    e.target.classList.add('activeSubmit');

    var params = {
        direction: elFromAccount.value,
        amount: Number(valTransferMoney.replace(/[,]/g, '')) / 1000
    };

    // if ((elFromAccount.value === "1" && elToAccount.value === '1') || (elFromAccount.value === "0" && elToAccount.value === '0')) {
    //     params.flag = 'ibcFundTransfer';
    // }
    if ((elFromAccount.value === "5" && elToAccount.value === '1') || (elFromAccount.value === "0" && elToAccount.value === '6')) {
        params.flag = 'ibc2FundTransfer';
    } else if ((elFromAccount.value === "6" && elToAccount.value === '1') || (elFromAccount.value === "0" && elToAccount.value === '7')) {
        params.flag = 'dgFundTransfer';
    } else if (elFromAccount.value === '2' && elToAccount.value === '1') {
        params.flag = 'habaFundTransferDeposit';
    } else if (elFromAccount.value === '0' && elToAccount.value === '2') {
        params.flag = 'habaFundTransferWithdraw';
    } else if (elFromAccount.value === '3' && elToAccount.value === '1') {
        params.flag = 'ebetFundTransferDeposit';
    } else if (elToAccount.value === '3' && elFromAccount.value === '0') {
        params.flag = 'ebetFundTransferWithdraw';
    } else if (elFromAccount.value === '4' && elToAccount.value === '1') {
        params.flag = 'bancaFundTransferDeposit';
    } else if (elFromAccount.value === '0' && elToAccount.value === '4') {
        params.flag = 'bancaFundTransferWithdraw';
    }

    dotLoading(false);

    requestHTTP(params, apiConfig.USER_SERVICE).then(function (data) {
        if (data.hasOwnProperty('message') && data.message !== '') {
            dotLoading(true, data.message, 0);
        } else if (data.hasOwnProperty('msg') && data.msg !== '') {
            dotLoading(true, data.msg, 0);
        } else {
            $('#transferMoney').loading();
            objBalanceAllGame.getBalanceMain().then(function (data) {
                objBalanceAllGame.updateDataMain(data);
                // if (params.flag === 'ibcFundTransfer') objBalanceAllGame.getBalanceIBC().then(function (data) {
                //     $('#transferMoney').loading({ remove: true });
                //     objBalanceAllGame.updateDataIBC(data);
                //     calcTotalBalance(objBalanceAllGame.objData);
                // });
                if (params.flag === 'ibc2FundTransfer') objBalanceAllGame.getBalanceIBC2().then(function (data) {
                    $('#transferMoney').loading({ remove: true });
                    objBalanceAllGame.updateDataIBC2(data);
                    calcTotalBalance(objBalanceAllGame.objData);
                });
                else if (params.flag === 'dgFundTransfer') objBalanceAllGame.getBalanceDaga().then(function (data) {
                    $('#transferMoney').loading({ remove: true });
                    objBalanceAllGame.updateDataDaga(data);
                    calcTotalBalance(objBalanceAllGame.objData);
                });
                else if ((params.flag === 'habaFundTransferDeposit') || (params.flag === 'habaFundTransferWithdraw')) objBalanceAllGame.getBalanceHaba().then(function (data) {
                    $('#transferMoney').loading({ remove: true });
                    objBalanceAllGame.updateDataHaba(data);
                    calcTotalBalance(objBalanceAllGame.objData);
                });
                else if ((params.flag === 'ebetFundTransferDeposit') || (params.flag === 'ebetFundTransferWithdraw')) objBalanceAllGame.getBalanceLiveCasino().then(function (data) {
                    $('#transferMoney').loading({ remove: true });
                    objBalanceAllGame.updateDataLiveCasino(data);
                    calcTotalBalance(objBalanceAllGame.objData);
                });
                else if ((params.flag === 'bancaFundTransferDeposit') || (params.flag === 'bancaFundTransferWithdraw')) objBalanceAllGame.getBalanceBanCa().then(function (data) {
                    $('#transferMoney').loading({ remove: true });
                    objBalanceAllGame.updateDataBanCa(data);
                    calcTotalBalance(objBalanceAllGame.objData);
                });
                dotLoading(true, 'Giao dịch thành công', 1);
            });
        }
        document.getElementById('valTransferMoney').value = '';
        e.target.classList.remove('activeSubmit');
    });
}

/**
 * @param flag = boolan is loading and loaded
 * @param msg = string ''
 * @param typeError = 0: error, 1: success
 */
var timeoutNoti;

function dotLoading(flag, msg, typeError) {
    clearTimeout(timeoutNoti);
    var elDotLoading = document.getElementById('noteTransfer'),
        elDotItem,
        htmlDot = '.',
        countDot = 0;

    elDotLoading.innerHTML = 'Đang xác nhận<span class="dotLoading"></span>';
    elDotLoading.style.display = 'inline-block';

    elDotItem = document.querySelector('.dotLoading');

    var loadingDot = setInterval(function () {
        elDotItem.innerHTML = htmlDot;
        htmlDot += '.';
        countDot += 1;
        if (countDot === 4) {
            htmlDot = '.';
            countDot = 0;
        }

        if (flag) {
            typeError ? elDotLoading.classList.add("successTransfer") : elDotLoading.classList.add("errorTransfer");
            elDotLoading.innerHTML = msg;
            clearInterval(loadingDot);
            timeoutNoti = setTimeout(function () {
                elDotLoading.innerHTML = '';
                elDotLoading.classList.remove("successTransfer", "errorTransfer");
            }, 2500);
        }
    }, 150);
}

function toggleBtnTryPlay(valCheckUserLogin) {
    if (!Number(valCheckUserLogin)) {
        var getPathHref = location.pathname;
        var elBtn = document.querySelector('.btnTryPlay');
        if (elBtn) {
            getPathHref.search('/sport.shtml') > -1 ? elBtn.style.display = 'none' : elBtn.style.display = 'flex';
        }
    }
}

var objBalanceAllGame = {
    objData: {
        msgMain: '', balanceMain: 0,
        // msgIBC: '', balanceIBC: 0,
        msgIBC2: '', balanceIBC2: 0,
        msgHABA: '', balanceHABA: 0,
        msgEBET: '', balanceEBET: 0,
        msgBANCA: '', balanceBANCA: 0,
        msgDAGA: '', balanceDAGA: 0
    },
    dataCheckInvalid: [],
    getBalanceMain: function () {
        return requestHTTP({ flag: 'balance' }, apiConfig.LOTTERY_SERVICE);
    },
    updateDataMain: function (data) {
        this.objData.msgMain = data['msg'];
        this.objData.balanceMain = (Number(data['balance'])).toFixed(0);
        $('#ebalance').innerHTML = numberWithCommas(this.objData.balanceMain);
    },
    // getBalanceIBC: function () {
    //     return requestHTTP({ flag: 'ibcCheckUserBalance' }, apiConfig.USER_SERVICE);
    // },
    // updateDataIBC: function (data) {
    //     if (data['message'] === 'maintain') {
    //         this.objData.msgIBC = 'Đang bảo trì';
    //         this.dataCheckInvalid.push('thethao');
    //     } else if (data['message'] !== '') {
    //         this.objData.msgIBC = data['message'];
    //         this.dataCheckInvalid.push('thethao');
    //     } else {
    //         this.objData.balanceIBC = (Number(data['balance']) * 1000).toFixed(0);
    //     }
    // },
    getBalanceIBC2: function () {
        return requestHTTP({ flag: 'ibc2CheckUserBalance' }, apiConfig.USER_SERVICE);
    },
    updateDataIBC2: function (data) {
        if (data['message'] === 'maintain') {
            this.objData.msgIBC2 = 'Đang bảo trì';
            this.dataCheckInvalid.push('thethao2');
        } else if (data['message'] !== '') {
            this.objData.msgIBC2 = data['message'];
            this.dataCheckInvalid.push('thethao2');
        } else {
            this.objData.balanceIBC2 = (Number(data['balance']) * 1000).toFixed(0);
        }
    },

    getBalanceDaga: function () {
        return requestHTTP({ flag: 'dgCheckUserBalance' }, apiConfig.USER_SERVICE);
    },
    updateDataDaga: function (data) {
        if (data['message'] === 'maintain') {
            this.objData.msgDAGA = 'Đang bảo trì';
            this.dataCheckInvalid.push('daga');
        } else if (data['message'] !== '') {
            this.objData.msgDAGA = data['message'];
            this.dataCheckInvalid.push('daga');
        } else {
            this.objData.balanceDAGA = (Number(data['balance']) * 1000).toFixed(0);
        }
    },

    getBalanceHaba: function () {
        return requestHTTP({ flag: 'habaCheckUserBalance' }, apiConfig.USER_SERVICE);
    },
    updateDataHaba: function (data) {
        if (data['msg'] && data['msg'] !== "") {
            showErrorAlert('Vui lòng đăng nhập lại', 2000);
            return;
        }
        if (data['message'] !== '') {
            this.objData.msgHABA = data['message'];
            this.dataCheckInvalid.push('haba');
        } else {
            this.objData.balanceHABA = (Number(data['balance'])).toFixed(0);
        }
    },
    getBalanceLiveCasino: function () {
        return requestHTTP({ flag: 'ebetUserInfor' }, location.origin + "/UserService.aspx");
    },
    updateDataLiveCasino: function (data) {
        if (data['message'] !== '') {
            this.objData.msgEBET = data['message'];
            this.dataCheckInvalid.push('ebet');
        } else {
            this.objData.balanceEBET = (Number(data['balance']) * 1000).toFixed(0);
        }
    },
    getBalanceBanCa: function () {
        return requestHTTP({ flag: 'bancaCheckUserBalance' }, location.origin + "/UserService.aspx");
    },
    updateDataBanCa: function (data) {
        if (data['message'] !== '') {
            this.objData.msgBANCA = data['message'];
            this.dataCheckInvalid.push('banca');
        } else {
            this.objData.balanceBANCA = (Number(data['balance'])).toFixed(0);
        }
    },
    getAllBalace: function () {
        $('#transferMoney').loading();
        if (document.querySelectorAll('#transferMoney')) document.getElementById('transferMoneyInner').style.display = 'block';
        var seft = this;
        Promise.all([this.getBalanceMain(), this.getBalanceHaba(), this.getBalanceLiveCasino(), this.getBalanceBanCa(), this.getBalanceIBC2(), this.getBalanceDaga()]).then(function (values) {
            $('#transferMoney').loading({ remove: true });
            var objMain = values[0];
            seft.updateDataMain(objMain);
            var objHaba = values[1];
            seft.updateDataHaba(objHaba);
            var objLiveCasino = values[2];
            seft.updateDataLiveCasino(objLiveCasino);
            var objBanCa = values[3];
            seft.updateDataBanCa(objBanCa);
            var objIBC2 = values[4];
            seft.updateDataIBC2(objIBC2);
            var objDaGa = values[5];
            seft.updateDataDaga(objDaGa);

            calcTotalBalance(seft.objData);
            transferToFormAccount();
        }).catch(function (error) {
            console.log('error', error)
        });
    }
};

function calcTotalBalance(objData) {
    var elBalanceMain = document.getElementById('balanceMain'),
        // elBalanceSport = document.getElementById('balanceSport'),
        elBalanceSport2 = document.getElementById('balanceSport2'),
        elBalanceDaGa = document.getElementById('balanceDaGa'),
        elBalanceHaba = document.getElementById('balanceHaba'),
        elBalanceLiveCasino = document.getElementById('balanceLiveCasino'),
        elTotalMoneyBanca = document.getElementById('balanceBanCa'),
        elTotalMoneyHave = document.getElementById('totalMoneyHave'),
        elBalanceMainTop = document.getElementById('ebalance');
    //$("#ebalance").html( numberWithCommas(balance) );;

    objData.msgMain !== '' ? elBalanceMain.innerHTML = objData.msgMain : (elBalanceMain.innerHTML = numberWithCommas(objData.balanceMain), elBalanceMainTop.innerHTML = numberWithCommas(objData.balanceMain));

    // objData.msgIBC !== '' ? elBalanceSport.innerHTML = objData.msgIBC : elBalanceSport.innerHTML = numberWithCommas(objData.balanceIBC);

    objData.msgIBC2 !== '' ? elBalanceSport2.innerHTML = objData.msgIBC2 : elBalanceSport2.innerHTML = numberWithCommas(objData.balanceIBC2);

    objData.msgDAGA !== '' ? elBalanceDaGa.innerHTML = objData.msgDAGA : elBalanceDaGa.innerHTML = numberWithCommas(objData.balanceDAGA);

    objData.msgHABA !== '' ? elBalanceHaba.innerHTML = objData.msgHABA : elBalanceHaba.innerHTML = numberWithCommas(objData.balanceHABA);

    objData.msgEBET !== '' ? elBalanceLiveCasino.innerHTML = objData.msgEBET : elBalanceLiveCasino.innerHTML = numberWithCommas(objData.balanceEBET);

    objData.msgBANCA !== '' ? elTotalMoneyBanca.innerHTML = objData.msgBANCA : elTotalMoneyBanca.innerHTML = numberWithCommas(objData.balanceBANCA);

    elTotalMoneyHave.innerHTML = numberWithCommas(Number(objData.balanceMain) + Number(objData.balanceHABA) + Number(objData.balanceEBET) + Number(objData.balanceBANCA) + Number(objData.balanceIBC2) + Number(objData.balanceDAGA));
}
