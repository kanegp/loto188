!function () {
    $(function ($) {
        function numberWithCommas(e) {
            return e.toString()
                .replace(/\D/g, "")
                .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
        }

        function layeropen(e) {
            return layer.open({
                type: 1,
                title: !1,
                closeBtn: 0,
                skin: "layui-layer-nobg",
                area: e.area,
                shadeClose: e.shadeClose,
                content: e.content
            })
        }

        function fftime(e) {
            return Number(e) < 10 ? "0" + Number(e) : Number(e)
        }

        function format(e) {
            return new Date(e.replace(/[\-\u4e00-\u9fa5]/g, "/"))
        }

        function diff(e) {
            return 0 < e ? {
                day: Math.floor(e / 86400),
                hour: Math.floor(e % 86400 / 3600),
                minute: Math.floor(e % 3600 / 60),
                second: Math.floor(e % 60)
            } : {
                    day: 0,
                    hour: 0,
                    minute: 0,
                    second: 0
                }
        }

        function request(param, callback, dataType) {
            $.ajax({
                type: "POST",
                url: global_game_config.lt_ajaxurl,
                dataType: dataType ? "json" : "text",
                data: param,
                success: function (data) {
                    if (dataType && (data = JSON.stringify(data)),
                        data.length <= 0)
                        return alert("获取数据失败,请刷新页面"),
                            !1;
                    var partn = /<script.*>.*<\/script>/;
                    return partn.test(data) ? (alert("登陆超时，或者帐户在其他地方登陆，请重新登陆"),
                        top.location.href = "./",
                        !1) : "empty" == data ? (alert("未到销售时间"),
                            !1) : (eval("data=" + data),
                                void (callback && callback(data)))
                },
                error: function () {
                    console.log("error")
                }
            })
        }

        function winnumberHTML(e) {
            if (!e) return "";
            for (var t = "", i = 0, n = 0; n < e.length; n++) t += '<i class="mdice mdice-' + e[n] + '"></i>', i += Number(e[n]);
            return e.charAt(0) == e.charAt(1) && e.charAt(1) == e.charAt(2) ? t += '<i class="sign sign-baozi">-</i><i class="sign sign-baozi">-</i>' : (t += 10 < i ? '<i class="sign sign-big">T</i>' : '<i class="sign sign-small">X</i>', t += i % 2 == 1 ? '<i class="sign sign-odd">L</i>' : '<i class="sign sign-even">C</i>'), t
        }

        function updateBalance(e) {
            var t = $("#J-user-balance"),
                i = $("#J-money-tip"),
                n = t.text(),
                a = Number(n.replaceAll(",", "")),
                o = e - a;
            t.text(formatMoney(e, 3));
            if (formatMoney(o, 2) === '-0') return;
            a && Number(o.toFixed(2)) && (i.removeClass("money-tip-down"), 0 < o ? i.text("+ " + formatMoney(o, 2)) : (i.text("- " + formatMoney(Math.abs(o), 2)), i.addClass("money-tip-down")),
                i.css({
                    top: 0,
                    right: -1 * i.width() - 10
                }), i.stop()
                    .animate({
                        opacity: 1
                    }, function () {
                        i.stop()
                            .animate({
                                top: 50,
                                opacity: 0
                            }, 2e3)
                    }))
        }

        function renderBetHistory() {
            request("flag=getbetcodehistory&sid=" + global_game_config.gameid, function (e) {
                var t = e.data.reslist;
                if (updateBalance(balance = e.data.userbalance), t && t.length) {
                    for (var i = document.createDocumentFragment(), n = 0; n < t.length; n++) {
                        var a = t[n],
                            o = document.createElement("tr");
                        o.innerHTML = '<td class="date">' + a.issue.slice(9) + '</td><td class="state status_' + a.status + '">' + a.statusname + '</td><td class="betMoney">' + numberWithCommas(a.amount) + '</td><td class="getMoney">' + formatMoney(a.bonus) + '</td><td><div class="see" sid="' + a.id + '"></div></td>', i.appendChild(o)
                    }
                    game_record_tableDom.innerHTML = "", game_record_tableDom.appendChild(i)
                }
            })
        }

        function renderOpenHistory() {
            request("flag=getopencodehistory&sid=" + global_game_config.gameid + "&num=10", function (e) {
                for (var t = e.data, i = document.createDocumentFragment(), n = 0; n < t.length; n++) {
                    var a = t[n];
                    if (!a.winnumber) return;
                    var o = document.createElement("tr");
                    o.innerHTML = '<td class="dl_issue">' + a.issue + '</td><td class="dl_number">' + winnumberHTML(a.winnumber) + "</td>", i.appendChild(o)
                }
                dl_listDom.innerHTML = "", dl_listDom.appendChild(i)
            })
        }

        function getLastOpenCode() {
            drawLet(function (e) {
                var t = e.slice(0, 9),
                    i = Number(e.slice(9)) - 1;
                if (!i) return "";
                for (var n = String(i), a = 4 - n.length, o = 0; o < a; o++) n = "0" + n;
                return t + n
            }(curr_issue.issue))
        }

        function initScrollPanel() {
            $("#go_info_panel")
                .jScrollPane({
                    autoReinitialise: !0,
                    hideFocus: !0,
                    verticalGutter: 0
                }), $(".game_record_container")
                    .jScrollPane({
                        autoReinitialise: !0,
                        hideFocus: !0,
                        verticalGutter: 0
                    }), $("#game_record_info")
                        .jScrollPane({
                            autoReinitialise: !0,
                            hideFocus: !0,
                            verticalGutter: 0
                        }), $("#codesList")
                            .jScrollPane({
                                autoReinitialise: !0,
                                hideFocus: !0,
                                verticalGutter: 0
                            }), $("#betInfoList")
                                .jScrollPane({
                                    autoReinitialise: !0,
                                    hideFocus: !0,
                                    verticalGutter: 0
                                })
        }

        function initRequest(i) {
            request("flag=init", function (e) {
                if ("success" == e.status) {
                    e = e.data,
                        global_game_config.methods = e.k31fmethod,
                        global_game_config.issueliststr = e.issueliststr,
                        global_game_config.maxbetmoney = e.maxbetmoney,
                        curr_issue = e.curissue;
                    var t = e.lastopenissue;
                    lt_opentimer(t.endtime, t.opentime, t.issue), i && i()
                } else maxInitRequestTime && setTimeout(function () {
                    initRequest(i), maxInitRequestTime--
                }, 3e3)
            })
        }

        function lt_reset() {
            request("flag=getCurIssure&lotteryid=" + global_game_config.gameid, function (e) {
                "success" === e.status ? (e = e.data, curr_issue = e, $record_date.html(e.issue), lt_timerHtml(e.servertime, e.endtime)) : lt_reset()
            })
        }

        function stopOpenCodeAnimate() {
            $(".mdice_animate")
                .hide(), $(".drawCode")
                    .show(), animateInterval = null
        }

        function startOpenCodeAnimate() {
            animateInterval || ($(".drawCode")
                .hide(), $(".mdice_animate")
                    .show())
        }

        var isLocked = !1,
            balance = 0,
            betInfoIndex, lastIssue, gameNameConst = {
                hezhi: "Cược tổng",
                baozi: "Bộ 3 cụ thể",
                duizi: "Bộ đôi đồng nhất",
                xiao: "Xỉu",
                da: "Tài",
                dan: "Lẻ",
                shuang: "Chẵn",
                erma: "2 số",
                yima: "1 số",
                nnn: "（Bộ 3 bất kỳ）"
            };
        layeropen({
            area: ["366px", "286px"],
            shadeClose: !0,
            content: $("#J-tpl-modal-first-tip")
        }), $("[rel=modal_close]")
            .on("click", function () {
                layer.closeAll()
            }), $("[rel=modal_close_self]")
                .on("click", function () {
                    layer.close(betInfoIndex)
                });
        var global_game_config = {
            poscfg: [{
                name: "da",
                width: 151,
                height: 124
            }, {
                left: 756,
                name: "xiao",
                width: 151,
                height: 128
            }, {
                top: 130,
                name: "dan",
                width: 153,
                height: 75
            }, {
                left: 756,
                top: 130,
                name: "shuang",
                width: 153,
                height: 75
            }, {
                name: "duizi_66",
                left: 158,
                top: 24,
                width: 107,
                height: 59
            }, {
                name: "duizi_55",
                left: 158,
                top: 82,
                width: 107,
                height: 59
            }, {
                name: "duizi_44",
                left: 158,
                top: 141,
                width: 107,
                height: 59
            }, {
                name: "duizi_33",
                left: 645,
                top: 24,
                width: 107,
                height: 59
            }, {
                name: "duizi_22",
                left: 645,
                top: 82,
                width: 107,
                height: 59
            }, {
                name: "duizi_11",
                left: 645,
                top: 141,
                width: 107,
                height: 59
            }, {
                name: "baozi_666",
                left: 275,
                top: 34,
                width: 61,
                height: 61
            }, {
                name: "baozi_555",
                left: 334,
                top: 34,
                width: 61,
                height: 61
            }, {
                name: "baozi_444",
                left: 395,
                top: 34,
                width: 61,
                height: 61
            }, {
                name: "baozi_333",
                left: 454,
                top: 34,
                width: 61,
                height: 61
            }, {
                name: "baozi_222",
                left: 515,
                top: 34,
                width: 61,
                height: 61
            }, {
                name: "baozi_111",
                left: 575,
                top: 34,
                width: 61,
                height: 61
            }, {
                name: "baozi_nnn",
                left: 275,
                top: 100,
                width: 365,
                height: 100
            }, {
                name: "hezhi_17",
                left: 4,
                top: 208,
                width: 62,
                height: 70
            }, {
                name: "hezhi_16",
                left: 67,
                top: 208,
                width: 62,
                height: 70
            }, {
                name: "hezhi_15",
                left: 130,
                top: 208,
                width: 62,
                height: 70
            }, {
                name: "hezhi_14",
                left: 193,
                top: 208,
                width: 62,
                height: 70
            }, {
                name: "hezhi_13",
                left: 261,
                top: 208,
                width: 62,
                height: 70
            }, {
                name: "hezhi_12",
                left: 323,
                top: 208,
                width: 62,
                height: 70
            }, {
                name: "hezhi_11",
                left: 393,
                top: 208,
                width: 62,
                height: 70
            }, {
                name: "hezhi_10",
                left: 458,
                top: 208,
                width: 62,
                height: 70
            }, {
                name: "hezhi_9",
                left: 523,
                top: 208,
                width: 62,
                height: 70
            }, {
                name: "hezhi_8",
                left: 588,
                top: 208,
                width: 62,
                height: 70
            }, {
                name: "hezhi_7",
                left: 653,
                top: 208,
                width: 62,
                height: 70
            }, {
                name: "hezhi_6",
                left: 718,
                top: 208,
                width: 62,
                height: 70
            }, {
                name: "hezhi_5",
                left: 783,
                top: 208,
                width: 62,
                height: 70
            }, {
                name: "hezhi_4",
                left: 848,
                top: 208,
                width: 62,
                height: 70
            }, {
                name: "erma_56",
                left: 6,
                top: 309,
                width: 58,
                height: 104
            }, {
                name: "erma_46",
                left: 66,
                top: 309,
                width: 58,
                height: 104
            }, {
                name: "erma_45",
                left: 126,
                top: 309,
                width: 58,
                height: 104
            }, {
                name: "erma_36",
                left: 186,
                top: 309,
                width: 58,
                height: 104
            }, {
                name: "erma_35",
                left: 246,
                top: 309,
                width: 58,
                height: 104
            }, {
                name: "erma_34",
                left: 306,
                top: 309,
                width: 58,
                height: 104
            }, {
                name: "erma_26",
                left: 366,
                top: 309,
                width: 58,
                height: 104
            }, {
                name: "erma_25",
                left: 426,
                top: 309,
                width: 58,
                height: 104
            }, {
                name: "erma_24",
                left: 486,
                top: 309,
                width: 58,
                height: 104
            }, {
                name: "erma_23",
                left: 546,
                top: 309,
                width: 58,
                height: 104
            }, {
                name: "erma_16",
                left: 606,
                top: 309,
                width: 58,
                height: 104
            }, {
                name: "erma_15",
                left: 666,
                top: 309,
                width: 58,
                height: 104
            }, {
                name: "erma_14",
                left: 726,
                top: 309,
                width: 58,
                height: 104
            }, {
                name: "erma_13",
                left: 786,
                top: 309,
                width: 58,
                height: 104
            }, {
                name: "erma_12",
                left: 846,
                top: 309,
                width: 58,
                height: 104
            }, {
                name: "yima_6",
                left: 129,
                top: 431,
                width: 82,
                height: 82
            }, {
                name: "yima_5",
                left: 241,
                top: 431,
                width: 82,
                height: 82
            }, {
                name: "yima_4",
                left: 357,
                top: 431,
                width: 82,
                height: 82
            }, {
                name: "yima_3",
                left: 469,
                top: 431,
                width: 82,
                height: 82
            }, {
                name: "yima_2",
                left: 585,
                top: 431,
                width: 82,
                height: 82
            }, {
                name: "yima_1",
                left: 699,
                top: 431,
                width: 82,
                height: 82
            }],
            lt_ajaxurl: "/K31fGameService.aspx",
            gameid: 80
        },
            curr_issue = null,
            dl_listDom = document.querySelector(".dl_list"),
            game_record_tableDom = document.querySelector(".game_record_table"),
            $record_date = $(".record_date"),
            $record_time = $(".record_time"),
            $history_head = $(".game_head .history_head"),
            $mdice_history_body = $(".game_head .mdice_history_body"),
            $draw_lottery_head = $(".draw_lottery .draw_lottery_head"),
            $draw_lottery_body = $(".draw_lottery .draw_lottery_body"),
            $draw_lottery_closeBtn = $(".draw_lottery .draw_lottery_body .closeBtn"),
            $betDetailTable = $(".game_record_messageBox table"),
            loginnameDom = $betDetailTable.find("tr:nth-child(1) td:nth-child(2)"),
            statusDom = $betDetailTable.find("tr:nth-child(1) td:nth-child(4)"),
            sidDom = $betDetailTable.find("tr:nth-child(2) td:nth-child(2)"),
            betTimeDom = $betDetailTable.find("tr:nth-child(2) td:nth-child(4)"),
            gameNameDom = $betDetailTable.find("tr:nth-child(3) td:nth-child(2)"),
            issueDom = $betDetailTable.find("tr:nth-child(3) td:nth-child(4)"),
            betMoneyDom = $betDetailTable.find("tr:nth-child(5) td:nth-child(2)"),
            opencodeDom = $betDetailTable.find("tr:nth-child(5) td:nth-child(4)"),
            winNumDom = $betDetailTable.find("tr:nth-child(6) td:nth-child(2)"),
            bonusDom = $betDetailTable.find("tr:nth-child(6) td:nth-child(4)"),
            isTraceDom = $betDetailTable.find("tr:nth-child(7) td:nth-child(2)"),
            traceNumDom = $betDetailTable.find("tr:nth-child(7) td:nth-child(4)"),
            betDetailDom = $betDetailTable.find("#game_record_info div"),
            betInfoListDom = document.querySelector("#betInfoBox #betInfoList div"),
            cancel_btnDom = $(".game_record_messageBox .cancelBetBtn"),
            $chipgroup = $("#J-chipgroup-cont"),
            $chipgroup_others = $("#chipgroup_others"),
            lt_time_leave = 0,
            template = function (e, t) {
                var i, n, a = t;
                for (i in a) a.hasOwnProperty(i) && (n = RegExp("<#=" + i + "#>", "g"), e = e.replace(n, a[i]));
                return e
            },
            formatMoney = function (e, t) {
                e = Number(e), t = null == t || t < 0 ? 2 : t;
                var i = /(-?\d+)(\d{3})/;
                t = 0;
                for (e = "" + (e = Number.prototype.toFixed ? e.toFixed(t) : Math.round(e * Math.pow(10, t)) / Math.pow(10, t)); i.test(e);) e = e.replace(i, "$1,$2");
                return e
            },
            numToCss = function (e) {
                return ("" + Number(e)
                    .toFixed(2))
                    .replace(".", "_")
            };
        String.prototype.replaceAll = function (e, t) {
            return this.replace(new RegExp(e, "g"), t)
        };
        var maxInitRequestTime = 3,
            drawLet = function (e) {
                var t = 0,
                    a = window.setInterval(function () {
                        20 < t ? (clearInterval(a), console.log("can not get the open code!!!")) : (t++ , request("flag=getopencode&lotteryid=" + global_game_config.gameid + "&issue=" + e, function (e) {
                            var t = e.data.code;
                            if (t) {
                                clearInterval(a);
                                for (var i = $(".game_head .game_record_time .drawLet .drawCode i"), n = 0; n < 3; n++) i.get(n)
                                    .className = "mdice mdice-" + t[n];
                                stopOpenCodeAnimate(), renderOpenHistory()
                            }
                        }))
                    }, 1500)
            },
            lt_opentimer = function (e, t, i) {
                startOpenCodeAnimate(), $(".game_head .game_record_time .issueno")
                    .text(i.slice(9));
                var n;
                n = "" == e || "" == t ? 0 : (format(t)
                    .getTime() - format(e)
                        .getTime()) / 1e3,
                    window.setTimeout(function () {
                        drawLet(i)
                    }, 1e3 * n)
            },
            lt_timerHtml = function (e, t) {
                lt_time_leave = "" == e || "" == t ? 0 : (format(t)
                    .getTime() - format(e)
                        .getTime()) / 1e3;
                var i = Math.ceil(59 * Math.random() + 210),
                    n = Math.ceil(59 * Math.random() + 30),
                    a = window.setInterval(function () {
                        0 < lt_time_leave && (lt_time_leave % i == 0 || lt_time_leave == n) && request("flag=getCurIssure&lotteryid=" + global_game_config.gameid, function (e) {
                            e = e.data, lt_time_leave = (format(e.endtime)
                                .getTime() - format(e.servertime)
                                    .getTime()) / 1e3
                        }), lt_time_leave <= 0 && (clearInterval(a), lt_opentimer(curr_issue.endtime, curr_issue.opentime, curr_issue.issue), lt_reset());
                        var e = diff(lt_time_leave--);
                        $record_time.html((0 < e.day ? e.day + lot_lang.dec_s21 + " " : "") + (fftime(e.hour) + "")
                            .substr(0, 2) + " : " + (fftime(e.minute) + "")
                                .substr(0, 2) + " : " + (fftime(e.second) + "")
                                    .substr(0, 2))
                    }, 1e3)
            },
            initEventHandler = function () {
                function a() {
                    var e = $(".isGo:checked"),
                        t = 0,
                        i = e.length;
                    $.each(e, function () {
                        var e = Number($(this).parent().siblings(".trace_money").children(".moneyValue").text().replace(/[,]/g, ''));
                        t += e
                    });

                    t ? $("#J-button-comfirm-bet-many").removeClass("btn-disabled") : $("#J-button-comfirm-bet-many").addClass("btn-disabled");
                    $("#J-tpl-modal-bet-many .qi").text(i);
                    $("#J-tpl-modal-bet-many .totalMoney").text(numberWithCommas(t));
                }

                $history_head.on("click", function () {
                    $mdice_history_body.slideDown()
                }), $(".mdice_history_body")
                    .on("click", ".closeBtn,.close_span", function () {
                        $mdice_history_body.slideUp()
                    }), $draw_lottery_head.on("click", function () {
                        $draw_lottery_body.slideDown(), renderOpenHistory()
                    }), $(".draw_lottery_body")
                        .on("click", ".closeBtn,.close_span", function () {
                            $draw_lottery_body.slideUp()
                        }), $chipgroup_others.sortable({
                            helper: "clone"
                        }), $chipgroup_others.on("click", ".chip", function () {
                            $chipgroup_others.find(".chip.chip-selected")
                                .removeClass("chip-selected"), $(this)
                                    .addClass("chip-selected")
                        }), $(".changeChips,#chipgroup_closeBtn")
                            .on("click", function () {
                                "visible" === $chipgroup.css("visibility") ? ($chipgroup_others.css("left", $chipgroup.position()
                                    .left), $chipgroup.css("visibility", "hidden"), $chipgroup_others.show()) : ($chipgroup.empty()
                                        .append($chipgroup_others.find(".chip")
                                            .slice(-5, 15)
                                            .clone()), $chipgroup.find(".chip:first")
                                                .trigger("click")
                                                .addClass("chip-selected"), $chipgroup.css("visibility", "visible"), $chipgroup_others.hide())
                            }),
                    $("#go_info_panel")
                        .on("click", "tr", function () {
                            var e = $(this),
                                t = e.find(".select .isGo");
                            e.hasClass("selected") ? t.prop("checked", !1) : t.prop("checked", !0), e.toggleClass("selected"), a()
                        });
                $("#go_info_panel").on("click", ".add_btn", function (e) {
                    var t = parseInt($("#J-num-money").text().replace(/[,]/g, '')),
                        i = $(this).siblings(".moneyValue");

                    return i.text(numberWithCommas(Number(i.text().replace(/[,]/g, '')) + t)), a(), !1
                });
                $("#go_info_panel").on("click", ".minus_btn", function (e) {
                    var t = parseInt($("#J-num-money").text().replace(/[,]/g, '')),
                        i = $(this).siblings(".moneyValue"),
                        n = Number(i.text().replace(/[,]/g, '')) - t;

                    return 0 < n && (i.text(numberWithCommas(n)), a()), !1
                });
                $(".game_record_table").on("click", ".see", function () {
                    request("flag=getbetdetail&sid=" + $(this)
                        .attr("sid"),
                        function (e) {
                            if ("success" == e.status) {
                                var t = e.data,
                                    i = t.betinfo;
                                loginnameDom.text(t.curloginname);
                                statusDom.addClass("status_" + i.status);
                                "0" != i.status ? $(".game_record_messageBox_body .cancelBetBtn").hide() : $(".game_record_messageBox_body .cancelBetBtn").show();
                                statusDom.text(i.statusname);
                                sidDom.text(i.id);
                                betTimeDom.text(i.bettime);
                                gameNameDom.text(i.lotteryname);
                                issueDom.text(i.issue);
                                betMoneyDom.text(numberWithCommas(i.amount));
                                opencodeDom.html(winnumberHTML(i.winnumber));
                                winNumDom.text(i.wintimes);
                                bonusDom.text(numberWithCommas(i.bonus));
                                isTraceDom.text(1 == i.source ? "Có" : "Không");
                                traceNumDom.text(i.traceid);
                                betDetailDom.html(i.codes);
                                cancel_btnDom.attr("sid", i.id);
                                layeropen({
                                    area: ["606px", "506px"],
                                    shadeClose: !1,
                                    content: $(".game_record_messageBox")
                                })
                            }
                        })
                }), $(".game_record_messageBox")
                    .on("click", ".cancelBetBtn", function () {
                        request("flag=cancelbet&sid=" + $(this)
                            .attr("sid"),
                            function (e) {
                                var t = "Hủy cược thành công.";
                                "error" == e.status && (t = e.data), showMessageBox(t), setTimeout(function () {
                                    layer.closeAll()
                                }, 1500)
                            })
                    }), $(".viewAllDraws")
                        .on("click", function () {
                            var s = document.querySelector("#codesList table");
                            request("flag=getopencodehistory&sid=" + global_game_config.gameid + "&num=100", function (e) {
                                for (var t = e.data, i = document.createDocumentFragment(), n = 0; n < t.length; n++) {
                                    var a = t[n];
                                    if (!a.winnumber) return;
                                    var o = document.createElement("tr");
                                    o.innerHTML = '<td class="issueno">' + a.issue + '</td><td class="opencode">' + winnumberHTML(a.winnumber) + "</td>", o.innerHTML += '<td class="opentime">' + a.opentime + "</td>", i.appendChild(o)
                                }
                                s.innerHTML = "", s.appendChild(i)
                            }), layeropen({
                                area: ["606px", "506px"],
                                shadeClose: !1,
                                content: $(".viewAllCode_modal")
                            })
                        })
            },
            showMessageBox = function (e) {
                $("#messageBox p")
                    .text(e), layeropen({
                        area: ["366px", "156px"],
                        shadeClose: !0,
                        content: $("#messageBox")
                    })
            },
            initAnimate = function () {
                function m(e) {
                    this.money = Number(e);
                    this.getMoney = function () {
                        return this.money
                    };
                    this.toString = function () {
                        return String(this.money)
                    }
                }

                function e(e) {
                    var t = this;
                    t.name = e.name, t.id = e.id, t.prize = e.prize, t.max = e.max, t.left = e.left, t.top = e.top, t.width = e.width, t.height = e.height, t.chips = [], this.toString = function () {
                        return t.name + " " + t.id + " " + t.prize + " " + t.max + " " + t.left + " " + t.top + " " + t.width + " " + t.height
                    }, t.getId = function () {
                        return this.id
                    }, t.getName = function () {
                        return this.name
                    }, t.getPrize = function () {
                        return this.prize
                    }, t.getMax = function () {
                        return this.max
                    }, t.getChips = function () {
                        return this.chips
                    }, t.addChip = function (e) {
                        this.chips.push(e)
                    }, t.removeChip = function (e) {
                        var t = this.getChips(),
                            i = 0,
                            n = t.length;
                        for (i = 0; i < n; i++) e == t[i] && t.splice(i, 1);
                        this.chips = t
                    }, t.getMoney = function () {
                        var e = this.chips,
                            t = 0,
                            i = e.length,
                            n = 0;
                        for (t = 0; t < i; t++) n += e[t].getMoney();
                        return n
                    }, t.getChipsNum = function () {
                        return this.getChips()
                            .length
                    }, t.goBack = function () {
                        return this.getChips()
                            .pop()
                    }
                }

                function u() {
                    for (var e = 0, t = 0; t < _.length; t++) e += _[t].chip.getMoney();
                    $("#J-num-money")
                        .text(numberWithCommas(1e3 * Number(e)))
                }

                function p() {
                    _.length ? ($("#cancelBtn")
                        .removeClass("btn-disabled"), $("#clearBtn")
                            .removeClass("btn-disabled"), $("#J-button-submit")
                                .removeClass("btn-disabled"), $("#J-button-bet-many")
                                    .removeClass("btn-disabled")) : ($("#cancelBtn")
                                        .addClass("btn-disabled"), $("#clearBtn")
                                            .addClass("btn-disabled"), $("#J-button-submit")
                                                .addClass("btn-disabled"), $("#J-button-bet-many")
                                                    .addClass("btn-disabled"))
                }

                function t(e) {
                    if (e) {
                        var t = e.item,
                            i = e.chip;
                        t.removeChip(i), v(t, i), $("div[data-name=" + t.name + "] .text")
                            .text(formatMoney(t.getMoney()))
                    }
                }

                function s() {
                    for (; ;) {
                        var e = _.pop();
                        if (!e) break;
                        t(e)
                    }
                    u()
                }

                function l(e) {
                    var t = e.split("_"),
                        i = t[0],
                        n = t[1],
                        a = gameNameConst[i];
                    return "duizi" == i && (n = n.substr(0, 1)), n && (a += gameNameConst[n] ? gameNameConst[n] : n), a
                }

                var i, n, d, c, h = $("#J-table-desktop"),
                    f = [],
                    g = [],
                    _ = [];
                n = global_game_config.methods, d = global_game_config.poscfg, c = {}, $.each(n, function () {
                    c[this.name] || (c[this.name] = this)
                }), $.each(d, function () {
                    c[this.name] && (i = c[this.name], f.push(new e({
                        name: this.name,
                        id: i.id,
                        prize: i.prize,
                        max: i.max,
                        left: this.left,
                        top: this.top,
                        width: this.width,
                        height: this.height
                    })))
                }),
                    function () {
                        for (var e = 0; e < f.length; e++) g[f[e].name] = f[e];
                        var t, i, n, a = [];
                        for (t in g) g.hasOwnProperty(t) && (n = {
                            name: (i = g[t])
                                .name,
                            id: i.id,
                            left: i.left,
                            top: i.top,
                            width: i.width,
                            height: i.height
                        },
                            a.push(template('<div data-name="<#=name#>" class="item item-<#=name#>" style="left:<#=left#>px;top:<#=top#>px;width:<#=width#>px;height:<#=height#>px;"><span class="tip"><span class="text">0.00</span><span class="arrow"></span></span></div>', n)));
                        $(a.join(""))
                            .appendTo(h)
                    }(), h.on("mouseenter", ".item", function () {
                        var e, t = $(this),
                            i = t.attr("data-name");
                        0 < (e = g[i].getMoney()) && (t.find(".tip")
                            .addClass("tip-show")
                            .find(".text")
                            .text(numberWithCommas(e * 1000)), t.addClass("item-hover"))
                    }), h.on("mouseleave", ".item", function () {
                        var e = $(this);
                        e.find(".tip")
                            .removeClass("tip-show"), e.removeClass("item-hover"), u()
                    }), h.on("contextmenu", ".chip", function (e) {
                        var t = $(this)
                            .parent(),
                            i = t.attr("data-name"),
                            n = g[i],
                            a = n.getChips(),
                            o = a[a.length - 1];
                        o && (function (e, t) {
                            for (var i = 0; i < _.length; i++) {
                                var n = _[i];
                                if (n.item.toString() == e.toString() && n.chip.toString() == t.toString()) {
                                    _.splice(i, 1);
                                    break
                                }
                            }
                        }(n, o), u()), n.removeChip(o), v(n, o), t.find(".text")
                            .text(numberWithCommas((n.getMoney()) * 1000)), e.preventDefault()
                    }), h.on("contextmenu", function (e) {
                        e.preventDefault()
                    });
                var b = $("#J-chipgroup-cont"),
                    y = $("body"),
                    v = function (e, t) {
                        var i = b.find(".chip-" + numToCss(t.getMoney()));
                        i.size() || (i = b.find("i:first"));
                        var n = $("#J-table-desktop")
                            .find(".item-" + e.getName()),
                            a = n.find(".chip"),
                            o = a.eq(a.size() - 1),
                            s = o.clone(),
                            r = n.offset(),
                            l = i.offset();
                        s.removeClass("chip-selected"), s.addClass("move-chip"), s.appendTo(y), s.css({
                            left: r.left + n.width() / 2 - s.width() / 2,
                            top: r.top + n.height() / 2 - s.height() / 2
                        }), o.remove(), s.animate({
                            left: l.left,
                            top: l.top
                        }, function () {
                            s.remove(), p()
                        })
                    };
                h.on("click", ".item", function () {
                    var e = $(this),
                        t = null;
                    t = $chipgroup_others.is(":visible") ? $chipgroup_others : b, n = e.attr("data-name"), r = g[n], o = r.getMoney(), a = r.getMax(), h = t.children(".chip-selected");
                    for (var i = 0, n = 0; n < _.length; n++) i += _[n].chip.getMoney();
                    return parseFloat(h.data("money")) + i > parseFloat(balance) ? ($("#J-tip-text-max-money-tip")
                        .text(formatMoney(balance, 3)), $("#J-tpl-modal-tip .msgTitle")
                            .text("Số dư không đủ"), void layeropen({
                                area: ["366px", "156px"],
                                shadeClose: !0,
                                content: $("#J-tpl-modal-tip")
                            })) : parseFloat(h.data("money")) + i > global_game_config.maxbetmoney ? ($("#J-tip-text-max-money-tip")
                                .text(formatMoney(global_game_config.maxbetmoney)), $("#J-tpl-modal-tip .msgTitle")
                                    .text("Số tiền vượt quá giới hạn tối đa cho 1 lần cược."), void layeropen({
                                        area: ["366px", "156px"],
                                        shadeClose: !0,
                                        content: $("#J-tpl-modal-tip")
                                    })) : void (o + parseFloat(h.data("money")) > a ? ($("#J-tip-text-max-money-tip")
                                        .text(formatMoney(a)), $("#J-tpl-modal-tip .msgTitle")
                                            .text("Số tiền cược vượt quá giới hạn."), layeropen({
                                                area: ["366px", "156px"],
                                                shadeClose: !0,
                                                content: $("#J-tpl-modal-tip")
                                            })) : (e.addClass("item-hover"), function (e) {
                                                var t = null;
                                                t = $chipgroup_others.is(":visible") ? $chipgroup_others : b;
                                                var i = $("#J-table-desktop")
                                                    .find(".item-" + e.getName()),
                                                    n = e.getChipsNum(),
                                                    a = new m(t.children(".chip-selected")
                                                        .data("money")),
                                                    o = (e.addChip(a), "chip-" + numToCss(a.getMoney())),
                                                    s = "global-chip",
                                                    r = t.find("." + o)
                                                        .eq(0),
                                                    l = r.clone(),
                                                    d = l.clone(),
                                                    c = r.offset(),
                                                    h = i.offset();
                                                d.addClass(o)
                                                    .addClass(s)
                                                    .removeClass("chip-selected"), l.css({
                                                        left: c.left,
                                                        top: c.top
                                                    }), l.addClass(o)
                                                        .addClass(s)
                                                        .removeClass("chip-selected"), l.appendTo(y), l.animate({
                                                            left: h.left + i.width() / 2 - l.width() / 2,
                                                            top: h.top + i.height() / 2 - l.height() / 2 - 3 * (n - 1)
                                                        }, function () {
                                                            l.remove(), d.appendTo(i), d.css({
                                                                left: i.width() / 2 - d.width() / 2,
                                                                top: i.height() / 2 - d.height() / 2 - 3 * n
                                                            }), i.find(".tip")
                                                                .css({
                                                                    top: -35 - 2 * e.getChipsNum()
                                                                }), i.find(".text")
                                                                    .text(numberWithCommas((e.getMoney()) * 1000)), _.push({
                                                                        item: e,
                                                                        chip: a
                                                                    }), u(), p()
                                                        })
                                            }(r)))
                }), $("#cancelBtn")
                    .on("click", function (e) {
                        _.length && (t(_.pop()), u(), e.preventDefault())
                    }), $("#clearBtn")
                        .on("click", function () {
                            _.length && s()
                        });
                var x = function () {
                    for (var e = 0, t = {}, i = 0; i < _.length; i++) {
                        var n = _[i].item,
                            a = _[i].chip.getMoney();
                        t[n.getName()] ? t[n.getName()] += a * 1000 : t[n.getName()] = a * 1000;
                        e += a;
                    }
                    e *= 1000;

                    return {
                        flag: "bet",
                        gameid: global_game_config.gameid,
                        trace_if: 0,
                        total_nums: 1,
                        total_money: e,
                        betdata: t,
                        buyitem: ["{'money':" + e + ", 'nums': 1, 'times': '1'}"]
                    }
                };
                $("#betInfoBox .confirm_btn")
                    .on("click", function () {
                        if (!isLocked) {
                            isLocked = !0;
                            var i = x();
                            if (i.issue_start = lastIssue, i.total_money)
                                if ("false" == $("#betInfoBox").data("isTrace")) request(i, function (e) {
                                    var t = "Đặt cược thành công.";
                                    "error" == e.status && (t = e.data), showMessageBox(t), setTimeout(function () {
                                        layer.closeAll()
                                    }, 1500), renderBetHistory(), s(), isLocked = !1
                                }, "json");
                                else {
                                    var e = "no";
                                    $(".isStopGO input")
                                        .is(":checked") && (e = "yes");
                                    var t = $(".isGo:checked"),
                                        n = (t.length, []);
                                    $.each(t, function () {
                                        var e, t = $(this);
                                        e = "{'issue':" + t.attr("ids") + ",'times': '" + t.parent().siblings(".trace_money").find(".moneyValue").text().replace(/[,]/g, '') / i.total_money + "'}";
                                        n.push(e);
                                    }), i.trace_total_money = parseInt($("#J-tpl-modal-bet-many .totalMoney")
                                        .text()), i.total_money = parseInt(i.total_money), i.trace_if = 1, i.total_nums = 1, i.trace_count_input = $("#J-tpl-modal-bet-many .qi")
                                            .text(), i.trace_stop = e, i.buytrace = n, request(i, function (e) {
                                                var t = "Đặt cược thành công.";
                                                "error" == e.status && (t = e.data), showMessageBox(t), setTimeout(function () {
                                                    layer.closeAll()
                                                }, 1500), renderBetHistory(), s(), isLocked = !1
                                            }, "json")
                                }
                        }
                    }),
                    $("#J-button-submit")
                        .on("click", function () {
                            if (_.length) {
                                $("#betInfoBox .traceInfo")
                                    .hide();
                                var e = x();
                                lastIssue = curr_issue.issue, $("#betInfoBox .title")
                                    .text("Xác nhận cược: " + lastIssue);
                                var t = e.betdata,
                                    i = document.createDocumentFragment();
                                for (var n in t) {
                                    var a = document.createElement("p");
                                    a.innerHTML = "【" + l(n) + "】Cược: " + numberWithCommas(t[n]) + " VNĐ", i.appendChild(a)
                                }
                                betInfoListDom.innerHTML = "";
                                betInfoListDom.appendChild(i);
                                $("#betInfoBox .totalMoney").text(numberWithCommas((e.total_money)));
                                $("#betInfoBox").data("isTrace", "false"), betInfoIndex = layeropen({
                                    area: ["606px", "263px"],
                                    shadeClose: !1,
                                    content: $("#betInfoBox")
                                })
                            }
                        }), $("#J-button-bet-many")
                            .on("click", function () {
                                for (var e = 0, t = 0; t < _.length; t++) {
                                    e += _[t].chip.getMoney();
                                }
                                e = e * 1000;
                                if (e) {
                                    alert("Tính năng đang được phát triển. Vui lòng thử lại sau");
                                    return;

                                    lastIssue = curr_issue.issue;
                                    var i = global_game_config.issueliststr,
                                        n = document.querySelector("#go_info_panel table"),
                                        a = document.createDocumentFragment(),
                                        o = 0;
                                    for (t = 0; t < i.length; t++) {
                                        var s = i[t],
                                            r = document.createElement("tr");
                                        s.issue >= curr_issue.issue && (o++ , r.innerHTML = '<td class="id ">' + o + '</td><td class="select "><input type="checkbox" class="isGo" ids="' + s.issue + '"></td><td class="num ">' + s.issue + '</td><td class="trace_money "><div class="add_btn"></div><span class="moneyValue">' + numberWithCommas(e) + '</span><div class="minus_btn"></div></td><td class="date ">' + s.opentime + "</td>", a.appendChild(r))
                                    }
                                    n.innerHTML = "";
                                    n.appendChild(a);
                                    $("#J-button-comfirm-bet-many").addClass("btn-disabled"), $("#J-tpl-modal-bet-many .qi").text("0"), $("#J-tpl-modal-bet-many .totalMoney").text("0");
                                    $("#betInfoBox").data("isTrace", "true");
                                    layeropen({
                                        area: ["606px", "506px"],
                                        shadeClose: !1,
                                        content: $("#J-tpl-modal-bet-many")
                                    })
                                }
                            }), $(document)
                                .on("click", "#J-button-comfirm-bet-many:not(.btn-disabled)", function () {
                                    $("#betInfoBox .traceInfo").show();
                                    var e = $("#J-tpl-modal-bet-many .qi").text();
                                    $("#betInfoBox .title").text("Xác nhận nuôi: " + lastIssue), $("#betInfoBox .traceInfo .qi").text(e);
                                    var t = $("#J-tpl-modal-bet-many .totalMoney").text();
                                    $("#betInfoBox .totalMoney").text(t);
                                    var i = x(),
                                        n = Number(t.replace(/[,]/g, '')) / i.total_money,
                                        a = i.betdata,
                                        o = document.createDocumentFragment();
                                    for (var s in a) {
                                        var r = document.createElement("p");
                                        r.innerHTML = "【" + l(s) + "】 Cược: " + numberWithCommas(a[s] * n) + " VND";
                                        o.appendChild(r);
                                    }
                                    betInfoListDom.innerHTML = "";
                                    betInfoListDom.appendChild(o);
                                    betInfoIndex = layeropen({
                                        area: ["606px", "263px"],
                                        shadeClose: !1,
                                        content: $("#betInfoBox")
                                    })
                                }), b.on("click", ".chip", function () {
                                    b.find(".chip-selected")
                                        .removeClass("chip-selected"), $(this)
                                            .addClass("chip-selected")
                                }), $(document)
                                    .on("click", ".isStopText", function () {
                                        $(this)
                                            .siblings("input")
                                            .prop("checked") ? $(this)
                                                .siblings("input")
                                                .prop("checked", !1) : $(this)
                                                    .siblings("input")
                                                    .prop("checked", !0)
                                    })
            },
            animateInterval = null;
        initEventHandler(),
            initRequest(function () {
                lt_reset(),
                    getLastOpenCode(),
                    renderBetHistory(),
                    setInterval(function () {
                        renderBetHistory()
                    }, 10000),
                    initScrollPanel(),
                    initAnimate()
            })
    }($))
}();
