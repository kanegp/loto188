var mwInterval = null;
var mbInterval = null;
$(document).ready(function () {
	checkUser();
	header_visibility();
	if (checkCookie()) {
		loadmenu();
		loadData();
	} else {
		remove_cookie();
	}
	$(".boardgame_dl03").mouseover(function () {
		$(this).find("dt").stop(true, true).show().css("cursor", "pointer");
	});
	$(".boardgame_dl03").mouseout(function () {
		$(this).find("dt").stop(true, true).hide();
	});
	$("#enav").children("li").click(function () {
		$("#enav").children("li").each(function () {
			var s = $(this).attr("class");
			if (s.indexOf("current") > 0) {
				s = s.replace('current', '');
				$(this).attr("class", s);
			}
		});
		var f = $(this).attr("class");
		f += ' current';
		$(this).attr("class", f);
		$("#mainiframe").attr("src", $(this).attr("data"));
		$("#eleftMenu").children("ul").children("li").each(function () {
			$(this).attr("style", "");
		});
	});
	$("#eleftMenu").children("ul").children("li").click(function () {
		$("#eleftMenu").children("ul").children("li").each(function () {
			$(this).attr("style", "");
		});
		$("#enav").children("li").each(function () {
			var s = $(this).attr("class");
			if (s.indexOf("current") > 0) {
				s = s.replace('current', '');
				$(this).attr("class", s);
			}
		});
		$(this).attr("style", "background: #303842;color:#90ff00;");
		var urls = $(this).attr("data");
		$("#mainiframe").attr("src", urls);
	});
	$("#eDpIcon").click(function () {
		$("#eleftMenu").children("ul").children("li").each(function () {
			$(this).attr("style", "");
		});
		$("#enav").children("li").each(function () {
			var s = $(this).attr("class");
			if (s.indexOf("current") > 0) {
				s = s.replace('current', '');
				$(this).attr("class", s);
			}
		});
		//mBank
		var f = $("#mBank").attr("class");
		f += ' current';
		$("#mBank").attr("class", f);
		$("#mainiframe").attr("src", "/page/DWDeposit.shtml");
	});


	$("#eHiddenMan").click(function () {
		$("#e_hiddenbalance").hide("fast");
		$("#e_showbalance").show("fast");
	});
	$("#e_showbalance").click(function () {
		$("#e_hiddenbalance").show("slow");
		$("#e_showbalance").hide("slow");
	});
	$("#aMsg").click(function () {
		$("#top_submenu2").children(".top_menu_sel").each(function () {
			$(this).attr("class", "top_menu_float");
		});
		$("#left_ban").children(".left_items").each(function () {
			$(this).attr("style", "");
		});
		$("#ePersonInfo").attr("class", "top_menu_sel");
		$("#mainiframe").attr("src", "/page/PersonMsg.shtml");
	});
	$("#depositBnt").click(function () {
		$("#top_submenu2").children(".top_menu_sel").each(function () {
			$(this).attr("class", "top_menu_float");
		});
		$("#left_ban").children(".left_items").each(function () {
			$(this).attr("style", "");
		});
		$("#ebankitem").attr("class", "top_menu_sel");
		$("#mainiframe").attr("src", "/page/DWDeposit.shtml");
	});
	$("#withDrawalBnt").click(function () {
		$("#top_submenu2").children(".top_menu_sel").each(function () {
			$(this).attr("class", "top_menu_float");
		});
		$("#left_ban").children(".left_items").each(function () {
			$(this).attr("style", "");
		});
		$("#ebankitem").attr("class", "top_menu_sel");
		$("#mainiframe").attr("src", "/page/DWWithDrawl.shtml");
	});

	$("#left_ban").children(".left_items").click(function () {
		$("#left_ban").children(".left_items").each(function () {
			$(this).attr("style", "");
		});
		$("#top_submenu2").children(".top_menu_sel").each(function () {
			$(this).attr("class", "top_menu_float");
		});
		$(this).attr("style", "background:#444444;color:#fecb5e");
		$("#mainiframe").attr("src", $(this).attr("data"));
		$("#mainiframe").attr("height", "750px");
	});
	$("#financeOpt").click(function () {
		//alert("商家未提供财务转账地址！");
		$("#mainiframe").attr("src", "/page/MifTest.html");
	});
	$("#bnt_refreshbalance").click(function (arg) {
		var evnt = window.event ? window.event : arg;
		if (evnt.stopPropagation) {
			evnt.stopPropagation();
		} else {
			evnt.cancelBubble = true;
		}
		$.blockUI({
			message: '<div style="width:200px;padding:10px 100px;background-color:#fff;border:4px #666 solid;"><img src="/images/loading2.gif" style="margin-right:10px;"/>正在查询余额中...</div>',
			overlayCSS: {
				backgroundColor: '#000000',
				opacity: 0.3,
				cursor: 'wait'
			}
		});
		$.ajax({
			type: 'POST',
			url: '/LotteryService.aspx',
			data: 'flag=balance',
			success: function (data) {
				$.unblockUI({
					fadeInTime: 0,
					fadeOutTime: 0
				});
				try {
					$('#ebalance').html(data);
				} catch (e) {
					alert('<img src="/images/icon.png" class="icons_mb5_e" style="margin:5px 15px 0 0;width: 40px; height: 40px;"/>Permintaan gagal. Coba lagi.');
				}
			}
		});
	});
	//测试
	//mwInterval = window.setInterval('loadInfor();',60*1000);
	mbInterval = window.setInterval('loadbalance();', 15 * 1000);


	//
	$("#mobile").hover(function () {
		$("#mobile_div").css("display", "block");
	}, function () {
		$("#mobile_div").css("display", "none");
	});

	$('.hkd').click(function () {
		var layerBoxId = layer.open({
			type: 1,
			title: false,
			closeBtn: 0,
			closeBtn: false,
			area: '755px',
			content: $('#down-panel')
		});

		$("#down-panel .close").click(function () {
			layer.close(layerBoxId);
		});

		$(".pc-down-btn").click(function () {
			location.href = "http://xspc.pcdown.info/down/13522389/PC_Client/XS_PC_Client.zip";
		});
	});
});

function resize(gameContentHeight) {
	var obj = $("#body", window.parent.document);
	var finalHeight = gameContentHeight + 220 + 50;
	obj.css('min-height', finalHeight + 'px');
}

function loadbalance() {
	$.ajax({
		type: 'POST',
		url: '/LotteryService.aspx',
		data: 'flag=balance',
		success: function (data) {
			try {
				data = $.trim(data);
				if (data.indexOf('-') > 0) {
					var arr = new Array();
					arr = data.split('-');
					if (arr.length == 1) {
						$('#ebalance').html(arr[0] + '');
					} else if (arr.length > 1) {
						$('#ebalance').html(arr[0] + '');
						//						$('#ebalance').html(10+'');
						if (arr[1] != '') {
							layer.open({
								type: 1,
								skin: 'layui-layer-rim', //加上边框
								area: ['420px', '240px'], //宽高
								offset: 'rb',
								time: 5000, //2秒后自动关闭
								//							    shadeClose: true, //开启遮罩关闭
								shade: 0.0,
								content: '<div style="padding:20px;color:gray;">' + arr[1] + '</div>'
							});
						}
						if (arr[2] != '') {
							layer.open({
								type: 1,
								skin: 'layui-layer-rim', //加上边框
								area: ['350px', '200px'], //宽高
								offset: 'rb',
								time: 5000, //2秒后自动关闭
								shadeClose: true, //开启遮罩关闭
								shade: 0.0,
								content: '<div style="padding:20px;color:gray;">' + arr[2] + '</div>'
							});
						}
					} else {
						console.log('查询余额失败，后台报错了');
						//					alert(data);
					}
				}
			} catch (e) {
				console.log(e)
				alert('Permintaan gagal. Coba lagi.');
			}
		}
	});
}
function menuToMain() {
	$('#top_submenu').children('div').each(function () {
		$(this).attr('class', 'top_menu_float');
	});
	$('#left_ban').children('.left_items').each(function () {
		$(this).attr('style', '');
	});
	$('#eMain').attr('class', 'top_menu_sel');
	$('#mainiframe').attr('src', '/page/Index.shtml');
	//$('#mainiframe').attr('height', '610px');
}
function helpCenter() {
	$('#top_submenu').children('div').each(function () {
		$(this).attr('class', 'top_menu_float');
	});
	$('#left_ban').children('.left_items').each(function () {
		$(this).attr('style', '');
	});
	$('#mainiframe').attr('src', '/page/help.shtml');
	//$('#mainiframe').attr('height', '610px');
}
function exitLogin() {
	window.top.location = '/';
}

function goWgGame(type01) {
	//加载层
	console.log("------------------------------");
	var loadi = layer.load(0, { shade: 0.5 }); //0代表加载的风格，支持0-2
	$.post('/WgGameService.aspx', {
		flag: "open",
		type: type01
	}, function (data, status) {
		if ("success" == status) {
			if (data.indexOf("*") > 0) {
				var arr = new Array();
				arr = data.split("*");
				if (arr.length > 0) {
					if ("0" == arr[0]) {
						var url = arr[1];
						//							window.location.href=url;
						window.open(url);
					} else {
						alert(arr[1]);
					}
				}
			} else {
				alert(data);
			}
		} else {
			alert("请重试");
		}
		layer.close(loadi);
	});
}


function goGGGame(type01) {
	//加载层
	console.log("------------------------------");
	var loadi = layer.load(0, { shade: 0.5 }); //0代表加载的风格，支持0-2
	$.post('/GGService.aspx', {
		flag: "open",
		type: type01
	}, function (data, status) {
		if ("success" == status) {
			if (data.indexOf("*") > 0) {
				var arr = new Array();
				arr = data.split("*");
				if (arr.length > 0) {
					if ("0" == arr[0]) {
						var url = arr[1];
						//							window.location.href=url;
						window.open(url);
					} else {
						alert(arr[1]);
					}
				}
			} else {
				alert(data);
			}
		} else {
			alert("Silakan coba lagi");
		}
		layer.close(loadi);
	});
}



function removeItem(obj, lotteryid, flag, topmenu) {
	$.ajax({
		url: '/LotteryService.aspx?flag=UDellFavorite',
		dataType: 'json',
		type: 'POST',
		data: { flag: 'UDellFavorite', lotteryid: lotteryid },
		success: function (data) {
			if (flag == 0) {
				/* alert("该游戏已取消收藏."); */
				if (topmenu == 1) {
					$("#body").contents().find("#starSrc" + lotteryid).attr('src', '/images/index/star.png');
				} else {
					$("#starSrc" + lotteryid).attr("src", "/images/index/star.png");
				}
				$("#body").contents().find("#favoriteFlag" + lotteryid).val("0");
				$("#" + lotteryid).parent().remove();
			} else {
				$("#" + lotteryid).parent().remove();
			}
			layer.msg('Game koleksi berhasil dihapus!');
		},
		error: function (XMLHttpRequest, textStatus, errorThrown) {
			alert('Pengiriman data gagal, segarkan halaman dan coba lagi');
		}
	});
	loadmenu();
}

function FavoriteItem(obj) {
	var lotteryid = $("#lotteryid").val();
	var lotteryname = $("#lotteryname").val();
	if (1 == $("#favoriteFlag" + pri_lotteryid).val()) {
		$("#starSrc" + pri_lotteryid).attr("src", "/images/index/star.png");
		$("#favoriteFlag" + pri_lotteryid).val(0);
		window.parent.removeItem(obj, pri_lotteryid, 0);
	} else {
		$.ajax({
			url: '/LotteryService.aspx?flag=UAddFavorite',
			dataType: 'json',
			type: 'POST',
			async: false,
			data: { flag: 'UAddFavorite', lotteryid: pri_lotteryid },
			success: function (data) {
				var stats = data[0].code;
				if (stats == 'success') {
					var ddLength = $(".ddClass").length;
					// Besar于5时删除
					if (ddLength == 5) {
						// 删除第一个   补充后台删session
						$(".ddClass :first").remove();
					}
					$("#starSrc" + pri_lotteryid).attr("src", "/images/index/starx.png");
					$("#favoriteFlag" + lotteryid).val(1);
					$(".dlClass", window.parent.document).append('<li class="ddClass"><a href="#" onclick="menuclick(\'' + obj.id + '\')" ">' + lotteryname + '</a><a href="javascript:void(0);" onclick="removeItem(this,\'' + pri_lotteryid + '\',0,1);"><img src="/images/public/pci_bar_gameico02_r2_c3.png" alt="" /></a><input type="hidden" name="50" id="50" value="50" /></li>');
					layer.msg('Koleksi game berhasil!');
				} else {
					var ms = data[1].ms;
					alert(ms);
				}
			},
			error: function (XMLHttpRequest, textStatus, errorThrown) {
				alert('Pengiriman data gagal, segarkan halaman dan coba lagi');
			}
		});
	}
	loadmenu();
	//loadData();
}
function loadmenu() {
	var html = "";
	html += '<li class="li01">Koleksi game：</li>';
	$.ajax({
		url: '/UserService.aspx?flag=LoginBean',
		dataType: 'json',
		type: 'POST',
		async: false,
		data: { flag: 'LoginBean' },
		contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
	})
		.done(
			function (data) {
				$("#title").val(data["webname"]);
				$.each(data["sonList"], function (index, value) {
					if (index < 5) {
						html += '<li class="ddClass">';
						//html += '<a href="'+ value["url"] +'">'	+ value["lotteryname"] + '</a>';
						html += '<a style="cursor: pointer;" onclick=\'loadGamePage("' + escape(value["url"]) + '","' + data["webname"] + '-官方网站")\'>' + value["lotteryname"] + '</a>';
						html += '<a href="javascript:void(0);" onclick="removeItem(this,' + value["lotteryid"] + ',0);">';
						html += '<img src="/images/public/pci_bar_gameico02_r2_c3.png" alt=""/></a>';
						html += '<input type="hidden" name="' + value["lotteryid"] + '" id="' + value["lotteryid"] + '" value="' + value["lotteryid"] + '" /></li>';
					}
				});

			});
	$("#lottery_sub_menu").html(html);
}

function loadData() {

	// Load Nickname
	$.ajax({
		url: '/UserService.aspx?LoginBean_Online',
		dataType: 'json',
		type: 'POST',
		data: { flag: "LoginBean_Online" },
		async: false
	}).done(function (data, status) {
		var user_id = eval("(" + data.user.id + ")");
		$("#userid").val(user_id);
		$("#showData1").text(data.user.nickname);
	});

	// Load Messages
	$.ajax({
		url: "/UserService.aspx?flag=PersonMessageBean_view",
		dataType: 'json',
		type: 'POST',
		data: { flag: "PersonMessageBean_view" },
		async: false
	}).done(function (data, status) {
		$("#showData3").text(+data.totalrecord);
	});

	// Load Balance
	$.ajax({
		url: "/LotteryService.aspx?flag=balance",
		dataType: 'json',
		type: 'POST',
		data: { flag: "balance" },
		async: false
	}).done(function (data, status) {
		$("#showData2").text(addCommas(data.balance));
	});
}

function loadNickname() {
	// Load Nickname
	$.ajax({
		url: '/UserService.aspx?LoginBean_Online',
		dataType: 'json',
		type: 'POST',
		data: { flag: "LoginBean_Online" },
		async: false
	}).done(function (data, status) {
		$("#showData1", window.parent.document).text(data.user.nickname);
	});
}

function getBalance() {
	$.post("/LotteryService.aspx", {
		flag: "balance"
	}, function (data, status) {
		data = JSON.parse(data);
		var money = data.balance.split('.');
		var _decimal = formatNumber(money[0]);
		$('em.currency', window.parent.document).html(_decimal + "." + money[1]);
	});
}

function loadBalance2() {
	var loadi;
	//loadi = layer.load(2, {	shade : 0.5 })
	// Load Balance
	$.ajax({
		url: "/LotteryService.aspx?flag=balance",
		dataType: 'json',
		type: 'POST',
		data: { flag: "balance" },
		async: false
	}).done(function (data, status) {
		$("#showData2", window.parent.document).text(addCommas(data.balance));

	});
	//layer.close(loadi);
}

function checkCookie() {
	if (document.cookie.indexOf("sessionId") == -1 || document.cookie.indexOf("sessionId") == null) {
		return false;
	} else {
		//have cookie
		var cookie = getCookie("sessionId");
		if (cookie) {
			return true;
		}
	}
	if (document.cookie.indexOf("pagesrc") == -1 || document.cookie.indexOf("pagesrc") == "") {
		return false;
	} else {
		//have cookie
		var cookie = getCookie("pagesrc");
		if (cookie) {
			return true;
		}
	}
}

$(function () {
	$('a#logout').click(function () {
		if (confirm('Harap konfirmasi: Apakah sistem keluar?')) {
			setCookie("pagesrc", "", 1);
			setCookie("sessionId", "");
			remove_cookie();
			$.post("/UserService.aspx", { flag: "LoginBean_loginout" },
				function (data, status) {
				});
			window.parent.location.href = "/";
		}
		return false;
	});
});
function login() {
	if (checkCookie()) {
		//window.parent.location.href = "/home.html";
	} else {
		remove_cookie();
	}
}

function showSorrybox(it) {
	layer.tips('Fungsi ini belum terbuka, jadi tetaplah disini!', it, {
		tips: [1, '#030303']
		//还可配置颜色
	});
}
function showSorrybox_1() {
	layer.msg('Fungsi ini belum terbuka, jadi tetaplah disini!');
}

function ReplaceAll(string, omit, place, prevstring) {
	if (prevstring && string === prevstring)
		return string;
	prevstring = string.replace(omit, place);
	return ReplaceAll(prevstring, omit, place, string)
}

function loadPage(id, page) {
	$.ajax({
		type: 'POST',
		url: page,
		async: false,
		success: function (data) {
			$("#" + id, window.parent.document).html(data);
		}
	});
}

function loadPage2(id, page) {
	$.ajax({
		type: 'POST',
		url: page,
		contentType: 'default: text/html; charset=UTF-8',
		async: true,
		success: function (data) {
			$("#" + id).html(data);
		}
	});
}

function loadGamePage(page, title) {
	document.title = title;
	setCookie("pagesrc", page);

	setFlatMenuPosition(page);

	$("#body", window.parent.document).attr("src", page);
}

function setFlatMenuPosition(page) {
	if (page === '/games/dice.html' || page === '/games/dice2.html' || page === '/games/dice3.html' || page === '/games/mdice.html') {
		$('.menu-wrapper').hide();
	} else {
		$('.menu-wrapper').show();
	}

	if (page.search('game') == 1) {
		$('.menu-wrapper').css('top', '400px');
		$('.menu-child').slideUp("fast", function () {
			$('.one').hide();
			$('.one-text').show();
			$('#pacman').removeClass('selected');
			$('.close-menu').hide();
		});
	} else if (page !== '/page/mains/main.html') {
		$('.menu-wrapper').css('top', '240px');
	} else {
		$('.menu-wrapper').css('top', '700px');
	}
}

function resizeFrame() {
	$("iframe").height(document.getElementById('body').contentWindow.document.body.offsetHeight)
	$('iframe').width($(window).width());
}

function resizeFrame2() {
	/*	var height = $(document).height();
		if(height > document.body.clientHeight) {
			height = document.body.clientHeight
		}*/
	var section_height = $(".section").height();
	var section_tab_height = $(".section-tabs").height();
	var total_height = section_tab_height + section_height + 100;
	$("#body", window.parent.document).height(total_height)
}

function setPagesrc(src) {
	setCookie("pagesrc", "", 1);
	setCookie("pagesrc", src, 1)
}

function main() {
	if (!checkCookie() || checkCookie() == 'undefined') {
		window.parent.location.href = "/";
	}
	$.ajax({
		url: '/UserService.aspx?flag=LoginBean',
		dataType: 'json',
		type: 'POST',
		async: false,
		data: {
			flag: 'LoginBean'
		}
	})
		.done(
			function (data) {
				if (getCookie("pagesrc") == "" || getCookie("pagesrc") == null) {
					loadGamePage("/main.html", data["webname"] + "-Situs web resmi");
				} else {
					loadGamePage(getCookie("pagesrc"), data["webname"] + "-Situs web resmi");
				}

			}
		);
	// Remove Header & Footer for dice game
	/*if(getCookie("pagesrc")!="/games/dice.html") {
		loadPage("mhead", "/header.html")
		loadPage("foot", "/foot.html")
	}
	else {
		loadPage("mhead", "/clear.html")
		loadPage("foot", "/clear.html")
	}*/
}

function loadMsg() {
	$.post("/UserService.aspx", { flag: "SysMsgBean_showmsg", id: $("#userid").val() }, function (data, status) {
		var data = eval("(" + data + ")");
		var s;
		//$("#userstatus").val(data.showitem.title);
		s = data.showitem.title
		if (s != "") {
			layer.open({
				type: 2,
				title: 'Pengumuman sistem',
				shadeClose: true,
				shade: 0.6,
				area: ['700px', '500px'],
				content: '/IndexMsgDefSub.html' //iframe的url
			})
		}
	});
}


function setCookie(cname, cvalue, exdays) {
	var d = new Date();
	d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
	var expires = "expires=" + d.toUTCString();
	document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
	var name = cname + "=";
	var decodedCookie = decodeURIComponent(document.cookie);
	var ca = decodedCookie.split(';');
	for (var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ') {
			c = c.substring(1);
		}
		if (c.indexOf(name) == 0) {
			return c.substring(name.length, c.length);
		}
	}
	return "";
}

//Menu Click Events
function menuclick(id) {
	var stitle = $("#title", window.parent.document).val();
	var spage = "";
	$("#tk02", window.parent.document).hide();
	/*$("#mhead", window.parent.document).css("height", "")
	loadPage("mhead", "/header.html")
	loadPage("foot", "/foot.html")
	*/
	alert(id + "1");
	switch (id) {
		// Lottery Games
		case "a1": stitle += "-Situs web resmi";
			loadGamePage("/main.html", stitle);
			setPagesrc("/main.html");
			break;
		case "a2": stitle += "-Situs web resmi";
			loadGamePage("/games/mmc.html", stitle);
			setPagesrc("/games/mmc.html");
			break;
		case "a3": stitle += "-Situs web resmi";
			loadGamePage("/games/ssccs.html", stitle);
			setPagesrc("/games/ssccs.html");
			break;
		case "a4": stitle += "-Situs web resmi";
			loadGamePage("/games/sscbf.html", stitle);
			setPagesrc("/games/sscbf.html");
			break;
		case "a5": stitle += "-Situs web resmi";
			loadGamePage("/games/ssccq.html", stitle);
			setPagesrc("/games/ssccq.html");
			break;
		case "a6": stitle += "-Situs web resmi";
			loadGamePage("/games/sscxj.html", stitle);
			setPagesrc("/games/sscxj.html");
			break;
		case "a7": stitle += "-Situs web resmi";
			loadGamePage("/games/ssctj.html", stitle);
			setPagesrc("/games/ssctj.html");
			break;
		case "a9": stitle += "-Situs web resmi";
			loadGamePage("/games/pk105f.html", stitle);
			setPagesrc("/games/pk105f.html");
			break;
		case "a8": stitle += "-Situs web resmi";
			loadGamePage("/games/pk10.html", stitle);
			setPagesrc("/games/pk10.html");
			break;
		case "a10": stitle += "-Situs web resmi";
			loadGamePage("/games/hg1_5.html", stitle);
			setPagesrc("/games/hg1_5.html");
			break;
		case "a11": stitle += "-Situs web resmi";
			loadGamePage("/games/cs115.html", stitle);
			setPagesrc("/games/cs115.html");
			break;
		case "a12": stitle += "-Situs web resmi";
			loadGamePage("/games/sd115.html", stitle);
			setPagesrc("/games/sd115.html");
			break;
		case "a13": stitle += "-Situs web resmi";
			loadGamePage("/games/gd115.html", stitle);
			setPagesrc("/games/gd115.html");
			break;
		case "a14": stitle += "-Situs web resmi";
			loadGamePage("/games/jx115.html", stitle);
			setPagesrc("/games/jx115.html");
			break;
		case "a15": stitle += "-Situs web resmi";
			loadGamePage("/games/cs3d.html", stitle);
			setPagesrc("/games/cs3d.html");
			break;
		case "a16": stitle += "-Situs web resmi";
			loadGamePage("/games/pl35.html", stitle);
			setPagesrc("/games/pl35.html");
			break;
		case "a17": stitle += "-Situs web resmi";
			loadGamePage("/games/fc3d.html", stitle);
			setPagesrc("/games/fc3d.html");
			break;
		case "a18": stitle += "-Situs web resmi";
			loadGamePage("/games/jz45.html", stitle);
			setPagesrc("/games/jz45.html");
			break;
		case "a19": stitle += "-Situs web resmi";
			loadGamePage("/games/sydney19.html", stitle);
			setPagesrc("/games/sydney19.html");
			break;
		case "a20": stitle += "-Situs web resmi";
			loadGamePage("/games/pudong20.html", stitle);
			setPagesrc("/games/pudong20.html");
			break;
		case "a21": stitle += "-Situs web resmi";
			loadGamePage("/games/netherlands21.html", stitle);
			setPagesrc("/games/netherlands21.html");
			break;
		case "a22": stitle += "-Situs web resmi";
			loadGamePage("/games/macau22.html", stitle);
			setPagesrc("/games/macau22.html");
			break;
		case "a23": stitle += "-Situs web resmi";
			loadGamePage("/games/greece.html", stitle);
			setPagesrc("/games/greece.html");
			break;
		case "a24": stitle += "-Situs web resmi";
			loadGamePage("/games/roma.html", stitle);
			setPagesrc("/games/roma.html");
			break;
		case "a25": stitle += "-Situs web resmi";
			loadGamePage("/games/singapore.html", stitle);
			setPagesrc("/games/singapore.html");
			break;
		// Dice Games
		case "b1": stitle += "-Situs web resmi";
			loadGamePage("/games/dice.html", stitle);
			setPagesrc("/games/dice.html");
			break;
		case "b2": stitle += "-Situs web resmi";
			loadGamePage("/games/dice2.html", stitle);
			setPagesrc("/games/dice2.html");
			break;
		case "b3": stitle += "-Situs web resmi";
			loadGamePage("/games/dice3.html", stitle);
			setPagesrc("/games/dice3.html");
			break;
		case "b4": stitle += "-Situs web resmi";
			loadGamePage("/games/mdice.html", stitle);
			setPagesrc("/games/mdice.html");
			break;


		// Person Pages
		case "c1": stitle += "-Situs web resmi";
			loadGamePage("/page/PersonBank1.html", stitle);
			setPagesrc("/page/PersonBank1.html");
			break;
		case "c2": stitle += "-Situs web resmi";
			loadGamePage("/page/PersonPwd.html", stitle);
			setPagesrc("/page/PersonPwd.html");
			break;
		case "c3": stitle += "-Situs web resmi";
			loadGamePage("/page/PriceList.html", stitle);
			setPagesrc("/page/PriceList.html");
			break;



		// Agent Pages
		case "d1": stitle += "-Situs web resmi";
			loadGamePage("/page/AgentReg.html", stitle);
			setPagesrc("/page/AgentReg.html");
			break;
		case "d2": stitle += "-Situs web resmi";
			loadGamePage("/page/AgentUser.html", stitle);
			setPagesrc("/page/AgentUser.html");
			break;
		case "d3": stitle += "-Situs web resmi";
			loadGamePage("/page/AgentAutoReg.html", stitle);
			setPagesrc("/page/AgentAutoReg.html");
			break;
		case "d4": stitle += "-Situs web resmi";
			loadGamePage("/page/AgentPE.html", stitle);
			setPagesrc("/page/AgentPE.html");
			break;
		case "d5": stitle += "-Situs web resmi";
			loadGamePage("/page/AgentBalance.html", stitle);
			setPagesrc("/page/AgentBalance.html");
			break;

		// Report Pages
		case "e1": stitle += "-Situs web resmi";
			loadGamePage("/page/ReportZb.html", stitle);
			setPagesrc("/page/ReportZb.html");
			break;
		case "e2": stitle += "-Situs web resmi";
			loadGamePage("/page/BonusReport.html", stitle);
			setPagesrc("/page/BonusReport.html");
			break;
		case "e3": stitle += "-Situs web resmi";
			loadGamePage("/page/ReportWL.html", stitle);
			setPagesrc("/page/ReportWL.html");
			break;
		case "e4": stitle += "-Situs web resmi";
			loadGamePage("/page/ReportTD.html", stitle);
			setPagesrc("/page/ReportTD.html");
			break;
		case "e5": stitle += "-Situs web resmi";
			loadGamePage("/page/ReportDay.html", stitle);
			setPagesrc("/page/ReportDay.html");
			break;

		//
		case "g1": stitle += "-Situs web resmi";
			loadGamePage("/page/PersonMsg.html", stitle);
			setPagesrc("/page/PersonMsg.html");
			break;
		case "g2": stitle += "-Situs web resmi";
			loadGamePage("Index.html", stitle);
			setPagesrc("Index.html");
			break;
		case "g3": stitle += "-Situs web resmi";
			loadGamePage("/Export.html", stitle);
			setPagesrc("/Export.html");
			break;


		case "h1": stitle += "-Situs web resmi";
			loadGamePage("/BetRecord.html", stitle);
			setPagesrc("/BetRecord.html");
			break;
		case "h2": stitle += "-Situs web resmi";
			loadGamePage("/TraceRecord.html", stitle);
			setPagesrc("/TraceRecord.html");
			break;

		case "i1": stitle += "-Situs web resmi";
			loadGamePage("/page/DWDeposit.html", stitle);
			setPagesrc("/page/DWDeposit.html");
			break;
		case "i2": stitle += "-Situs web resmi";
			loadGamePage("/page/DWWithDrawl.html", stitle);
			setPagesrc("/page/DWWithDrawl.html");
			break;

		case "n1": stitle += "-Situs web resmi";
			loadGamePage("/page/notice.html", stitle);
			setPagesrc("/page/notice.html");
			break;
		case "j1":
			stitle += "-六合彩【官方】";
			loadGamePage("/games/sixmark.html", stitle);
			setPagesrc("/games/sixmark.html");
			window.parent.location.href = "/games/sixmark.html"
			break;
		case "j2":
			stitle += "-六合彩【MS】";
			loadGamePage("/games/sixmark1f.html", stitle);
			setPagesrc("/games/sixmark1f.html");
			window.parent.location.href = "/games/sixmark1f.html"
			break;
	}
}
function header_visibility() {
	var page = getCookie("pagesrc");
	if (page) {
		if (page == "/games/dice.html" || page == "/games/dice2.html" || page == "/games/dice3.html" || page == "/games/mdice.html") {
			$("#mhead", window.parent.document).css("display", "none");
			$("#foot", window.parent.document).css("display", "none");
			$("#body", window.parent.document).css("position", "absolute");
			$("#body", window.parent.document).css("width", "100%");
			$("#body", window.parent.document).css("height", "100%");

		} else {
			$("#mhead", window.parent.document).css("display", "block");
			$("#foot", window.parent.document).css("display", "block");
			$("#body", window.parent.document).css("position", "");
			$("#body", window.parent.document).css("width", "");
			$("#body", window.parent.document).css("height", "");
		}
	}
}
function remove_cookie() {
	var path = window.location.pathname;
	var delete_cookie = function (name) {
		document.cookie = name + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
	};
	delete_cookie('sessionId');
	delete_cookie('pagesrc');

	if (path != "/") {
		// setTimeout('window.parent.location.href = "/"', 2000)
	}
}
function formatNumber(num) {
	return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
}
function checkUser() {
	$.post('/UserService.aspx', {
		flag: "LoginBean_Online"
	}, function (data, status) {
		// var data =  eval("(" + data + ")");
		if (data.msg != "") {
			remove_cookie();
		}
	});

}

function changeNickname() {
	layer.config({
		extend: 'extend/layer.ext.js'
	});
	layer.ready(function () {
		layer.prompt({
			title: '请输入要修改的昵称',
			formType: 0
		}, function (pass) {
			if (pass != '' || pass != null) {
				var data01 = {
					flag: "changeNickName",
					userNickname: pass
				};

				$.post("/UserService.aspx", { flag: "LoginBean_changeNickName", userNickname: pass },
					function (data, status) {
						//  var data = eval("(" + data + ")");
						if (data.msg != "") {
							alert(data.msg);
						} else {
							layer.msg('修改成功，请记住您的昵称为：' + pass);
							loadData();
						}
					});
			}
		});
	});
}
function menuclick_home(id) {

	var stitle = $("#title", window.parent.document).val();
	var spage = "";
	/*$("#mhead", window.parent.document).css("height", "")
	loadPage("mhead", "/header.html")
	loadPage("foot", "/foot.html")
	*/
	//alert(id + "2");
	switch (id) {
		// Lottery Games
		case "a1": stitle += "-Situs web resmi";
			/*loadGamePage("/page/mains/main.html", stitle);
			setPagesrc("/page/mains/main.html");
			window.parent.location.href = "/home.html"*/
			window.parent.location.href = "/pages/indexpage.html"
			break;
		case "a2": stitle += "-Situs web resmi";
			/*loadGamePage("/games/mmc.html", stitle);
			setPagesrc("/games/mmc.html");
			window.parent.location.href = "/home.html"
			window.parent.location.href = "/games/mmc.html"*/
			window.parent.location.href = "../home.html?gamename=mmc.html"
			break;
		case "a3": stitle += "-Situs web resmi";
			/*loadGamePage("/games/ssccs.html", stitle);
			setPagesrc("/games/ssccs.html");
			window.parent.location.href = "/home.html"
			window.parent.location.href = "/games/mmc.html"*/
			window.parent.location.href = "../home.html?gamename=mmc.html"
			break;
		case "a4": stitle += "-Situs web resmi";
			/*loadGamePage("/games/sscbf.html", stitle);
			setPagesrc("/games/sscbf.html");
			window.parent.location.href = "/home.html"
			window.parent.location.href = "/games/sscbf.html"*/
			window.parent.location.href = "../home.html?gamename=sscbf.html"
			break;
		case "a5": stitle += "-Situs web resmi";
			/*loadGamePage("/games/ssccq.html", stitle);
			setPagesrc("/games/ssccq.html");
			window.parent.location.href = "/home.html"
			window.parent.location.href = "/games/ssccq.html"*/
			window.parent.location.href = "../home.html?gamename=ssccq.html"
			break;
		case "a6": stitle += "-Situs web resmi";
			/*loadGamePage("/games/sscxj.html", stitle);
			setPagesrc("/games/sscxj.html");
			window.parent.location.href = "/home.html"
			window.parent.location.href = "/games/sscxj.html"*/
			window.parent.location.href = "../home.html?gamename=sscxj.html"
			break;
		case "a7": stitle += "-Situs web resmi";
			/*loadGamePage("/games/ssctj.html", stitle);
			setPagesrc("/games/ssctj.html");
			window.parent.location.href = "/home.html"
			window.parent.location.href = "/games/ssctj.html"*/
			window.parent.location.href = "../home.html?gamename=ssctj.html"
			break;
		case "a8": stitle += "-Situs web resmi";
			/*loadGamePage("/games/pk105f.html", stitle);
			setPagesrc("/games/pk105f.html");
			window.parent.location.href = "/home.html"
			window.parent.location.href = "/games/pk105f.html"*/
			window.parent.location.href = "../home.html?gamename=pk105f.html"
			break;
		case "a9": stitle += "-Situs web resmi";
			/*loadGamePage("/games/pk10.html", stitle);
			setPagesrc("/games/pk10.html");
			window.parent.location.href = "/home.html"
			window.parent.location.href = "/games/pk10.html"*/
			window.parent.location.href = "../home.html?gamename=pk10.html"
			break;
		case "a10": stitle += "-Situs web resmi";
			/*loadGamePage("/games/hg1_5.html", stitle);
			setPagesrc("/games/hg1_5.html");
			window.parent.location.href = "/home.html"
			window.parent.location.href = "/games/hg1_5.html"*/
			window.parent.location.href = "../home.html?gamename=hg1_5.html"
			break;
		case "a11": stitle += "-Situs web resmi";
			/*loadGamePage("/games/cs115.html", stitle);
			setPagesrc("/games/cs115.html");
			window.parent.location.href = "/home.html"
			window.parent.location.href = "/games/cs115.html"*/
			window.parent.location.href = "../home.html?gamename=cs115.html"
			break;
		case "a12": stitle += "-Situs web resmi";
			/*loadGamePage("/games/sd115.html", stitle);
			setPagesrc("/games/sd115.html");
			window.parent.location.href = "/home.html"
			window.parent.location.href = "/games/sd115.html"*/
			window.parent.location.href = "../home.html?gamename=sd115.html"
			break;
		case "a13": stitle += "-Situs web resmi";
			/*loadGamePage("/games/gd115.html", stitle);
			setPagesrc("/games/gd115.html");
			window.parent.location.href = "/home.html"
			window.parent.location.href = "/games/gd115.html"*/
			window.parent.location.href = "../home.html?gamename=gd115.html"
			break;
		case "a14": stitle += "-Situs web resmi";
			/*loadGamePage("/games/jx115.html", stitle);
			setPagesrc("/games/jx115.html");
			window.parent.location.href = "/home.html"
			window.parent.location.href = "/games/jx115.html"*/
			window.parent.location.href = "../home.html?gamename=jx115.html"
			break;
		case "a15": stitle += "-Situs web resmi";
			/*loadGamePage("/games/cs3d.html", stitle);
			setPagesrc("/games/cs3d.html");
			window.parent.location.href = "/home.html"
			window.parent.location.href = "/games/cs3d.html"*/
			window.parent.location.href = "../home.html?gamename=cs3d.html"
			break;
		case "a16": stitle += "-Situs web resmi";
			/*loadGamePage("/games/pl35.html", stitle);
			setPagesrc("/games/pl35.html");
			window.parent.location.href = "/home.html"
			window.parent.location.href = "/games/pl35.html"*/
			window.parent.location.href = "../home.html?gamename=pl35.html"
			break;
		case "a17": stitle += "-Situs web resmi";
			/*loadGamePage("/games/fc3d.html", stitle);
			setPagesrc("/games/fc3d.html");
			window.parent.location.href = "/home.html"
			window.parent.location.href = "/games/fc3d.html"*/
			window.parent.location.href = "../home.html?gamename=fc3d.html"
			break;
		case "a18": stitle += "-Situs web resmi";
			/*loadGamePage("/games/jz45.html", stitle);
			setPagesrc("/games/jz45.html");
			window.parent.location.href = "/home.html"
			window.parent.location.href = "/games/jz45.html"*/
			window.parent.location.href = "../home.html"
			break;
		case "a19": stitle += "-悉尼一分彩";
			/*loadGamePage("/games/sydney19.html", stitle);
			setPagesrc("/games/sydney19.html");
			window.parent.location.href = "/home.html"
			window.parent.location.href = "/games/sydney19.html"*/
			window.parent.location.href = "../home.html?gamename=sydney19.html"
			break;
		case "a20": stitle += "浦东一分彩";
			/*loadGamePage("/games/pudong20.html", stitle);
			setPagesrc("/games/pudong20.html");
			window.parent.location.href = "/home.html"
			window.parent.location.href = "/games/pudong20.html"*/
			window.parent.location.href = "../home.html?gamename=pudong20.html"
			break;
		case "a21": stitle += "荷兰1.5分彩";
			/*loadGamePage("/games/netherlands21.html", stitle);
			setPagesrc("/games/netherlands21.html");
			window.parent.location.href = "/home.html"
			window.parent.location.href = "/games/netherlands21.html"*/
			window.parent.location.href = "../home.html?gamename=netherlands21.html"
			break;
		case "a22": stitle += "澳门1.5分彩";
			/*loadGamePage("/games/macau22.html", stitle);
			setPagesrc("/games/macau22.html");
			window.parent.location.href = "/home.html"
			window.parent.location.href = "/games/macau22.html"*/
			window.parent.location.href = "../home.html?gamename=macau22.html"
			break;
		case "a23": stitle += "希腊1分彩";
			window.parent.location.href = "../home.html?gamename=greece.html"
			break;
		case "a24": stitle += "罗马1分彩";
			window.parent.location.href = "../home.html?gamename=roma.html"
			break;
		case "a25": stitle += "新加坡1.5分彩";
			window.parent.location.href = "../home.html?gamename=singapore.html"
			break;

		// Dice Games
		case "b1": stitle += "-Situs web resmi";
			/*loadGamePage("/games/dice.html", stitle);
			setPagesrc("/games/dice.html");
			window.parent.location.href = "/home.html"*/
			window.parent.location.href = "/games/dice.html"
			break;
		case "b2": stitle += "-Situs web resmi";
			/*loadGamePage("/games/dice2.html", stitle);
			setPagesrc("/games/dice2.html");
			window.parent.location.href = "/home.html"*/
			window.parent.location.href = "/games/dice2.html"
			break;
		case "b3": stitle += "-Situs web resmi";
			/*loadGamePage("/games/dice3.html", stitle);
			setPagesrc("/games/dice3.html");
			window.parent.location.href = "/home.html"*/
			window.parent.location.href = "/games/dice3.html"
			break;
		case "b4": stitle += "-Situs web resmi";
			/*loadGamePage("/games/mdice.html", stitle);
			setPagesrc("/games/mdice.html");
			window.parent.location.href = "/games/mdice.html"*/
			window.parent.location.href = "/games/mdice.html"
			break;


		// Person Pages
		case "c1": stitle += "-Situs web resmi";
			/*loadGamePage("/pages/personbank.html", stitle);
			setPagesrc("/pages/personbank.html");
			href="/pages/deposit_record.html"
			window.parent.location.href = "/pages/indexpage.html"*/
			window.parent.location.href = "../home.html?pagename=personbank.html";
			break;
		case "c2": stitle += "-Situs web resmi";
			/*loadGamePage("/page/persons/personpwd.html", stitle);
			setPagesrc("/page/persons/personpwd.html");
			window.parent.location.href = "/home.html"*/
			window.parent.location.href = "../home.html?pagename=personpwd.html"
			break;
		case "c3": stitle += "-Situs web resmi";
			/*loadGamePage("/page/persons/pricelist.html", stitle);
			setPagesrc("/page/persons/pricelist.html");
			window.parent.location.href = "/home.html"*/
			window.parent.location.href = "../home.html?pagename=pricelist.html"
			break;



		// Agent Pages
		case "d1": stitle += "-Situs web resmi";
			/*loadGamePage("/page/agents/agentreg.html", stitle);
			setPagesrc("/page/agents/agentreg.html")
			window.parent.location.href = "/home.html"*/
			window.parent.location.href = "../home.html?pagename=agentreg.html"
			break;
		case "d2": stitle += "-Situs web resmi";
			/*loadGamePage("/page/agents/agentuser.html", stitle);
			setPagesrc("/page/agents/agentuser.html");
			window.parent.location.href = "/home.html"*/
			window.parent.location.href = "../home.html?pagename=agentuser.html"
			break;
		case "d3": stitle += "-Situs web resmi";
			/*loadGamePage("/page/agents/agentautoreg.html", stitle);
			setPagesrc("/page/agents/agentautoreg.html");
			window.parent.location.href = "/home.html"*/
			window.parent.location.href = "../home.html?pagename=agentautoreg.html"
			break;
		case "d4": stitle += "-Situs web resmi";
			/*loadGamePage("/page/agents/agentpe.html", stitle);
			setPagesrc("/page/agents/agentpe.html");
			window.parent.location.href = "/home.html"*/
			window.parent.location.href = "../home.html?pagename=agentpe.html"
			break;
		case "d5": stitle += "-Situs web resmi";
			/*loadGamePage("/page/agents/agentbalance.html", stitle);
			setPagesrc("/page/agents/agentbalance.html");
			window.parent.location.href = "/home.html"*/
			window.parent.location.href = "../home.html?pagename=agentbalance.html"
			break;

		// Report Pages
		case "e1": stitle += "-Situs web resmi";
			/*loadGamePage("/page/reports/reportZB.html", stitle);
			setPagesrc("/page/reports/reportZB.html");
			window.parent.location.href = "/home.html"*/
			window.parent.location.href = "../home.html?pagename=reportzb.html"
			break;
		case "e2": stitle += "-Situs web resmi";
			/*loadGamePage("/page/reports/bonusreport.html", stitle);
			setPagesrc("/page/reports/bonusreport.html");
			window.parent.location.href = "/home.html"*/
			window.parent.location.href = "../home.html?pagename=bonusreport.html"
			break;
		case "e3": stitle += "-Situs web resmi";
			/*loadGamePage("/page/reports/reportWL.html", stitle);
			setPagesrc("/page/reports/reportWL.html");
			window.parent.location.href = "/home.html"*/
			window.parent.location.href = "../home.html?pagename=reportwl.html"
			break;
		case "e4": stitle += "-Situs web resmi";
			/*loadGamePage("/page/reports/reportTD.html", stitle);
			setPagesrc("/page/reports/reportTD.html");
			window.parent.location.href = "/home.html"*/
			window.parent.location.href = "../home.html?pagename=reporttd.html"
			break;
		case "e5": stitle += "-Situs web resmi";
			/*loadGamePage("/page/reports/reportday.html", stitle);
			setPagesrc("/page/reports/reportday.html");
			window.parent.location.href = "/home.html"*/
			window.parent.location.href = "../home.html?pagename=reportday.html"
			break;

		//
		case "g1": stitle += "-Situs web resmi";
			/*loadGamePage("/page/persons/personmsg.html", stitle);
			setPagesrc("/page/persons/personmsg.html");
			window.parent.location.href = "/home.html"*/
			window.parent.location.href = "../home.html?pagename=personmsg.html"
			break;
		case "g2": stitle += "-Situs web resmi";
			/*loadGamePage("/page/persons/indexpage.html", stitle);
			setPagesrc("/page/persons/indexpage.html");
			window.parent.location.href = "/home.html"*/
			window.parent.location.href = "../home.html?pagename=indexpage.html"
			break;
		case "g3": stitle += "-Situs web resmi";
			/*loadGamePage("/Export.html", stitle);
			setPagesrc("/Export.html");
			window.parent.location.href = "/home.html"*/
			window.parent.location.href = "/Export.html"
			break;


		case "h1": stitle += "-Situs web resmi";
			/*loadGamePage("/page/records/betrecord.html", stitle);
			setPagesrc("/page/records/betrecord.html");
			window.parent.location.href = "/home.html"*/
			window.parent.location.href = "../home.html?pagename=betrecord.html"
			break;
		case "h2": stitle += "-Situs web resmi";
			/*loadGamePage("/page/records/tracerecord.html", stitle);
			setPagesrc("/page/records/tracerecord.html");
			window.parent.location.href = "/home.html"
			window.parent.location.href = "/pages/tracerecord.html"*/
			window.parent.location.href = "../home?gamename=tracerecord.html"
			break;

		case "i1": stitle += "-Situs web resmi";
			/*loadGamePage("/page/deposits/dwdepositrecord.html", stitle);
			setPagesrc("/page/deposits/dwdepositrecord.html");
			window.parent.location.href = "/home.html"*/
			window.parent.location.href = "../home.html?pagename=deposit.html"
			break;
		case "i2": stitle += "-Situs web resmi";
			/*loadGamePage("/page/withdrawals/withdrawl.html", stitle);
			setPagesrc("/page/withdrawals/withdrawl.html");
			window.parent.location.href = "/home.html"*/
			window.parent.location.href = "../home.html?pagename=withdrawal.html"
			break;

		case "n1": stitle += "-Situs web resmi";
			/*loadGamePage("/page/notices/notice1.html", stitle);
			setPagesrc("/page/notices/notice1.html");
			window.parent.location.href = "/home.html"*/
			window.parent.location.href = "../home.html?pagename=notice1.html"
			break;

		case "z1": stitle += "-Situs web resmi";
			/*loadGamePage("/games/pk105f.html", stitle);
			setPagesrc("/games/pk105f.html");
			window.parent.location.href = "/home.html"
			window.parent.location.href = "/games/pk105f.html"*/
			window.parent.location.href = "../home.html?gamename=pk105f.html"
			break;

		case "z2": stitle += "-Situs web resmi";
			/*loadGamePage("/games/pk10.html", stitle);
			setPagesrc("/games/pk10.html");
			window.parent.location.href = "/home.html"*/
			window.parent.location.href = "../home.html?gamename=pk10.html"
			break;

		case "z3": stitle += "-Situs web resmi";
			/*loadGamePage("/games/hg1_5.html", stitle);
			setPagesrc("/games/hg1_5.html");
			window.parent.location.href = "/home.html"
			window.parent.location.href = "/games/hg1_5.html"*/
			window.parent.location.href = "../home.html?gamename=hg1_5.html"
			break;

		case "z4": stitle += "-Situs web resmi";
			/*loadGamePage("/games/jz45.html", stitle);
			setPagesrc("/games/jz45.html");
			window.parent.location.href = "/home.html"
			window.parent.location.href = "/games/jz45.html"*/
			window.parent.location.href = "../home.html?gamename=jz45.html"
			break;



		case "j1": stitle += "-六合彩【官方】";
			/*loadGamePage("/games/sixmark.html", stitle);
			setPagesrc("/games/sixmark.html");
			window.parent.location.href = "/games/sixmark.html"*/
			window.parent.location.href = "/games/sixmark.html"
			break;
		case "j2": stitle += "-六合彩【MS】";
			/*loadGamePage("/games/sixmark1f.html", stitle);
			setPagesrc("/games/sixmark1f.html");
			window.parent.location.href = "/games/sixmark1f.html"*/
			window.parent.location.href = "/games/sixmark1f.html"
			break;
	}
}

function sixmarkmenu() {
	//stitle += "-Situs web resmi";
	/*setPagesrc("../betrecord.html");
	window.open("../games/jz45.html")*/
	window.parent.location.href = "/home.html"
}

function addCommas(nStr) {
	nStr += '';
	x = nStr.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + ',' + '$2');
	}
	return x1 + x2;
}
