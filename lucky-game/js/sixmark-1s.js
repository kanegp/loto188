!
	function (t, e) {
		"object" == typeof module && "object" == typeof module.exports ? module.exports = t.document ? e(t, !0) : function (t) {
			if (!t.document) throw new Error("jQuery requires a window with a document");
			return e(t)
		} : e(t)
	}("undefined" != typeof window ? window : this, function (t, e) {
		function n(t) {
			var e = !!t && "length" in t && t.length,
				n = rt.type(t);
			return "function" === n || rt.isWindow(t) ? !1 : "array" === n || 0 === e || "number" == typeof e && e > 0 && e - 1 in t
		}
		function i(t, e, n) {
			if (rt.isFunction(e)) return rt.grep(t, function (t, i) {
				return !!e.call(t, i, t) !== n
			});
			if (e.nodeType) return rt.grep(t, function (t) {
				return t === e !== n
			});
			if ("string" == typeof e) {
				if (ht.test(e)) return rt.filter(e, t, n);
				e = rt.filter(e, t)
			}
			return rt.grep(t, function (t) {
				return J.call(e, t) > -1 !== n
			})
		}
		function o(t, e) {
			for (;
				(t = t[e]) && 1 !== t.nodeType;);
			return t
		}
		function r(t) {
			var e = {};
			return rt.each(t.match(xt) || [], function (t, n) {
				e[n] = !0
			}), e
		}
		function a() {
			U.removeEventListener("DOMContentLoaded", a), t.removeEventListener("load", a), rt.ready()
		}
		function l() {
			this.expando = rt.expando + l.uid++
		}
		function s(t, e, n) {
			var i;
			if (void 0 === n && 1 === t.nodeType) if (i = "data-" + e.replace($t, "-$&").toLowerCase(), n = t.getAttribute(i), "string" == typeof n) {
				try {
					n = "true" === n ? !0 : "false" === n ? !1 : "null" === n ? null : +n + "" === n ? +n : St.test(n) ? rt.parseJSON(n) : n
				} catch (o) { }
				jt.set(t, e, n)
			} else n = void 0;
			return n
		}
		function d(t, e, n, i) {
			var o, r = 1,
				a = 20,
				l = i ?
					function () {
						return i.cur()
					} : function () {
						return rt.css(t, e, "")
					}, s = l(), d = n && n[3] || (rt.cssNumber[e] ? "" : "px"), c = (rt.cssNumber[e] || "px" !== d && +s) && Nt.exec(rt.css(t, e));
			if (c && c[3] !== d) {
				d = d || c[3], n = n || [], c = +s || 1;
				do r = r || ".5", c /= r, rt.style(t, e, c + d);
				while (r !== (r = l() / s) && 1 !== r && --a)
			}
			return n && (c = +c || +s || 0, o = n[1] ? c + (n[1] + 1) * n[2] : +n[2], i && (i.unit = d, i.start = c, i.end = o)), o
		}
		function c(t, e) {
			var n = "undefined" != typeof t.getElementsByTagName ? t.getElementsByTagName(e || "*") : "undefined" != typeof t.querySelectorAll ? t.querySelectorAll(e || "*") : [];
			return void 0 === e || e && rt.nodeName(t, e) ? rt.merge([t], n) : n
		}
		function p(t, e) {
			for (var n = 0, i = t.length; i > n; n++) kt.set(t[n], "globalEval", !e || kt.get(e[n], "globalEval"))
		}
		function u(t, e, n, i, o) {
			for (var r, a, l, s, d, u, g = e.createDocumentFragment(), f = [], h = 0, m = t.length; m > h; h++) if (r = t[h], r || 0 === r) if ("object" === rt.type(r)) rt.merge(f, r.nodeType ? [r] : r);
			else if (Pt.test(r)) {
				for (a = a || g.appendChild(e.createElement("div")), l = (Ht.exec(r) || ["", ""])[1].toLowerCase(), s = Ot[l] || Ot._default, a.innerHTML = s[1] + rt.htmlPrefilter(r) + s[2], u = s[0]; u--;) a = a.lastChild;
				rt.merge(f, a.childNodes), a = g.firstChild, a.textContent = ""
			} else f.push(e.createTextNode(r));
			for (g.textContent = "", h = 0; r = f[h++];) if (i && rt.inArray(r, i) > -1) o && o.push(r);
			else if (d = rt.contains(r.ownerDocument, r), a = c(g.appendChild(r), "script"), d && p(a), n) for (u = 0; r = a[u++];) Lt.test(r.type || "") && n.push(r);
			return g
		}
		function g() {
			return !0
		}
		function f() {
			return !1
		}
		function h() {
			try {
				return U.activeElement
			} catch (t) { }
		}
		function m(t, e, n, i, o, r) {
			var a, l;
			if ("object" == typeof e) {
				"string" != typeof n && (i = i || n, n = void 0);
				for (l in e) m(t, l, n, i, e[l], r);
				return t
			}
			if (null == i && null == o ? (o = n, i = n = void 0) : null == o && ("string" == typeof n ? (o = i, i = void 0) : (o = i, i = n, n = void 0)), o === !1) o = f;
			else if (!o) return t;
			return 1 === r && (a = o, o = function (t) {
				return rt().off(t), a.apply(this, arguments)
			}, o.guid = a.guid || (a.guid = rt.guid++)), t.each(function () {
				rt.event.add(this, e, o, i, n)
			})
		}
		function v(t, e) {
			return rt.nodeName(t, "table") && rt.nodeName(11 !== e.nodeType ? e : e.firstChild, "tr") ? t.getElementsByTagName("tbody")[0] || t.appendChild(t.ownerDocument.createElement("tbody")) : t
		}
		function b(t) {
			return t.type = (null !== t.getAttribute("type")) + "/" + t.type, t
		}
		function y(t) {
			var e = Xt.exec(t.type);
			return e ? t.type = e[1] : t.removeAttribute("type"), t
		}
		function w(t, e) {
			var n, i, o, r, a, l, s, d;
			if (1 === e.nodeType) {
				if (kt.hasData(t) && (r = kt.access(t), a = kt.set(e, r), d = r.events)) {
					delete a.handle, a.events = {};
					for (o in d) for (n = 0, i = d[o].length; i > n; n++) rt.event.add(e, o, d[o][n])
				}
				jt.hasData(t) && (l = jt.access(t), s = rt.extend({}, l), jt.set(e, s))
			}
		}
		function x(t, e) {
			var n = e.nodeName.toLowerCase();
			"input" === n && Et.test(t.type) ? e.checked = t.checked : "input" !== n && "textarea" !== n || (e.defaultValue = t.defaultValue)
		}
		function _(t, e, n, i) {
			e = Z.apply([], e);
			var o, r, a, l, s, d, p = 0,
				g = t.length,
				f = g - 1,
				h = e[0],
				m = rt.isFunction(h);
			if (m || g > 1 && "string" == typeof h && !it.checkClone && Rt.test(h)) return t.each(function (o) {
				var r = t.eq(o);
				m && (e[0] = h.call(this, o, r.html())), _(r, e, n, i)
			});
			if (g && (o = u(e, t[0].ownerDocument, !1, t, i), r = o.firstChild, 1 === o.childNodes.length && (o = r), r || i)) {
				for (a = rt.map(c(o, "script"), b), l = a.length; g > p; p++) s = o, p !== f && (s = rt.clone(s, !0, !0), l && rt.merge(a, c(s, "script"))), n.call(t[p], s, p);
				if (l) for (d = a[a.length - 1].ownerDocument, rt.map(a, y), p = 0; l > p; p++) s = a[p], Lt.test(s.type || "") && !kt.access(s, "globalEval") && rt.contains(d, s) && (s.src ? rt._evalUrl && rt._evalUrl(s.src) : rt.globalEval(s.textContent.replace(zt, "")))
			}
			return t
		}
		function C(t, e, n) {
			for (var i, o = e ? rt.filter(e, t) : t, r = 0; null != (i = o[r]); r++) n || 1 !== i.nodeType || rt.cleanData(c(i)), i.parentNode && (n && rt.contains(i.ownerDocument, i) && p(c(i, "script")), i.parentNode.removeChild(i));
			return t
		}
		function T(t, e) {
			var n = rt(e.createElement(t)).appendTo(e.body),
				i = rt.css(n[0], "display");
			return n.detach(), i
		}
		function k(t) {
			var e = U,
				n = Vt[t];
			return n || (n = T(t, e), "none" !== n && n || (Yt = (Yt || rt("<iframe frameborder='0' width='0' height='0'/>")).appendTo(e.documentElement), e = Yt[0].contentDocument, e.write(), e.close(), n = T(t, e), Yt.detach()), Vt[t] = n), n
		}
		function j(t, e, n) {
			var i, o, r, a, l = t.style;
			return n = n || Kt(t), a = n ? n.getPropertyValue(e) || n[e] : void 0, "" !== a && void 0 !== a || rt.contains(t.ownerDocument, t) || (a = rt.style(t, e)), n && !it.pixelMarginRight() && Ut.test(a) && Gt.test(e) && (i = l.width, o = l.minWidth, r = l.maxWidth, l.minWidth = l.maxWidth = l.width = a, a = n.width, l.width = i, l.minWidth = o, l.maxWidth = r), void 0 !== a ? a + "" : a
		}
		function S(t, e) {
			return {
				get: function () {
					return t() ? void delete this.get : (this.get = e).apply(this, arguments)
				}
			}
		}
		function $(t) {
			if (t in ie) return t;
			for (var e = t[0].toUpperCase() + t.slice(1), n = ne.length; n--;) if (t = ne[n] + e, t in ie) return t
		}
		function D(t, e, n) {
			var i = Nt.exec(e);
			return i ? Math.max(0, i[2] - (n || 0)) + (i[3] || "px") : e
		}
		function N(t, e, n, i, o) {
			for (var r = n === (i ? "border" : "content") ? 4 : "width" === e ? 1 : 0, a = 0; 4 > r; r += 2)"margin" === n && (a += rt.css(t, n + Bt[r], !0, o)), i ? ("content" === n && (a -= rt.css(t, "padding" + Bt[r], !0, o)), "margin" !== n && (a -= rt.css(t, "border" + Bt[r] + "Width", !0, o))) : (a += rt.css(t, "padding" + Bt[r], !0, o), "padding" !== n && (a += rt.css(t, "border" + Bt[r] + "Width", !0, o)));
			return a
		}
		function B(e, n, i) {
			var o = !0,
				r = "width" === n ? e.offsetWidth : e.offsetHeight,
				a = Kt(e),
				l = "border-box" === rt.css(e, "boxSizing", !1, a);
			if (U.msFullscreenElement && t.top !== t && e.getClientRects().length && (r = Math.round(100 * e.getBoundingClientRect()[n])), 0 >= r || null == r) {
				if (r = j(e, n, a), (0 > r || null == r) && (r = e.style[n]), Ut.test(r)) return r;
				o = l && (it.boxSizingReliable() || r === e.style[n]), r = parseFloat(r) || 0
			}
			return r + N(e, n, i || (l ? "border" : "content"), o, a) + "px"
		}
		function A(t, e) {
			for (var n, i, o, r = [], a = 0, l = t.length; l > a; a++) i = t[a], i.style && (r[a] = kt.get(i, "olddisplay"), n = i.style.display, e ? (r[a] || "none" !== n || (i.style.display = ""), "" === i.style.display && At(i) && (r[a] = kt.access(i, "olddisplay", k(i.nodeName)))) : (o = At(i), "none" === n && o || kt.set(i, "olddisplay", o ? n : rt.css(i, "display"))));
			for (a = 0; l > a; a++) i = t[a], i.style && (e && "none" !== i.style.display && "" !== i.style.display || (i.style.display = e ? r[a] || "" : "none"));
			return t
		}
		function E(t, e, n, i, o) {
			return new E.prototype.init(t, e, n, i, o)
		}
		function H() {
			return t.setTimeout(function () {
				oe = void 0
			}), oe = rt.now()
		}
		function L(t, e) {
			var n, i = 0,
				o = {
					height: t
				};
			for (e = e ? 1 : 0; 4 > i; i += 2 - e) n = Bt[i], o["margin" + n] = o["padding" + n] = t;
			return e && (o.opacity = o.width = t), o
		}
		function O(t, e, n) {
			for (var i, o = (M.tweeners[e] || []).concat(M.tweeners["*"]), r = 0, a = o.length; a > r; r++) if (i = o[r].call(n, e, t)) return i
		}
		function P(t, e, n) {
			var i, o, r, a, l, s, d, c, p = this,
				u = {},
				g = t.style,
				f = t.nodeType && At(t),
				h = kt.get(t, "fxshow");
			n.queue || (l = rt._queueHooks(t, "fx"), null == l.unqueued && (l.unqueued = 0, s = l.empty.fire, l.empty.fire = function () {
				l.unqueued || s()
			}), l.unqueued++ , p.always(function () {
				p.always(function () {
					l.unqueued-- , rt.queue(t, "fx").length || l.empty.fire()
				})
			})), 1 === t.nodeType && ("height" in e || "width" in e) && (n.overflow = [g.overflow, g.overflowX, g.overflowY], d = rt.css(t, "display"), c = "none" === d ? kt.get(t, "olddisplay") || k(t.nodeName) : d, "inline" === c && "none" === rt.css(t, "float") && (g.display = "inline-block")), n.overflow && (g.overflow = "hidden", p.always(function () {
				g.overflow = n.overflow[0], g.overflowX = n.overflow[1], g.overflowY = n.overflow[2]
			}));
			for (i in e) if (o = e[i], ae.exec(o)) {
				if (delete e[i], r = r || "toggle" === o, o === (f ? "hide" : "show")) {
					if ("show" !== o || !h || void 0 === h[i]) continue;
					f = !0
				}
				u[i] = h && h[i] || rt.style(t, i)
			} else d = void 0;
			if (rt.isEmptyObject(u)) "inline" === ("none" === d ? k(t.nodeName) : d) && (g.display = d);
			else {
				h ? "hidden" in h && (f = h.hidden) : h = kt.access(t, "fxshow", {}), r && (h.hidden = !f), f ? rt(t).show() : p.done(function () {
					rt(t).hide()
				}), p.done(function () {
					var e;
					kt.remove(t, "fxshow");
					for (e in u) rt.style(t, e, u[e])
				});
				for (i in u) a = O(f ? h[i] : 0, i, p), i in h || (h[i] = a.start, f && (a.end = a.start, a.start = "width" === i || "height" === i ? 1 : 0))
			}
		}
		function I(t, e) {
			var n, i, o, r, a;
			for (n in t) if (i = rt.camelCase(n), o = e[i], r = t[n], rt.isArray(r) && (o = r[1], r = t[n] = r[0]), n !== i && (t[i] = r, delete t[n]), a = rt.cssHooks[i], a && "expand" in a) {
				r = a.expand(r), delete t[i];
				for (n in r) n in t || (t[n] = r[n], e[n] = o)
			} else e[i] = o
		}
		function M(t, e, n) {
			var i, o, r = 0,
				a = M.prefilters.length,
				l = rt.Deferred().always(function () {
					delete s.elem
				}),
				s = function () {
					if (o) return !1;
					for (var e = oe || H(), n = Math.max(0, d.startTime + d.duration - e), i = n / d.duration || 0, r = 1 - i, a = 0, s = d.tweens.length; s > a; a++) d.tweens[a].run(r);
					return l.notifyWith(t, [d, r, n]), 1 > r && s ? n : (l.resolveWith(t, [d]), !1)
				},
				d = l.promise({
					elem: t,
					props: rt.extend({}, e),
					opts: rt.extend(!0, {
						specialEasing: {},
						easing: rt.easing._default
					}, n),
					originalProperties: e,
					originalOptions: n,
					startTime: oe || H(),
					duration: n.duration,
					tweens: [],
					createTween: function (e, n) {
						var i = rt.Tween(t, d.opts, e, n, d.opts.specialEasing[e] || d.opts.easing);
						return d.tweens.push(i), i
					},
					stop: function (e) {
						var n = 0,
							i = e ? d.tweens.length : 0;
						if (o) return this;
						for (o = !0; i > n; n++) d.tweens[n].run(1);
						return e ? (l.notifyWith(t, [d, 1, 0]), l.resolveWith(t, [d, e])) : l.rejectWith(t, [d, e]), this
					}
				}),
				c = d.props;
			for (I(c, d.opts.specialEasing); a > r; r++) if (i = M.prefilters[r].call(d, t, c, d.opts)) return rt.isFunction(i.stop) && (rt._queueHooks(d.elem, d.opts.queue).stop = rt.proxy(i.stop, i)), i;
			return rt.map(c, O, d), rt.isFunction(d.opts.start) && d.opts.start.call(t, d), rt.fx.timer(rt.extend(s, {
				elem: t,
				anim: d,
				queue: d.opts.queue
			})), d.progress(d.opts.progress).done(d.opts.done, d.opts.complete).fail(d.opts.fail).always(d.opts.always)
		}
		function F(t) {
			return t.getAttribute && t.getAttribute("class") || ""
		}
		function W(t) {
			return function (e, n) {
				"string" != typeof e && (n = e, e = "*");
				var i, o = 0,
					r = e.toLowerCase().match(xt) || [];
				if (rt.isFunction(n)) for (; i = r[o++];)"+" === i[0] ? (i = i.slice(1) || "*", (t[i] = t[i] || []).unshift(n)) : (t[i] = t[i] || []).push(n)
			}
		}
		function q(t, e, n, i) {
			function o(l) {
				var s;
				return r[l] = !0, rt.each(t[l] || [], function (t, l) {
					var d = l(e, n, i);
					return "string" != typeof d || a || r[d] ? a ? !(s = d) : void 0 : (e.dataTypes.unshift(d), o(d), !1)
				}), s
			}
			var r = {},
				a = t === je;
			return o(e.dataTypes[0]) || !r["*"] && o("*")
		}
		function R(t, e) {
			var n, i, o = rt.ajaxSettings.flatOptions || {};
			for (n in e) void 0 !== e[n] && ((o[n] ? t : i || (i = {}))[n] = e[n]);
			return i && rt.extend(!0, t, i), t
		}
		function X(t, e, n) {
			for (var i, o, r, a, l = t.contents, s = t.dataTypes;
				"*" === s[0];) s.shift(), void 0 === i && (i = t.mimeType || e.getResponseHeader("Content-Type"));
			if (i) for (o in l) if (l[o] && l[o].test(i)) {
				s.unshift(o);
				break
			}
			if (s[0] in n) r = s[0];
			else {
				for (o in n) {
					if (!s[0] || t.converters[o + " " + s[0]]) {
						r = o;
						break
					}
					a || (a = o)
				}
				r = r || a
			}
			return r ? (r !== s[0] && s.unshift(r), n[r]) : void 0
		}
		function z(t, e, n, i) {
			var o, r, a, l, s, d = {},
				c = t.dataTypes.slice();
			if (c[1]) for (a in t.converters) d[a.toLowerCase()] = t.converters[a];
			for (r = c.shift(); r;) if (t.responseFields[r] && (n[t.responseFields[r]] = e), !s && i && t.dataFilter && (e = t.dataFilter(e, t.dataType)), s = r, r = c.shift()) if ("*" === r) r = s;
			else if ("*" !== s && s !== r) {
				if (a = d[s + " " + r] || d["* " + r], !a) for (o in d) if (l = o.split(" "), l[1] === r && (a = d[s + " " + l[0]] || d["* " + l[0]])) {
					a === !0 ? a = d[o] : d[o] !== !0 && (r = l[0], c.unshift(l[1]));
					break
				}
				if (a !== !0) if (a && t["throws"]) e = a(e);
				else try {
					e = a(e)
				} catch (p) {
					return {
						state: "parsererror",
						error: a ? p : "No conversion from " + s + " to " + r
					}
				}
			}
			return {
				state: "success",
				data: e
			}
		}
		function Y(t, e, n, i) {
			var o;
			if (rt.isArray(e)) rt.each(e, function (e, o) {
				n || Ne.test(t) ? i(t, o) : Y(t + "[" + ("object" == typeof o && null != o ? e : "") + "]", o, n, i)
			});
			else if (n || "object" !== rt.type(e)) i(t, e);
			else for (o in e) Y(t + "[" + o + "]", e[o], n, i)
		}
		function V(t) {
			return rt.isWindow(t) ? t : 9 === t.nodeType && t.defaultView
		}
		var G = [],
			U = t.document,
			K = G.slice,
			Z = G.concat,
			Q = G.push,
			J = G.indexOf,
			tt = {},
			et = tt.toString,
			nt = tt.hasOwnProperty,
			it = {},
			ot = "2.2.3",
			rt = function (t, e) {
				return new rt.fn.init(t, e)
			},
			at = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,
			lt = /^-ms-/,
			st = /-([\da-z])/gi,
			dt = function (t, e) {
				return e.toUpperCase()
			};
		rt.fn = rt.prototype = {
			jquery: ot,
			constructor: rt,
			selector: "",
			length: 0,
			toArray: function () {
				return K.call(this)
			},
			get: function (t) {
				return null != t ? 0 > t ? this[t + this.length] : this[t] : K.call(this)
			},
			pushStack: function (t) {
				var e = rt.merge(this.constructor(), t);
				return e.prevObject = this, e.context = this.context, e
			},
			each: function (t) {
				return rt.each(this, t)
			},
			map: function (t) {
				return this.pushStack(rt.map(this, function (e, n) {
					return t.call(e, n, e)
				}))
			},
			slice: function () {
				return this.pushStack(K.apply(this, arguments))
			},
			first: function () {
				return this.eq(0)
			},
			last: function () {
				return this.eq(-1)
			},
			eq: function (t) {
				var e = this.length,
					n = +t + (0 > t ? e : 0);
				return this.pushStack(n >= 0 && e > n ? [this[n]] : [])
			},
			end: function () {
				return this.prevObject || this.constructor()
			},
			push: Q,
			sort: G.sort,
			splice: G.splice
		}, rt.extend = rt.fn.extend = function () {
			var t, e, n, i, o, r, a = arguments[0] || {},
				l = 1,
				s = arguments.length,
				d = !1;
			for ("boolean" == typeof a && (d = a, a = arguments[l] || {}, l++), "object" == typeof a || rt.isFunction(a) || (a = {}), l === s && (a = this, l--); s > l; l++) if (null != (t = arguments[l])) for (e in t) n = a[e], i = t[e], a !== i && (d && i && (rt.isPlainObject(i) || (o = rt.isArray(i))) ? (o ? (o = !1, r = n && rt.isArray(n) ? n : []) : r = n && rt.isPlainObject(n) ? n : {}, a[e] = rt.extend(d, r, i)) : void 0 !== i && (a[e] = i));
			return a
		}, rt.extend({
			expando: "jQuery" + (ot + Math.random()).replace(/\D/g, ""),
			isReady: !0,
			error: function (t) {
				throw new Error(t)
			},
			noop: function () { },
			isFunction: function (t) {
				return "function" === rt.type(t)
			},
			isArray: Array.isArray,
			isWindow: function (t) {
				return null != t && t === t.window
			},
			isNumeric: function (t) {
				var e = t && t.toString();
				return !rt.isArray(t) && e - parseFloat(e) + 1 >= 0
			},
			isPlainObject: function (t) {
				var e;
				if ("object" !== rt.type(t) || t.nodeType || rt.isWindow(t)) return !1;
				if (t.constructor && !nt.call(t, "constructor") && !nt.call(t.constructor.prototype || {}, "isPrototypeOf")) return !1;
				for (e in t);
				return void 0 === e || nt.call(t, e)
			},
			isEmptyObject: function (t) {
				var e;
				for (e in t) return !1;
				return !0
			},
			type: function (t) {
				return null == t ? t + "" : "object" == typeof t || "function" == typeof t ? tt[et.call(t)] || "object" : typeof t
			},
			globalEval: function (t) {
				var e, n = eval;
				t = rt.trim(t), t && (1 === t.indexOf("use strict") ? (e = U.createElement("script"), e.text = t, U.head.appendChild(e).parentNode.removeChild(e)) : n(t))
			},
			camelCase: function (t) {
				return t.replace(lt, "ms-").replace(st, dt)
			},
			nodeName: function (t, e) {
				return t.nodeName && t.nodeName.toLowerCase() === e.toLowerCase()
			},
			each: function (t, e) {
				var i, o = 0;
				if (n(t)) for (i = t.length; i > o && e.call(t[o], o, t[o]) !== !1; o++);
				else for (o in t) if (e.call(t[o], o, t[o]) === !1) break;
				return t
			},
			trim: function (t) {
				return null == t ? "" : (t + "").replace(at, "")
			},
			makeArray: function (t, e) {
				var i = e || [];
				return null != t && (n(Object(t)) ? rt.merge(i, "string" == typeof t ? [t] : t) : Q.call(i, t)), i
			},
			inArray: function (t, e, n) {
				return null == e ? -1 : J.call(e, t, n)
			},
			merge: function (t, e) {
				for (var n = +e.length, i = 0, o = t.length; n > i; i++) t[o++] = e[i];
				return t.length = o, t
			},
			grep: function (t, e, n) {
				for (var i, o = [], r = 0, a = t.length, l = !n; a > r; r++) i = !e(t[r], r), i !== l && o.push(t[r]);
				return o
			},
			map: function (t, e, i) {
				var o, r, a = 0,
					l = [];
				if (n(t)) for (o = t.length; o > a; a++) r = e(t[a], a, i), null != r && l.push(r);
				else for (a in t) r = e(t[a], a, i), null != r && l.push(r);
				return Z.apply([], l)
			},
			guid: 1,
			proxy: function (t, e) {
				var n, i, o;
				return "string" == typeof e && (n = t[e], e = t, t = n), rt.isFunction(t) ? (i = K.call(arguments, 2), o = function () {
					return t.apply(e || this, i.concat(K.call(arguments)))
				}, o.guid = t.guid = t.guid || rt.guid++ , o) : void 0
			},
			now: Date.now,
			support: it
		}), "function" == typeof Symbol && (rt.fn[Symbol.iterator] = G[Symbol.iterator]), rt.each("Boolean Number String Function Array Date RegExp Object Error Symbol".split(" "), function (t, e) {
			tt["[object " + e + "]"] = e.toLowerCase()
		});
		var ct = function (t) {
			function e(t, e, n, i) {
				var o, r, a, l, s, d, p, g, f = e && e.ownerDocument,
					h = e ? e.nodeType : 9;
				if (n = n || [], "string" != typeof t || !t || 1 !== h && 9 !== h && 11 !== h) return n;
				if (!i && ((e ? e.ownerDocument || e : F) !== A && B(e), e = e || A, H)) {
					if (11 !== h && (d = vt.exec(t))) if (o = d[1]) {
						if (9 === h) {
							if (!(a = e.getElementById(o))) return n;
							if (a.id === o) return n.push(a), n
						} else if (f && (a = f.getElementById(o)) && I(e, a) && a.id === o) return n.push(a), n
					} else {
						if (d[2]) return Q.apply(n, e.getElementsByTagName(t)), n;
						if ((o = d[3]) && x.getElementsByClassName && e.getElementsByClassName) return Q.apply(n, e.getElementsByClassName(o)), n
					}
					if (x.qsa && !z[t + " "] && (!L || !L.test(t))) {
						if (1 !== h) f = e, g = t;
						else if ("object" !== e.nodeName.toLowerCase()) {
							for ((l = e.getAttribute("id")) ? l = l.replace(yt, "\\$&") : e.setAttribute("id", l = M), p = k(t), r = p.length, s = ut.test(l) ? "#" + l : "[id='" + l + "']"; r--;) p[r] = s + " " + u(p[r]);
							g = p.join(","), f = bt.test(t) && c(e.parentNode) || e
						}
						if (g) try {
							return Q.apply(n, f.querySelectorAll(g)), n
						} catch (m) { } finally {
								l === M && e.removeAttribute("id")
							}
					}
				}
				return S(t.replace(lt, "$1"), e, n, i)
			}
			function n() {
				function t(n, i) {
					return e.push(n + " ") > _.cacheLength && delete t[e.shift()], t[n + " "] = i
				}
				var e = [];
				return t
			}
			function i(t) {
				return t[M] = !0, t
			}
			function o(t) {
				var e = A.createElement("div");
				try {
					return !!t(e)
				} catch (n) {
					return !1
				} finally {
					e.parentNode && e.parentNode.removeChild(e), e = null
				}
			}
			function r(t, e) {
				for (var n = t.split("|"), i = n.length; i--;) _.attrHandle[n[i]] = e
			}
			function a(t, e) {
				var n = e && t,
					i = n && 1 === t.nodeType && 1 === e.nodeType && (~e.sourceIndex || V) - (~t.sourceIndex || V);
				if (i) return i;
				if (n) for (; n = n.nextSibling;) if (n === e) return -1;
				return t ? 1 : -1
			}
			function l(t) {
				return function (e) {
					var n = e.nodeName.toLowerCase();
					return "input" === n && e.type === t
				}
			}
			function s(t) {
				return function (e) {
					var n = e.nodeName.toLowerCase();
					return ("input" === n || "button" === n) && e.type === t
				}
			}
			function d(t) {
				return i(function (e) {
					return e = +e, i(function (n, i) {
						for (var o, r = t([], n.length, e), a = r.length; a--;) n[o = r[a]] && (n[o] = !(i[o] = n[o]))
					})
				})
			}
			function c(t) {
				return t && "undefined" != typeof t.getElementsByTagName && t
			}
			function p() { }
			function u(t) {
				for (var e = 0, n = t.length, i = ""; n > e; e++) i += t[e].value;
				return i
			}
			function g(t, e, n) {
				var i = e.dir,
					o = n && "parentNode" === i,
					r = q++;
				return e.first ?
					function (e, n, r) {
						for (; e = e[i];) if (1 === e.nodeType || o) return t(e, n, r)
					} : function (e, n, a) {
						var l, s, d, c = [W, r];
						if (a) {
							for (; e = e[i];) if ((1 === e.nodeType || o) && t(e, n, a)) return !0
						} else for (; e = e[i];) if (1 === e.nodeType || o) {
							if (d = e[M] || (e[M] = {}), s = d[e.uniqueID] || (d[e.uniqueID] = {}), (l = s[i]) && l[0] === W && l[1] === r) return c[2] = l[2];
							if (s[i] = c, c[2] = t(e, n, a)) return !0
						}
					}
			}
			function f(t) {
				return t.length > 1 ?
					function (e, n, i) {
						for (var o = t.length; o--;) if (!t[o](e, n, i)) return !1;
						return !0
					} : t[0]
			}
			function h(t, n, i) {
				for (var o = 0, r = n.length; r > o; o++) e(t, n[o], i);
				return i
			}
			function m(t, e, n, i, o) {
				for (var r, a = [], l = 0, s = t.length, d = null != e; s > l; l++)(r = t[l]) && (n && !n(r, i, o) || (a.push(r), d && e.push(l)));
				return a
			}
			function v(t, e, n, o, r, a) {
				return o && !o[M] && (o = v(o)), r && !r[M] && (r = v(r, a)), i(function (i, a, l, s) {
					var d, c, p, u = [],
						g = [],
						f = a.length,
						v = i || h(e || "*", l.nodeType ? [l] : l, []),
						b = !t || !i && e ? v : m(v, u, t, l, s),
						y = n ? r || (i ? t : f || o) ? [] : a : b;
					if (n && n(b, y, l, s), o) for (d = m(y, g), o(d, [], l, s), c = d.length; c--;)(p = d[c]) && (y[g[c]] = !(b[g[c]] = p));
					if (i) {
						if (r || t) {
							if (r) {
								for (d = [], c = y.length; c--;)(p = y[c]) && d.push(b[c] = p);
								r(null, y = [], d, s)
							}
							for (c = y.length; c--;)(p = y[c]) && (d = r ? tt(i, p) : u[c]) > -1 && (i[d] = !(a[d] = p))
						}
					} else y = m(y === a ? y.splice(f, y.length) : y), r ? r(null, a, y, s) : Q.apply(a, y)
				})
			}
			function b(t) {
				for (var e, n, i, o = t.length, r = _.relative[t[0].type], a = r || _.relative[" "], l = r ? 1 : 0, s = g(function (t) {
					return t === e
				}, a, !0), d = g(function (t) {
					return tt(e, t) > -1
				}, a, !0), c = [function (t, n, i) {
					var o = !r && (i || n !== $) || ((e = n).nodeType ? s(t, n, i) : d(t, n, i));
					return e = null, o
				}]; o > l; l++) if (n = _.relative[t[l].type]) c = [g(f(c), n)];
					else {
						if (n = _.filter[t[l].type].apply(null, t[l].matches), n[M]) {
							for (i = ++l; o > i && !_.relative[t[i].type]; i++);
							return v(l > 1 && f(c), l > 1 && u(t.slice(0, l - 1).concat({
								value: " " === t[l - 2].type ? "*" : ""
							})).replace(lt, "$1"), n, i > l && b(t.slice(l, i)), o > i && b(t = t.slice(i)), o > i && u(t))
						}
						c.push(n)
					}
				return f(c)
			}
			function y(t, n) {
				var o = n.length > 0,
					r = t.length > 0,
					a = function (i, a, l, s, d) {
						var c, p, u, g = 0,
							f = "0",
							h = i && [],
							v = [],
							b = $,
							y = i || r && _.find.TAG("*", d),
							w = W += null == b ? 1 : Math.random() || .1,
							x = y.length;
						for (d && ($ = a === A || a || d); f !== x && null != (c = y[f]); f++) {
							if (r && c) {
								for (p = 0, a || c.ownerDocument === A || (B(c), l = !H); u = t[p++];) if (u(c, a || A, l)) {
									s.push(c);
									break
								}
								d && (W = w)
							}
							o && ((c = !u && c) && g-- , i && h.push(c))
						}
						if (g += f, o && f !== g) {
							for (p = 0; u = n[p++];) u(h, v, a, l);
							if (i) {
								if (g > 0) for (; f--;) h[f] || v[f] || (v[f] = K.call(s));
								v = m(v)
							}
							Q.apply(s, v), d && !i && v.length > 0 && g + n.length > 1 && e.uniqueSort(s)
						}
						return d && (W = w, $ = b), h
					};
				return o ? i(a) : a
			}
			var w, x, _, C, T, k, j, S, $, D, N, B, A, E, H, L, O, P, I, M = "sizzle" + 1 * new Date,
				F = t.document,
				W = 0,
				q = 0,
				R = n(),
				X = n(),
				z = n(),
				Y = function (t, e) {
					return t === e && (N = !0), 0
				},
				V = 1 << 31,
				G = {}.hasOwnProperty,
				U = [],
				K = U.pop,
				Z = U.push,
				Q = U.push,
				J = U.slice,
				tt = function (t, e) {
					for (var n = 0, i = t.length; i > n; n++) if (t[n] === e) return n;
					return -1
				},
				et = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",
				nt = "[\\x20\\t\\r\\n\\f]",
				it = "(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+",
				ot = "\\[" + nt + "*(" + it + ")(?:" + nt + "*([*^$|!~]?=)" + nt + "*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + it + "))|)" + nt + "*\\]",
				rt = ":(" + it + ")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|" + ot + ")*)|.*)\\)|)",
				at = new RegExp(nt + "+", "g"),
				lt = new RegExp("^" + nt + "+|((?:^|[^\\\\])(?:\\\\.)*)" + nt + "+$", "g"),
				st = new RegExp("^" + nt + "*," + nt + "*"),
				dt = new RegExp("^" + nt + "*([>+~]|" + nt + ")" + nt + "*"),
				ct = new RegExp("=" + nt + "*([^\\]'\"]*?)" + nt + "*\\]", "g"),
				pt = new RegExp(rt),
				ut = new RegExp("^" + it + "$"),
				gt = {
					ID: new RegExp("^#(" + it + ")"),
					CLASS: new RegExp("^\\.(" + it + ")"),
					TAG: new RegExp("^(" + it + "|[*])"),
					ATTR: new RegExp("^" + ot),
					PSEUDO: new RegExp("^" + rt),
					CHILD: new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + nt + "*(even|odd|(([+-]|)(\\d*)n|)" + nt + "*(?:([+-]|)" + nt + "*(\\d+)|))" + nt + "*\\)|)", "i"),
					bool: new RegExp("^(?:" + et + ")$", "i"),
					needsContext: new RegExp("^" + nt + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + nt + "*((?:-\\d)?\\d*)" + nt + "*\\)|)(?=[^-]|$)", "i")
				},
				ft = /^(?:input|select|textarea|button)$/i,
				ht = /^h\d$/i,
				mt = /^[^{]+\{\s*\[native \w/,
				vt = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,
				bt = /[+~]/,
				yt = /'|\\/g,
				wt = new RegExp("\\\\([\\da-f]{1,6}" + nt + "?|(" + nt + ")|.)", "ig"),
				xt = function (t, e, n) {
					var i = "0x" + e - 65536;
					return i !== i || n ? e : 0 > i ? String.fromCharCode(i + 65536) : String.fromCharCode(i >> 10 | 55296, 1023 & i | 56320)
				},
				_t = function () {
					B()
				};
			try {
				Q.apply(U = J.call(F.childNodes), F.childNodes), U[F.childNodes.length].nodeType
			} catch (Ct) {
				Q = {
					apply: U.length ?
						function (t, e) {
							Z.apply(t, J.call(e))
						} : function (t, e) {
							for (var n = t.length, i = 0; t[n++] = e[i++];);
							t.length = n - 1
						}
				}
			}
			x = e.support = {}, T = e.isXML = function (t) {
				var e = t && (t.ownerDocument || t).documentElement;
				return e ? "HTML" !== e.nodeName : !1
			}, B = e.setDocument = function (t) {
				var e, n, i = t ? t.ownerDocument || t : F;
				return i !== A && 9 === i.nodeType && i.documentElement ? (A = i, E = A.documentElement, H = !T(A), (n = A.defaultView) && n.top !== n && (n.addEventListener ? n.addEventListener("unload", _t, !1) : n.attachEvent && n.attachEvent("onunload", _t)), x.attributes = o(function (t) {
					return t.className = "i", !t.getAttribute("className")
				}), x.getElementsByTagName = o(function (t) {
					return t.appendChild(A.createComment("")), !t.getElementsByTagName("*").length
				}), x.getElementsByClassName = mt.test(A.getElementsByClassName), x.getById = o(function (t) {
					return E.appendChild(t).id = M, !A.getElementsByName || !A.getElementsByName(M).length
				}), x.getById ? (_.find.ID = function (t, e) {
					if ("undefined" != typeof e.getElementById && H) {
						var n = e.getElementById(t);
						return n ? [n] : []
					}
				}, _.filter.ID = function (t) {
					var e = t.replace(wt, xt);
					return function (t) {
						return t.getAttribute("id") === e
					}
				}) : (delete _.find.ID, _.filter.ID = function (t) {
					var e = t.replace(wt, xt);
					return function (t) {
						var n = "undefined" != typeof t.getAttributeNode && t.getAttributeNode("id");
						return n && n.value === e
					}
				}), _.find.TAG = x.getElementsByTagName ?
					function (t, e) {
						return "undefined" != typeof e.getElementsByTagName ? e.getElementsByTagName(t) : x.qsa ? e.querySelectorAll(t) : void 0
					} : function (t, e) {
						var n, i = [],
							o = 0,
							r = e.getElementsByTagName(t);
						if ("*" === t) {
							for (; n = r[o++];) 1 === n.nodeType && i.push(n);
							return i
						}
						return r
					}, _.find.CLASS = x.getElementsByClassName &&
					function (t, e) {
						return "undefined" != typeof e.getElementsByClassName && H ? e.getElementsByClassName(t) : void 0
					}, O = [], L = [], (x.qsa = mt.test(A.querySelectorAll)) && (o(function (t) {
						E.appendChild(t).innerHTML = "<a id='" + M + "'></a><select id='" + M + "-\r\\' msallowcapture=''><option selected=''></option></select>", t.querySelectorAll("[msallowcapture^='']").length && L.push("[*^$]=" + nt + "*(?:''|\"\")"), t.querySelectorAll("[selected]").length || L.push("\\[" + nt + "*(?:value|" + et + ")"), t.querySelectorAll("[id~=" + M + "-]").length || L.push("~="), t.querySelectorAll(":checked").length || L.push(":checked"), t.querySelectorAll("a#" + M + "+*").length || L.push(".#.+[+~]")
					}), o(function (t) {
						var e = A.createElement("input");
						e.setAttribute("type", "hidden"), t.appendChild(e).setAttribute("name", "D"), t.querySelectorAll("[name=d]").length && L.push("name" + nt + "*[*^$|!~]?="), t.querySelectorAll(":enabled").length || L.push(":enabled", ":disabled"), t.querySelectorAll("*,:x"), L.push(",.*:")
					})), (x.matchesSelector = mt.test(P = E.matches || E.webkitMatchesSelector || E.mozMatchesSelector || E.oMatchesSelector || E.msMatchesSelector)) && o(function (t) {
						x.disconnectedMatch = P.call(t, "div"), P.call(t, "[s!='']:x"), O.push("!=", rt)
					}), L = L.length && new RegExp(L.join("|")), O = O.length && new RegExp(O.join("|")), e = mt.test(E.compareDocumentPosition), I = e || mt.test(E.contains) ?
						function (t, e) {
							var n = 9 === t.nodeType ? t.documentElement : t,
								i = e && e.parentNode;
							return t === i || !(!i || 1 !== i.nodeType || !(n.contains ? n.contains(i) : t.compareDocumentPosition && 16 & t.compareDocumentPosition(i)))
						} : function (t, e) {
							if (e) for (; e = e.parentNode;) if (e === t) return !0;
							return !1
						}, Y = e ?
							function (t, e) {
								if (t === e) return N = !0, 0;
								var n = !t.compareDocumentPosition - !e.compareDocumentPosition;
								return n ? n : (n = (t.ownerDocument || t) === (e.ownerDocument || e) ? t.compareDocumentPosition(e) : 1, 1 & n || !x.sortDetached && e.compareDocumentPosition(t) === n ? t === A || t.ownerDocument === F && I(F, t) ? -1 : e === A || e.ownerDocument === F && I(F, e) ? 1 : D ? tt(D, t) - tt(D, e) : 0 : 4 & n ? -1 : 1)
							} : function (t, e) {
								if (t === e) return N = !0, 0;
								var n, i = 0,
									o = t.parentNode,
									r = e.parentNode,
									l = [t],
									s = [e];
								if (!o || !r) return t === A ? -1 : e === A ? 1 : o ? -1 : r ? 1 : D ? tt(D, t) - tt(D, e) : 0;
								if (o === r) return a(t, e);
								for (n = t; n = n.parentNode;) l.unshift(n);
								for (n = e; n = n.parentNode;) s.unshift(n);
								for (; l[i] === s[i];) i++;
								return i ? a(l[i], s[i]) : l[i] === F ? -1 : s[i] === F ? 1 : 0
							}, A) : A
			}, e.matches = function (t, n) {
				return e(t, null, null, n)
			}, e.matchesSelector = function (t, n) {
				if ((t.ownerDocument || t) !== A && B(t), n = n.replace(ct, "='$1']"), x.matchesSelector && H && !z[n + " "] && (!O || !O.test(n)) && (!L || !L.test(n))) try {
					var i = P.call(t, n);
					if (i || x.disconnectedMatch || t.document && 11 !== t.document.nodeType) return i
				} catch (o) { }
				return e(n, A, null, [t]).length > 0
			}, e.contains = function (t, e) {
				return (t.ownerDocument || t) !== A && B(t), I(t, e)
			}, e.attr = function (t, e) {
				(t.ownerDocument || t) !== A && B(t);
				var n = _.attrHandle[e.toLowerCase()],
					i = n && G.call(_.attrHandle, e.toLowerCase()) ? n(t, e, !H) : void 0;
				return void 0 !== i ? i : x.attributes || !H ? t.getAttribute(e) : (i = t.getAttributeNode(e)) && i.specified ? i.value : null
			}, e.error = function (t) {
				throw new Error("Syntax error, unrecognized expression: " + t)
			}, e.uniqueSort = function (t) {
				var e, n = [],
					i = 0,
					o = 0;
				if (N = !x.detectDuplicates, D = !x.sortStable && t.slice(0), t.sort(Y), N) {
					for (; e = t[o++];) e === t[o] && (i = n.push(o));
					for (; i--;) t.splice(n[i], 1)
				}
				return D = null, t
			}, C = e.getText = function (t) {
				var e, n = "",
					i = 0,
					o = t.nodeType;
				if (o) {
					if (1 === o || 9 === o || 11 === o) {
						if ("string" == typeof t.textContent) return t.textContent;
						for (t = t.firstChild; t; t = t.nextSibling) n += C(t)
					} else if (3 === o || 4 === o) return t.nodeValue
				} else for (; e = t[i++];) n += C(e);
				return n
			}, _ = e.selectors = {
				cacheLength: 50,
				createPseudo: i,
				match: gt,
				attrHandle: {},
				find: {},
				relative: {
					">": {
						dir: "parentNode",
						first: !0
					},
					" ": {
						dir: "parentNode"
					},
					"+": {
						dir: "previousSibling",
						first: !0
					},
					"~": {
						dir: "previousSibling"
					}
				},
				preFilter: {
					ATTR: function (t) {
						return t[1] = t[1].replace(wt, xt), t[3] = (t[3] || t[4] || t[5] || "").replace(wt, xt), "~=" === t[2] && (t[3] = " " + t[3] + " "), t.slice(0, 4)
					},
					CHILD: function (t) {
						return t[1] = t[1].toLowerCase(), "nth" === t[1].slice(0, 3) ? (t[3] || e.error(t[0]), t[4] = +(t[4] ? t[5] + (t[6] || 1) : 2 * ("even" === t[3] || "odd" === t[3])), t[5] = +(t[7] + t[8] || "odd" === t[3])) : t[3] && e.error(t[0]), t
					},
					PSEUDO: function (t) {
						var e, n = !t[6] && t[2];
						return gt.CHILD.test(t[0]) ? null : (t[3] ? t[2] = t[4] || t[5] || "" : n && pt.test(n) && (e = k(n, !0)) && (e = n.indexOf(")", n.length - e) - n.length) && (t[0] = t[0].slice(0, e), t[2] = n.slice(0, e)), t.slice(0, 3))
					}
				},
				filter: {
					TAG: function (t) {
						var e = t.replace(wt, xt).toLowerCase();
						return "*" === t ?
							function () {
								return !0
							} : function (t) {
								return t.nodeName && t.nodeName.toLowerCase() === e
							}
					},
					CLASS: function (t) {
						var e = R[t + " "];
						return e || (e = new RegExp("(^|" + nt + ")" + t + "(" + nt + "|$)")) && R(t, function (t) {
							return e.test("string" == typeof t.className && t.className || "undefined" != typeof t.getAttribute && t.getAttribute("class") || "")
						})
					},
					ATTR: function (t, n, i) {
						return function (o) {
							var r = e.attr(o, t);
							return null == r ? "!=" === n : n ? (r += "", "=" === n ? r === i : "!=" === n ? r !== i : "^=" === n ? i && 0 === r.indexOf(i) : "*=" === n ? i && r.indexOf(i) > -1 : "$=" === n ? i && r.slice(-i.length) === i : "~=" === n ? (" " + r.replace(at, " ") + " ").indexOf(i) > -1 : "|=" === n ? r === i || r.slice(0, i.length + 1) === i + "-" : !1) : !0
						}
					},
					CHILD: function (t, e, n, i, o) {
						var r = "nth" !== t.slice(0, 3),
							a = "last" !== t.slice(-4),
							l = "of-type" === e;
						return 1 === i && 0 === o ?
							function (t) {
								return !!t.parentNode
							} : function (e, n, s) {
								var d, c, p, u, g, f, h = r !== a ? "nextSibling" : "previousSibling",
									m = e.parentNode,
									v = l && e.nodeName.toLowerCase(),
									b = !s && !l,
									y = !1;
								if (m) {
									if (r) {
										for (; h;) {
											for (u = e; u = u[h];) if (l ? u.nodeName.toLowerCase() === v : 1 === u.nodeType) return !1;
											f = h = "only" === t && !f && "nextSibling"
										}
										return !0
									}
									if (f = [a ? m.firstChild : m.lastChild], a && b) {
										for (u = m, p = u[M] || (u[M] = {}), c = p[u.uniqueID] || (p[u.uniqueID] = {}), d = c[t] || [], g = d[0] === W && d[1], y = g && d[2], u = g && m.childNodes[g]; u = ++g && u && u[h] || (y = g = 0) || f.pop();) if (1 === u.nodeType && ++y && u === e) {
											c[t] = [W, g, y];
											break
										}
									} else if (b && (u = e, p = u[M] || (u[M] = {}), c = p[u.uniqueID] || (p[u.uniqueID] = {}), d = c[t] || [], g = d[0] === W && d[1], y = g), y === !1) for (;
										(u = ++g && u && u[h] || (y = g = 0) || f.pop()) && ((l ? u.nodeName.toLowerCase() !== v : 1 !== u.nodeType) || !++y || (b && (p = u[M] || (u[M] = {}), c = p[u.uniqueID] || (p[u.uniqueID] = {}), c[t] = [W, y]), u !== e)););
									return y -= o, y === i || y % i === 0 && y / i >= 0
								}
							}
					},
					PSEUDO: function (t, n) {
						var o, r = _.pseudos[t] || _.setFilters[t.toLowerCase()] || e.error("unsupported pseudo: " + t);
						return r[M] ? r(n) : r.length > 1 ? (o = [t, t, "", n], _.setFilters.hasOwnProperty(t.toLowerCase()) ? i(function (t, e) {
							for (var i, o = r(t, n), a = o.length; a--;) i = tt(t, o[a]), t[i] = !(e[i] = o[a])
						}) : function (t) {
							return r(t, 0, o)
						}) : r
					}
				},
				pseudos: {
					not: i(function (t) {
						var e = [],
							n = [],
							o = j(t.replace(lt, "$1"));
						return o[M] ? i(function (t, e, n, i) {
							for (var r, a = o(t, null, i, []), l = t.length; l--;)(r = a[l]) && (t[l] = !(e[l] = r))
						}) : function (t, i, r) {
							return e[0] = t, o(e, null, r, n), e[0] = null, !n.pop()
						}
					}),
					has: i(function (t) {
						return function (n) {
							return e(t, n).length > 0
						}
					}),
					contains: i(function (t) {
						return t = t.replace(wt, xt), function (e) {
							return (e.textContent || e.innerText || C(e)).indexOf(t) > -1
						}
					}),
					lang: i(function (t) {
						return ut.test(t || "") || e.error("unsupported lang: " + t), t = t.replace(wt, xt).toLowerCase(), function (e) {
							var n;
							do
								if (n = H ? e.lang : e.getAttribute("xml:lang") || e.getAttribute("lang")) return n = n.toLowerCase(), n === t || 0 === n.indexOf(t + "-");
							while ((e = e.parentNode) && 1 === e.nodeType);
							return !1
						}
					}),
					target: function (e) {
						var n = t.location && t.location.hash;
						return n && n.slice(1) === e.id
					},
					root: function (t) {
						return t === E
					},
					focus: function (t) {
						return t === A.activeElement && (!A.hasFocus || A.hasFocus()) && !!(t.type || t.href || ~t.tabIndex)
					},
					enabled: function (t) {
						return t.disabled === !1
					},
					disabled: function (t) {
						return t.disabled === !0
					},
					checked: function (t) {
						var e = t.nodeName.toLowerCase();
						return "input" === e && !!t.checked || "option" === e && !!t.selected
					},
					selected: function (t) {
						return t.parentNode && t.parentNode.selectedIndex, t.selected === !0
					},
					empty: function (t) {
						for (t = t.firstChild; t; t = t.nextSibling) if (t.nodeType < 6) return !1;
						return !0
					},
					parent: function (t) {
						return !_.pseudos.empty(t)
					},
					header: function (t) {
						return ht.test(t.nodeName)
					},
					input: function (t) {
						return ft.test(t.nodeName)
					},
					button: function (t) {
						var e = t.nodeName.toLowerCase();
						return "input" === e && "button" === t.type || "button" === e
					},
					text: function (t) {
						var e;
						return "input" === t.nodeName.toLowerCase() && "text" === t.type && (null == (e = t.getAttribute("type")) || "text" === e.toLowerCase())
					},
					first: d(function () {
						return [0]
					}),
					last: d(function (t, e) {
						return [e - 1]
					}),
					eq: d(function (t, e, n) {
						return [0 > n ? n + e : n]
					}),
					even: d(function (t, e) {
						for (var n = 0; e > n; n += 2) t.push(n);
						return t
					}),
					odd: d(function (t, e) {
						for (var n = 1; e > n; n += 2) t.push(n);
						return t
					}),
					lt: d(function (t, e, n) {
						for (var i = 0 > n ? n + e : n; --i >= 0;) t.push(i);
						return t
					}),
					gt: d(function (t, e, n) {
						for (var i = 0 > n ? n + e : n; ++i < e;) t.push(i);
						return t
					})
				}
			}, _.pseudos.nth = _.pseudos.eq;
			for (w in {
				radio: !0,
				checkbox: !0,
				file: !0,
				password: !0,
				image: !0
			}) _.pseudos[w] = l(w);
			for (w in {
				submit: !0,
				reset: !0
			}) _.pseudos[w] = s(w);
			return p.prototype = _.filters = _.pseudos, _.setFilters = new p, k = e.tokenize = function (t, n) {
				var i, o, r, a, l, s, d, c = X[t + " "];
				if (c) return n ? 0 : c.slice(0);
				for (l = t, s = [], d = _.preFilter; l;) {
					i && !(o = st.exec(l)) || (o && (l = l.slice(o[0].length) || l), s.push(r = [])), i = !1, (o = dt.exec(l)) && (i = o.shift(), r.push({
						value: i,
						type: o[0].replace(lt, " ")
					}), l = l.slice(i.length));
					for (a in _.filter) !(o = gt[a].exec(l)) || d[a] && !(o = d[a](o)) || (i = o.shift(), r.push({
						value: i,
						type: a,
						matches: o
					}), l = l.slice(i.length));
					if (!i) break
				}
				return n ? l.length : l ? e.error(t) : X(t, s).slice(0)
			}, j = e.compile = function (t, e) {
				var n, i = [],
					o = [],
					r = z[t + " "];
				if (!r) {
					for (e || (e = k(t)), n = e.length; n--;) r = b(e[n]), r[M] ? i.push(r) : o.push(r);
					r = z(t, y(o, i)), r.selector = t
				}
				return r
			}, S = e.select = function (t, e, n, i) {
				var o, r, a, l, s, d = "function" == typeof t && t,
					p = !i && k(t = d.selector || t);
				if (n = n || [], 1 === p.length) {
					if (r = p[0] = p[0].slice(0), r.length > 2 && "ID" === (a = r[0]).type && x.getById && 9 === e.nodeType && H && _.relative[r[1].type]) {
						if (e = (_.find.ID(a.matches[0].replace(wt, xt), e) || [])[0], !e) return n;
						d && (e = e.parentNode), t = t.slice(r.shift().value.length)
					}
					for (o = gt.needsContext.test(t) ? 0 : r.length; o-- && (a = r[o], !_.relative[l = a.type]);) if ((s = _.find[l]) && (i = s(a.matches[0].replace(wt, xt), bt.test(r[0].type) && c(e.parentNode) || e))) {
						if (r.splice(o, 1), t = i.length && u(r), !t) return Q.apply(n, i), n;
						break
					}
				}
				return (d || j(t, p))(i, e, !H, n, !e || bt.test(t) && c(e.parentNode) || e), n
			}, x.sortStable = M.split("").sort(Y).join("") === M, x.detectDuplicates = !!N, B(), x.sortDetached = o(function (t) {
				return 1 & t.compareDocumentPosition(A.createElement("div"))
			}), o(function (t) {
				return t.innerHTML = "<a href='#'></a>", "#" === t.firstChild.getAttribute("href")
			}) || r("type|href|height|width", function (t, e, n) {
				return n ? void 0 : t.getAttribute(e, "type" === e.toLowerCase() ? 1 : 2)
			}), x.attributes && o(function (t) {
				return t.innerHTML = "<input/>", t.firstChild.setAttribute("value", ""), "" === t.firstChild.getAttribute("value")
			}) || r("value", function (t, e, n) {
				return n || "input" !== t.nodeName.toLowerCase() ? void 0 : t.defaultValue
			}), o(function (t) {
				return null == t.getAttribute("disabled")
			}) || r(et, function (t, e, n) {
				var i;
				return n ? void 0 : t[e] === !0 ? e.toLowerCase() : (i = t.getAttributeNode(e)) && i.specified ? i.value : null
			}), e
		}(t);
		rt.find = ct, rt.expr = ct.selectors, rt.expr[":"] = rt.expr.pseudos, rt.uniqueSort = rt.unique = ct.uniqueSort, rt.text = ct.getText, rt.isXMLDoc = ct.isXML, rt.contains = ct.contains;
		var pt = function (t, e, n) {
			for (var i = [], o = void 0 !== n;
				(t = t[e]) && 9 !== t.nodeType;) if (1 === t.nodeType) {
					if (o && rt(t).is(n)) break;
					i.push(t)
				}
			return i
		},
			ut = function (t, e) {
				for (var n = []; t; t = t.nextSibling) 1 === t.nodeType && t !== e && n.push(t);
				return n
			},
			gt = rt.expr.match.needsContext,
			ft = /^<([\w-]+)\s*\/?>(?:<\/\1>|)$/,
			ht = /^.[^:#\[\.,]*$/;
		rt.filter = function (t, e, n) {
			var i = e[0];
			return n && (t = ":not(" + t + ")"), 1 === e.length && 1 === i.nodeType ? rt.find.matchesSelector(i, t) ? [i] : [] : rt.find.matches(t, rt.grep(e, function (t) {
				return 1 === t.nodeType
			}))
		}, rt.fn.extend({
			find: function (t) {
				var e, n = this.length,
					i = [],
					o = this;
				if ("string" != typeof t) return this.pushStack(rt(t).filter(function () {
					for (e = 0; n > e; e++) if (rt.contains(o[e], this)) return !0
				}));
				for (e = 0; n > e; e++) rt.find(t, o[e], i);
				return i = this.pushStack(n > 1 ? rt.unique(i) : i), i.selector = this.selector ? this.selector + " " + t : t, i
			},
			filter: function (t) {
				return this.pushStack(i(this, t || [], !1))
			},
			not: function (t) {
				return this.pushStack(i(this, t || [], !0))
			},
			is: function (t) {
				return !!i(this, "string" == typeof t && gt.test(t) ? rt(t) : t || [], !1).length
			}
		});
		var mt, vt = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]*))$/,
			bt = rt.fn.init = function (t, e, n) {
				var i, o;
				if (!t) return this;
				if (n = n || mt, "string" == typeof t) {
					if (i = "<" === t[0] && ">" === t[t.length - 1] && t.length >= 3 ? [null, t, null] : vt.exec(t), !i || !i[1] && e) return !e || e.jquery ? (e || n).find(t) : this.constructor(e).find(t);
					if (i[1]) {
						if (e = e instanceof rt ? e[0] : e, rt.merge(this, rt.parseHTML(i[1], e && e.nodeType ? e.ownerDocument || e : U, !0)), ft.test(i[1]) && rt.isPlainObject(e)) for (i in e) rt.isFunction(this[i]) ? this[i](e[i]) : this.attr(i, e[i]);
						return this
					}
					return o = U.getElementById(i[2]), o && o.parentNode && (this.length = 1, this[0] = o), this.context = U, this.selector = t, this
				}
				return t.nodeType ? (this.context = this[0] = t, this.length = 1, this) : rt.isFunction(t) ? void 0 !== n.ready ? n.ready(t) : t(rt) : (void 0 !== t.selector && (this.selector = t.selector, this.context = t.context), rt.makeArray(t, this))
			};
		bt.prototype = rt.fn, mt = rt(U);
		var yt = /^(?:parents|prev(?:Until|All))/,
			wt = {
				children: !0,
				contents: !0,
				next: !0,
				prev: !0
			};
		rt.fn.extend({
			has: function (t) {
				var e = rt(t, this),
					n = e.length;
				return this.filter(function () {
					for (var t = 0; n > t; t++) if (rt.contains(this, e[t])) return !0
				})
			},
			closest: function (t, e) {
				for (var n, i = 0, o = this.length, r = [], a = gt.test(t) || "string" != typeof t ? rt(t, e || this.context) : 0; o > i; i++) for (n = this[i]; n && n !== e; n = n.parentNode) if (n.nodeType < 11 && (a ? a.index(n) > -1 : 1 === n.nodeType && rt.find.matchesSelector(n, t))) {
					r.push(n);
					break
				}
				return this.pushStack(r.length > 1 ? rt.uniqueSort(r) : r)
			},
			index: function (t) {
				return t ? "string" == typeof t ? J.call(rt(t), this[0]) : J.call(this, t.jquery ? t[0] : t) : this[0] && this[0].parentNode ? this.first().prevAll().length : -1
			},
			add: function (t, e) {
				return this.pushStack(rt.uniqueSort(rt.merge(this.get(), rt(t, e))))
			},
			addBack: function (t) {
				return this.add(null == t ? this.prevObject : this.prevObject.filter(t))
			}
		}), rt.each({
			parent: function (t) {
				var e = t.parentNode;
				return e && 11 !== e.nodeType ? e : null
			},
			parents: function (t) {
				return pt(t, "parentNode")
			},
			parentsUntil: function (t, e, n) {
				return pt(t, "parentNode", n)
			},
			next: function (t) {
				return o(t, "nextSibling")
			},
			prev: function (t) {
				return o(t, "previousSibling")
			},
			nextAll: function (t) {
				return pt(t, "nextSibling")
			},
			prevAll: function (t) {
				return pt(t, "previousSibling")
			},
			nextUntil: function (t, e, n) {
				return pt(t, "nextSibling", n)
			},
			prevUntil: function (t, e, n) {
				return pt(t, "previousSibling", n)
			},
			siblings: function (t) {
				return ut((t.parentNode || {}).firstChild, t)
			},
			children: function (t) {
				return ut(t.firstChild)
			},
			contents: function (t) {
				return t.contentDocument || rt.merge([], t.childNodes)
			}
		}, function (t, e) {
			rt.fn[t] = function (n, i) {
				var o = rt.map(this, e, n);
				return "Until" !== t.slice(-5) && (i = n), i && "string" == typeof i && (o = rt.filter(i, o)), this.length > 1 && (wt[t] || rt.uniqueSort(o), yt.test(t) && o.reverse()), this.pushStack(o)
			}
		});
		var xt = /\S+/g;
		rt.Callbacks = function (t) {
			t = "string" == typeof t ? r(t) : rt.extend({}, t);
			var e, n, i, o, a = [],
				l = [],
				s = -1,
				d = function () {
					for (o = t.once, i = e = !0; l.length; s = -1) for (n = l.shift(); ++s < a.length;) a[s].apply(n[0], n[1]) === !1 && t.stopOnFalse && (s = a.length, n = !1);
					t.memory || (n = !1), e = !1, o && (a = n ? [] : "")
				},
				c = {
					add: function () {
						return a && (n && !e && (s = a.length - 1, l.push(n)), function i(e) {
							rt.each(e, function (e, n) {
								rt.isFunction(n) ? t.unique && c.has(n) || a.push(n) : n && n.length && "string" !== rt.type(n) && i(n)
							})
						}(arguments), n && !e && d()), this
					},
					remove: function () {
						return rt.each(arguments, function (t, e) {
							for (var n;
								(n = rt.inArray(e, a, n)) > -1;) a.splice(n, 1), s >= n && s--
						}), this
					},
					has: function (t) {
						return t ? rt.inArray(t, a) > -1 : a.length > 0
					},
					empty: function () {
						return a && (a = []), this
					},
					disable: function () {
						return o = l = [], a = n = "", this
					},
					disabled: function () {
						return !a
					},
					lock: function () {
						return o = l = [], n || (a = n = ""), this
					},
					locked: function () {
						return !!o
					},
					fireWith: function (t, n) {
						return o || (n = n || [], n = [t, n.slice ? n.slice() : n], l.push(n), e || d()), this
					},
					fire: function () {
						return c.fireWith(this, arguments), this
					},
					fired: function () {
						return !!i
					}
				};
			return c
		}, rt.extend({
			Deferred: function (t) {
				var e = [
					["resolve", "done", rt.Callbacks("once memory"), "resolved"],
					["reject", "fail", rt.Callbacks("once memory"), "rejected"],
					["notify", "progress", rt.Callbacks("memory")]
				],
					n = "pending",
					i = {
						state: function () {
							return n
						},
						always: function () {
							return o.done(arguments).fail(arguments), this
						},
						then: function () {
							var t = arguments;
							return rt.Deferred(function (n) {
								rt.each(e, function (e, r) {
									var a = rt.isFunction(t[e]) && t[e];
									o[r[1]](function () {
										var t = a && a.apply(this, arguments);
										t && rt.isFunction(t.promise) ? t.promise().progress(n.notify).done(n.resolve).fail(n.reject) : n[r[0] + "With"](this === i ? n.promise() : this, a ? [t] : arguments)
									})
								}), t = null
							}).promise()
						},
						promise: function (t) {
							return null != t ? rt.extend(t, i) : i
						}
					},
					o = {};
				return i.pipe = i.then, rt.each(e, function (t, r) {
					var a = r[2],
						l = r[3];
					i[r[1]] = a.add, l && a.add(function () {
						n = l
					}, e[1 ^ t][2].disable, e[2][2].lock), o[r[0]] = function () {
						return o[r[0] + "With"](this === o ? i : this, arguments), this
					}, o[r[0] + "With"] = a.fireWith
				}), i.promise(o), t && t.call(o, o), o
			},
			when: function (t) {
				var e, n, i, o = 0,
					r = K.call(arguments),
					a = r.length,
					l = 1 !== a || t && rt.isFunction(t.promise) ? a : 0,
					s = 1 === l ? t : rt.Deferred(),
					d = function (t, n, i) {
						return function (o) {
							n[t] = this, i[t] = arguments.length > 1 ? K.call(arguments) : o, i === e ? s.notifyWith(n, i) : --l || s.resolveWith(n, i)
						}
					};
				if (a > 1) for (e = new Array(a), n = new Array(a), i = new Array(a); a > o; o++) r[o] && rt.isFunction(r[o].promise) ? r[o].promise().progress(d(o, n, e)).done(d(o, i, r)).fail(s.reject) : --l;
				return l || s.resolveWith(i, r), s.promise()
			}
		});
		var _t;
		rt.fn.ready = function (t) {
			return rt.ready.promise().done(t), this
		}, rt.extend({
			isReady: !1,
			readyWait: 1,
			holdReady: function (t) {
				t ? rt.readyWait++ : rt.ready(!0)
			},
			ready: function (t) {
				(t === !0 ? --rt.readyWait : rt.isReady) || (rt.isReady = !0, t !== !0 && --rt.readyWait > 0 || (_t.resolveWith(U, [rt]), rt.fn.triggerHandler && (rt(U).triggerHandler("ready"), rt(U).off("ready"))))
			}
		}), rt.ready.promise = function (e) {
			return _t || (_t = rt.Deferred(), "complete" === U.readyState || "loading" !== U.readyState && !U.documentElement.doScroll ? t.setTimeout(rt.ready) : (U.addEventListener("DOMContentLoaded", a), t.addEventListener("load", a))), _t.promise(e)
		}, rt.ready.promise();
		var Ct = function (t, e, n, i, o, r, a) {
			var l = 0,
				s = t.length,
				d = null == n;
			if ("object" === rt.type(n)) {
				o = !0;
				for (l in n) Ct(t, e, l, n[l], !0, r, a)
			} else if (void 0 !== i && (o = !0, rt.isFunction(i) || (a = !0), d && (a ? (e.call(t, i), e = null) : (d = e, e = function (t, e, n) {
				return d.call(rt(t), n)
			})), e)) for (; s > l; l++) e(t[l], n, a ? i : i.call(t[l], l, e(t[l], n)));
			return o ? t : d ? e.call(t) : s ? e(t[0], n) : r
		},
			Tt = function (t) {
				return 1 === t.nodeType || 9 === t.nodeType || !+t.nodeType
			};
		l.uid = 1, l.prototype = {
			register: function (t, e) {
				var n = e || {};
				return t.nodeType ? t[this.expando] = n : Object.defineProperty(t, this.expando, {
					value: n,
					writable: !0,
					configurable: !0
				}), t[this.expando]
			},
			cache: function (t) {
				if (!Tt(t)) return {};
				var e = t[this.expando];
				return e || (e = {}, Tt(t) && (t.nodeType ? t[this.expando] = e : Object.defineProperty(t, this.expando, {
					value: e,
					configurable: !0
				}))), e
			},
			set: function (t, e, n) {
				var i, o = this.cache(t);
				if ("string" == typeof e) o[e] = n;
				else for (i in e) o[i] = e[i];
				return o
			},
			get: function (t, e) {
				return void 0 === e ? this.cache(t) : t[this.expando] && t[this.expando][e]
			},
			access: function (t, e, n) {
				var i;
				return void 0 === e || e && "string" == typeof e && void 0 === n ? (i = this.get(t, e), void 0 !== i ? i : this.get(t, rt.camelCase(e))) : (this.set(t, e, n), void 0 !== n ? n : e)
			},
			remove: function (t, e) {
				var n, i, o, r = t[this.expando];
				if (void 0 !== r) {
					if (void 0 === e) this.register(t);
					else {
						rt.isArray(e) ? i = e.concat(e.map(rt.camelCase)) : (o = rt.camelCase(e), e in r ? i = [e, o] : (i = o, i = i in r ? [i] : i.match(xt) || [])), n = i.length;
						for (; n--;) delete r[i[n]]
					} (void 0 === e || rt.isEmptyObject(r)) && (t.nodeType ? t[this.expando] = void 0 : delete t[this.expando])
				}
			},
			hasData: function (t) {
				var e = t[this.expando];
				return void 0 !== e && !rt.isEmptyObject(e)
			}
		};
		var kt = new l,
			jt = new l,
			St = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
			$t = /[A-Z]/g;
		rt.extend({
			hasData: function (t) {
				return jt.hasData(t) || kt.hasData(t)
			},
			data: function (t, e, n) {
				return jt.access(t, e, n)
			},
			removeData: function (t, e) {
				jt.remove(t, e)
			},
			_data: function (t, e, n) {
				return kt.access(t, e, n)
			},
			_removeData: function (t, e) {
				kt.remove(t, e)
			}
		}), rt.fn.extend({
			data: function (t, e) {
				var n, i, o, r = this[0],
					a = r && r.attributes;
				if (void 0 === t) {
					if (this.length && (o = jt.get(r), 1 === r.nodeType && !kt.get(r, "hasDataAttrs"))) {
						for (n = a.length; n--;) a[n] && (i = a[n].name, 0 === i.indexOf("data-") && (i = rt.camelCase(i.slice(5)), s(r, i, o[i])));
						kt.set(r, "hasDataAttrs", !0)
					}
					return o
				}
				return "object" == typeof t ? this.each(function () {
					jt.set(this, t)
				}) : Ct(this, function (e) {
					var n, i;
					if (r && void 0 === e) {
						if (n = jt.get(r, t) || jt.get(r, t.replace($t, "-$&").toLowerCase()), void 0 !== n) return n;
						if (i = rt.camelCase(t), n = jt.get(r, i), void 0 !== n) return n;
						if (n = s(r, i, void 0), void 0 !== n) return n
					} else i = rt.camelCase(t), this.each(function () {
						var n = jt.get(this, i);
						jt.set(this, i, e), t.indexOf("-") > -1 && void 0 !== n && jt.set(this, t, e)
					})
				}, null, e, arguments.length > 1, null, !0)
			},
			removeData: function (t) {
				return this.each(function () {
					jt.remove(this, t)
				})
			}
		}), rt.extend({
			queue: function (t, e, n) {
				var i;
				return t ? (e = (e || "fx") + "queue", i = kt.get(t, e), n && (!i || rt.isArray(n) ? i = kt.access(t, e, rt.makeArray(n)) : i.push(n)), i || []) : void 0
			},
			dequeue: function (t, e) {
				e = e || "fx";
				var n = rt.queue(t, e),
					i = n.length,
					o = n.shift(),
					r = rt._queueHooks(t, e),
					a = function () {
						rt.dequeue(t, e)
					};
				"inprogress" === o && (o = n.shift(), i--), o && ("fx" === e && n.unshift("inprogress"), delete r.stop, o.call(t, a, r)), !i && r && r.empty.fire()
			},
			_queueHooks: function (t, e) {
				var n = e + "queueHooks";
				return kt.get(t, n) || kt.access(t, n, {
					empty: rt.Callbacks("once memory").add(function () {
						kt.remove(t, [e + "queue", n])
					})
				})
			}
		}), rt.fn.extend({
			queue: function (t, e) {
				var n = 2;
				return "string" != typeof t && (e = t, t = "fx", n--), arguments.length < n ? rt.queue(this[0], t) : void 0 === e ? this : this.each(function () {
					var n = rt.queue(this, t, e);
					rt._queueHooks(this, t), "fx" === t && "inprogress" !== n[0] && rt.dequeue(this, t)
				})
			},
			dequeue: function (t) {
				return this.each(function () {
					rt.dequeue(this, t)
				})
			},
			clearQueue: function (t) {
				return this.queue(t || "fx", [])
			},
			promise: function (t, e) {
				var n, i = 1,
					o = rt.Deferred(),
					r = this,
					a = this.length,
					l = function () {
						--i || o.resolveWith(r, [r])
					};
				for ("string" != typeof t && (e = t, t = void 0), t = t || "fx"; a--;) n = kt.get(r[a], t + "queueHooks"), n && n.empty && (i++ , n.empty.add(l));
				return l(), o.promise(e)
			}
		});
		var Dt = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,
			Nt = new RegExp("^(?:([+-])=|)(" + Dt + ")([a-z%]*)$", "i"),
			Bt = ["Top", "Right", "Bottom", "Left"],
			At = function (t, e) {
				return t = e || t, "none" === rt.css(t, "display") || !rt.contains(t.ownerDocument, t)
			},
			Et = /^(?:checkbox|radio)$/i,
			Ht = /<([\w:-]+)/,
			Lt = /^$|\/(?:java|ecma)script/i,
			Ot = {
				option: [1, "<select multiple='multiple'>", "</select>"],
				thead: [1, "<table>", "</table>"],
				col: [2, "<table><colgroup>", "</colgroup></table>"],
				tr: [2, "<table><tbody>", "</tbody></table>"],
				td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
				_default: [0, "", ""]
			};
		Ot.optgroup = Ot.option, Ot.tbody = Ot.tfoot = Ot.colgroup = Ot.caption = Ot.thead, Ot.th = Ot.td;
		var Pt = /<|&#?\w+;/;
		!
			function () {
				var t = U.createDocumentFragment(),
					e = t.appendChild(U.createElement("div")),
					n = U.createElement("input");
				n.setAttribute("type", "radio"), n.setAttribute("checked", "checked"), n.setAttribute("name", "t"), e.appendChild(n), it.checkClone = e.cloneNode(!0).cloneNode(!0).lastChild.checked, e.innerHTML = "<textarea>x</textarea>", it.noCloneChecked = !!e.cloneNode(!0).lastChild.defaultValue
			}();
		var It = /^key/,
			Mt = /^(?:mouse|pointer|contextmenu|drag|drop)|click/,
			Ft = /^([^.]*)(?:\.(.+)|)/;
		rt.event = {
			global: {},
			add: function (t, e, n, i, o) {
				var r, a, l, s, d, c, p, u, g, f, h, m = kt.get(t);
				if (m) for (n.handler && (r = n, n = r.handler, o = r.selector), n.guid || (n.guid = rt.guid++), (s = m.events) || (s = m.events = {}), (a = m.handle) || (a = m.handle = function (e) {
					return "undefined" != typeof rt && rt.event.triggered !== e.type ? rt.event.dispatch.apply(t, arguments) : void 0
				}), e = (e || "").match(xt) || [""], d = e.length; d--;) l = Ft.exec(e[d]) || [], g = h = l[1], f = (l[2] || "").split(".").sort(), g && (p = rt.event.special[g] || {}, g = (o ? p.delegateType : p.bindType) || g, p = rt.event.special[g] || {}, c = rt.extend({
					type: g,
					origType: h,
					data: i,
					handler: n,
					guid: n.guid,
					selector: o,
					needsContext: o && rt.expr.match.needsContext.test(o),
					namespace: f.join(".")
				}, r), (u = s[g]) || (u = s[g] = [], u.delegateCount = 0, p.setup && p.setup.call(t, i, f, a) !== !1 || t.addEventListener && t.addEventListener(g, a)), p.add && (p.add.call(t, c), c.handler.guid || (c.handler.guid = n.guid)), o ? u.splice(u.delegateCount++, 0, c) : u.push(c), rt.event.global[g] = !0)
			},
			remove: function (t, e, n, i, o) {
				var r, a, l, s, d, c, p, u, g, f, h, m = kt.hasData(t) && kt.get(t);
				if (m && (s = m.events)) {
					for (e = (e || "").match(xt) || [""], d = e.length; d--;) if (l = Ft.exec(e[d]) || [], g = h = l[1], f = (l[2] || "").split(".").sort(), g) {
						for (p = rt.event.special[g] || {}, g = (i ? p.delegateType : p.bindType) || g, u = s[g] || [], l = l[2] && new RegExp("(^|\\.)" + f.join("\\.(?:.*\\.|)") + "(\\.|$)"), a = r = u.length; r--;) c = u[r], !o && h !== c.origType || n && n.guid !== c.guid || l && !l.test(c.namespace) || i && i !== c.selector && ("**" !== i || !c.selector) || (u.splice(r, 1), c.selector && u.delegateCount-- , p.remove && p.remove.call(t, c));
						a && !u.length && (p.teardown && p.teardown.call(t, f, m.handle) !== !1 || rt.removeEvent(t, g, m.handle), delete s[g])
					} else for (g in s) rt.event.remove(t, g + e[d], n, i, !0);
					rt.isEmptyObject(s) && kt.remove(t, "handle events")
				}
			},
			dispatch: function (t) {
				t = rt.event.fix(t);
				var e, n, i, o, r, a = [],
					l = K.call(arguments),
					s = (kt.get(this, "events") || {})[t.type] || [],
					d = rt.event.special[t.type] || {};
				if (l[0] = t, t.delegateTarget = this, !d.preDispatch || d.preDispatch.call(this, t) !== !1) {
					for (a = rt.event.handlers.call(this, t, s), e = 0;
						(o = a[e++]) && !t.isPropagationStopped();) for (t.currentTarget = o.elem, n = 0;
							(r = o.handlers[n++]) && !t.isImmediatePropagationStopped();) t.rnamespace && !t.rnamespace.test(r.namespace) || (t.handleObj = r, t.data = r.data, i = ((rt.event.special[r.origType] || {}).handle || r.handler).apply(o.elem, l), void 0 !== i && (t.result = i) === !1 && (t.preventDefault(), t.stopPropagation()));
					return d.postDispatch && d.postDispatch.call(this, t), t.result
				}
			},
			handlers: function (t, e) {
				var n, i, o, r, a = [],
					l = e.delegateCount,
					s = t.target;
				if (l && s.nodeType && ("click" !== t.type || isNaN(t.button) || t.button < 1)) for (; s !== this; s = s.parentNode || this) if (1 === s.nodeType && (s.disabled !== !0 || "click" !== t.type)) {
					for (i = [], n = 0; l > n; n++) r = e[n], o = r.selector + " ", void 0 === i[o] && (i[o] = r.needsContext ? rt(o, this).index(s) > -1 : rt.find(o, this, null, [s]).length), i[o] && i.push(r);
					i.length && a.push({
						elem: s,
						handlers: i
					})
				}
				return l < e.length && a.push({
					elem: this,
					handlers: e.slice(l)
				}), a
			},
			props: "altKey bubbles cancelable ctrlKey currentTarget detail eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),
			fixHooks: {},
			keyHooks: {
				props: "char charCode key keyCode".split(" "),
				filter: function (t, e) {
					return null == t.which && (t.which = null != e.charCode ? e.charCode : e.keyCode), t
				}
			},
			mouseHooks: {
				props: "button buttons clientX clientY offsetX offsetY pageX pageY screenX screenY toElement".split(" "),
				filter: function (t, e) {
					var n, i, o, r = e.button;
					return null == t.pageX && null != e.clientX && (n = t.target.ownerDocument || U, i = n.documentElement, o = n.body, t.pageX = e.clientX + (i && i.scrollLeft || o && o.scrollLeft || 0) - (i && i.clientLeft || o && o.clientLeft || 0), t.pageY = e.clientY + (i && i.scrollTop || o && o.scrollTop || 0) - (i && i.clientTop || o && o.clientTop || 0)), t.which || void 0 === r || (t.which = 1 & r ? 1 : 2 & r ? 3 : 4 & r ? 2 : 0), t
				}
			},
			fix: function (t) {
				if (t[rt.expando]) return t;
				var e, n, i, o = t.type,
					r = t,
					a = this.fixHooks[o];
				for (a || (this.fixHooks[o] = a = Mt.test(o) ? this.mouseHooks : It.test(o) ? this.keyHooks : {}), i = a.props ? this.props.concat(a.props) : this.props, t = new rt.Event(r), e = i.length; e--;) n = i[e], t[n] = r[n];
				return t.target || (t.target = U), 3 === t.target.nodeType && (t.target = t.target.parentNode), a.filter ? a.filter(t, r) : t
			},
			special: {
				load: {
					noBubble: !0
				},
				focus: {
					trigger: function () {
						return this !== h() && this.focus ? (this.focus(), !1) : void 0
					},
					delegateType: "focusin"
				},
				blur: {
					trigger: function () {
						return this === h() && this.blur ? (this.blur(), !1) : void 0
					},
					delegateType: "focusout"
				},
				click: {
					trigger: function () {
						return "checkbox" === this.type && this.click && rt.nodeName(this, "input") ? (this.click(), !1) : void 0
					},
					_default: function (t) {
						return rt.nodeName(t.target, "a")
					}
				},
				beforeunload: {
					postDispatch: function (t) {
						void 0 !== t.result && t.originalEvent && (t.originalEvent.returnValue = t.result)
					}
				}
			}
		}, rt.removeEvent = function (t, e, n) {
			t.removeEventListener && t.removeEventListener(e, n)
		}, rt.Event = function (t, e) {
			return this instanceof rt.Event ? (t && t.type ? (this.originalEvent = t, this.type = t.type, this.isDefaultPrevented = t.defaultPrevented || void 0 === t.defaultPrevented && t.returnValue === !1 ? g : f) : this.type = t, e && rt.extend(this, e), this.timeStamp = t && t.timeStamp || rt.now(), void (this[rt.expando] = !0)) : new rt.Event(t, e)
		}, rt.Event.prototype = {
			constructor: rt.Event,
			isDefaultPrevented: f,
			isPropagationStopped: f,
			isImmediatePropagationStopped: f,
			preventDefault: function () {
				var t = this.originalEvent;
				this.isDefaultPrevented = g, t && t.preventDefault()
			},
			stopPropagation: function () {
				var t = this.originalEvent;
				this.isPropagationStopped = g, t && t.stopPropagation()
			},
			stopImmediatePropagation: function () {
				var t = this.originalEvent;
				this.isImmediatePropagationStopped = g, t && t.stopImmediatePropagation(), this.stopPropagation()
			}
		}, rt.each({
			mouseenter: "mouseover",
			mouseleave: "mouseout",
			pointerenter: "pointerover",
			pointerleave: "pointerout"
		}, function (t, e) {
			rt.event.special[t] = {
				delegateType: e,
				bindType: e,
				handle: function (t) {
					var n, i = this,
						o = t.relatedTarget,
						r = t.handleObj;
					return o && (o === i || rt.contains(i, o)) || (t.type = r.origType, n = r.handler.apply(this, arguments), t.type = e), n
				}
			}
		}), rt.fn.extend({
			on: function (t, e, n, i) {
				return m(this, t, e, n, i)
			},
			one: function (t, e, n, i) {
				return m(this, t, e, n, i, 1)
			},
			off: function (t, e, n) {
				var i, o;
				if (t && t.preventDefault && t.handleObj) return i = t.handleObj, rt(t.delegateTarget).off(i.namespace ? i.origType + "." + i.namespace : i.origType, i.selector, i.handler), this;
				if ("object" == typeof t) {
					for (o in t) this.off(o, e, t[o]);
					return this
				}
				return e !== !1 && "function" != typeof e || (n = e, e = void 0), n === !1 && (n = f), this.each(function () {
					rt.event.remove(this, t, n, e)
				})
			}
		});
		var Wt = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:-]+)[^>]*)\/>/gi,
			qt = /<script|<style|<link/i,
			Rt = /checked\s*(?:[^=]|=\s*.checked.)/i,
			Xt = /^true\/(.*)/,
			zt = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g;
		rt.extend({
			htmlPrefilter: function (t) {
				return t.replace(Wt, "<$1></$2>")
			},
			clone: function (t, e, n) {
				var i, o, r, a, l = t.cloneNode(!0),
					s = rt.contains(t.ownerDocument, t);
				if (!(it.noCloneChecked || 1 !== t.nodeType && 11 !== t.nodeType || rt.isXMLDoc(t))) for (a = c(l), r = c(t), i = 0, o = r.length; o > i; i++) x(r[i], a[i]);
				if (e) if (n) for (r = r || c(t), a = a || c(l), i = 0, o = r.length; o > i; i++) w(r[i], a[i]);
				else w(t, l);
				return a = c(l, "script"), a.length > 0 && p(a, !s && c(t, "script")), l
			},
			cleanData: function (t) {
				for (var e, n, i, o = rt.event.special, r = 0; void 0 !== (n = t[r]); r++) if (Tt(n)) {
					if (e = n[kt.expando]) {
						if (e.events) for (i in e.events) o[i] ? rt.event.remove(n, i) : rt.removeEvent(n, i, e.handle);
						n[kt.expando] = void 0
					}
					n[jt.expando] && (n[jt.expando] = void 0)
				}
			}
		}), rt.fn.extend({
			domManip: _,
			detach: function (t) {
				return C(this, t, !0)
			},
			remove: function (t) {
				return C(this, t)
			},
			text: function (t) {
				return Ct(this, function (t) {
					return void 0 === t ? rt.text(this) : this.empty().each(function () {
						1 !== this.nodeType && 11 !== this.nodeType && 9 !== this.nodeType || (this.textContent = t)
					})
				}, null, t, arguments.length)
			},
			append: function () {
				return _(this, arguments, function (t) {
					if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
						var e = v(this, t);
						e.appendChild(t)
					}
				})
			},
			prepend: function () {
				return _(this, arguments, function (t) {
					if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
						var e = v(this, t);
						e.insertBefore(t, e.firstChild)
					}
				})
			},
			before: function () {
				return _(this, arguments, function (t) {
					this.parentNode && this.parentNode.insertBefore(t, this)
				})
			},
			after: function () {
				return _(this, arguments, function (t) {
					this.parentNode && this.parentNode.insertBefore(t, this.nextSibling)
				})
			},
			empty: function () {
				for (var t, e = 0; null != (t = this[e]); e++) 1 === t.nodeType && (rt.cleanData(c(t, !1)), t.textContent = "");
				return this
			},
			clone: function (t, e) {
				return t = null == t ? !1 : t, e = null == e ? t : e, this.map(function () {
					return rt.clone(this, t, e)
				})
			},
			html: function (t) {
				return Ct(this, function (t) {
					var e = this[0] || {},
						n = 0,
						i = this.length;
					if (void 0 === t && 1 === e.nodeType) return e.innerHTML;
					if ("string" == typeof t && !qt.test(t) && !Ot[(Ht.exec(t) || ["", ""])[1].toLowerCase()]) {
						t = rt.htmlPrefilter(t);
						try {
							for (; i > n; n++) e = this[n] || {}, 1 === e.nodeType && (rt.cleanData(c(e, !1)), e.innerHTML = t);
							e = 0
						} catch (o) { }
					}
					e && this.empty().append(t)
				}, null, t, arguments.length)
			},
			replaceWith: function () {
				var t = [];
				return _(this, arguments, function (e) {
					var n = this.parentNode;
					rt.inArray(this, t) < 0 && (rt.cleanData(c(this)), n && n.replaceChild(e, this))
				}, t)
			}
		}), rt.each({
			appendTo: "append",
			prependTo: "prepend",
			insertBefore: "before",
			insertAfter: "after",
			replaceAll: "replaceWith"
		}, function (t, e) {
			rt.fn[t] = function (t) {
				for (var n, i = [], o = rt(t), r = o.length - 1, a = 0; r >= a; a++) n = a === r ? this : this.clone(!0), rt(o[a])[e](n), Q.apply(i, n.get());
				return this.pushStack(i)
			}
		});
		var Yt, Vt = {
			HTML: "block",
			BODY: "block"
		},
			Gt = /^margin/,
			Ut = new RegExp("^(" + Dt + ")(?!px)[a-z%]+$", "i"),
			Kt = function (e) {
				var n = e.ownerDocument.defaultView;
				return n && n.opener || (n = t), n.getComputedStyle(e)
			},
			Zt = function (t, e, n, i) {
				var o, r, a = {};
				for (r in e) a[r] = t.style[r], t.style[r] = e[r];
				o = n.apply(t, i || []);
				for (r in e) t.style[r] = a[r];
				return o
			},
			Qt = U.documentElement;
		!
			function () {
				function e() {
					l.style.cssText = "-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;position:relative;display:block;margin:auto;border:1px;padding:1px;top:1%;width:50%", l.innerHTML = "", Qt.appendChild(a);
					var e = t.getComputedStyle(l);
					n = "1%" !== e.top, r = "2px" === e.marginLeft, i = "4px" === e.width, l.style.marginRight = "50%", o = "4px" === e.marginRight, Qt.removeChild(a)
				}
				var n, i, o, r, a = U.createElement("div"),
					l = U.createElement("div");
				l.style && (l.style.backgroundClip = "content-box", l.cloneNode(!0).style.backgroundClip = "", it.clearCloneStyle = "content-box" === l.style.backgroundClip, a.style.cssText = "border:0;width:8px;height:0;top:0;left:-9999px;padding:0;margin-top:1px;position:absolute", a.appendChild(l), rt.extend(it, {
					pixelPosition: function () {
						return e(), n
					},
					boxSizingReliable: function () {
						return null == i && e(), i
					},
					pixelMarginRight: function () {
						return null == i && e(), o
					},
					reliableMarginLeft: function () {
						return null == i && e(), r
					},
					reliableMarginRight: function () {
						var e, n = l.appendChild(U.createElement("div"));
						return n.style.cssText = l.style.cssText = "-webkit-box-sizing:content-box;box-sizing:content-box;display:block;margin:0;border:0;padding:0", n.style.marginRight = n.style.width = "0", l.style.width = "1px", Qt.appendChild(a), e = !parseFloat(t.getComputedStyle(n).marginRight), Qt.removeChild(a), l.removeChild(n), e
					}
				}))
			}();
		var Jt = /^(none|table(?!-c[ea]).+)/,
			te = {
				position: "absolute",
				visibility: "hidden",
				display: "block"
			},
			ee = {
				letterSpacing: "0",
				fontWeight: "400"
			},
			ne = ["Webkit", "O", "Moz", "ms"],
			ie = U.createElement("div").style;
		rt.extend({
			cssHooks: {
				opacity: {
					get: function (t, e) {
						if (e) {
							var n = j(t, "opacity");
							return "" === n ? "1" : n
						}
					}
				}
			},
			cssNumber: {
				animationIterationCount: !0,
				columnCount: !0,
				fillOpacity: !0,
				flexGrow: !0,
				flexShrink: !0,
				fontWeight: !0,
				lineHeight: !0,
				opacity: !0,
				order: !0,
				orphans: !0,
				widows: !0,
				zIndex: !0,
				zoom: !0
			},
			cssProps: {
				"float": "cssFloat"
			},
			style: function (t, e, n, i) {
				if (t && 3 !== t.nodeType && 8 !== t.nodeType && t.style) {
					var o, r, a, l = rt.camelCase(e),
						s = t.style;
					return e = rt.cssProps[l] || (rt.cssProps[l] = $(l) || l), a = rt.cssHooks[e] || rt.cssHooks[l], void 0 === n ? a && "get" in a && void 0 !== (o = a.get(t, !1, i)) ? o : s[e] : (r = typeof n, "string" === r && (o = Nt.exec(n)) && o[1] && (n = d(t, e, o), r = "number"), null != n && n === n && ("number" === r && (n += o && o[3] || (rt.cssNumber[l] ? "" : "px")), it.clearCloneStyle || "" !== n || 0 !== e.indexOf("background") || (s[e] = "inherit"), a && "set" in a && void 0 === (n = a.set(t, n, i)) || (s[e] = n)), void 0)
				}
			},
			css: function (t, e, n, i) {
				var o, r, a, l = rt.camelCase(e);
				return e = rt.cssProps[l] || (rt.cssProps[l] = $(l) || l), a = rt.cssHooks[e] || rt.cssHooks[l], a && "get" in a && (o = a.get(t, !0, n)), void 0 === o && (o = j(t, e, i)), "normal" === o && e in ee && (o = ee[e]), "" === n || n ? (r = parseFloat(o), n === !0 || isFinite(r) ? r || 0 : o) : o
			}
		}), rt.each(["height", "width"], function (t, e) {
			rt.cssHooks[e] = {
				get: function (t, n, i) {
					return n ? Jt.test(rt.css(t, "display")) && 0 === t.offsetWidth ? Zt(t, te, function () {
						return B(t, e, i)
					}) : B(t, e, i) : void 0
				},
				set: function (t, n, i) {
					var o, r = i && Kt(t),
						a = i && N(t, e, i, "border-box" === rt.css(t, "boxSizing", !1, r), r);
					return a && (o = Nt.exec(n)) && "px" !== (o[3] || "px") && (t.style[e] = n, n = rt.css(t, e)), D(t, n, a)
				}
			}
		}), rt.cssHooks.marginLeft = S(it.reliableMarginLeft, function (t, e) {
			return e ? (parseFloat(j(t, "marginLeft")) || t.getBoundingClientRect().left - Zt(t, {
				marginLeft: 0
			}, function () {
				return t.getBoundingClientRect().left
			})) + "px" : void 0
		}), rt.cssHooks.marginRight = S(it.reliableMarginRight, function (t, e) {
			return e ? Zt(t, {
				display: "inline-block"
			}, j, [t, "marginRight"]) : void 0
		}), rt.each({
			margin: "",
			padding: "",
			border: "Width"
		}, function (t, e) {
			rt.cssHooks[t + e] = {
				expand: function (n) {
					for (var i = 0, o = {}, r = "string" == typeof n ? n.split(" ") : [n]; 4 > i; i++) o[t + Bt[i] + e] = r[i] || r[i - 2] || r[0];
					return o
				}
			}, Gt.test(t) || (rt.cssHooks[t + e].set = D)
		}), rt.fn.extend({
			css: function (t, e) {
				return Ct(this, function (t, e, n) {
					var i, o, r = {},
						a = 0;
					if (rt.isArray(e)) {
						for (i = Kt(t), o = e.length; o > a; a++) r[e[a]] = rt.css(t, e[a], !1, i);
						return r
					}
					return void 0 !== n ? rt.style(t, e, n) : rt.css(t, e)
				}, t, e, arguments.length > 1)
			},
			show: function () {
				return A(this, !0)
			},
			hide: function () {
				return A(this)
			},
			toggle: function (t) {
				return "boolean" == typeof t ? t ? this.show() : this.hide() : this.each(function () {
					At(this) ? rt(this).show() : rt(this).hide()
				})
			}
		}), rt.Tween = E, E.prototype = {
			constructor: E,
			init: function (t, e, n, i, o, r) {
				this.elem = t, this.prop = n, this.easing = o || rt.easing._default, this.options = e, this.start = this.now = this.cur(), this.end = i, this.unit = r || (rt.cssNumber[n] ? "" : "px")
			},
			cur: function () {
				var t = E.propHooks[this.prop];
				return t && t.get ? t.get(this) : E.propHooks._default.get(this)
			},
			run: function (t) {
				var e, n = E.propHooks[this.prop];
				return this.options.duration ? this.pos = e = rt.easing[this.easing](t, this.options.duration * t, 0, 1, this.options.duration) : this.pos = e = t, this.now = (this.end - this.start) * e + this.start, this.options.step && this.options.step.call(this.elem, this.now, this), n && n.set ? n.set(this) : E.propHooks._default.set(this), this
			}
		}, E.prototype.init.prototype = E.prototype, E.propHooks = {
			_default: {
				get: function (t) {
					var e;
					return 1 !== t.elem.nodeType || null != t.elem[t.prop] && null == t.elem.style[t.prop] ? t.elem[t.prop] : (e = rt.css(t.elem, t.prop, ""), e && "auto" !== e ? e : 0)
				},
				set: function (t) {
					rt.fx.step[t.prop] ? rt.fx.step[t.prop](t) : 1 !== t.elem.nodeType || null == t.elem.style[rt.cssProps[t.prop]] && !rt.cssHooks[t.prop] ? t.elem[t.prop] = t.now : rt.style(t.elem, t.prop, t.now + t.unit)
				}
			}
		}, E.propHooks.scrollTop = E.propHooks.scrollLeft = {
			set: function (t) {
				t.elem.nodeType && t.elem.parentNode && (t.elem[t.prop] = t.now)
			}
		}, rt.easing = {
			linear: function (t) {
				return t
			},
			swing: function (t) {
				return .5 - Math.cos(t * Math.PI) / 2
			},
			_default: "swing"
		}, rt.fx = E.prototype.init, rt.fx.step = {};
		var oe, re, ae = /^(?:toggle|show|hide)$/,
			le = /queueHooks$/;
		rt.Animation = rt.extend(M, {
			tweeners: {
				"*": [function (t, e) {
					var n = this.createTween(t, e);
					return d(n.elem, t, Nt.exec(e), n), n
				}]
			},
			tweener: function (t, e) {
				rt.isFunction(t) ? (e = t, t = ["*"]) : t = t.match(xt);
				for (var n, i = 0, o = t.length; o > i; i++) n = t[i], M.tweeners[n] = M.tweeners[n] || [], M.tweeners[n].unshift(e)
			},
			prefilters: [P],
			prefilter: function (t, e) {
				e ? M.prefilters.unshift(t) : M.prefilters.push(t)
			}
		}), rt.speed = function (t, e, n) {
			var i = t && "object" == typeof t ? rt.extend({}, t) : {
				complete: n || !n && e || rt.isFunction(t) && t,
				duration: t,
				easing: n && e || e && !rt.isFunction(e) && e
			};
			return i.duration = rt.fx.off ? 0 : "number" == typeof i.duration ? i.duration : i.duration in rt.fx.speeds ? rt.fx.speeds[i.duration] : rt.fx.speeds._default, null != i.queue && i.queue !== !0 || (i.queue = "fx"), i.old = i.complete, i.complete = function () {
				rt.isFunction(i.old) && i.old.call(this), i.queue && rt.dequeue(this, i.queue)
			}, i
		}, rt.fn.extend({
			fadeTo: function (t, e, n, i) {
				return this.filter(At).css("opacity", 0).show().end().animate({
					opacity: e
				}, t, n, i)
			},
			animate: function (t, e, n, i) {
				var o = rt.isEmptyObject(t),
					r = rt.speed(e, n, i),
					a = function () {
						var e = M(this, rt.extend({}, t), r);
						(o || kt.get(this, "finish")) && e.stop(!0)
					};
				return a.finish = a, o || r.queue === !1 ? this.each(a) : this.queue(r.queue, a)
			},
			stop: function (t, e, n) {
				var i = function (t) {
					var e = t.stop;
					delete t.stop, e(n)
				};
				return "string" != typeof t && (n = e, e = t, t = void 0), e && t !== !1 && this.queue(t || "fx", []), this.each(function () {
					var e = !0,
						o = null != t && t + "queueHooks",
						r = rt.timers,
						a = kt.get(this);
					if (o) a[o] && a[o].stop && i(a[o]);
					else for (o in a) a[o] && a[o].stop && le.test(o) && i(a[o]);
					for (o = r.length; o--;) r[o].elem !== this || null != t && r[o].queue !== t || (r[o].anim.stop(n), e = !1, r.splice(o, 1));
					!e && n || rt.dequeue(this, t)
				})
			},
			finish: function (t) {
				return t !== !1 && (t = t || "fx"), this.each(function () {
					var e, n = kt.get(this),
						i = n[t + "queue"],
						o = n[t + "queueHooks"],
						r = rt.timers,
						a = i ? i.length : 0;
					for (n.finish = !0, rt.queue(this, t, []), o && o.stop && o.stop.call(this, !0), e = r.length; e--;) r[e].elem === this && r[e].queue === t && (r[e].anim.stop(!0), r.splice(e, 1));
					for (e = 0; a > e; e++) i[e] && i[e].finish && i[e].finish.call(this);
					delete n.finish
				})
			}
		}), rt.each(["toggle", "show", "hide"], function (t, e) {
			var n = rt.fn[e];
			rt.fn[e] = function (t, i, o) {
				return null == t || "boolean" == typeof t ? n.apply(this, arguments) : this.animate(L(e, !0), t, i, o)
			}
		}), rt.each({
			slideDown: L("show"),
			slideUp: L("hide"),
			slideToggle: L("toggle"),
			fadeIn: {
				opacity: "show"
			},
			fadeOut: {
				opacity: "hide"
			},
			fadeToggle: {
				opacity: "toggle"
			}
		}, function (t, e) {
			rt.fn[t] = function (t, n, i) {
				return this.animate(e, t, n, i)
			}
		}), rt.timers = [], rt.fx.tick = function () {
			var t, e = 0,
				n = rt.timers;
			for (oe = rt.now(); e < n.length; e++) t = n[e], t() || n[e] !== t || n.splice(e--, 1);
			n.length || rt.fx.stop(), oe = void 0
		}, rt.fx.timer = function (t) {
			rt.timers.push(t), t() ? rt.fx.start() : rt.timers.pop()
		}, rt.fx.interval = 13, rt.fx.start = function () {
			re || (re = t.setInterval(rt.fx.tick, rt.fx.interval))
		}, rt.fx.stop = function () {
			t.clearInterval(re), re = null
		}, rt.fx.speeds = {
			slow: 600,
			fast: 200,
			_default: 400
		}, rt.fn.delay = function (e, n) {
			return e = rt.fx ? rt.fx.speeds[e] || e : e, n = n || "fx", this.queue(n, function (n, i) {
				var o = t.setTimeout(n, e);
				i.stop = function () {
					t.clearTimeout(o)
				}
			})
		}, function () {
			var t = U.createElement("input"),
				e = U.createElement("select"),
				n = e.appendChild(U.createElement("option"));
			t.type = "checkbox", it.checkOn = "" !== t.value, it.optSelected = n.selected, e.disabled = !0, it.optDisabled = !n.disabled, t = U.createElement("input"), t.value = "t", t.type = "radio", it.radioValue = "t" === t.value
		}();
		var se, de = rt.expr.attrHandle;
		rt.fn.extend({
			attr: function (t, e) {
				return Ct(this, rt.attr, t, e, arguments.length > 1)
			},
			removeAttr: function (t) {
				return this.each(function () {
					rt.removeAttr(this, t)
				})
			}
		}), rt.extend({
			attr: function (t, e, n) {
				var i, o, r = t.nodeType;
				if (3 !== r && 8 !== r && 2 !== r) return "undefined" == typeof t.getAttribute ? rt.prop(t, e, n) : (1 === r && rt.isXMLDoc(t) || (e = e.toLowerCase(), o = rt.attrHooks[e] || (rt.expr.match.bool.test(e) ? se : void 0)), void 0 !== n ? null === n ? void rt.removeAttr(t, e) : o && "set" in o && void 0 !== (i = o.set(t, n, e)) ? i : (t.setAttribute(e, n + ""), n) : o && "get" in o && null !== (i = o.get(t, e)) ? i : (i = rt.find.attr(t, e), null == i ? void 0 : i))
			},
			attrHooks: {
				type: {
					set: function (t, e) {
						if (!it.radioValue && "radio" === e && rt.nodeName(t, "input")) {
							var n = t.value;
							return t.setAttribute("type", e), n && (t.value = n), e
						}
					}
				}
			},
			removeAttr: function (t, e) {
				var n, i, o = 0,
					r = e && e.match(xt);
				if (r && 1 === t.nodeType) for (; n = r[o++];) i = rt.propFix[n] || n, rt.expr.match.bool.test(n) && (t[i] = !1), t.removeAttribute(n)
			}
		}), se = {
			set: function (t, e, n) {
				return e === !1 ? rt.removeAttr(t, n) : t.setAttribute(n, n), n
			}
		}, rt.each(rt.expr.match.bool.source.match(/\w+/g), function (t, e) {
			var n = de[e] || rt.find.attr;
			de[e] = function (t, e, i) {
				var o, r;
				return i || (r = de[e], de[e] = o, o = null != n(t, e, i) ? e.toLowerCase() : null, de[e] = r), o
			}
		});
		var ce = /^(?:input|select|textarea|button)$/i,
			pe = /^(?:a|area)$/i;
		rt.fn.extend({
			prop: function (t, e) {
				return Ct(this, rt.prop, t, e, arguments.length > 1)
			},
			removeProp: function (t) {
				return this.each(function () {
					delete this[rt.propFix[t] || t]
				})
			}
		}), rt.extend({
			prop: function (t, e, n) {
				var i, o, r = t.nodeType;
				if (3 !== r && 8 !== r && 2 !== r) return 1 === r && rt.isXMLDoc(t) || (e = rt.propFix[e] || e, o = rt.propHooks[e]), void 0 !== n ? o && "set" in o && void 0 !== (i = o.set(t, n, e)) ? i : t[e] = n : o && "get" in o && null !== (i = o.get(t, e)) ? i : t[e]
			},
			propHooks: {
				tabIndex: {
					get: function (t) {
						var e = rt.find.attr(t, "tabindex");
						return e ? parseInt(e, 10) : ce.test(t.nodeName) || pe.test(t.nodeName) && t.href ? 0 : -1
					}
				}
			},
			propFix: {
				"for": "htmlFor",
				"class": "className"
			}
		}), it.optSelected || (rt.propHooks.selected = {
			get: function (t) {
				var e = t.parentNode;
				return e && e.parentNode && e.parentNode.selectedIndex, null
			},
			set: function (t) {
				var e = t.parentNode;
				e && (e.selectedIndex, e.parentNode && e.parentNode.selectedIndex)
			}
		}), rt.each(["tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable"], function () {
			rt.propFix[this.toLowerCase()] = this
		});
		var ue = /[\t\r\n\f]/g;
		rt.fn.extend({
			addClass: function (t) {
				var e, n, i, o, r, a, l, s = 0;
				if (rt.isFunction(t)) return this.each(function (e) {
					rt(this).addClass(t.call(this, e, F(this)))
				});
				if ("string" == typeof t && t) for (e = t.match(xt) || []; n = this[s++];) if (o = F(n), i = 1 === n.nodeType && (" " + o + " ").replace(ue, " ")) {
					for (a = 0; r = e[a++];) i.indexOf(" " + r + " ") < 0 && (i += r + " ");
					l = rt.trim(i), o !== l && n.setAttribute("class", l)
				}
				return this
			},
			removeClass: function (t) {
				var e, n, i, o, r, a, l, s = 0;
				if (rt.isFunction(t)) return this.each(function (e) {
					rt(this).removeClass(t.call(this, e, F(this)))
				});
				if (!arguments.length) return this.attr("class", "");
				if ("string" == typeof t && t) for (e = t.match(xt) || []; n = this[s++];) if (o = F(n), i = 1 === n.nodeType && (" " + o + " ").replace(ue, " ")) {
					for (a = 0; r = e[a++];) for (; i.indexOf(" " + r + " ") > -1;) i = i.replace(" " + r + " ", " ");
					l = rt.trim(i), o !== l && n.setAttribute("class", l)
				}
				return this
			},
			toggleClass: function (t, e) {
				var n = typeof t;
				return "boolean" == typeof e && "string" === n ? e ? this.addClass(t) : this.removeClass(t) : rt.isFunction(t) ? this.each(function (n) {
					rt(this).toggleClass(t.call(this, n, F(this), e), e)
				}) : this.each(function () {
					var e, i, o, r;
					if ("string" === n) for (i = 0, o = rt(this), r = t.match(xt) || []; e = r[i++];) o.hasClass(e) ? o.removeClass(e) : o.addClass(e);
					else void 0 !== t && "boolean" !== n || (e = F(this), e && kt.set(this, "__className__", e), this.setAttribute && this.setAttribute("class", e || t === !1 ? "" : kt.get(this, "__className__") || ""))
				})
			},
			hasClass: function (t) {
				var e, n, i = 0;
				for (e = " " + t + " "; n = this[i++];) if (1 === n.nodeType && (" " + F(n) + " ").replace(ue, " ").indexOf(e) > -1) return !0;
				return !1
			}
		});
		var ge = /\r/g,
			fe = /[\x20\t\r\n\f]+/g;
		rt.fn.extend({
			val: function (t) {
				var e, n, i, o = this[0]; {
					if (arguments.length) return i = rt.isFunction(t), this.each(function (n) {
						var o;
						1 === this.nodeType && (o = i ? t.call(this, n, rt(this).val()) : t, null == o ? o = "" : "number" == typeof o ? o += "" : rt.isArray(o) && (o = rt.map(o, function (t) {
							return null == t ? "" : t + ""
						})), e = rt.valHooks[this.type] || rt.valHooks[this.nodeName.toLowerCase()], e && "set" in e && void 0 !== e.set(this, o, "value") || (this.value = o))
					});
					if (o) return e = rt.valHooks[o.type] || rt.valHooks[o.nodeName.toLowerCase()], e && "get" in e && void 0 !== (n = e.get(o, "value")) ? n : (n = o.value, "string" == typeof n ? n.replace(ge, "") : null == n ? "" : n)
				}
			}
		}), rt.extend({
			valHooks: {
				option: {
					get: function (t) {
						var e = rt.find.attr(t, "value");
						return null != e ? e : rt.trim(rt.text(t)).replace(fe, " ")
					}
				},
				select: {
					get: function (t) {
						for (var e, n, i = t.options, o = t.selectedIndex, r = "select-one" === t.type || 0 > o, a = r ? null : [], l = r ? o + 1 : i.length, s = 0 > o ? l : r ? o : 0; l > s; s++) if (n = i[s], (n.selected || s === o) && (it.optDisabled ? !n.disabled : null === n.getAttribute("disabled")) && (!n.parentNode.disabled || !rt.nodeName(n.parentNode, "optgroup"))) {
							if (e = rt(n).val(), r) return e;
							a.push(e)
						}
						return a
					},
					set: function (t, e) {
						for (var n, i, o = t.options, r = rt.makeArray(e), a = o.length; a--;) i = o[a], (i.selected = rt.inArray(rt.valHooks.option.get(i), r) > -1) && (n = !0);
						return n || (t.selectedIndex = -1), r
					}
				}
			}
		}), rt.each(["radio", "checkbox"], function () {
			rt.valHooks[this] = {
				set: function (t, e) {
					return rt.isArray(e) ? t.checked = rt.inArray(rt(t).val(), e) > -1 : void 0
				}
			}, it.checkOn || (rt.valHooks[this].get = function (t) {
				return null === t.getAttribute("value") ? "on" : t.value
			})
		});
		var he = /^(?:focusinfocus|focusoutblur)$/;
		rt.extend(rt.event, {
			trigger: function (e, n, i, o) {
				var r, a, l, s, d, c, p, u = [i || U],
					g = nt.call(e, "type") ? e.type : e,
					f = nt.call(e, "namespace") ? e.namespace.split(".") : [];
				if (a = l = i = i || U, 3 !== i.nodeType && 8 !== i.nodeType && !he.test(g + rt.event.triggered) && (g.indexOf(".") > -1 && (f = g.split("."), g = f.shift(), f.sort()), d = g.indexOf(":") < 0 && "on" + g, e = e[rt.expando] ? e : new rt.Event(g, "object" == typeof e && e), e.isTrigger = o ? 2 : 3, e.namespace = f.join("."), e.rnamespace = e.namespace ? new RegExp("(^|\\.)" + f.join("\\.(?:.*\\.|)") + "(\\.|$)") : null, e.result = void 0, e.target || (e.target = i), n = null == n ? [e] : rt.makeArray(n, [e]), p = rt.event.special[g] || {}, o || !p.trigger || p.trigger.apply(i, n) !== !1)) {
					if (!o && !p.noBubble && !rt.isWindow(i)) {
						for (s = p.delegateType || g, he.test(s + g) || (a = a.parentNode); a; a = a.parentNode) u.push(a), l = a;
						l === (i.ownerDocument || U) && u.push(l.defaultView || l.parentWindow || t)
					}
					for (r = 0;
						(a = u[r++]) && !e.isPropagationStopped();) e.type = r > 1 ? s : p.bindType || g, c = (kt.get(a, "events") || {})[e.type] && kt.get(a, "handle"), c && c.apply(a, n), c = d && a[d], c && c.apply && Tt(a) && (e.result = c.apply(a, n), e.result === !1 && e.preventDefault());
					return e.type = g, o || e.isDefaultPrevented() || p._default && p._default.apply(u.pop(), n) !== !1 || !Tt(i) || d && rt.isFunction(i[g]) && !rt.isWindow(i) && (l = i[d], l && (i[d] = null), rt.event.triggered = g, i[g](), rt.event.triggered = void 0, l && (i[d] = l)), e.result
				}
			},
			simulate: function (t, e, n) {
				var i = rt.extend(new rt.Event, n, {
					type: t,
					isSimulated: !0
				});
				rt.event.trigger(i, null, e), i.isDefaultPrevented() && n.preventDefault()
			}
		}), rt.fn.extend({
			trigger: function (t, e) {
				return this.each(function () {
					rt.event.trigger(t, e, this)
				})
			},
			triggerHandler: function (t, e) {
				var n = this[0];
				return n ? rt.event.trigger(t, e, n, !0) : void 0
			}
		}), rt.each("blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu".split(" "), function (t, e) {
			rt.fn[e] = function (t, n) {
				return arguments.length > 0 ? this.on(e, null, t, n) : this.trigger(e)
			}
		}), rt.fn.extend({
			hover: function (t, e) {
				return this.mouseenter(t).mouseleave(e || t)
			}
		}), it.focusin = "onfocusin" in t, it.focusin || rt.each({
			focus: "focusin",
			blur: "focusout"
		}, function (t, e) {
			var n = function (t) {
				rt.event.simulate(e, t.target, rt.event.fix(t))
			};
			rt.event.special[e] = {
				setup: function () {
					var i = this.ownerDocument || this,
						o = kt.access(i, e);
					o || i.addEventListener(t, n, !0), kt.access(i, e, (o || 0) + 1)
				},
				teardown: function () {
					var i = this.ownerDocument || this,
						o = kt.access(i, e) - 1;
					o ? kt.access(i, e, o) : (i.removeEventListener(t, n, !0), kt.remove(i, e))
				}
			}
		});
		var me = t.location,
			ve = rt.now(),
			be = /\?/;
		rt.parseJSON = function (t) {
			return JSON.parse(t + "")
		}, rt.parseXML = function (e) {
			var n;
			if (!e || "string" != typeof e) return null;
			try {
				n = (new t.DOMParser).parseFromString(e, "text/xml")
			} catch (i) {
				n = void 0
			}
			return n && !n.getElementsByTagName("parsererror").length || rt.error("Invalid XML: " + e), n
		};
		var ye = /#.*$/,
			we = /([?&])_=[^&]*/,
			xe = /^(.*?):[ \t]*([^\r\n]*)$/gm,
			_e = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/,
			Ce = /^(?:GET|HEAD)$/,
			Te = /^\/\//,
			ke = {},
			je = {},
			Se = "*/".concat("*"),
			$e = U.createElement("a");
		$e.href = me.href, rt.extend({
			active: 0,
			lastModified: {},
			etag: {},
			ajaxSettings: {
				url: me.href,
				type: "GET",
				isLocal: _e.test(me.protocol),
				global: !0,
				processData: !0,
				async: !0,
				contentType: "application/x-www-form-urlencoded; charset=UTF-8",
				accepts: {
					"*": Se,
					text: "text/plain",
					html: "text/html",
					xml: "application/xml, text/xml",
					json: "application/json, text/javascript"
				},
				contents: {
					xml: /\bxml\b/,
					html: /\bhtml/,
					json: /\bjson\b/
				},
				responseFields: {
					xml: "responseXML",
					text: "responseText",
					json: "responseJSON"
				},
				converters: {
					"* text": String,
					"text html": !0,
					"text json": rt.parseJSON,
					"text xml": rt.parseXML
				},
				flatOptions: {
					url: !0,
					context: !0
				}
			},
			ajaxSetup: function (t, e) {
				return e ? R(R(t, rt.ajaxSettings), e) : R(rt.ajaxSettings, t)
			},
			ajaxPrefilter: W(ke),
			ajaxTransport: W(je),
			ajax: function (e, n) {
				function i(e, n, i, l) {
					var d, p, b, y, x, C = n;
					2 !== w && (w = 2, s && t.clearTimeout(s), o = void 0, a = l || "", _.readyState = e > 0 ? 4 : 0, d = e >= 200 && 300 > e || 304 === e, i && (y = X(u, _, i)), y = z(u, y, _, d), d ? (u.ifModified && (x = _.getResponseHeader("Last-Modified"), x && (rt.lastModified[r] = x), x = _.getResponseHeader("etag"), x && (rt.etag[r] = x)), 204 === e || "HEAD" === u.type ? C = "nocontent" : 304 === e ? C = "notmodified" : (C = y.state, p = y.data, b = y.error, d = !b)) : (b = C, !e && C || (C = "error", 0 > e && (e = 0))), _.status = e, _.statusText = (n || C) + "", d ? h.resolveWith(g, [p, C, _]) : h.rejectWith(g, [_, C, b]), _.statusCode(v), v = void 0, c && f.trigger(d ? "ajaxSuccess" : "ajaxError", [_, u, d ? p : b]), m.fireWith(g, [_, C]), c && (f.trigger("ajaxComplete", [_, u]), --rt.active || rt.event.trigger("ajaxStop")))
				}
				"object" == typeof e && (n = e, e = void 0), n = n || {};
				var o, r, a, l, s, d, c, p, u = rt.ajaxSetup({}, n),
					g = u.context || u,
					f = u.context && (g.nodeType || g.jquery) ? rt(g) : rt.event,
					h = rt.Deferred(),
					m = rt.Callbacks("once memory"),
					v = u.statusCode || {},
					b = {},
					y = {},
					w = 0,
					x = "canceled",
					_ = {
						readyState: 0,
						getResponseHeader: function (t) {
							var e;
							if (2 === w) {
								if (!l) for (l = {}; e = xe.exec(a);) l[e[1].toLowerCase()] = e[2];
								e = l[t.toLowerCase()]
							}
							return null == e ? null : e
						},
						getAllResponseHeaders: function () {
							return 2 === w ? a : null
						},
						setRequestHeader: function (t, e) {
							var n = t.toLowerCase();
							return w || (t = y[n] = y[n] || t, b[t] = e), this
						},
						overrideMimeType: function (t) {
							return w || (u.mimeType = t), this
						},
						statusCode: function (t) {
							var e;
							if (t) if (2 > w) for (e in t) v[e] = [v[e], t[e]];
							else _.always(t[_.status]);
							return this
						},
						abort: function (t) {
							var e = t || x;
							return o && o.abort(e), i(0, e), this
						}
					};
				if (h.promise(_).complete = m.add, _.success = _.done, _.error = _.fail, u.url = ((e || u.url || me.href) + "").replace(ye, "").replace(Te, me.protocol + "//"), u.type = n.method || n.type || u.method || u.type, u.dataTypes = rt.trim(u.dataType || "*").toLowerCase().match(xt) || [""], null == u.crossDomain) {
					d = U.createElement("a");
					try {
						d.href = u.url, d.href = d.href, u.crossDomain = $e.protocol + "//" + $e.host != d.protocol + "//" + d.host
					} catch (C) {
						u.crossDomain = !0
					}
				}
				if (u.data && u.processData && "string" != typeof u.data && (u.data = rt.param(u.data, u.traditional)), q(ke, u, n, _), 2 === w) return _;
				c = rt.event && u.global, c && 0 === rt.active++ && rt.event.trigger("ajaxStart"), u.type = u.type.toUpperCase(), u.hasContent = !Ce.test(u.type), r = u.url, u.hasContent || (u.data && (r = u.url += (be.test(r) ? "&" : "?") + u.data, delete u.data), u.cache === !1 && (u.url = we.test(r) ? r.replace(we, "$1_=" + ve++) : r + (be.test(r) ? "&" : "?") + "_=" + ve++)), u.ifModified && (rt.lastModified[r] && _.setRequestHeader("If-Modified-Since", rt.lastModified[r]), rt.etag[r] && _.setRequestHeader("If-None-Match", rt.etag[r])), (u.data && u.hasContent && u.contentType !== !1 || n.contentType) && _.setRequestHeader("Content-Type", u.contentType), _.setRequestHeader("Accept", u.dataTypes[0] && u.accepts[u.dataTypes[0]] ? u.accepts[u.dataTypes[0]] + ("*" !== u.dataTypes[0] ? ", " + Se + "; q=0.01" : "") : u.accepts["*"]);
				for (p in u.headers) _.setRequestHeader(p, u.headers[p]);
				if (u.beforeSend && (u.beforeSend.call(g, _, u) === !1 || 2 === w)) return _.abort();
				x = "abort";
				for (p in {
					success: 1,
					error: 1,
					complete: 1
				}) _[p](u[p]);
				if (o = q(je, u, n, _)) {
					if (_.readyState = 1, c && f.trigger("ajaxSend", [_, u]), 2 === w) return _;
					u.async && u.timeout > 0 && (s = t.setTimeout(function () {
						_.abort("timeout")
					}, u.timeout));
					try {
						w = 1, o.send(b, i)
					} catch (C) {
						if (!(2 > w)) throw C;
						i(-1, C)
					}
				} else i(-1, "No Transport");
				return _
			},
			getJSON: function (t, e, n) {
				return rt.get(t, e, n, "json")
			},
			getScript: function (t, e) {
				return rt.get(t, void 0, e, "script")
			}
		}), rt.each(["get", "post"], function (t, e) {
			rt[e] = function (t, n, i, o) {
				return rt.isFunction(n) && (o = o || i, i = n, n = void 0), rt.ajax(rt.extend({
					url: t,
					type: e,
					dataType: o,
					data: n,
					success: i
				}, rt.isPlainObject(t) && t))
			}
		}), rt._evalUrl = function (t) {
			return rt.ajax({
				url: t,
				type: "GET",
				dataType: "script",
				async: !1,
				global: !1,
				"throws": !0
			})
		}, rt.fn.extend({
			wrapAll: function (t) {
				var e;
				return rt.isFunction(t) ? this.each(function (e) {
					rt(this).wrapAll(t.call(this, e))
				}) : (this[0] && (e = rt(t, this[0].ownerDocument).eq(0).clone(!0), this[0].parentNode && e.insertBefore(this[0]), e.map(function () {
					for (var t = this; t.firstElementChild;) t = t.firstElementChild;
					return t
				}).append(this)), this)
			},
			wrapInner: function (t) {
				return rt.isFunction(t) ? this.each(function (e) {
					rt(this).wrapInner(t.call(this, e))
				}) : this.each(function () {
					var e = rt(this),
						n = e.contents();
					n.length ? n.wrapAll(t) : e.append(t)
				})
			},
			wrap: function (t) {
				var e = rt.isFunction(t);
				return this.each(function (n) {
					rt(this).wrapAll(e ? t.call(this, n) : t)
				})
			},
			unwrap: function () {
				return this.parent().each(function () {
					rt.nodeName(this, "body") || rt(this).replaceWith(this.childNodes)
				}).end()
			}
		}), rt.expr.filters.hidden = function (t) {
			return !rt.expr.filters.visible(t)
		}, rt.expr.filters.visible = function (t) {
			return t.offsetWidth > 0 || t.offsetHeight > 0 || t.getClientRects().length > 0
		};
		var De = /%20/g,
			Ne = /\[\]$/,
			Be = /\r?\n/g,
			Ae = /^(?:submit|button|image|reset|file)$/i,
			Ee = /^(?:input|select|textarea|keygen)/i;
		rt.param = function (t, e) {
			var n, i = [],
				o = function (t, e) {
					e = rt.isFunction(e) ? e() : null == e ? "" : e, i[i.length] = encodeURIComponent(t) + "=" + encodeURIComponent(e)
				};
			if (void 0 === e && (e = rt.ajaxSettings && rt.ajaxSettings.traditional), rt.isArray(t) || t.jquery && !rt.isPlainObject(t)) rt.each(t, function () {
				o(this.name, this.value)
			});
			else for (n in t) Y(n, t[n], e, o);
			return i.join("&").replace(De, "+")
		}, rt.fn.extend({
			serialize: function () {
				return rt.param(this.serializeArray())
			},
			serializeArray: function () {
				return this.map(function () {
					var t = rt.prop(this, "elements");
					return t ? rt.makeArray(t) : this
				}).filter(function () {
					var t = this.type;
					return this.name && !rt(this).is(":disabled") && Ee.test(this.nodeName) && !Ae.test(t) && (this.checked || !Et.test(t))
				}).map(function (t, e) {
					var n = rt(this).val();
					return null == n ? null : rt.isArray(n) ? rt.map(n, function (t) {
						return {
							name: e.name,
							value: t.replace(Be, "\r\n")
						}
					}) : {
							name: e.name,
							value: n.replace(Be, "\r\n")
						}
				}).get()
			}
		}), rt.ajaxSettings.xhr = function () {
			try {
				return new t.XMLHttpRequest
			} catch (e) { }
		};
		var He = {
			0: 200,
			1223: 204
		},
			Le = rt.ajaxSettings.xhr();
		it.cors = !!Le && "withCredentials" in Le, it.ajax = Le = !!Le, rt.ajaxTransport(function (e) {
			var n, i;
			return it.cors || Le && !e.crossDomain ? {
				send: function (o, r) {
					var a, l = e.xhr();
					if (l.open(e.type, e.url, e.async, e.username, e.password), e.xhrFields) for (a in e.xhrFields) l[a] = e.xhrFields[a];
					e.mimeType && l.overrideMimeType && l.overrideMimeType(e.mimeType), e.crossDomain || o["X-Requested-With"] || (o["X-Requested-With"] = "XMLHttpRequest");
					for (a in o) l.setRequestHeader(a, o[a]);
					n = function (t) {
						return function () {
							n && (n = i = l.onload = l.onerror = l.onabort = l.onreadystatechange = null, "abort" === t ? l.abort() : "error" === t ? "number" != typeof l.status ? r(0, "error") : r(l.status, l.statusText) : r(He[l.status] || l.status, l.statusText, "text" !== (l.responseType || "text") || "string" != typeof l.responseText ? {
								binary: l.response
							} : {
									text: l.responseText
								}, l.getAllResponseHeaders()))
						}
					}, l.onload = n(), i = l.onerror = n("error"), void 0 !== l.onabort ? l.onabort = i : l.onreadystatechange = function () {
						4 === l.readyState && t.setTimeout(function () {
							n && i()
						})
					}, n = n("abort");
					try {
						l.send(e.hasContent && e.data || null)
					} catch (s) {
						if (n) throw s
					}
				},
				abort: function () {
					n && n()
				}
			} : void 0
		}), rt.ajaxSetup({
			accepts: {
				script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"
			},
			contents: {
				script: /\b(?:java|ecma)script\b/
			},
			converters: {
				"text script": function (t) {
					return rt.globalEval(t), t
				}
			}
		}), rt.ajaxPrefilter("script", function (t) {
			void 0 === t.cache && (t.cache = !1), t.crossDomain && (t.type = "GET")
		}), rt.ajaxTransport("script", function (t) {
			if (t.crossDomain) {
				var e, n;
				return {
					send: function (i, o) {
						e = rt("<script>").prop({
							charset: t.scriptCharset,
							src: t.url
						}).on("load error", n = function (t) {
							e.remove(), n = null, t && o("error" === t.type ? 404 : 200, t.type)
						}), U.head.appendChild(e[0])
					},
					abort: function () {
						n && n()
					}
				}
			}
		});
		var Oe = [],
			Pe = /(=)\?(?=&|$)|\?\?/;
		rt.ajaxSetup({
			jsonp: "callback",
			jsonpCallback: function () {
				var t = Oe.pop() || rt.expando + "_" + ve++;
				return this[t] = !0, t
			}
		}), rt.ajaxPrefilter("json jsonp", function (e, n, i) {
			var o, r, a, l = e.jsonp !== !1 && (Pe.test(e.url) ? "url" : "string" == typeof e.data && 0 === (e.contentType || "").indexOf("application/x-www-form-urlencoded") && Pe.test(e.data) && "data");
			return l || "jsonp" === e.dataTypes[0] ? (o = e.jsonpCallback = rt.isFunction(e.jsonpCallback) ? e.jsonpCallback() : e.jsonpCallback, l ? e[l] = e[l].replace(Pe, "$1" + o) : e.jsonp !== !1 && (e.url += (be.test(e.url) ? "&" : "?") + e.jsonp + "=" + o), e.converters["script json"] = function () {
				return a || rt.error(o + " was not called"), a[0]
			}, e.dataTypes[0] = "json", r = t[o], t[o] = function () {
				a = arguments
			}, i.always(function () {
				void 0 === r ? rt(t).removeProp(o) : t[o] = r, e[o] && (e.jsonpCallback = n.jsonpCallback, Oe.push(o)), a && rt.isFunction(r) && r(a[0]), a = r = void 0
			}), "script") : void 0
		}), rt.parseHTML = function (t, e, n) {
			if (!t || "string" != typeof t) return null;
			"boolean" == typeof e && (n = e, e = !1), e = e || U;
			var i = ft.exec(t),
				o = !n && [];
			return i ? [e.createElement(i[1])] : (i = u([t], e, o), o && o.length && rt(o).remove(), rt.merge([], i.childNodes))
		};
		var Ie = rt.fn.load;
		rt.fn.load = function (t, e, n) {
			if ("string" != typeof t && Ie) return Ie.apply(this, arguments);
			var i, o, r, a = this,
				l = t.indexOf(" ");
			return l > -1 && (i = rt.trim(t.slice(l)), t = t.slice(0, l)), rt.isFunction(e) ? (n = e, e = void 0) : e && "object" == typeof e && (o = "POST"), a.length > 0 && rt.ajax({
				url: t,
				type: o || "GET",
				dataType: "html",
				data: e
			}).done(function (t) {
				r = arguments, a.html(i ? rt("<div>").append(rt.parseHTML(t)).find(i) : t)
			}).always(n &&
				function (t, e) {
					a.each(function () {
						n.apply(this, r || [t.responseText, e, t])
					})
				}), this
		}, rt.each(["ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend"], function (t, e) {
			rt.fn[e] = function (t) {
				return this.on(e, t)
			}
		}), rt.expr.filters.animated = function (t) {
			return rt.grep(rt.timers, function (e) {
				return t === e.elem
			}).length
		}, rt.offset = {
			setOffset: function (t, e, n) {
				var i, o, r, a, l, s, d, c = rt.css(t, "position"),
					p = rt(t),
					u = {};
				"static" === c && (t.style.position = "relative"), l = p.offset(), r = rt.css(t, "top"), s = rt.css(t, "left"), d = ("absolute" === c || "fixed" === c) && (r + s).indexOf("auto") > -1, d ? (i = p.position(), a = i.top, o = i.left) : (a = parseFloat(r) || 0, o = parseFloat(s) || 0), rt.isFunction(e) && (e = e.call(t, n, rt.extend({}, l))), null != e.top && (u.top = e.top - l.top + a), null != e.left && (u.left = e.left - l.left + o), "using" in e ? e.using.call(t, u) : p.css(u)
			}
		}, rt.fn.extend({
			offset: function (t) {
				if (arguments.length) return void 0 === t ? this : this.each(function (e) {
					rt.offset.setOffset(this, t, e)
				});
				var e, n, i = this[0],
					o = {
						top: 0,
						left: 0
					},
					r = i && i.ownerDocument;
				if (r) return e = r.documentElement, rt.contains(e, i) ? (o = i.getBoundingClientRect(), n = V(r), {
					top: o.top + n.pageYOffset - e.clientTop,
					left: o.left + n.pageXOffset - e.clientLeft
				}) : o
			},
			position: function () {
				if (this[0]) {
					var t, e, n = this[0],
						i = {
							top: 0,
							left: 0
						};
					return "fixed" === rt.css(n, "position") ? e = n.getBoundingClientRect() : (t = this.offsetParent(), e = this.offset(), rt.nodeName(t[0], "html") || (i = t.offset()), i.top += rt.css(t[0], "borderTopWidth", !0), i.left += rt.css(t[0], "borderLeftWidth", !0)), {
						top: e.top - i.top - rt.css(n, "marginTop", !0),
						left: e.left - i.left - rt.css(n, "marginLeft", !0)
					}
				}
			},
			offsetParent: function () {
				return this.map(function () {
					for (var t = this.offsetParent; t && "static" === rt.css(t, "position");) t = t.offsetParent;
					return t || Qt
				})
			}
		}), rt.each({
			scrollLeft: "pageXOffset",
			scrollTop: "pageYOffset"
		}, function (t, e) {
			var n = "pageYOffset" === e;
			rt.fn[t] = function (i) {
				return Ct(this, function (t, i, o) {
					var r = V(t);
					return void 0 === o ? r ? r[e] : t[i] : void (r ? r.scrollTo(n ? r.pageXOffset : o, n ? o : r.pageYOffset) : t[i] = o)
				}, t, i, arguments.length)
			}
		}), rt.each(["top", "left"], function (t, e) {
			rt.cssHooks[e] = S(it.pixelPosition, function (t, n) {
				return n ? (n = j(t, e), Ut.test(n) ? rt(t).position()[e] + "px" : n) : void 0
			})
		}), rt.each({
			Height: "height",
			Width: "width"
		}, function (t, e) {
			rt.each({
				padding: "inner" + t,
				content: e,
				"": "outer" + t
			}, function (n, i) {
				rt.fn[i] = function (i, o) {
					var r = arguments.length && (n || "boolean" != typeof i),
						a = n || (i === !0 || o === !0 ? "margin" : "border");
					return Ct(this, function (e, n, i) {
						var o;
						return rt.isWindow(e) ? e.document.documentElement["client" + t] : 9 === e.nodeType ? (o = e.documentElement, Math.max(e.body["scroll" + t], o["scroll" + t], e.body["offset" + t], o["offset" + t], o["client" + t])) : void 0 === i ? rt.css(e, n, a) : rt.style(e, n, i, a)
					}, e, r ? i : void 0, r, null)
				}
			})
		}), rt.fn.extend({
			bind: function (t, e, n) {
				return this.on(t, null, e, n)
			},
			unbind: function (t, e) {
				return this.off(t, null, e)
			},
			delegate: function (t, e, n, i) {
				return this.on(e, t, n, i)
			},
			undelegate: function (t, e, n) {
				return 1 === arguments.length ? this.off(t, "**") : this.off(e, t || "**", n)
			},
			size: function () {
				return this.length
			}
		}), rt.fn.andSelf = rt.fn.addBack, "function" == typeof define && define.amd && define("jquery", [], function () {
			return rt
		});
		var Me = t.jQuery,
			Fe = t.$;
		return rt.noConflict = function (e) {
			return t.$ === rt && (t.$ = Fe), e && t.jQuery === rt && (t.jQuery = Me), rt
		}, e || (t.jQuery = t.$ = rt), rt
	}), function (t) {
		"function" == typeof define && define.amd ? define(["jquery"], t) : "object" == typeof exports ? module.exports = t(require("jquery")) : t(jQuery)
	}(function (t) {
		t.fn.jScrollPane = function (e) {
			function n(e, n) {
				function i(n) {
					var r, l, d, c, p, f, h = !1,
						m = !1;
					if (M = n, void 0 === F) p = e.scrollTop(), f = e.scrollLeft(), e.css({
						overflow: "hidden",
						padding: 0
					}), W = e.innerWidth() + vt, q = e.innerHeight(), e.width(W), F = t('<div class="jspPane" />').css("padding", mt).append(e.children()), R = t('<div class="jspContainer" />').css({
						width: W + "px",
						height: q + "px"
					}).append(F).appendTo(e);
					else {
						if (e.css("width", ""), h = M.stickToBottom && j(), m = M.stickToRight && S(), c = e.innerWidth() + vt != W || e.outerHeight() != q, c && (W = e.innerWidth() + vt, q = e.innerHeight(), R.css({
							width: W + "px",
							height: q + "px"
						})), !c && bt == X && F.outerHeight() == z) return void e.width(W);
						bt = X, F.css("width", ""), e.width(W), R.find(">.jspVerticalBar,>.jspHorizontalBar").remove().end()
					}
					F.css("overflow", "auto"), X = n.contentWidth ? n.contentWidth : F[0].scrollWidth, z = F[0].scrollHeight, F.css("overflow", ""), Y = X / W, V = z / q, G = V > 1, U = Y > 1, U || G ? (e.addClass("jspScrollable"), r = M.maintainPosition && (Q || et), r && (l = T(), d = k()), o(), a(), s(), r && (_(m ? X - W : l, !1), x(h ? z - q : d, !1)), B(), $(), P(), M.enableKeyboardNavigation && E(), M.clickOnTrack && u(), L(), M.hijackInternalLinks && O()) : (e.removeClass("jspScrollable"), F.css({
						top: 0,
						left: 0,
						width: R.width() - vt
					}), D(), A(), H(), g()), M.autoReinitialise && !ht ? ht = setInterval(function () {
						i(M)
					}, M.autoReinitialiseDelay) : !M.autoReinitialise && ht && clearInterval(ht), p && e.scrollTop(0) && x(p, !1), f && e.scrollLeft(0) && _(f, !1), e.trigger("jsp-initialised", [U || G])
				}
				function o() {
					G && (R.append(t('<div class="jspVerticalBar" />').append(t('<div class="jspCap jspCapTop" />'), t('<div class="jspTrack" />').append(t('<div class="jspDrag" />').append(t('<div class="jspDragTop" />'), t('<div class="jspDragBottom" />'))), t('<div class="jspCap jspCapBottom" />'))), nt = R.find(">.jspVerticalBar"), it = nt.find(">.jspTrack"), K = it.find(">.jspDrag"), M.showArrows && (lt = t('<a class="jspArrow jspArrowUp" />').bind("mousedown.jsp", c(0, -1)).bind("click.jsp", N), st = t('<a class="jspArrow jspArrowDown" />').bind("mousedown.jsp", c(0, 1)).bind("click.jsp", N), M.arrowScrollOnHover && (lt.bind("mouseover.jsp", c(0, -1, lt)), st.bind("mouseover.jsp", c(0, 1, st))), d(it, M.verticalArrowPositions, lt, st)), rt = q, R.find(">.jspVerticalBar>.jspCap:visible,>.jspVerticalBar>.jspArrow").each(function () {
						rt -= t(this).outerHeight()
					}), K.hover(function () {
						K.addClass("jspHover")
					}, function () {
						K.removeClass("jspHover")
					}).bind("mousedown.jsp", function (e) {
						t("html").bind("dragstart.jsp selectstart.jsp", N), K.addClass("jspActive");
						var n = e.pageY - K.position().top;
						return t("html").bind("mousemove.jsp", function (t) {
							h(t.pageY - n, !1)
						}).bind("mouseup.jsp mouseleave.jsp", f), !1
					}), r())
				}
				function r() {
					it.height(rt + "px"), Q = 0, ot = M.verticalGutter + it.outerWidth(), F.width(W - ot - vt);
					try {
						0 === nt.position().left && F.css("margin-left", ot + "px")
					} catch (t) { }
				}
				function a() {
					U && (R.append(t('<div class="jspHorizontalBar" />').append(t('<div class="jspCap jspCapLeft" />'), t('<div class="jspTrack" />').append(t('<div class="jspDrag" />').append(t('<div class="jspDragLeft" />'), t('<div class="jspDragRight" />'))), t('<div class="jspCap jspCapRight" />'))), dt = R.find(">.jspHorizontalBar"), ct = dt.find(">.jspTrack"), J = ct.find(">.jspDrag"), M.showArrows && (gt = t('<a class="jspArrow jspArrowLeft" />').bind("mousedown.jsp", c(-1, 0)).bind("click.jsp", N), ft = t('<a class="jspArrow jspArrowRight" />').bind("mousedown.jsp", c(1, 0)).bind("click.jsp", N), M.arrowScrollOnHover && (gt.bind("mouseover.jsp", c(-1, 0, gt)), ft.bind("mouseover.jsp", c(1, 0, ft))), d(ct, M.horizontalArrowPositions, gt, ft)), J.hover(function () {
						J.addClass("jspHover")
					}, function () {
						J.removeClass("jspHover")
					}).bind("mousedown.jsp", function (e) {
						t("html").bind("dragstart.jsp selectstart.jsp", N), J.addClass("jspActive");
						var n = e.pageX - J.position().left;
						return t("html").bind("mousemove.jsp", function (t) {
							v(t.pageX - n, !1)
						}).bind("mouseup.jsp mouseleave.jsp", f), !1
					}), pt = R.innerWidth(), l())
				}
				function l() {
					R.find(">.jspHorizontalBar>.jspCap:visible,>.jspHorizontalBar>.jspArrow").each(function () {
						pt -= t(this).outerWidth()
					}), ct.width(pt + "px"), et = 0
				}
				function s() {
					if (U && G) {
						var e = ct.outerHeight(),
							n = it.outerWidth();
						rt -= e, t(dt).find(">.jspCap:visible,>.jspArrow").each(function () {
							pt += t(this).outerWidth()
						}), pt -= n, q -= n, W -= e, ct.parent().append(t('<div class="jspCorner" />').css("width", e + "px")), r(), l()
					}
					U && F.width(R.outerWidth() - vt + "px"), z = F.outerHeight(), V = z / q, U && (ut = Math.ceil(1 / Y * pt), ut > M.horizontalDragMaxWidth ? ut = M.horizontalDragMaxWidth : ut < M.horizontalDragMinWidth && (ut = M.horizontalDragMinWidth), J.width(ut + "px"), tt = pt - ut, b(et)), G && (at = Math.ceil(1 / V * rt), at > M.verticalDragMaxHeight ? at = M.verticalDragMaxHeight : at < M.verticalDragMinHeight && (at = M.verticalDragMinHeight), K.height(at + "px"), Z = rt - at, m(Q))
				}
				function d(t, e, n, i) {
					var o, r = "before",
						a = "after";
					"os" == e && (e = /Mac/.test(navigator.platform) ? "after" : "split"), e == r ? a = e : e == a && (r = e, o = n, n = i, i = o), t[r](n)[a](i)
				}
				function c(t, e, n) {
					return function () {
						return p(t, e, this, n), this.blur(), !1
					}
				}
				function p(e, n, i, o) {
					i = t(i).addClass("jspActive");
					var r, a, l = !0,
						s = function () {
							0 !== e && yt.scrollByX(e * M.arrowButtonSpeed), 0 !== n && yt.scrollByY(n * M.arrowButtonSpeed), a = setTimeout(s, l ? M.initialDelay : M.arrowRepeatFreq), l = !1
						};
					s(), r = o ? "mouseout.jsp" : "mouseup.jsp", o = o || t("html"), o.bind(r, function () {
						i.removeClass("jspActive"), a && clearTimeout(a), a = null, o.unbind(r)
					})
				}
				function u() {
					g(), G && it.bind("mousedown.jsp", function (e) {
						if (void 0 === e.originalTarget || e.originalTarget == e.currentTarget) {
							var n, i = t(this),
								o = i.offset(),
								r = e.pageY - o.top - Q,
								a = !0,
								l = function () {
									var t = i.offset(),
										o = e.pageY - t.top - at / 2,
										d = q * M.scrollPagePercent,
										c = Z * d / (z - q);
									if (0 > r) Q - c > o ? yt.scrollByY(-d) : h(o);
									else {
										if (!(r > 0)) return void s();
										o > Q + c ? yt.scrollByY(d) : h(o)
									}
									n = setTimeout(l, a ? M.initialDelay : M.trackClickRepeatFreq), a = !1
								},
								s = function () {
									n && clearTimeout(n), n = null, t(document).unbind("mouseup.jsp", s)
								};
							return l(), t(document).bind("mouseup.jsp", s), !1
						}
					}), U && ct.bind("mousedown.jsp", function (e) {
						if (void 0 === e.originalTarget || e.originalTarget == e.currentTarget) {
							var n, i = t(this),
								o = i.offset(),
								r = e.pageX - o.left - et,
								a = !0,
								l = function () {
									var t = i.offset(),
										o = e.pageX - t.left - ut / 2,
										d = W * M.scrollPagePercent,
										c = tt * d / (X - W);
									if (0 > r) et - c > o ? yt.scrollByX(-d) : v(o);
									else {
										if (!(r > 0)) return void s();
										o > et + c ? yt.scrollByX(d) : v(o)
									}
									n = setTimeout(l, a ? M.initialDelay : M.trackClickRepeatFreq), a = !1
								},
								s = function () {
									n && clearTimeout(n), n = null, t(document).unbind("mouseup.jsp", s)
								};
							return l(), t(document).bind("mouseup.jsp", s), !1
						}
					})
				}
				function g() {
					ct && ct.unbind("mousedown.jsp"), it && it.unbind("mousedown.jsp")
				}
				function f() {
					t("html").unbind("dragstart.jsp selectstart.jsp mousemove.jsp mouseup.jsp mouseleave.jsp"), K && K.removeClass("jspActive"), J && J.removeClass("jspActive")
				}
				function h(n, i) {
					if (G) {
						0 > n ? n = 0 : n > Z && (n = Z);
						var o = new t.Event("jsp-will-scroll-y");
						if (e.trigger(o, [n]), !o.isDefaultPrevented()) {
							var r = n || 0,
								a = 0 === r,
								l = r == Z,
								s = n / Z,
								d = -s * (z - q);
							void 0 === i && (i = M.animateScroll), i ? yt.animate(K, "top", n, m, function () {
								e.trigger("jsp-user-scroll-y", [-d, a, l])
							}) : (K.css("top", n), m(n), e.trigger("jsp-user-scroll-y", [-d, a, l]))
						}
					}
				}
				function m(t) {
					void 0 === t && (t = K.position().top), R.scrollTop(0), Q = t || 0;
					var n = 0 === Q,
						i = Q == Z,
						o = t / Z,
						r = -o * (z - q);
					wt == n && _t == i || (wt = n, _t = i, e.trigger("jsp-arrow-change", [wt, _t, xt, Ct])), y(n, i), F.css("top", r), e.trigger("jsp-scroll-y", [-r, n, i]).trigger("scroll")
				}
				function v(n, i) {
					if (U) {
						0 > n ? n = 0 : n > tt && (n = tt);
						var o = new t.Event("jsp-will-scroll-x");
						if (e.trigger(o, [n]), !o.isDefaultPrevented()) {
							var r = n || 0,
								a = 0 === r,
								l = r == tt,
								s = n / tt,
								d = -s * (X - W);
							void 0 === i && (i = M.animateScroll), i ? yt.animate(J, "left", n, b, function () {
								e.trigger("jsp-user-scroll-x", [-d, a, l])
							}) : (J.css("left", n), b(n), e.trigger("jsp-user-scroll-x", [-d, a, l]))
						}
					}
				}
				function b(t) {
					void 0 === t && (t = J.position().left), R.scrollTop(0), et = t || 0;
					var n = 0 === et,
						i = et == tt,
						o = t / tt,
						r = -o * (X - W);
					xt == n && Ct == i || (xt = n, Ct = i, e.trigger("jsp-arrow-change", [wt, _t, xt, Ct])), w(n, i), F.css("left", r), e.trigger("jsp-scroll-x", [-r, n, i]).trigger("scroll")
				}
				function y(t, e) {
					M.showArrows && (lt[t ? "addClass" : "removeClass"]("jspDisabled"), st[e ? "addClass" : "removeClass"]("jspDisabled"))
				}
				function w(t, e) {
					M.showArrows && (gt[t ? "addClass" : "removeClass"]("jspDisabled"), ft[e ? "addClass" : "removeClass"]("jspDisabled"))
				}
				function x(t, e) {
					var n = t / (z - q);
					h(n * Z, e)
				}
				function _(t, e) {
					var n = t / (X - W);
					v(n * tt, e)
				}
				function C(e, n, i) {
					var o, r, a, l, s, d, c, p, u, g = 0,
						f = 0;
					try {
						o = t(e)
					} catch (h) {
						return
					}
					for (r = o.outerHeight(), a = o.outerWidth(), R.scrollTop(0), R.scrollLeft(0); !o.is(".jspPane");) if (g += o.position().top, f += o.position().left, o = o.offsetParent(), /^body|html$/i.test(o[0].nodeName)) return;
					l = k(), d = l + q, l > g || n ? p = g - M.horizontalGutter : g + r > d && (p = g - q + r + M.horizontalGutter), isNaN(p) || x(p, i), s = T(), c = s + W, s > f || n ? u = f - M.horizontalGutter : f + a > c && (u = f - W + a + M.horizontalGutter), isNaN(u) || _(u, i)
				}
				function T() {
					return -F.position().left
				}
				function k() {
					return -F.position().top
				}
				function j() {
					var t = z - q;
					return t > 20 && t - k() < 10
				}
				function S() {
					var t = X - W;
					return t > 20 && t - T() < 10
				}
				function $() {
					R.unbind(kt).bind(kt, function (t, e, n, i) {
						et || (et = 0), Q || (Q = 0);
						var o = et,
							r = Q,
							a = t.deltaFactor || M.mouseWheelSpeed;
						return yt.scrollBy(n * a, -i * a, !1), o == et && r == Q
					})
				}
				function D() {
					R.unbind(kt)
				}
				function N() {
					return !1
				}
				function B() {
					F.find(":input,a").unbind("focus.jsp").bind("focus.jsp", function (t) {
						C(t.target, !1)
					})
				}
				function A() {
					F.find(":input,a").unbind("focus.jsp")
				}
				function E() {
					function n() {
						var t = et,
							e = Q;
						switch (i) {
							case 40:
								yt.scrollByY(M.keyboardSpeed, !1);
								break;
							case 38:
								yt.scrollByY(-M.keyboardSpeed, !1);
								break;
							case 34:
							case 32:
								yt.scrollByY(q * M.scrollPagePercent, !1);
								break;
							case 33:
								yt.scrollByY(-q * M.scrollPagePercent, !1);
								break;
							case 39:
								yt.scrollByX(M.keyboardSpeed, !1);
								break;
							case 37:
								yt.scrollByX(-M.keyboardSpeed, !1)
						}
						return o = t != et || e != Q
					}
					var i, o, r = [];
					U && r.push(dt[0]), G && r.push(nt[0]), F.bind("focus.jsp", function () {
						e.focus()
					}), e.attr("tabindex", 0).unbind("keydown.jsp keypress.jsp").bind("keydown.jsp", function (e) {
						if (e.target === this || r.length && t(e.target).closest(r).length) {
							var a = et,
								l = Q;
							switch (e.keyCode) {
								case 40:
								case 38:
								case 34:
								case 32:
								case 33:
								case 39:
								case 37:
									i = e.keyCode, n();
									break;
								case 35:
									x(z - q), i = null;
									break;
								case 36:
									x(0), i = null
							}
							return o = e.keyCode == i && a != et || l != Q, !o
						}
					}).bind("keypress.jsp", function (e) {
						return e.keyCode == i && n(), e.target === this || r.length && t(e.target).closest(r).length ? !o : void 0
					}), M.hideFocus ? (e.css("outline", "none"), "hideFocus" in R[0] && e.attr("hideFocus", !0)) : (e.css("outline", ""), "hideFocus" in R[0] && e.attr("hideFocus", !1))
				}
				function H() {
					e.attr("tabindex", "-1").removeAttr("tabindex").unbind("keydown.jsp keypress.jsp"), F.unbind(".jsp")
				}
				function L() {
					if (location.hash && location.hash.length > 1) {
						var e, n, i = escape(location.hash.substr(1));
						try {
							e = t("#" + i + ', a[name="' + i + '"]')
						} catch (o) {
							return
						}
						e.length && F.find(i) && (0 === R.scrollTop() ? n = setInterval(function () {
							R.scrollTop() > 0 && (C(e, !0), t(document).scrollTop(R.position().top), clearInterval(n))
						}, 50) : (C(e, !0), t(document).scrollTop(R.position().top)))
					}
				}
				function O() {
					t(document.body).data("jspHijack") || (t(document.body).data("jspHijack", !0), t(document.body).delegate('a[href*="#"]', "click", function (e) {
						var n, i, o, r, a, l, s = this.href.substr(0, this.href.indexOf("#")),
							d = location.href;
						if (-1 !== location.href.indexOf("#") && (d = location.href.substr(0, location.href.indexOf("#"))), s === d) {
							n = escape(this.href.substr(this.href.indexOf("#") + 1));
							try {
								i = t("#" + n + ', a[name="' + n + '"]')
							} catch (c) {
								return
							}
							i.length && (o = i.closest(".jspScrollable"), r = o.data("jsp"), r.scrollToElement(i, !0), o[0].scrollIntoView && (a = t(window).scrollTop(), l = i.offset().top, (a > l || l > a + t(window).height()) && o[0].scrollIntoView()), e.preventDefault())
						}
					}))
				}
				function P() {
					var t, e, n, i, o, r = !1;
					R.unbind("touchstart.jsp touchmove.jsp touchend.jsp click.jsp-touchclick").bind("touchstart.jsp", function (a) {
						var l = a.originalEvent.touches[0];
						t = T(), e = k(), n = l.pageX, i = l.pageY, o = !1, r = !0
					}).bind("touchmove.jsp", function (a) {
						if (r) {
							var l = a.originalEvent.touches[0],
								s = et,
								d = Q;
							return yt.scrollTo(t + n - l.pageX, e + i - l.pageY), o = o || Math.abs(n - l.pageX) > 5 || Math.abs(i - l.pageY) > 5, s == et && d == Q
						}
					}).bind("touchend.jsp", function (t) {
						r = !1
					}).bind("click.jsp-touchclick", function (t) {
						return o ? (o = !1, !1) : void 0
					})
				}
				function I() {
					var t = k(),
						n = T();
					e.removeClass("jspScrollable").unbind(".jsp"), F.unbind(".jsp"), e.replaceWith(Tt.append(F.children())), Tt.scrollTop(t), Tt.scrollLeft(n), ht && clearInterval(ht)
				}
				var M, F, W, q, R, X, z, Y, V, G, U, K, Z, Q, J, tt, et, nt, it, ot, rt, at, lt, st, dt, ct, pt, ut, gt, ft, ht, mt, vt, bt, yt = this,
					wt = !0,
					xt = !0,
					_t = !1,
					Ct = !1,
					Tt = e.clone(!1, !1).empty(),
					kt = t.fn.mwheelIntent ? "mwheelIntent.jsp" : "mousewheel.jsp";
				"border-box" === e.css("box-sizing") ? (mt = 0, vt = 0) : (mt = e.css("paddingTop") + " " + e.css("paddingRight") + " " + e.css("paddingBottom") + " " + e.css("paddingLeft"), vt = (parseInt(e.css("paddingLeft"), 10) || 0) + (parseInt(e.css("paddingRight"), 10) || 0)), t.extend(yt, {
					reinitialise: function (e) {
						e = t.extend({}, M, e), i(e)
					},
					scrollToElement: function (t, e, n) {
						C(t, e, n)
					},
					scrollTo: function (t, e, n) {
						_(t, n), x(e, n)
					},
					scrollToX: function (t, e) {
						_(t, e)
					},
					scrollToY: function (t, e) {
						x(t, e)
					},
					scrollToPercentX: function (t, e) {
						_(t * (X - W), e)
					},
					scrollToPercentY: function (t, e) {
						x(t * (z - q), e)
					},
					scrollBy: function (t, e, n) {
						yt.scrollByX(t, n), yt.scrollByY(e, n)
					},
					scrollByX: function (t, e) {
						var n = T() + Math[0 > t ? "floor" : "ceil"](t),
							i = n / (X - W);
						v(i * tt, e)
					},
					scrollByY: function (t, e) {
						var n = k() + Math[0 > t ? "floor" : "ceil"](t),
							i = n / (z - q);
						h(i * Z, e)
					},
					positionDragX: function (t, e) {
						v(t, e)
					},
					positionDragY: function (t, e) {
						h(t, e)
					},
					animate: function (t, e, n, i, o) {
						var r = {};
						r[e] = n, t.animate(r, {
							duration: M.animateDuration,
							easing: M.animateEase,
							queue: !1,
							step: i,
							complete: o
						})
					},
					getContentPositionX: function () {
						return T()
					},
					getContentPositionY: function () {
						return k()
					},
					getContentWidth: function () {
						return X
					},
					getContentHeight: function () {
						return z
					},
					getPercentScrolledX: function () {
						return T() / (X - W)
					},
					getPercentScrolledY: function () {
						return k() / (z - q)
					},
					getIsScrollableH: function () {
						return U
					},
					getIsScrollableV: function () {
						return G
					},
					getContentPane: function () {
						return F
					},
					scrollToBottom: function (t) {
						h(Z, t)
					},
					hijackInternalLinks: t.noop,
					destroy: function () {
						I()
					}
				}), i(n)
			}
			return e = t.extend({}, t.fn.jScrollPane.defaults, e), t.each(["arrowButtonSpeed", "trackClickSpeed", "keyboardSpeed"], function () {
				e[this] = e[this] || e.speed
			}), this.each(function () {
				var i = t(this),
					o = i.data("jsp");
				o ? o.reinitialise(e) : (t("script", i).filter('[type="text/javascript"],:not([type])').remove(), o = new n(i, e), i.data("jsp", o))
			})
		}, t.fn.jScrollPane.defaults = {
			showArrows: !1,
			maintainPosition: !0,
			stickToBottom: !1,
			stickToRight: !1,
			clickOnTrack: !0,
			autoReinitialise: !1,
			autoReinitialiseDelay: 500,
			verticalDragMinHeight: 0,
			verticalDragMaxHeight: 99999,
			horizontalDragMinWidth: 0,
			horizontalDragMaxWidth: 99999,
			contentWidth: void 0,
			animateScroll: !1,
			animateDuration: 300,
			animateEase: "linear",
			hijackInternalLinks: !1,
			verticalGutter: 4,
			horizontalGutter: 4,
			mouseWheelSpeed: 3,
			arrowButtonSpeed: 0,
			arrowRepeatFreq: 50,
			arrowScrollOnHover: !1,
			trackClickSpeed: 0,
			trackClickRepeatFreq: 70,
			verticalArrowPositions: "split",
			horizontalArrowPositions: "split",
			enableKeyboardNavigation: !0,
			hideFocus: !1,
			keyboardSpeed: 0,
			initialDelay: 300,
			speed: 30,
			scrollPagePercent: .8
		}
	}), function (t) {
		"function" == typeof define && define.amd ? define(["jquery"], t) : "object" == typeof exports ? module.exports = t : t(jQuery)
	}(function (t) {
		function e(e) {
			var a = e || window.event,
				l = s.call(arguments, 1),
				d = 0,
				p = 0,
				u = 0,
				g = 0,
				f = 0,
				h = 0;
			if (e = t.event.fix(a), e.type = "mousewheel", "detail" in a && (u = -1 * a.detail), "wheelDelta" in a && (u = a.wheelDelta), "wheelDeltaY" in a && (u = a.wheelDeltaY), "wheelDeltaX" in a && (p = -1 * a.wheelDeltaX), "axis" in a && a.axis === a.HORIZONTAL_AXIS && (p = -1 * u, u = 0), d = 0 === u ? p : u, "deltaY" in a && (u = -1 * a.deltaY, d = u), "deltaX" in a && (p = a.deltaX, 0 === u && (d = -1 * p)), 0 !== u || 0 !== p) {
				if (1 === a.deltaMode) {
					var m = t.data(this, "mousewheel-line-height");
					d *= m, u *= m, p *= m
				} else if (2 === a.deltaMode) {
					var v = t.data(this, "mousewheel-page-height");
					d *= v, u *= v, p *= v
				}
				if (g = Math.max(Math.abs(u), Math.abs(p)), (!r || r > g) && (r = g, i(a, g) && (r /= 40)), i(a, g) && (d /= 40, p /= 40, u /= 40), d = Math[d >= 1 ? "floor" : "ceil"](d / r), p = Math[p >= 1 ? "floor" : "ceil"](p / r), u = Math[u >= 1 ? "floor" : "ceil"](u / r), c.settings.normalizeOffset && this.getBoundingClientRect) {
					var b = this.getBoundingClientRect();
					f = e.clientX - b.left, h = e.clientY - b.top
				}
				return e.deltaX = p, e.deltaY = u, e.deltaFactor = r, e.offsetX = f, e.offsetY = h, e.deltaMode = 0, l.unshift(e, d, p, u), o && clearTimeout(o), o = setTimeout(n, 200), (t.event.dispatch || t.event.handle).apply(this, l)
			}
		}
		function n() {
			r = null
		}
		function i(t, e) {
			return c.settings.adjustOldDeltas && "mousewheel" === t.type && e % 120 === 0
		}
		var o, r, a = ["wheel", "mousewheel", "DOMMouseScroll", "MozMousePixelScroll"],
			l = "onwheel" in document || document.documentMode >= 9 ? ["wheel"] : ["mousewheel", "DomMouseScroll", "MozMousePixelScroll"],
			s = Array.prototype.slice;
		if (t.event.fixHooks) for (var d = a.length; d;) t.event.fixHooks[a[--d]] = t.event.mouseHooks;
		var c = t.event.special.mousewheel = {
			version: "3.1.12",
			setup: function () {
				if (this.addEventListener) for (var n = l.length; n;) this.addEventListener(l[--n], e, !1);
				else this.onmousewheel = e;
				t.data(this, "mousewheel-line-height", c.getLineHeight(this)), t.data(this, "mousewheel-page-height", c.getPageHeight(this))
			},
			teardown: function () {
				if (this.removeEventListener) for (var n = l.length; n;) this.removeEventListener(l[--n], e, !1);
				else this.onmousewheel = null;
				t.removeData(this, "mousewheel-line-height"), t.removeData(this, "mousewheel-page-height")
			},
			getLineHeight: function (e) {
				var n = t(e),
					i = n["offsetParent" in t.fn ? "offsetParent" : "parent"]();
				return i.length || (i = t("body")), parseInt(i.css("fontSize"), 10) || parseInt(n.css("fontSize"), 10) || 16
			},
			getPageHeight: function (e) {
				return t(e).height()
			},
			settings: {
				adjustOldDeltas: !0,
				normalizeOffset: !0
			}
		};
		t.fn.extend({
			mousewheel: function (t) {
				return t ? this.bind("mousewheel", t) : this.trigger("mousewheel")
			},
			unmousewheel: function (t) {
				return this.unbind("mousewheel", t)
			}
		})
	});
var _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ?
	function (t) {
		return typeof t
	} : function (t) {
		return t && "function" == typeof Symbol && t.constructor === Symbol ? "symbol" : typeof t
	};
!
	function (t) {
		"use strict";
		"function" == typeof define && define.amd ? define(t) : "undefined" != typeof module && "undefined" != typeof module.exports ? module.exports = t() : "undefined" != typeof Package ? Sortable = t() : window.Sortable = t()
	}(function () {
		"use strict";

		function t(t, e) {
			if (!t || !t.nodeType || 1 !== t.nodeType) throw "Sortable: `el` must be HTMLElement, and not " + {}.toString.call(t);
			this.el = t, this.options = e = v({}, e), t[P] = this;
			var n = {
				group: Math.random(),
				sort: !0,
				disabled: !1,
				store: null,
				handle: null,
				scroll: !0,
				scrollSensitivity: 30,
				scrollSpeed: 10,
				draggable: /[uo]l/i.test(t.nodeName) ? "li" : ">*",
				ghostClass: "sortable-ghost",
				chosenClass: "sortable-chosen",
				ignore: "a, img",
				filter: null,
				animation: 0,
				setData: function (t, e) {
					t.setData("Text", e.textContent)
				},
				dropBubble: !1,
				dragoverBubble: !1,
				dataIdAttr: "data-id",
				delay: 0,
				forceFallback: !1,
				fallbackClass: "sortable-fallback",
				fallbackOnBody: !1
			};
			for (var i in n) !(i in e) && (e[i] = n[i]);
			V(e);
			for (var r in this) "_" === r.charAt(0) && (this[r] = this[r].bind(this));
			this.nativeDraggable = e.forceFallback ? !1 : W, o(t, "mousedown", this._onTapStart), o(t, "touchstart", this._onTapStart), this.nativeDraggable && (o(t, "dragover", this), o(t, "dragenter", this)), z.push(this._onDragOver), e.store && this.sort(e.store.get(this))
		}
		function e(t) {
			x && x.state !== t && (l(x, "display", t ? "none" : ""), !t && x.state && _.insertBefore(x, b), x.state = t)
		}
		function n(t, e, n) {
			if (t) {
				n = n || M, e = e.split(".");
				var i = e.shift().toUpperCase(),
					o = new RegExp("\\s(" + e.join("|") + ")(?=\\s)", "g");
				do
					if (">*" === i && t.parentNode === n || ("" === i || t.nodeName.toUpperCase() == i) && (!e.length || ((" " + t.className + " ").match(o) || []).length == e.length)) return t;
				while (t !== n && (t = t.parentNode))
			}
			return null
		}
		function i(t) {
			t.dataTransfer && (t.dataTransfer.dropEffect = "move"), t.preventDefault()
		}
		function o(t, e, n) {
			t.addEventListener(e, n, !1)
		}
		function r(t, e, n) {
			t.removeEventListener(e, n, !1)
		}
		function a(t, e, n) {
			if (t) if (t.classList) t.classList[n ? "add" : "remove"](e);
			else {
				var i = (" " + t.className + " ").replace(O, " ").replace(" " + e + " ", " ");
				t.className = (i + (n ? " " + e : "")).replace(O, " ")
			}
		}
		function l(t, e, n) {
			var i = t && t.style;
			if (i) {
				if (void 0 === n) return M.defaultView && M.defaultView.getComputedStyle ? n = M.defaultView.getComputedStyle(t, "") : t.currentStyle && (n = t.currentStyle), void 0 === e ? n : n[e];
				e in i || (e = "-webkit-" + e), i[e] = n + ("string" == typeof n ? "" : "px")
			}
		}
		function s(t, e, n) {
			if (t) {
				var i = t.getElementsByTagName(e),
					o = 0,
					r = i.length;
				if (n) for (; r > o; o++) n(i[o], o);
				return i
			}
			return []
		}
		function d(t, e, n, i, o, r, a) {
			var l = M.createEvent("Event"),
				s = (t || e[P]).options,
				d = "on" + n.charAt(0).toUpperCase() + n.substr(1);
			l.initEvent(n, !0, !0), l.to = e, l.from = o || e, l.item = i || e, l.clone = x, l.oldIndex = r, l.newIndex = a, e.dispatchEvent(l), s[d] && s[d].call(t, l)
		}
		function c(t, e, n, i, o, r) {
			var a, l, s = t[P],
				d = s.options.onMove;
			return a = M.createEvent("Event"), a.initEvent("move", !0, !0), a.to = e, a.from = t, a.dragged = n, a.draggedRect = i, a.related = o || e, a.relatedRect = r || e.getBoundingClientRect(), t.dispatchEvent(a), d && (l = d.call(s, a)), l
		}
		function p(t) {
			t.draggable = !1
		}
		function u() {
			R = !1
		}
		function g(t, e) {
			var n = t.lastElementChild,
				i = n.getBoundingClientRect();
			return (e.clientY - (i.top + i.height) > 5 || e.clientX - (i.right + i.width) > 5) && n
		}
		function f(t) {
			for (var e = t.tagName + t.className + t.src + t.href + t.textContent, n = e.length, i = 0; n--;) i += e.charCodeAt(n);
			return i.toString(36)
		}
		function h(t) {
			var e = 0;
			if (!t || !t.parentNode) return -1;
			for (; t && (t = t.previousElementSibling);)"TEMPLATE" !== t.nodeName.toUpperCase() && e++;
			return e
		}
		function m(t, e) {
			var n, i;
			return function () {
				void 0 === n && (n = arguments, i = this, setTimeout(function () {
					1 === n.length ? t.call(i, n[0]) : t.apply(i, n), n = void 0
				}, e))
			}
		}
		function v(t, e) {
			if (t && e) for (var n in e) e.hasOwnProperty(n) && (t[n] = e[n]);
			return t
		}
		var b, y, w, x, _, C, T, k, j, S, $, D, N, B, A, E, H, L = {},
			O = /\s+/g,
			P = "Sortable" + (new Date).getTime(),
			I = window,
			M = I.document,
			F = I.parseInt,
			W = !!("draggable" in M.createElement("div")),
			q = function (t) {
				return t = M.createElement("x"), t.style.cssText = "pointer-events:auto", "auto" === t.style.pointerEvents
			}(),
			R = !1,
			X = Math.abs,
			z = ([].slice, []),
			Y = m(function (t, e, n) {
				if (n && e.scroll) {
					var i, o, r, a, l = e.scrollSensitivity,
						s = e.scrollSpeed,
						d = t.clientX,
						c = t.clientY,
						p = window.innerWidth,
						u = window.innerHeight;
					if (k !== n && (T = e.scroll, k = n, T === !0)) {
						T = n;
						do
							if (T.offsetWidth < T.scrollWidth || T.offsetHeight < T.scrollHeight) break;
						while (T = T.parentNode)
					}
					T && (i = T, o = T.getBoundingClientRect(), r = (X(o.right - d) <= l) - (X(o.left - d) <= l), a = (X(o.bottom - c) <= l) - (X(o.top - c) <= l)), r || a || (r = (l >= p - d) - (l >= d), a = (l >= u - c) - (l >= c), (r || a) && (i = I)), (L.vx !== r || L.vy !== a || L.el !== i) && (L.el = i, L.vx = r, L.vy = a, clearInterval(L.pid), i && (L.pid = setInterval(function () {
						i === I ? I.scrollTo(I.pageXOffset + r * s, I.pageYOffset + a * s) : (a && (i.scrollTop += a * s), r && (i.scrollLeft += r * s))
					}, 24)))
				}
			}, 30),
			V = function (t) {
				var e = t.group;
				e && "object" == ("undefined" == typeof e ? "undefined" : _typeof(e)) || (e = t.group = {
					name: e
				}), ["pull", "put"].forEach(function (t) {
					t in e || (e[t] = !0)
				}), t.groups = " " + e.name + (e.put.join ? " " + e.put.join(" ") : "") + " "
			};
		return t.prototype = {
			constructor: t,
			_onTapStart: function (t) {
				var e = this,
					i = this.el,
					o = this.options,
					r = t.type,
					a = t.touches && t.touches[0],
					l = (a || t).target,
					s = l,
					c = o.filter;
				if (!("mousedown" === r && 0 !== t.button || o.disabled) && (l = n(l, o.draggable, i))) {
					if (D = h(l), "function" == typeof c) {
						if (c.call(this, t, l, this)) return d(e, s, "filter", l, i, D), void t.preventDefault()
					} else if (c && (c = c.split(",").some(function (t) {
						return t = n(s, t.trim(), i), t ? (d(e, t, "filter", l, i, D), !0) : void 0
					}))) return void t.preventDefault();
					(!o.handle || n(s, o.handle, i)) && this._prepareDragStart(t, a, l)
				}
			},
			_prepareDragStart: function (t, e, n) {
				var i, r = this,
					l = r.el,
					d = r.options,
					c = l.ownerDocument;
				n && !b && n.parentNode === l && (A = t, _ = l, b = n, y = b.parentNode, C = b.nextSibling, B = d.group, i = function () {
					r._disableDelayedDrag(), b.draggable = !0, a(b, r.options.chosenClass, !0), r._triggerDragStart(e)
				}, d.ignore.split(",").forEach(function (t) {
					s(b, t.trim(), p)
				}), o(c, "mouseup", r._onDrop), o(c, "touchend", r._onDrop), o(c, "touchcancel", r._onDrop), d.delay ? (o(c, "mouseup", r._disableDelayedDrag), o(c, "touchend", r._disableDelayedDrag), o(c, "touchcancel", r._disableDelayedDrag), o(c, "mousemove", r._disableDelayedDrag), o(c, "touchmove", r._disableDelayedDrag), r._dragStartTimer = setTimeout(i, d.delay)) : i())
			},
			_disableDelayedDrag: function () {
				var t = this.el.ownerDocument;
				clearTimeout(this._dragStartTimer), r(t, "mouseup", this._disableDelayedDrag), r(t, "touchend", this._disableDelayedDrag), r(t, "touchcancel", this._disableDelayedDrag), r(t, "mousemove", this._disableDelayedDrag), r(t, "touchmove", this._disableDelayedDrag)
			},
			_triggerDragStart: function (t) {
				t ? (A = {
					target: b,
					clientX: t.clientX,
					clientY: t.clientY
				}, this._onDragStart(A, "touch")) : this.nativeDraggable ? (o(b, "dragend", this), o(_, "dragstart", this._onDragStart)) : this._onDragStart(A, !0);
				try {
					M.selection ? M.selection.empty() : window.getSelection().removeAllRanges()
				} catch (e) { }
			},
			_dragStarted: function () {
				_ && b && (a(b, this.options.ghostClass, !0), t.active = this, d(this, _, "start", b, _, D))
			},
			_emulateDragOver: function () {
				if (E) {
					if (this._lastX === E.clientX && this._lastY === E.clientY) return;
					this._lastX = E.clientX, this._lastY = E.clientY, q || l(w, "display", "none");
					var t = M.elementFromPoint(E.clientX, E.clientY),
						e = t,
						n = " " + this.options.group.name,
						i = z.length;
					if (e) do {
						if (e[P] && e[P].options.groups.indexOf(n) > -1) {
							for (; i--;) z[i]({
								clientX: E.clientX,
								clientY: E.clientY,
								target: t,
								rootEl: e
							});
							break
						}
						t = e
					} while (e = e.parentNode);
					q || l(w, "display", "")
				}
			},
			_onTouchMove: function (e) {
				if (A) {
					t.active || this._dragStarted(), this._appendGhost();
					var n = e.touches ? e.touches[0] : e,
						i = n.clientX - A.clientX,
						o = n.clientY - A.clientY,
						r = e.touches ? "translate3d(" + i + "px," + o + "px,0)" : "translate(" + i + "px," + o + "px)";
					H = !0, E = n, l(w, "webkitTransform", r), l(w, "mozTransform", r), l(w, "msTransform", r), l(w, "transform", r), e.preventDefault()
				}
			},
			_appendGhost: function () {
				if (!w) {
					var t, e = b.getBoundingClientRect(),
						n = l(b),
						i = this.options;
					w = b.cloneNode(!0), a(w, i.ghostClass, !1), a(w, i.fallbackClass, !0), l(w, "top", e.top - F(n.marginTop, 10)), l(w, "left", e.left - F(n.marginLeft, 10)), l(w, "width", e.width), l(w, "height", e.height), l(w, "opacity", "0.8"), l(w, "position", "fixed"), l(w, "zIndex", "100000"), l(w, "pointerEvents", "none"), i.fallbackOnBody && M.body.appendChild(w) || _.appendChild(w), t = w.getBoundingClientRect(), l(w, "width", 2 * e.width - t.width), l(w, "height", 2 * e.height - t.height)
				}
			},
			_onDragStart: function (t, e) {
				var n = t.dataTransfer,
					i = this.options;
				this._offUpEvents(), "clone" == B.pull && (x = b.cloneNode(!0), l(x, "display", "none"), _.insertBefore(x, b)), e ? ("touch" === e ? (o(M, "touchmove", this._onTouchMove), o(M, "touchend", this._onDrop), o(M, "touchcancel", this._onDrop)) : (o(M, "mousemove", this._onTouchMove), o(M, "mouseup", this._onDrop)), this._loopId = setInterval(this._emulateDragOver, 50)) : (n && (n.effectAllowed = "move", i.setData && i.setData.call(this, n, b)), o(M, "drop", this), setTimeout(this._dragStarted, 0))
			},
			_onDragOver: function (t) {
				var i, o, r, a = this.el,
					s = this.options,
					d = s.group,
					p = d.put,
					f = B === d,
					h = s.sort;
				if (void 0 !== t.preventDefault && (t.preventDefault(), !s.dragoverBubble && t.stopPropagation()), H = !0, B && !s.disabled && (f ? h || (r = !_.contains(b)) : B.pull && p && (B.name === d.name || p.indexOf && ~p.indexOf(B.name))) && (void 0 === t.rootEl || t.rootEl === this.el)) {
					if (Y(t, s, this.el), R) return;
					if (i = n(t.target, s.draggable, a), o = b.getBoundingClientRect(), r) return e(!0), void (x || C ? _.insertBefore(b, x || C) : h || _.appendChild(b));
					if (0 === a.children.length || a.children[0] === w || a === t.target && (i = g(a, t))) {
						if (i) {
							if (i.animated) return;
							v = i.getBoundingClientRect()
						}
						e(f), c(_, a, b, o, i, v) !== !1 && (b.contains(a) || (a.appendChild(b), y = a), this._animate(o, b), i && this._animate(v, i))
					} else if (i && !i.animated && i !== b && void 0 !== i.parentNode[P]) {
						j !== i && (j = i, S = l(i), $ = l(i.parentNode));
						var m, v = i.getBoundingClientRect(),
							T = v.right - v.left,
							k = v.bottom - v.top,
							D = /left|right|inline/.test(S.cssFloat + S.display) || "flex" == $.display && 0 === $["flex-direction"].indexOf("row"),
							N = i.offsetWidth > b.offsetWidth,
							A = i.offsetHeight > b.offsetHeight,
							E = (D ? (t.clientX - v.left) / T : (t.clientY - v.top) / k) > .5,
							L = i.nextElementSibling,
							O = c(_, a, b, o, i, v);
						if (O !== !1) {
							if (R = !0, setTimeout(u, 30), e(f), 1 === O || -1 === O) m = 1 === O;
							else if (D) {
								var I = b.offsetTop,
									M = i.offsetTop;
								m = I === M ? i.previousElementSibling === b && !N || E && N : M > I
							} else m = L !== b && !A || E && A;
							b.contains(a) || (m && !L ? a.appendChild(b) : i.parentNode.insertBefore(b, m ? L : i)), y = b.parentNode, this._animate(o, b), this._animate(v, i)
						}
					}
				}
			},
			_animate: function (t, e) {
				var n = this.options.animation;
				if (n) {
					var i = e.getBoundingClientRect();
					l(e, "transition", "none"), l(e, "transform", "translate3d(" + (t.left - i.left) + "px," + (t.top - i.top) + "px,0)"), e.offsetWidth, l(e, "transition", "all " + n + "ms"), l(e, "transform", "translate3d(0,0,0)"), clearTimeout(e.animated), e.animated = setTimeout(function () {
						l(e, "transition", ""), l(e, "transform", ""), e.animated = !1
					}, n)
				}
			},
			_offUpEvents: function () {
				var t = this.el.ownerDocument;
				r(M, "touchmove", this._onTouchMove), r(t, "mouseup", this._onDrop), r(t, "touchend", this._onDrop), r(t, "touchcancel", this._onDrop)
			},
			_onDrop: function (e) {
				var n = this.el,
					i = this.options;
				clearInterval(this._loopId), clearInterval(L.pid), clearTimeout(this._dragStartTimer), r(M, "mousemove", this._onTouchMove), this.nativeDraggable && (r(M, "drop", this), r(n, "dragstart", this._onDragStart)), this._offUpEvents(), e && (H && (e.preventDefault(), !i.dropBubble && e.stopPropagation()), w && w.parentNode.removeChild(w), b && (this.nativeDraggable && r(b, "dragend", this), p(b), a(b, this.options.ghostClass, !1), a(b, this.options.chosenClass, !1), _ !== y ? (N = h(b), N >= 0 && (d(null, y, "sort", b, _, D, N), d(this, _, "sort", b, _, D, N), d(null, y, "add", b, _, D, N), d(this, _, "remove", b, _, D, N))) : (x && x.parentNode.removeChild(x), b.nextSibling !== C && (N = h(b), N >= 0 && (d(this, _, "update", b, _, D, N), d(this, _, "sort", b, _, D, N)))), t.active && ((null === N || -1 === N) && (N = D), d(this, _, "end", b, _, D, N), this.save())), _ = b = y = w = C = x = T = k = A = E = H = N = j = S = B = t.active = null)
			},
			handleEvent: function (t) {
				var e = t.type;
				"dragover" === e || "dragenter" === e ? b && (this._onDragOver(t), i(t)) : ("drop" === e || "dragend" === e) && this._onDrop(t)
			},
			toArray: function () {
				for (var t, e = [], i = this.el.children, o = 0, r = i.length, a = this.options; r > o; o++) t = i[o], n(t, a.draggable, this.el) && e.push(t.getAttribute(a.dataIdAttr) || f(t));
				return e
			},
			sort: function (t) {
				var e = {},
					i = this.el;
				this.toArray().forEach(function (t, o) {
					var r = i.children[o];
					n(r, this.options.draggable, i) && (e[t] = r)
				}, this), t.forEach(function (t) {
					e[t] && (i.removeChild(e[t]), i.appendChild(e[t]))
				})
			},
			save: function () {
				var t = this.options.store;
				t && t.set(this)
			},
			closest: function (t, e) {
				return n(t, e || this.options.draggable, this.el)
			},
			option: function (t, e) {
				var n = this.options;
				return void 0 === e ? n[t] : (n[t] = e, void ("group" === t && V(n)))
			},
			destroy: function () {
				var t = this.el;
				t[P] = null, r(t, "mousedown", this._onTapStart), r(t, "touchstart", this._onTapStart), this.nativeDraggable && (r(t, "dragover", this), r(t, "dragenter", this)), Array.prototype.forEach.call(t.querySelectorAll("[draggable]"), function (t) {
					t.removeAttribute("draggable")
				}), z.splice(z.indexOf(this._onDragOver), 1), this._onDrop(), this.el = t = null
			}
		}, t.utils = {
			on: o,
			off: r,
			css: l,
			find: s,
			is: function (t, e) {
				return !!n(t, e, t)
			},
			extend: v,
			throttle: m,
			closest: n,
			toggleClass: a,
			index: h
		}, t.create = function (e, n) {
			return new t(e, n)
		}, t.version = "1.4.2", t
	}), function (t) {
		"use strict";
		"function" == typeof define && define.amd ? define(["jquery"], t) : t(jQuery)
	}(function (t) {
		"use strict";
		t.fn.sortable = function (e) {
			var n, i = arguments;
			return this.each(function () {
				var o = t(this),
					r = o.data("sortable");
				if (r || !(e instanceof Object) && e || (r = new Sortable(this, e), o.data("sortable", r)), r) {
					if ("widget" === e) return r;
					"destroy" === e ? (r.destroy(), o.removeData("sortable")) : "function" == typeof r[e] ? n = r[e].apply(r, [].slice.call(i, 1)) : e in r.options && (n = r.option.apply(r, i))
				}
			}), void 0 === n ? this : n
		}
	});
var _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ?
	function (t) {
		return typeof t
	} : function (t) {
		return t && "function" == typeof Symbol && t.constructor === Symbol ? "symbol" : typeof t
	};
!
	function (t, e) {
		"use strict";
		var n, i, o = {
			getPath: function () {
				var t = document.scripts,
					e = t[t.length - 1],
					n = e.src;
				return e.getAttribute("merge") ? void 0 : n.substring(0, n.lastIndexOf("/") + 1)
			}(),
			enter: function (t) {
				13 === t.keyCode && t.preventDefault()
			},
			config: {},
			end: {},
			btn: ["&#x786E;&#x5B9A;", "&#x53D6;&#x6D88;"],
			type: ["dialog", "page", "iframe", "loading", "tips"]
		},
			r = {
				v: "2.2",
				ie6: !!t.ActiveXObject && !t.XMLHttpRequest,
				index: 0,
				path: o.getPath,
				config: function (t, e) {
					var i = 0;
					return t = t || {}, r.cache = o.config = n.extend(o.config, t), r.path = o.config.path || r.path, "string" == typeof t.extend && (t.extend = [t.extend]), r.use("skin/layer.css", t.extend && t.extend.length > 0 ?
						function a() {
							var n = t.extend;
							r.use(n[n[i] ? i : i - 1], i < n.length ?
								function () {
									return ++i, a
								}() : e)
						}() : e), this
				},
				use: function (t, e, i) {
					var o = n("head")[0],
						t = t.replace(/\s/g, ""),
						a = /\.css$/.test(t),
						l = document.createElement(a ? "link" : "script"),
						s = "layui_layer_" + t.replace(/\.|\//g, "");
					return r.path ? (a && (l.rel = "stylesheet"), l[a ? "href" : "src"] = /^http:\/\//.test(t) ? t : r.path + t, l.id = s, n("#" + s)[0] || o.appendChild(l), function d() {
						(a ? 1989 === parseInt(n("#" + s).css("width")) : r[i || s]) ?
							function () {
								e && e();
								try {
									a || o.removeChild(l)
								} catch (t) { }
							}() : setTimeout(d, 100)
					}(), this) : void 0
				},
				ready: function (t, e) {
					var i = "function" == typeof t;
					return i && (e = t), r.config(n.extend(o.config, function () {
						return i ? {} : {
							path: t
						}
					}()), e), this
				},
				alert: function (t, e, i) {
					var o = "function" == typeof e;
					return o && (i = e), r.open(n.extend({
						content: t,
						yes: i
					}, o ? {} : e))
				},
				confirm: function (t, e, i, a) {
					var l = "function" == typeof e;
					return l && (a = i, i = e), r.open(n.extend({
						content: t,
						btn: o.btn,
						yes: i,
						cancel: a
					}, l ? {} : e))
				},
				msg: function (t, i, a) {
					var s = "function" == typeof i,
						d = o.config.skin,
						c = (d ? d + " " + d + "-msg" : "") || "layui-layer-msg",
						p = l.anim.length - 1;
					return s && (a = i), r.open(n.extend({
						content: t,
						time: 3e3,
						shade: !1,
						skin: c,
						title: !1,
						closeBtn: !1,
						btn: !1,
						end: a
					}, s && !o.config.skin ? {
						skin: c + " layui-layer-hui",
						shift: p
					} : function () {
						return i = i || {}, (-1 === i.icon || i.icon === e && !o.config.skin) && (i.skin = c + " " + (i.skin || "layui-layer-hui")), i
					}()))
				},
				load: function (t, e) {
					return r.open(n.extend({
						type: 3,
						icon: t || 0,
						shade: .01
					}, e))
				},
				tips: function (t, e, i) {
					return r.open(n.extend({
						type: 4,
						content: [t, e],
						closeBtn: !1,
						time: 3e3,
						shade: !1,
						maxWidth: 210
					}, i))
				}
			},
			a = function (t) {
				var e = this;
				e.index = ++r.index, e.config = n.extend({}, e.config, o.config, t), e.creat()
			};
		a.pt = a.prototype;
		var l = ["layui-layer", ".layui-layer-title", ".layui-layer-main", ".layui-layer-dialog", "layui-layer-iframe", "layui-layer-content", "layui-layer-btn", "layui-layer-close"];
		l.anim = ["layui-anim", "layui-anim-01", "layui-anim-02", "layui-anim-03", "layui-anim-04", "layui-anim-05", "layui-anim-06"], a.pt.config = {
			type: 0,
			shade: .3,
			fix: !0,
			move: l[1],
			title: "&#x4FE1;&#x606F;",
			offset: "auto",
			area: "auto",
			closeBtn: 1,
			time: 0,
			zIndex: 19891014,
			maxWidth: 360,
			shift: 0,
			icon: -1,
			scrollbar: !0,
			tips: 2
		}, a.pt.vessel = function (t, e) {
			var n = this,
				i = n.index,
				r = n.config,
				a = r.zIndex + i,
				s = "object" == _typeof(r.title),
				d = r.maxmin && (1 === r.type || 2 === r.type),
				c = r.title ? '<div class="layui-layer-title" style="' + (s ? r.title[1] : "") + '">' + (s ? r.title[0] : r.title) + "</div>" : "";
			return r.zIndex = a, e([r.shade ? '<div class="layui-layer-shade" id="layui-layer-shade' + i + '" times="' + i + '" style="' + ("z-index:" + (a - 1) + "; background-color:" + (r.shade[1] || "#000") + "; opacity:" + (r.shade[0] || r.shade) + "; filter:alpha(opacity=" + (100 * r.shade[0] || 100 * r.shade) + ");") + '"></div>' : "", '<div class="' + l[0] + " " + (l.anim[r.shift] || "") + (" layui-layer-" + o.type[r.type]) + (0 != r.type && 2 != r.type || r.shade ? "" : " layui-layer-border") + " " + (r.skin || "") + '" id="' + l[0] + i + '" type="' + o.type[r.type] + '" times="' + i + '" showtime="' + r.time + '" conType="' + (t ? "object" : "string") + '" style="z-index: ' + a + "; width:" + r.area[0] + ";height:" + r.area[1] + (r.fix ? "" : ";position:absolute;") + '">' + (t && 2 != r.type ? "" : c) + '<div id="' + (r.id || "") + '" class="layui-layer-content' + (0 == r.type && -1 !== r.icon ? " layui-layer-padding" : "") + (3 == r.type ? " layui-layer-loading" + r.icon : "") + '">' + (0 == r.type && -1 !== r.icon ? '<i class="layui-layer-ico layui-layer-ico' + r.icon + '"></i>' : "") + (1 == r.type && t ? "" : r.content || "") + '</div><span class="layui-layer-setwin">' +
				function () {
					var t = d ? '<a class="layui-layer-min" href="javascript:;"><cite></cite></a><a class="layui-layer-ico layui-layer-max" href="javascript:;"></a>' : "";
					return r.closeBtn && (t += '<a class="layui-layer-ico ' + l[7] + " " + l[7] + (r.title ? r.closeBtn : 4 == r.type ? "1" : "2") + '" href="javascript:;"></a>'), t
				}() + "</span>" + (r.btn ?
					function () {
						var t = "";
						"string" == typeof r.btn && (r.btn = [r.btn]);
						for (var e = 0, n = r.btn.length; n > e; e++) t += '<a class="' + l[6] + e + '">' + r.btn[e] + "</a>";
						return '<div class="' + l[6] + '">' + t + "</div>"
					}() : "") + "</div>"], c), n
		}, a.pt.creat = function () {
			var t = this,
				e = t.config,
				a = t.index,
				s = e.content,
				d = "object" == ("undefined" == typeof s ? "undefined" : _typeof(s));
			if (!n("#" + e.id)[0]) {
				switch ("string" == typeof e.area && (e.area = "auto" === e.area ? ["", ""] : [e.area, ""]), e.type) {
					case 0:
						e.btn = "btn" in e ? e.btn : o.btn[0], r.closeAll("dialog");
						break;
					case 2:
						var s = e.content = d ? e.content : [e.content || "http://layer.layui.com", "auto"];
						e.content = '<iframe scrolling="' + (e.content[1] || "auto") + '" allowtransparency="true" id="' + l[4] + a + '" name="' + l[4] + a + '" onload="this.className=\'\';" class="layui-layer-load" frameborder="0" src="' + e.content[0] + '"></iframe>';
						break;
					case 3:
						e.title = !1, e.closeBtn = !1, -1 === e.icon && 0 === e.icon, r.closeAll("loading");
						break;
					case 4:
						d || (e.content = [e.content, "body"]), e.follow = e.content[1], e.content = e.content[0] + '<i class="layui-layer-TipsG"></i>', e.title = !1, e.fix = !1, e.tips = "object" == _typeof(e.tips) ? e.tips : [e.tips, !0], e.tipsMore || r.closeAll("tips")
				}
				t.vessel(d, function (i, o) {
					n("body").append(i[0]), d ?
						function () {
							2 == e.type || 4 == e.type ?
								function () {
									n("body").append(i[1])
								}() : function () {
									s.parents("." + l[0])[0] || (s.show().addClass("layui-layer-wrap").wrap(i[1]), n("#" + l[0] + a).find("." + l[5]).before(o))
								}()
						}() : n("body").append(i[1]), t.layero = n("#" + l[0] + a), e.scrollbar || l.html.css("overflow", "hidden").attr("layer-full", a)
				}).auto(a), 2 == e.type && r.ie6 && t.layero.find("iframe").attr("src", s[0]), n(document).off("keydown", o.enter).on("keydown", o.enter), t.layero.on("keydown", function (t) {
					n(document).off("keydown", o.enter)
				}), 4 == e.type ? t.tips() : t.offset(), e.fix && i.on("resize", function () {
					t.offset(), (/^\d+%$/.test(e.area[0]) || /^\d+%$/.test(e.area[1])) && t.auto(a), 4 == e.type && t.tips()
				}), e.time <= 0 || setTimeout(function () {
					r.close(t.index)
				}, e.time), t.move().callback()
			}
		}, a.pt.auto = function (t) {
			function e(t) {
				t = a.find(t), t.height(s[1] - d - c - 2 * (0 | parseFloat(t.css("padding"))))
			}
			var o = this,
				r = o.config,
				a = n("#" + l[0] + t);
			"" === r.area[0] && r.maxWidth > 0 && (/MSIE 7/.test(navigator.userAgent) && r.btn && a.width(a.innerWidth()), a.outerWidth() > r.maxWidth && a.width(r.maxWidth));
			var s = [a.innerWidth(), a.innerHeight()],
				d = a.find(l[1]).outerHeight() || 0,
				c = a.find("." + l[6]).outerHeight() || 0;
			switch (r.type) {
				case 2:
					e("iframe");
					break;
				default:
					"" === r.area[1] ? r.fix && s[1] >= i.height() && (s[1] = i.height(), e("." + l[5])) : e("." + l[5])
			}
			return o
		}, a.pt.offset = function () {
			var t = this,
				e = t.config,
				n = t.layero,
				o = [n.outerWidth(), n.outerHeight()],
				r = "object" == _typeof(e.offset);
			t.offsetTop = (i.height() - o[1]) / 2, t.offsetLeft = (i.width() - o[0]) / 2, r ? (t.offsetTop = e.offset[0], t.offsetLeft = e.offset[1] || t.offsetLeft) : "auto" !== e.offset && (t.offsetTop = e.offset, "rb" === e.offset && (t.offsetTop = i.height() - o[1], t.offsetLeft = i.width() - o[0])), e.fix || (t.offsetTop = /%$/.test(t.offsetTop) ? i.height() * parseFloat(t.offsetTop) / 100 : parseFloat(t.offsetTop), t.offsetLeft = /%$/.test(t.offsetLeft) ? i.width() * parseFloat(t.offsetLeft) / 100 : parseFloat(t.offsetLeft), t.offsetTop += i.scrollTop(), t.offsetLeft += i.scrollLeft()), n.css({
				top: t.offsetTop,
				left: t.offsetLeft
			})
		}, a.pt.tips = function () {
			var t = this,
				e = t.config,
				o = t.layero,
				r = [o.outerWidth(), o.outerHeight()],
				a = n(e.follow);
			a[0] || (a = n("body"));
			var s = {
				width: a.outerWidth(),
				height: a.outerHeight(),
				top: a.offset().top,
				left: a.offset().left
			},
				d = o.find(".layui-layer-TipsG"),
				c = e.tips[0];
			e.tips[1] || d.remove(), s.autoLeft = function () {
				s.left + r[0] - i.width() > 0 ? (s.tipLeft = s.left + s.width - r[0], d.css({
					right: 12,
					left: "auto"
				})) : s.tipLeft = s.left
			}, s.where = [function () {
				s.autoLeft(), s.tipTop = s.top - r[1] - 10, d.removeClass("layui-layer-TipsB").addClass("layui-layer-TipsT").css("border-right-color", e.tips[1])
			}, function () {
				s.tipLeft = s.left + s.width + 10, s.tipTop = s.top, d.removeClass("layui-layer-TipsL").addClass("layui-layer-TipsR").css("border-bottom-color", e.tips[1])
			}, function () {
				s.autoLeft(), s.tipTop = s.top + s.height + 10, d.removeClass("layui-layer-TipsT").addClass("layui-layer-TipsB").css("border-right-color", e.tips[1])
			}, function () {
				s.tipLeft = s.left - r[0] - 10, s.tipTop = s.top, d.removeClass("layui-layer-TipsR").addClass("layui-layer-TipsL").css("border-bottom-color", e.tips[1])
			}], s.where[c - 1](), 1 === c ? s.top - (i.scrollTop() + r[1] + 16) < 0 && s.where[2]() : 2 === c ? i.width() - (s.left + s.width + r[0] + 16) > 0 || s.where[3]() : 3 === c ? s.top - i.scrollTop() + s.height + r[1] + 16 - i.height() > 0 && s.where[0]() : 4 === c && r[0] + 16 - s.left > 0 && s.where[1](), o.find("." + l[5]).css({
				"background-color": e.tips[1],
				"padding-right": e.closeBtn ? "30px" : ""
			}), o.css({
				left: s.tipLeft,
				top: s.tipTop
			})
		}, a.pt.move = function () {
			var t = this,
				e = t.config,
				o = {
					setY: 0,
					moveLayer: function () {
						var t = o.layero,
							e = parseInt(t.css("margin-left")),
							n = parseInt(o.move.css("left"));
						0 === e || (n -= e), "fixed" !== t.css("position") && (n -= t.parent().offset().left, o.setY = 0), t.css({
							left: n,
							top: parseInt(o.move.css("top")) - o.setY
						})
					}
				},
				r = t.layero.find(e.move);
			return e.move && r.attr("move", "ok"), r.css({
				cursor: e.move ? "move" : "auto"
			}), n(e.move).on("mousedown", function (t) {
				if (t.preventDefault(), "ok" === n(this).attr("move")) {
					o.ismove = !0, o.layero = n(this).parents("." + l[0]);
					var r = o.layero.offset().left,
						a = o.layero.offset().top,
						s = o.layero.outerWidth() - 6,
						d = o.layero.outerHeight() - 6;
					n("#layui-layer-moves")[0] || n("body").append('<div id="layui-layer-moves" class="layui-layer-moves" style="left:' + r + "px; top:" + a + "px; width:" + s + "px; height:" + d + 'px; z-index:2147483584"></div>'), o.move = n("#layui-layer-moves"), e.moveType && o.move.css({
						visibility: "hidden"
					}), o.moveX = t.pageX - o.move.position().left, o.moveY = t.pageY - o.move.position().top, "fixed" !== o.layero.css("position") || (o.setY = i.scrollTop())
				}
			}), n(document).mousemove(function (t) {
				if (o.ismove) {
					var n = t.pageX - o.moveX,
						r = t.pageY - o.moveY;
					if (t.preventDefault(), !e.moveOut) {
						o.setY = i.scrollTop();
						var a = i.width() - o.move.outerWidth(),
							l = o.setY;
						0 > n && (n = 0), n > a && (n = a), l > r && (r = l), r > i.height() - o.move.outerHeight() + o.setY && (r = i.height() - o.move.outerHeight() + o.setY)
					}
					o.move.css({
						left: n,
						top: r
					}), e.moveType && o.moveLayer(), n = r = a = l = null
				}
			}).mouseup(function () {
				try {
					o.ismove && (o.moveLayer(), o.move.remove(), e.moveEnd && e.moveEnd()), o.ismove = !1
				} catch (t) {
					o.ismove = !1
				}
			}), t
		}, a.pt.callback = function () {
			function t() {
				var t = a.cancel && a.cancel(e.index);
				t === !1 || r.close(e.index)
			}
			var e = this,
				i = e.layero,
				a = e.config;
			e.openLayer(), a.success && (2 == a.type ? i.find("iframe").on("load", function () {
				a.success(i, e.index)
			}) : a.success(i, e.index)), r.ie6 && e.IE6(i), i.find("." + l[6]).children("a").on("click", function () {
				var o = n(this).index();
				a["btn" + (o + 1)] && a["btn" + (o + 1)](e.index, i), 0 === o ? a.yes ? a.yes(e.index, i) : r.close(e.index) : 1 === o ? t() : a["btn" + (o + 1)] || r.close(e.index)
			}), i.find("." + l[7]).on("click", t), a.shadeClose && n("#layui-layer-shade" + e.index).on("click", function () {
				r.close(e.index)
			}), i.find(".layui-layer-min").on("click", function () {
				r.min(e.index, a), a.min && a.min(i)
			}), i.find(".layui-layer-max").on("click", function () {
				n(this).hasClass("layui-layer-maxmin") ? (r.restore(e.index), a.restore && a.restore(i)) : (r.full(e.index, a), a.full && a.full(i))
			}), a.end && (o.end[e.index] = a.end)
		}, o.reselect = function () {
			n.each(n("select"), function (t, e) {
				var i = n(this);
				i.parents("." + l[0])[0] || 1 == i.attr("layer") && n("." + l[0]).length < 1 && i.removeAttr("layer").show(), i = null
			})
		}, a.pt.IE6 = function (t) {
			function e() {
				t.css({
					top: r + (o.config.fix ? i.scrollTop() : 0)
				})
			}
			var o = this,
				r = t.offset().top;
			e(), i.scroll(e), n("select").each(function (t, e) {
				var i = n(this);
				i.parents("." + l[0])[0] || "none" === i.css("display") || i.attr({
					layer: "1"
				}).hide(), i = null
			})
		}, a.pt.openLayer = function () {
			var t = this;
			r.zIndex = t.config.zIndex, r.setTop = function (t) {
				var e = function () {
					r.zIndex++ , t.css("z-index", r.zIndex + 1)
				};
				return r.zIndex = parseInt(t[0].style.zIndex), t.on("mousedown", e), r.zIndex
			}
		}, o.record = function (t) {
			var e = [t.outerWidth(), t.outerHeight(), t.position().top, t.position().left + parseFloat(t.css("margin-left"))];
			t.find(".layui-layer-max").addClass("layui-layer-maxmin"), t.attr({
				area: e
			})
		}, o.rescollbar = function (t) {
			l.html.attr("layer-full") == t && (l.html[0].style.removeProperty ? l.html[0].style.removeProperty("overflow") : l.html[0].style.removeAttribute("overflow"), l.html.removeAttr("layer-full"))
		}, t.layer = r, r.getChildFrame = function (t, e) {
			return e = e || n("." + l[4]).attr("times"), n("#" + l[0] + e).find("iframe").contents().find(t)
		}, r.getFrameIndex = function (t) {
			return n("#" + t).parents("." + l[4]).attr("times")
		}, r.iframeAuto = function (t) {
			if (t) {
				var e = r.getChildFrame("html", t).outerHeight(),
					i = n("#" + l[0] + t),
					o = i.find(l[1]).outerHeight() || 0,
					a = i.find("." + l[6]).outerHeight() || 0;
				i.css({
					height: e + o + a
				}), i.find("iframe").css({
					height: e
				})
			}
		}, r.iframeSrc = function (t, e) {
			n("#" + l[0] + t).find("iframe").attr("src", e)
		}, r.style = function (t, e) {
			var i = n("#" + l[0] + t),
				r = i.attr("type"),
				a = i.find(l[1]).outerHeight() || 0,
				s = i.find("." + l[6]).outerHeight() || 0;
			(r === o.type[1] || r === o.type[2]) && (i.css(e), r === o.type[2] && i.find("iframe").css({
				height: parseFloat(e.height) - a - s
			}))
		}, r.min = function (t, e) {
			var i = n("#" + l[0] + t),
				a = i.find(l[1]).outerHeight() || 0;
			o.record(i), r.style(t, {
				width: 180,
				height: a,
				overflow: "hidden"
			}), i.find(".layui-layer-min").hide(), "page" === i.attr("type") && i.find(l[4]).hide(), o.rescollbar(t)
		}, r.restore = function (t) {
			var e = n("#" + l[0] + t),
				i = e.attr("area").split(",");
			e.attr("type"), r.style(t, {
				width: parseFloat(i[0]),
				height: parseFloat(i[1]),
				top: parseFloat(i[2]),
				left: parseFloat(i[3]),
				overflow: "visible"
			}), e.find(".layui-layer-max").removeClass("layui-layer-maxmin"), e.find(".layui-layer-min").show(), "page" === e.attr("type") && e.find(l[4]).show(), o.rescollbar(t)
		}, r.full = function (t) {
			var e, a = n("#" + l[0] + t);
			o.record(a), l.html.attr("layer-full") || l.html.css("overflow", "hidden").attr("layer-full", t), clearTimeout(e), e = setTimeout(function () {
				var e = "fixed" === a.css("position");
				r.style(t, {
					top: e ? 0 : i.scrollTop(),
					left: e ? 0 : i.scrollLeft(),
					width: i.width(),
					height: i.height()
				}), a.find(".layui-layer-min").hide()
			}, 100)
		}, r.title = function (t, e) {
			var i = n("#" + l[0] + (e || r.index)).find(l[1]);
			i.html(t)
		}, r.close = function (t) {
			var e = n("#" + l[0] + t),
				i = e.attr("type");
			if (e[0]) {
				if (i === o.type[1] && "object" === e.attr("conType")) {
					e.children(":not(." + l[5] + ")").remove();
					for (var a = 0; 2 > a; a++) e.find(".layui-layer-wrap").unwrap().hide()
				} else {
					if (i === o.type[2]) try {
						var s = n("#" + l[4] + t)[0];
						s.contentWindow.document.write(""), s.contentWindow.close(), e.find("." + l[5])[0].removeChild(s)
					} catch (d) { }
					e[0].innerHTML = "", e.remove()
				}
				n("#layui-layer-moves, #layui-layer-shade" + t).remove(), r.ie6 && o.reselect(), o.rescollbar(t), n(document).off("keydown", o.enter), "function" == typeof o.end[t] && o.end[t](), delete o.end[t]
			}
		}, r.closeAll = function (t) {
			n.each(n("." + l[0]), function () {
				var e = n(this),
					i = t ? e.attr("type") === t : 1;
				i && r.close(e.attr("times")), i = null
			})
		}, o.run = function () {
			n = jQuery, i = n(t), l.html = n("html"), r.open = function (t) {
				var e = new a(t);
				return e.index
			}
		}, "function" == typeof define ? define(function () {
			return o.run(), r
		}) : function () {
			o.run()
		}()
	}(window);
var tm1Config = {
	numberText: "Đặc Biệt",
	SBtype: [{
		key: "SB_H",
		text: "Màu Đỏ"
	}, {
		key: "SB_LV",
		text: "Màu Xanh Lá"
	}, {
		key: "SB_L",
		text: "Màu Xanh Dương"
	}],
	SXtype: [{
		key: "SX_S",
		text: "Tí"
	}, {
		key: "SX_N",
		text: "Sửu"
	}, {
		key: "SX_HU",
		text: "Dần"
	}, {
		key: "SX_T",
		text: "Mão"
	}, {
		key: "SX_L",
		text: "Thìn"
	}, {
		key: "SX_SHE",
		text: "Tỵ"
	}, {
		key: "SX_MA",
		text: "Ngọ"
	}, {
		key: "SX_Y",
		text: "Mùi"
	}, {
		key: "SX_H",
		text: "Thân"
	}, {
		key: "SX_J",
		text: "Dậu"
	}, {
		key: "SX_G",
		text: "Tuất"
	}, {
		key: "SX_Z",
		text: "Hợi"
	}],
	animalNum: [
		[11, 23, 35, 47],
		[10, 22, 34, 46],
		[9, 21, 33, 45],
		[8, 20, 32, 44],
		[7, 19, 31, 43],
		[6, 18, 30, 42],
		[5, 17, 29, 41],
		[4, 16, 28, 40],
		[3, 15, 27, 39],
		[2, 14, 26, 38],
		[1, 13, 25, 37, 49],
		[12, 24, 36, 48]
	],
	red: [1, 2, 7, 8, 12, 13, 18, 19, 23, 24, 29, 30, 34, 35, 40, 45, 46],
	blue: [3, 4, 9, 10, 14, 15, 20, 25, 26, 31, 36, 37, 41, 42, 47, 48],
	green: [5, 6, 11, 16, 17, 21, 22, 27, 28, 32, 33, 38, 39, 43, 44, 49],
	helpInfo: [
		{
			label: "Đoán số",
			title: "Chọn các ô số trong bảng (49 ô số) để đặt cược. So sánh với kết quả mở thưởng, nếu số bạn đã đặt cược trùng với số đặc biệt trong kỳ đó thì bạn trúng thưởng, các số còn lại không trúng thưởng.",
			text: "Bạn cược số 12, 45, 49. Kết quả ra: 45 23 25 11 28 2 27 thì bạn trúng thưởng."
		},
		{
			label: "Đoán số",
			title: "Chọn Con Giáp để đặt cược. Kết quả mở thưởng ra số đặc biệt trùng với nhóm Con Giáp bạn đã đặt cược thì trúng thưởng.",
			text: "Bạn cược Tý. Kết quả ra: 12 24 36 48 9 13 15 thì bạn trúng thưởng."
		},
		{
			label: "Đoán số",
			title: "Chọn nhóm màu (đỏ, xanh lá, xanh dương) để đặt cược. Kết quả mở thưởng ra số đặc biệt trùng với nhóm màu bạn đã đặt cược thì trúng thưởng.",
			text: "Bạn cược màu đỏ. Kết quả ra: 2 28 48 28 45 9 26 thì bạn trúng thưởng."
		},
		// {
		// 	label: "Đoán số",
		// 	title: "Chọn nhóm màu (đỏ, xanh lá, xanh dương) để đặt cược. Kết quả mở thưởng ra số đặc biệt trùng với nhóm màu bạn đã đặt cược thì trúng thưởng.",
		// 	text: "VD: Bạn cược màu đỏ. Kết quả ra: 3, 4, 9, 10, 14, 15, 20, 25, 26, 31, 36, 37, 41, 42, 47, 48 thì bạn trúng thưởng."
		// },
		// {
		// 	label: "Đoán số",
		// 	title: "Chọn nhóm màu (đỏ, xanh lá, xanh dương) để đặt cược. Kết quả mở thưởng ra số đặc biệt trùng với nhóm màu bạn đã đặt cược thì trúng thưởng.",
		// 	text: "VD: Bạn cược màu đỏ. Kết quả ra: 5, 6, 11, 16, 17, 21, 22, 27, 28, 32, 33, 38, 39, 43, 44, 49 thì bạn trúng thưởng."
		// }
	]
},
	tm2Config = {
		TMWStype: [{
			key: "TMWS_0",
			text: "Đơn vị Đặc Biệt số 0"
		}, {
			key: "TMWS_1",
			text: "Đơn vị Đặc Biệt số 1"
		}, {
			key: "TMWS_2",
			text: "Đơn vị Đặc Biệt số 2"
		}, {
			key: "TMWS_3",
			text: "Đơn vị Đặc Biệt số 3"
		}, {
			key: "TMWS_4",
			text: "Đơn vị Đặc Biệt số 4"
		}, {
			key: "TMWS_5",
			text: "Đơn vị Đặc Biệt số 5"
		}, {
			key: "TMWS_6",
			text: "Đơn vị Đặc Biệt số 6"
		}, {
			key: "TMWS_7",
			text: "Đơn vị Đặc Biệt số 7"
		}, {
			key: "TMWS_8",
			text: "Đơn vị Đặc Biệt số 8"
		}, {
			key: "TMWS_9",
			text: "Đơn vị Đặc Biệt số 9"
		}],
		TWtype: [{
			key: "WSDAN",
			text: "Đơn vị Đặc Biệt Lẻ"
		}, {
			key: "WSS",
			text: "Đơn vị Đặc Biệt Chẵn"
		}, {
			key: "TMDAN",
			text: "Đặc Biệt Lẻ"
		}, {
			key: "TMS",
			text: "Đặc Biệt Chẵn"
		}, {
			key: "WSD",
			text: "Đơn vị Tài"
		}, {
			key: "TMD",
			text: "Đặc Biệt Tài"
		}, {
			key: "WSX",
			text: "Đơn vị Xỉu"
		}, {
			key: "TMX",
			text: "Đặc Biệt Xỉu"
		}],
		helpInfo: [{
			label: "Đoán số",
			title: "Bạn dự đoán chữ số hàng đơn vị của số đặc biệt (Từ 0 đến 9).",
			// text: "Số bạn chọn là 0, số cuối cùng được quay là 0, bạn thắng."
		}, {
			label: "Đoán số",
			title: "Bạn dự đoán số đặc biệt có chữ số đơn vị là Lẻ.",
			// text: "Số bạn chọn không chia hết cho 2 thì bạn thắng."
		}, {
			label: "Đoán số",
			title: "Bạn dự đoán số đặc biệt có chữ số đơn vị là Chẵn.",
			// text: "Số bạn chọn chia hết cho 2 thì bạn thắng."
		}, {
			label: "Đoán số",
			title: "Bạn dự đoán số đặc biệt ra Lẻ. Số đặc biệt ra 49 là Hòa, được hoàn lại tiền cược",
			// text: "Số đặc biệt bạn chọn chia hết cho 2 thì bạn thắng."
		}, {
			label: "Đoán số",
			title: "Bạn dự đoán số đặc biệt ra Chẵn. Số đặc biệt ra 49 là Hòa, được hoàn lại tiền cược",
			// text: "Số đặc biệt bạn chọn không chia hết cho 2 thì bạn thắng."
		}, {
			label: "Đoán số",
			title: "Kết quả số đặc biệt có chữ số đơn vị là 5 - 9 gọi là Đơn vị Tài.",
			text: "Bạn đặt cược Đơn vị Tài, kết quả số đặc biệt ra 26 thì bạn trúng thưởng."
		}, {
			label: "Đoán số",
			title: "Kết quả số đặc biệt có chữ số đơn vị là 0 - 4 gọi là Đơn vị Xỉu.",
			text: "Bạn đặt cược Đơn vị Xỉu, kết quả số đặc biệt ra 3 thì bạn trúng thưởng."
		}, {
			label: "Đoán số",
			title: "Số đặc biệt lớn hơn hoặc bằng 25 được gọi là Đặc biệt Tài. Số đặc biệt ra 49 thì Hòa, được hoàn lại tiền cược.",
			text: "Bạn chọn Đặc biệt Tài. Số đặc biệt ra là số 42 thì bạn thắng."
		}, {
			label: "Đoán số",
			title: "Số đặc biệt nhỏ hơn 25 được gọi là Đặc Biệt Xỉu. Số đặc biệt ra 49 thì Hòa, được hoàn lại tiền cược.",
			text: "Bạn chọn Đặc biệt Xỉu. Số đặc biệt ra là số 22 thì bạn thắng."
		}]
	},
	zmConfig = {
		ZYtype: [
			{
				key: "ZYMD",
				text: "Số thường Tài"
			},
			{
				key: "ZYMX",
				text: "Số thường Xỉu"
			},
			{
				key: "ZYMS",
				text: "Số thường Chẵn"
			},
			{
				key: "ZYMDAN",
				text: "Số thường Lẻ"
			}
		],
		numberText: "Số thường",
		helpInfo: [{
			label: "Bảng thông thường",
			title: "Chọn các ô số trong bảng (49 ô số) để đặt cược. So sánh với kết quả mở thưởng, nếu số bạn đã đặt cược trùng với một trong 6 số thường của kỳ đó thì bạn trúng thưởng.",
			text: "Bạn đặt cược ô số 4, 5, 6. kết quả ra 22 5 48 47 45 9 26 thì bạn trúng thưởng."
		}, {
			label: "Bảng thông thường lớn",
			title: "Số thường đầu tiên lớn hơn hoặc bằng 25 được gọi là Tài. Số thường đầu tiên ra 49 thì Hòa, được hoàn lại tiền cược.",
			text: "Bạn đặt cược Tài, kết quả ra 22 35 48 47 45 9 26 thì bạn trúng thưởng."
		}, {
			label: "Bảng thông thường lớn",
			title: "Số thường đầu tiên nhỏ hơn 25 được gọi là Xỉu. Số thường đầu tiên ra 49 thì Hòa, được hoàn lại tiền cược.",
			text: "Bạn đặt cược Xỉu, kết quả ra 22 5 48 47 45 9 26 thì bạn trúng thưởng."
		}, {
			label: "Bảng thông thường lớn",
			title: "Bạn dự đoán số thường đầu tiên ra Chẵn. Số thường đầu tiên ra 49 là Hòa, được hoàn lại tiền cược.",
			// text: "Chọn số 28 cho số đặc biệt, kết quả số đặc biệt là 28, bạn thắng. "
		}, {
			label: "Bảng thông thường lớn",
			title: "Bạn dự đoán số thường đầu tiên ra Lẻ. Số thường đầu tiên ra 49 là Hòa, được hoàn lại tiền cược.",
			// text: "Chọn số 23 cho số đặc biệt, kết quả số đặc biệt là 23, bạn thắng."
		}]
	},
	helpCenterHtml = '<p>Có 7 quả bóng số được xổ ngẫu nhiên mỗi kỳ. Trong 1 kỳ mở thưởng, mỗi quả bóng xuất hiện một con số từ 1 đến 49 và 7 quả bóng số không được trùng nhau.</p><p>&nbsp;</p><table style="height: 700px; width: 574px;"><tbody><tr><td colspan="2"><p align="center">Gợi ý</p></td></tr><tr><td valign="center" width="50"><p align="center">Số đặc biệt</p></td><td valign="center" width="50"><p align="center">Là con số ở quả bóng đầu tiên</p></td></tr><tr><td valign="center" width="50"><p align="center">Số thường</p></td><td valign="center" width="50"><p align="center">Là con số ở 6 quả bóng còn lại</p></td></tr><tr><td valign="center" width="50"><p align="center">Đỏ</p></td><td valign="center" width="50"><p align="center">1, 2, 7, 8, 12, 13, 18, 19, 23, 24 29, 30, 34, 35, 40, 45, 46</p></td></tr><tr><td valign="center" width="50"><p align="center">Xanh Lá</p></td><td valign="center" width="50"><p align="center">5, 6, 11, 16, 17, 21, 22, 27, 28, 32, 33, 38, 39, 43, 44, 49</p></td></tr><tr><td valign="center" width="50"><p align="center">Xanh Dương</p></td><td valign="center" width="50"><p align="center">3, 4 9, 10, 14, 15 20, 25, 26 , 31, 36, 37, 41, 42, 47, 48</p></td></tr><tr><td valign="center" width="50"><p align="center">Tí</p></td><td valign="center" width="50"><p align="center">12, 24, 36, 48</p></td></tr><tr><td valign="center" width="50"><p align="center">Sửu</p></td><td valign="center" width="50"><p align="center">11, 23, 35, 47</p></td></tr><tr><td valign="center" width="50"><p align="center">Dần</p></td><td valign="center" width="50"><p align="center">10, 22, 34, 46</p></td></tr><tr><td valign="center" width="50"><p align="center">Mão</p></td><td valign="center" width="50"><p align="center">9, 21, 33, 45</p></td></tr><tr><td valign="center" width="50"><p align="center">Thìn</p></td><td valign="center" width="50"><p align="center">8, 20, 32, 44</p></td></tr><tr><td valign="center" width="50"><p align="center">Tỵ</p></td><td valign="center" width="50"><p align="center">7, 19, 31, 43</p></td></tr><tr><td valign="center" width="50"><p align="center">Ngọ</p></td><td valign="center" width="50"><p align="center">6, 18, 30, 42</p></td></tr><tr><td valign="center" width="50"><p align="center">Mùi</p></td><td valign="center" width="50"><p align="center">5, 17, 29, 41</p></td></tr><tr><td valign="center" width="50"><p align="center">Thân</p></td><td valign="center" width="50"><p align="center">4, 16, 28, 40</p></td></tr><tr><td valign="center" width="50"><p align="center">Dậu</p></td><td valign="center" width="50"><p align="center">3, 15, 27, 39</p></td></tr><tr><td valign="center" width="50"><p align="center">Tuất</p></td><td valign="center" width="50"><p align="center">2, 14, 26, 38</p></td></tr><tr><td valign="center" width="50"><p align="center">Hợi</p></td><td valign="center" width="50"><p align="center">1, 13, 25, 37, 49</p></td></tr></tbody></table><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><table style="height: 1000px; width: 574px;"><tbody><tr><td colspan="3"><p align="center">Luật chơi</p></td></tr><tr><td valign="center" width="44"><p align="center"><strong>12 Con Giáp</strong></p></td><td valign="center" width="95"><p align="center"><strong>Kiểu Chơi</strong></p></td><td valign="center" width="111"><p align="center"><strong>Mô Tả</strong></p></td></tr><tr><td rowspan="6" valign="center" width="44"><p align="center">Bảng 1</p></td><td rowspan="2" valign="center" width="95"><p align="center">Số Đơn</p></td><td valign="center" width="111"><p>Chọn các ô số trong bảng (49 ô số) để đặt cược. So sánh với kết quả mở thưởng, nếu số bạn đã đặt cược trùng với số đặc biệt trong kỳ đó thì bạn trúng thưởng, các số còn lại không trúng thưởng.</p></td></tr><tr><td valign="center" width="276"><p>VD: Bạn cược số 12, 45, 49. Kết quả ra: 45 23 25 11 28 2 27 thì bạn trúng thưởng.</p></td></tr><tr><td rowspan="2" valign="center" width="95"><p align="center"> Nhóm Màu</p></td><td valign="center" width="111"><p>Chọn nhóm màu (đỏ, xanh lá, xanh dương) để đặt cược. Kết quả mở thưởng ra số đặc biệt trùng với nhóm màu bạn đã đặt cược thì trúng thưởng.</p></td></tr><tr><td valign="center" width="276"><p>VD: Bạn cược màu đỏ. Kết quả ra: 2 28 48 28 45 9 26 thì bạn trúng thưởng</p></td></tr><tr><td rowspan="2" valign="center" width="95"><p align="center">Con Giáp</p></td><td valign="center" width="111"><p>Chọn Con Giáp để đặt cược. Kết quả mở thưởng ra số đặc biệt trùng với nhóm Con Giáp bạn đã đặt cược thì trúng thưởng.</p></td></tr><tr><td valign="center" width="276"><p>VD: Bạn cược Tý. Kết quả ra: 12 24 36 48 9 13 15 thì bạn trúng thưởng.</p></td></tr><tr><td rowspan="7" valign="center" width="44"><p align="center">Bảng 2</p></td><td rowspan="2" valign="center" width="95"><p align="center">Đặc biệt Tài/Xỉu</p></td><td valign="center" width="111"><p>Số đặc biệt lớn hơn hoặc bằng 25 được gọi là Đặc biệt Tài. Số đặc biệt nhỏ hơn 25 được gọi là Đặc Biệt Xỉu. Số đặc biệt ra 49 thì Hòa, được hoàn lại tiền cược.</p></td></tr><tr><td valign="center" width="276"><p>VD: Bạn chọn Đặc biệt Tài. Số đặc biệt ra là số 42 thì bạn thắng.</p></td></tr><tr><td valign="center" width="95"><p align="center">Đặc biệt Chẵn/Lẻ</p></td><td valign="center" width="111"><p>Bạn dự đoán số đặc biệt ra chẵn hoặc lẻ. Số đặc biệt ra 49 là Hòa, được hoàn lại tiền cược.</p></td></tr><tr><td rowspan="2" valign="center" width="95"><p align="center">Đơn vị Tài/Xỉu</p></td><td valign="center" width="111"><p>Kết quả số đặc biệt có chữ số đơn vị là 5-9 gọi là Đơn vị Tài, chữ số đơn vị là 0 - 4 gọi là Đơn vịXỉu</p></td></tr><tr><td valign="center" width="276"><p>VD: Bạn đặt cược Đơn vị Tài, kết quả số đặc biệt ra 26 thì bạn trúng thưởng.</p></td></tr><tr><td valign="center" width="95"><p align="center">Đơn vị Chẵn/Lẻ</p></td><td valign="center" width="111"><p>Bạn dự đoán số đặc biệt có chữ số đơn vị là Chẵn hoặc Lẻ</p></td></tr><tr><td valign="center" width="95"><p align="center">Đơn vị Đặc biệt</p></td><td valign="center" width="276"><p>Bạn dự đoán chữ số hàng đơn vị của số đặc biệt (Từ 0 đến 9).</p></td></tr><tr><td rowspan="5" valign="center" width="44"><p align="center">Bảng thường</p></td><td rowspan="2" valign="center" width="95"><p align="center">Số Đơn</p></td><td valign="center" width="111"><p>Chọn các ô số trong bảng (49 ô số) để đặt cược. So sánh với kết quả mở thưởng, nếu số bạn đã đặt cược trùng với một trong 6 số thường của kỳ đó thì bạn trúng thưởng.</p></td></tr><tr><td valign="center" width="276"><p>VD: Bạn đặt cược ô số 4, 5, 6. kết quả ra 22 5 48 47 45 9 26 thì bạn trúng thưởng.</p></td></tr><tr><td rowspan="2" valign="center" width="95"><p align="center">Số thường Tài/Xỉu</p></td><td valign="center" width="111"><p>Số thường đầu tiên lớn hơn hoặc bằng 25 được gọi là Tài, nhỏ hơn 25 được gọi là Xỉu. Số thường đầu tiên ra 49 thì Hòa, được hoàn lại tiền cược.</p></td></tr><tr><td valign="center" width="276"><p>VD: Bạn đặt cược Tài, kết quả ra 22 35 48 47 45 9 26 thì bạn trúng thưởng.</p></td></tr><tr><td valign="center" width="95"><p align="center">Số thường Chẵn/Lẻ</p></td><td valign="center" width="111"><p>Bạn dự đoán số thường đầu tiên ra chẵn hoặc lẻ. Số thường đầu tiên ra 49 là Hòa, được hoàn lại tiền cược.</p></td></tr></tbody></table>',
	gameCommon = {
		animationSwitch: !0,
		isLog: !1,
		log: function (t) {
			1 == gameCommon.isLog && console.error(t)
		},
		flyAddChip: function (t, e) {
			if (t == null && t == undefined)
				return;
			if (gameCommon.animationSwitch) {
				gameCommon.animationSwitch = !1;
				var n = $("body"),
					i = $(t),
					o = $(".option_2_2_selectBox").hasClass("open") ? $(".option_2_2_selectBox [select]") : $(".option_2_2_selected [select]"),
					r = o.clone(),
					a = o.clone(),
					l = o.offset(),
					s = i.offset(),
					d = i.position(),
					c = i.attr("data-type"),
					p = parseFloat(o.attr("chip-value")),
					u = i.attr("data-chip-Num"),
					g = $(t).find(".tip"),
					f = $(t).find(".text"),
					h = gameCommon.formatFloat(f.text()),
					m = i.attr("ctext"),
					v = "";
				if (u = void 0 === u ? 0 : Number(u), e.userbalance - e.pourTotal < p) return void gameCommon.dialogBox("Bạn không đủ số dư");
				if (e.maxbetmoney - e.pourTotal < p) return void gameCommon.dialogBox("Giới hạn đặt cược tối đa của trò chơi là:" + gameCommon.moneyFormat(e.maxbetmoney) + "VND");
				var b = 0;
				if (isNaN(c)) b = e.marksixmethod[c];
				else {
					var y = "tm1" == e.gameType ? "YMZT" : "YMBDW";
					b = e.marksixmethod[y]
				}
				if (p > b - h) return void gameCommon.dialogBox("Giới hạn đặt cược tối đa của trò chơi là:" + gameCommon.moneyFormat(b) + "VND");
				r.css({
					left: l.left,
					top: l.top,
					position: "absolute"
				}), r.appendTo(n), u++ , v = c + "-" + u, i.attr("data-chip-Num", u), a.attr("history-chip", v), a.removeClass("chip-selected"), r.animate({
					left: s.left + i.width() / 2 - o.width() / 2 + 8,
					top: s.top + i.height() / 2 - o.height() / 2
				}, function () {
					r.remove(), a.appendTo(i), a.css({
						left: d.left + i.width() / 2 - o.width() / 2,
						top: d.top + i.height() / 2 - o.height() / 2 - 2 * u,
						position: "absolute"
					}), g.css({
						left: d.left + i.width() / 2 - o.width() / 2 - 16,
						top: d.top + i.height() / 2 - o.height() / 2 - 30 - 1.5 * u
					}), e.historyKey.push(v), e.historyValue[v] = {
						historyChip: v,
						dataType: c,
						chipValue: p
					}, h += p, e.betCellTotal[c] = {
						money: h,
						text: m
					}, e.pourTotal += parseInt(p), e.setPour(e.pourTotal), f.text(gameCommon.moneyFormat(h)), "true" == i.attr("mousein") && g.show(), gameCommon.animationSwitch = !0
				})
			}
		},
		flyRemoveChip: function (t, e, n) {
			if (gameCommon.animationSwitch) {
				gameCommon.animationSwitch = !1;
				var i, o, r, a = $("body"),
					l = t.attr("chip-value"),
					s = e.find(".tip"),
					d = t.attr("history-chip"),
					c = $(e).find(".text"),
					p = e.attr("data-type"),
					u = gameCommon.formatFloat(c.text()),
					g = Number(e.attr("data-chip-Num")),
					f = e.attr("ctext");
				if ($(".option_2_2_selectBox").hasClass("open")) i = $(".option_2_2_selectBox div[chip-value = " + l + "]"), r = i.offset();
				else if (i = $(".option_2_2_selected div[chip-value = " + l + "]"), r = i.offset(), 0 == i.length) {
					var h = $(".option_2_2_selected .chip-selected");
					i = $(".option_2_2_selectBox div[chip-value = " + l + "]"), r = h.offset()
				}
				o = i.clone(), o.css({
					left: t.offset().left,
					top: t.offset().top,
					position: "absolute"
				}), o.appendTo(a), t.remove(), o.animate({
					left: r.left,
					top: r.top
				}, function () {
					o.remove(), g-- , e.attr("data-chip-Num", g), s.css({
						top: parseFloat(s.css("top")) + 1.5
					}), u -= l, c.text(gameCommon.moneyFormat(u)), u > 0 ? n.betCellTotal[p] = {
						money: u,
						text: f
					} : (delete n.betCellTotal[p], s.hide()), n.historyKey.splice(n.historyKey.indexOf(d), 1), delete n.historyValue[d], n.pourTotal -= l, n.setPour(n.pourTotal), gameCommon.animationSwitch = !0
				})
			}
		},
		clearChip: function (t) {
			$.each(t.historyKey, function (e, n) {
				var i, o, r, a = $('[history-chip = "' + t.historyValue[n].historyChip + '"]'),
					l = $(".game_table_" + t.gameType + ' [data-type = "' + t.historyValue[n].dataType + '"]'),
					s = $("body"),
					d = a.attr("chip-value");
				a.attr("history-chip"), l.attr("data-type");
				if ($(".option_2_2_selectBox").hasClass("open")) i = $(".option_2_2_selectBox div[chip-value = " + d + "]"), r = i.offset();
				else if (i = $(".option_2_2_selected div[chip-value = " + d + "]"), r = i.offset(), 0 == i.length) {
					var c = $(".option_2_2_selected .chip-selected");
					i = $(".option_2_2_selectBox div[chip-value = " + d + "]"), r = c.offset()
				}
				o = i.clone(), o.css({
					left: a.offset().left,
					top: a.offset().top,
					position: "absolute"
				}), o.appendTo(s), a.remove(), o.animate({
					left: r.left,
					top: r.top
				}, function () {
					o.remove()
				})
			}), $.each(t.betCellTotal, function (e) {
				var n = $(".game_table_" + t.gameType + ' [data-type = "' + e + '"]'),
					i = Number(n.attr("data-chip-Num")),
					o = n.find(".tip"),
					r = n.find(".text");
				n.attr("data-chip-Num", 0), o.css({
					top: parseFloat(o.css("top")) + 2 * i
				}), r.text("0.00")
			}), t.historyKey = [], t.historyValue = {}, t.betCellTotal = {}, t.pourTotal = 0, t.setPour(t.pourTotal)
		},
		historyBack: function (t) {
			if (t.historyKey.length > 0) {
				var e = [].concat(t.historyKey).pop(),
					n = t.historyValue[e],
					i = $('[history-chip = "' + n.historyChip + '"]'),
					o = $(".game_table_" + t.gameType + ' [data-type = "' + n.dataType + '"]');
				gameCommon.flyRemoveChip(i, o, t)
			}
		},
		loadDialog: function () {
			// var t = layer.open({
			// 	type: 1,
			// 	title: !1,
			// 	closeBtn: 0,
			// 	shadeClose: !1,
			// 	maxWidth: 366,
			// 	skin: "loading_messageBox",
			// 	content: '<div class="content"><img src="/lucky-game/sixmark/loading.gif" ></div>'
			// });
			// return t
		},
		dialogBox: function (t) {
			gameCommon.animationSwitch = !0;
			var e = layer.open({
				type: 1,
				title: !1,
				closeBtn: 0,
				shadeClose: !1,
				maxWidth: 366,
				skin: "default_messageBox",
				content: '<div class="content">' + t + '</div><div class="ok_box_btn btn"></div>'
			});
			$(".default_messageBox .ok_box_btn").click(function () {
				layer.close(e)
			})
		},
		dialogBoxTip: function (t, e) {
			gameCommon.animationSwitch = !0;
			var n = layer.open({
				type: 1,
				title: !1,
				closeBtn: 0,
				shadeClose: !1,
				maxWidth: 366,
				skin: "default_messageBox",
				content: '<div class="content">' + t + "</div>"
			});
			setTimeout(function () {
				layer.close(n)
			}, 2e3 | e)
		},
		moneyFormat: function (t) {
			var e = Number(t) < 0 ? "-" : "";
			if (t = t.toString().replace(/[^\d.]/g, ""), t = t.replace(/\.{2,}/g, "."), t = t.replace(".", "$#$").replace(/\./g, "").replace("$#$", "."), -1 != t.indexOf(".")) {
				var n = t.split(".");
				n[0] = n[0].substr(0, 15);
				for (var i = [], o = n[0].length; o > 0; o -= 3) i.unshift(n[0].substring(o, o - 3));
				n[0] = i.join(","), t = n[0] + "." + n[1].substr(0, 4)
			} else {
				t = t.substr(0, 15);
				for (var i = [], o = t.length; o > 0; o -= 3) i.unshift(t.substring(o, o - 3));
				t = i.join(",");
			}
			return e + t
		},
		formatFloat: function (t) {
			var t = t.replace(/^[^\d]/g, "");
			if (t = t.replace(/[^\d.]/g, ""), t = t.replace(/\.{2,}/g, "."), t = t.replace(".", "$#$").replace(/\./g, "").replace("$#$", "."), -1 != t.indexOf(".")) {
				var e = t.split(".");
				t = e[0].substr(0, 15) + "." + e[1].substr(0, 2)
			} else t = t.substr(0, 15);
			return parseFloat(t)
		},
		winNumberFormatList: function (t) {
			try {
				var e = t.split("&"),
					n = e[0].split("|"),
					i = [];
				return n = [e[1]].concat(n), $.each(n, function (t, e) {
					var n = {};
					e = parseInt(e), -1 != tm1Config.red.indexOf(e) ? n.type = "red" : -1 != tm1Config.blue.indexOf(e) ? n.type = "blue" : -1 != tm1Config.green.indexOf(e) && (n.type = "green"), n.num = e, i.push(n)
				}), i
			} catch (t) { }
		},
		api: function (t) {
			var e;
			t.hasOwnProperty("loading") && (e = gameCommon.loadDialog()), t.data.lotteryid = 96, $.ajax({
				type: "POST",
				dataType: "json",
				url: "/MarkSixService.aspx",
				data: t.data,
				success: function (n) {
					t.hasOwnProperty("loading") && layer.close(e), "success" == n.status ? t.success(n.data) : t.hasOwnProperty("fail") ? t.fail(n) : gameCommon.dialogBox(n.data), t.hasOwnProperty("optionDom") && t.optionDom.removeAttr("disabled")
				},
				fail: function (n) {
					t.hasOwnProperty("loading") && layer.close(e), t.hasOwnProperty("fail") && t.fail(n), t.hasOwnProperty("optionDom") && t.optionDom.removeAttr("disabled")
				}
			})
		},
		initApi: function (t, e) {
			gameCommon.log("Meminta inisialisasi server"), gameCommon.api({
				data: {
					gameid: t.lotteryid,
					flag: "init"
				},
				success: function (n) {
					gameCommon.log("Server mengembalikan data", n), t.userbalance = parseFloat(n.userbalance), t.lotteryid = n.lotteryid;
					var i = n.curissue;
					t.lotteryName = n.lotteryname,
						t.servertime = n.servertime,
						t.issueliststr = n.issueliststr,
						// t.setBonusgroup(n.bonusgroup),
						t.setBonusTip(n.bonusgroups),
						t.setMaxbonusMoney(n.maxbonusmoney);
					var o = n.marksixmethod;
					$.each(o, function (e, n) {
						t.marksixmethod[n.name] = n.max
					}), t.showOpenCode(!1),
						null != n.lastopenissue ? (t.lastopenissue = n.lastopenissue.issue, t.setOnefreshOpenNum(), t.setFreshOpenNum()) : t.lastopenissue = null,
						e()
					var d = [];
					$.each(n.history, function (n, g) {
						var i = [];
						i = gameCommon.winNumberFormatList(g.detail),
							d.push({
								list: i,
								issue: g.issue,
								openTime: g.date.substring(10, 19)
							})
					}),
						t.historyResult = d,
						t.historyResult = t.historyResult.slice(0, 10),
						t.setOpenInfoPanel(t.historyResult);
				}
			})
		},
		betApi: function (t) {
			var e = [],
				n = {};
			$.each(t.betCellTotal, function (t, i) {
				i = i.money, isNaN(t) ? n[t] = i : e.push(i + "#" + t)
			}), gameCommon.log(t.betCellTotal);
			var i = "tm1" == t.gameType ? "YMZT" : "YMBDW";
			n[i] = e, gameCommon.api({
				loading: !0,
				data: {
					flag: "ms1sbet",
					gameid: t.lotteryid,
					total_money: t.pourTotal,
					trace_if: 0,
					total_nums: 1,
					issue_start: t.issue,
					"buyitem[]": "{'money':'" + t.pourTotal + "','nums':1,'times':'1'}",
					betdata: n,
					betFrom: 0,
				},
				success: function (e) {
					gameCommon.dialogBoxTip("Đặt cược thành công")
					gameCommon.clearChip(t),
						t.setBalance(parseInt(e.balance, 10)),
						gameCommon.getBetHistoryApi(t),
						setTimeout(function () {
							t.showOpenCode(!0, gameCommon.winNumberFormatList(e.result));
							if (t.historyResult.length < 10) {
								t.historyResult.unshift({
									list: gameCommon.winNumberFormatList(e.result),
									openTime: e.date.substring(10, 19)
								})
								t.setOpenInfoPanel(t.historyResult);
							} else {
								t.historyResult.pop(),
									t.historyResult.unshift({
										list: gameCommon.winNumberFormatList(e.result),
										openTime: e.date.substring(10, 19)
									})
								t.setOpenInfoPanel(t.historyResult);
							}
						}, 2e3 + 100)
				}
			})
		},
		goBetApi: function (t, e, n) {
			var i = [],
				o = {};
			$.each(t.betCellTotal, function (t, e) {
				e = e.money, isNaN(t) ? o[t] = e : i.push(e + "#" + t)
			});
			var r = "tm1" == t.gameType ? "YMZT" : "YMBDW";
			o[r] = i;
			var a = null,
				l = [];
			$.each(t.isssueInfoList, function (e, n) {
				null == a && (a = e), l.push(JSON.stringify({
					issue: e,
					times: n.money / t.pourTotal
				}))
			}), gameCommon.api({
				loading: !0,
				data: {
					flag: "ms1sbet",
					gameid: t.lotteryid,
					total_money: t.pourTotal,
					trace_total_money: e,
					trace_if: 1,
					total_nums: 1,
					trace_count_input: l.length,
					trace_stop: n ? "yes" : "no",
					issue_start: t.issue,
					"buyitem[]": "{'money':'" + t.pourTotal + "','nums':1,'times':'1'}",
					betdata: o,
					buytrace: l
				},
				success: function (e) {
					gameCommon.clearChip(t),
						t.setBalance(e),
						gameCommon.getBetHistoryApi(t),
						t.setOnefreshOpenNum(),
						t.setFreshOpenNum(),
						gameCommon.dialogBoxTip("Đặt cược thành công")
				}
			})
		},
		getBetHistoryApi: function (t) {
			gameCommon.api({
				data: {
					flag: "getbetcodehistory",
					sid: t.lotteryid
				},
				success: function (e) {
					t.setRecordListPanel(e[0].reslist, e[0].freshtime), t.setBalance(e[0].userbalance)
				}
			})
		},
		getBetDetailApi: function (t, e) {
			gameCommon.api({
				data: {
					flag: "getbetdetail",
					sid: e
				},
				success: function (e) {
					t.setBetInfoPanel(e)
				}
			})
		},
		getHistoryApi: function (t, e, n) {
			gameCommon.api({
				data: {
					flag: "getopencodehistory",
					sid: 81,
					num: e
				},
				success: function (t) {
					var e = [];
					$.each(t, function (t, n) {
						var i = [];
						i = gameCommon.winNumberFormatList(n.winnumber), e.push({
							list: i,
							issue: n.issue,
							openTime: n.opentime.substring(0, 10)
						})
					}), n(e)
				}
			})
		},
		cancelBetApi: function (t, e, n) {
			gameCommon.api({
				data: {
					flag: "cancelbet",
					sid: e
				},
				success: function (t) {
					n(), gameCommon.dialogBoxTip("Huỷ thành công")
				}
			})
		},
		getOpenCodeApi: function (t, e, n, i) {
			gameCommon.api({
				data: {
					flag: "getopencode",
					issue: e
				},
				success: function (t) {
					("" == t || "Chưa mở" == t) ? i() : n(t.code)
				}
			})
		}
	};
$(function () {
	var t = {
		gameType: "tm1",
		historyKey: [],
		historyValue: {},
		betCellTotal: {},
		pourTotal: 0,
		maxbetmoney: 5e7,
		lotteryName: "",
		lotteryid: lotteryid,
		refreshOpenTime: refreshOpenTime,
		marksixmethod: {},
		issue: "1 Giây",
		lastopenissue: null,
		userbalance: 0,
		opentime: "",
		servertime: "",
		issueliststr: [],
		isssueInfoList: {},
		timeInterval: [],
		freshBetRecordInterval: null,
		freshOpenHistoryInterval: null,
		historyResult: [],
		initTm1: function () {
			for (var e = '<div class="game_table_1">', n = 0; 4 > n; n++) {
				for (var i = '<div class="game_table_row_1">', o = 1; 12 >= o; o++) {
					var r = 12 * n + o,
						a = tm1Config.numberText;
					i += '<div ctext="' + a + '" data-type="' + r + '" class="cell_' + r + '"><span class="tip"><span class="text">0.00</span><span class="arrow"></span></span></div>'
				}
				i += "</div>", e += i
			}
			var l = (new Date(t.servertime.replace("-", "/")).getFullYear() - 2016) / 12;
			tm1Config.animalNum = tm1Config.animalNum.slice(12 - l, 12).concat(tm1Config.animalNum.slice(0, 12 - l));
			for (var s = '<div class="game_table_row_2">', o = 1; 12 >= o; o++) {
				var r = o - 1,
					d = tm1Config.SXtype[r],
					c = tm1Config.animalNum[r].join("|");
				s += "<div animalNum = " + c + ' ctext="' + d.text + '" data-type="' + d.key + '" class="cell_' + r + '"><span class="tip"><span class="text">0.00</span><span class="arrow"></span></span></div>'
			}
			s += "</div>", e += s, e += "</div>";
			for (var p = '<div class="game_table_2">', o = 1; 4 >= o; o++) {
				var d, u, r = o,
					g = "";
				1 == o ? (d = 49, u = 1, g = tm1Config.numberText) : (d = tm1Config.SBtype[o - 2].key, g = tm1Config.SBtype[o - 2].text, u = o + " cell_large"), p += '<div ctext="' + g + '" data-type="' + d + '" class="cell_' + u + '"><span class="tip"><span class="text">0.00</span><span class="arrow"></span></span></div>'
			}
			p += "</div>";
			var f = "";
			$.each(tm1Config.helpInfo, function (t, e) {
				var n = t;
				if (e.text === undefined) {
					i = '<div class="game_help_tip info_item_' + n + '"><div class="text"><span class="title">【' + e.label + "】：</span>" + e.title + "</div></div>";
				} else {
					i = '<div class="game_help_tip info_item_' + n + '"><div class="text"><span class="title">【' + e.label + "】：</span>" + e.title + '<br><span class="title">【Ví dụ】：</span>' + e.text + "</div></div>";
				}
				f += i
			}), $(".game_table_tm1").append($(f + e + p))
		},
		initTm2: function () {
			var t = '<div class="game_table_5">';
			$.each(tm2Config.TMWStype, function (e, n) {
				var i = e + 1;
				t += '<div  ctext="' + n.text + '" data-type="' + n.key + '" class="cell_' + i + '"><span class="tip"><span class="text">0.00</span><span class="arrow"></span></span></div>'
			}), t += "</div>";
			for (var e = '<div class="game_table_4">', n = 0; 4 > n; n++) {
				var i, o = n + 1,
					i = tm2Config.TWtype[n];
				e += '<div  ctext="' + i.text + '"  data-type="' + i.key + '" class="cell_' + o + '"><span class="tip"><span class="text">0.00</span><span class="arrow"></span></span></div>'
			}
			e += "</div>";
			var r = '<div class="game_table_6">';
			$.each(tm2Config.TWtype.slice(4), function (t, e) {
				var n = t + 1;
				r += '<div ctext="' + e.text + '" data-type="' + e.key + '" class="cell_' + n + '"><span class="tip"><span class="text">0.00</span><span class="arrow"></span></span></div>'
			}), r += "</div>";
			var a = "";
			$.each(tm2Config.helpInfo, function (t, e) {
				var n = t + 5;
				if (e.text === undefined) {
					i = '<div class="game_help_tip info_item_' + n + '"><div class="text"><span class="title">【' + e.label + "】：</span>" + e.title + "</div></div>";
				} else {
					i = '<div class="game_help_tip info_item_' + n + '"><div class="text"><span class="title">【' + e.label + "】：</span>" + e.title + '<br><span class="title">【Ví dụ】：</span>' + e.text + "</div></div>";
				}
				a += i
			}), $(".game_table_tm2").append(t + e + r + a)
		},
		initZm: function () {
			for (var t = '<div class="game_table_1">', e = 0; 4 > e; e++) {
				for (var n = '<div class="game_table_row_1">', i = 1; 12 >= i; i++) {
					var o = 12 * e + i,
						r = zmConfig.numberText;
					n += '<div ctext="' + r + '" data-type="' + o + '" class="cell_' + o + '"><span class="tip"><span class="text">0.00</span><span class="arrow"></span></span></div>'
				}
				n += "</div>", t += n
			}
			t += "</div>";
			for (var a = '<div ctext="' + zmConfig.numberText + '" data-type="49" class="game_table_3 cell_49"><span class="tip"><span class="text">0.00</span><span class="arrow"></span></span></div>', l = '<div class="game_table_4">', i = 0; 4 > i; i++) {
				var s, o = i + 1,
					s = zmConfig.ZYtype[i];
				l += '<div  ctext="' + s.text + '"  data-type="' + s.key + '" class="cell_' + o + '"><span class="tip"><span class="text">0.00</span><span class="arrow"></span></span></div>'
			}
			l += "</div>";
			var d = "";
			$.each(zmConfig.helpInfo, function (t, e) {
				var n = t + 12;
				if (e.text === undefined) {
					i = '<div class="game_help_tip info_item_' + n + '"><div class="text"><span class="title">【' + e.label + "】：</span>" + e.title + "</div></div>";
				} else {
					i = '<div class="game_help_tip info_item_' + n + '"><div class="text"><span class="title">【' + e.label + "】：</span>" + e.title + '<br><span class="title">【Ví dụ】：</span>' + e.text + "</div></div>";
				}
				d += i
			}), $(".game_table_zm").append(t + a + l + d)
		},
		initEvent: function () {
			$("#tm1").click(function () {
				gameCommon.clearChip(t), $(".game_table_tm1").show(), $(this).addClass("active_1"), $(this).siblings().removeClass("active_1"), $(".game_table_tm2,.game_table_zm").hide(), t.gameType = "tm1"
			}), $("#zm").click(function () {
				gameCommon.clearChip(t), $(".game_table_tm1").hide(), $(".game_table_zm").show(), $(".game_table_tm2").hide(), t.gameType = "zm", $(this).addClass("active_1"), $(this).siblings().removeClass("active_1")
			}), $("#tm2").click(function () {
				gameCommon.clearChip(t), $(".game_table_tm1").hide(), $(".game_table_tm2").show(), $(".game_table_zm").hide(), t.gameType = "tm2", $(this).addClass("active_1"), $(this).siblings().removeClass("active_1")
			}), $(".game_table_tm1 [data-type],.game_table_tm2 [data-type],.game_table_zm [data-type]").on("click", function () {
				gameCommon.flyAddChip(this, t)
			}), $("[data-type=SB_H],[data-type=SB_L],[data-type=SB_LV]", ".game_table_tm1").on("mouseenter", function () {
				var t = $(this).attr("data-type");
				switch (t) {
					case "SB_H":
						$.each(tm1Config.red, function (t, e) {
							$("[data-type=" + e + "]", ".game_table_tm1").addClass("hover")
						});
						break;
					case "SB_L":
						$.each(tm1Config.blue, function (t, e) {
							$("[data-type=" + e + "]", ".game_table_tm1").addClass("hover")
						});
						break;
					case "SB_LV":
						$.each(tm1Config.green, function (t, e) {
							$("[data-type=" + e + "]", ".game_table_tm1").addClass("hover")
						})
				}
			}).on("mouseleave", function () {
				var t = $(this).attr("data-type");
				switch (t) {
					case "SB_H":
						$.each(tm1Config.red, function (t, e) {
							$("[data-type=" + e + "]", ".game_table_tm1").removeClass("hover")
						});
						break;
					case "SB_L":
						$.each(tm1Config.blue, function (t, e) {
							$("[data-type=" + e + "]", ".game_table_tm1").removeClass("hover")
						});
						break;
					case "SB_LV":
						$.each(tm1Config.green, function (t, e) {
							$("[data-type=" + e + "]", ".game_table_tm1").removeClass("hover")
						})
				}
			}), $("[animalNum]", ".game_table_tm1").on("mouseenter", function () {
				var t = $(this).attr("animalNum").split("|");
				$.each(t, function (t, e) {
					$("[data-type=" + e + "]", ".game_table_tm1").addClass("hover")
				})
			}).on("mouseleave", function () {
				var t = $(this).attr("animalNum").split("|");
				$.each(t, function (t, e) {
					$("[data-type=" + e + "]", ".game_table_tm1").removeClass("hover")
				})
			}), $(".game_table_tm1 [data-type],.game_table_tm2 [data-type],.game_table_zm [data-type]").on("mouseenter", function () {
				var t = parseFloat($(this).find(".text").text());
				t > 0 && $(this).find(".tip").show(), $(this).attr("mousein", !0)
			}), $(".game_table_tm1 [data-type],.game_table_tm2 [data-type],.game_table_zm [data-type]").on("mouseleave", function () {
				$(this).find(".tip").hide(), $(this).attr("mousein", !1)
			}), $(".game_table_tm1 [data-type],.game_table_tm2 [data-type],.game_table_zm [data-type]").on("contextmenu", function (e) {
				if (3 == e.which) {
					var n, i = $(this),
						o = i.attr("data-chip-Num");
					o > 0 && (n = i.children("[chip-value]").last(), gameCommon.flyRemoveChip(n, $(this), t))
				}
				return !1
			}), $(".game_record_open_btn").click(function () {
				$(".game_record_panel").slideDown("slow"), gameCommon.getBetHistoryApi(t)
			}), $(".game_record_panel .close,.game_record_panel .close_span").click(function () {
				clearTimeout(t.freshBetRecordInterval), $(".game_record_panel").hide()
			}), $("[action=record_history]").click(function () {
				gameCommon.getHistoryApi(t, 50, t.showHistoryPanel)
			}),
				$(".game_new_open_btn").click(function () {
					$(".game_new_open_panel").slideDown("slow")
					// gameCommon.getHistoryApi(t, 10, t.setOpenInfoPanel)
				}),
				$(".game_new_open_panel .close,.game_new_open_panel .close_span").click(function () {
					clearTimeout(t.freshOpenHistoryInterval), $(".game_new_open_panel").hide()
				}),
				$("#cancelBtn").click(function () {
					gameCommon.historyBack(t)
				}), $("#clearBtn").click(function () {
					gameCommon.clearChip(t)
				}), $("#changeResBtn").click(function () {
					var e = $(".option_2_2_selectBox"),
						n = $(".option_2_2_selected");
					e.hasClass("open") ? (e.removeClass("open"), e.hide(), n.show(), $(".option_selectBox").fadeIn("slow")) : (e.addClass("open"), e.show(), n.hide(), $(".option_selectBox").fadeIn("slow"))
				}), $(".option_2_2_selectBox").sortable({}), $(".selectBox_close").click(function () {
					t.closeChipBox()
				}), $(".option_2_2_selected [chip-value]").on("click", function () {
					$(this).siblings().removeClass("chip-selected").removeAttr("select"), $(this).addClass("chip-selected").attr("select", !0)
				}), $(".option_2_2_selectBox [chip-value]").on("click", function () {
					$(this).siblings().removeAttr("select").removeClass("select"), $(this).attr("select", !0).addClass("select")
				}), $("#goNumBtn").click(function () {
					t.showGoNumPanel()
				}), $("#confirmChipBtn").on("click", function () {
					if (0 === t.pourTotal) {
						void gameCommon.dialogBoxTip("Vui lòng đặt cược một số tiền nhất định")
					} else {
						void $(".number_box", ".game_record_time").hide(),
							void $(".record_number_loading", ".game_record_time").show(),
							void t.sureBetInfoPanel(t.pourTotal, function () { gameCommon.betApi(t) })
					}
				}), $(".game_help_tip").click(function () {
					t.showHelpPanel("Mô tả trò chơi", helpCenterHtml)
				}), $(".max_money").click(function () {
					t.showHelpPanel("Mô tả trò chơi", helpCenterHtml)
				})
			$('#edit-chip-input').on(('input paste'), function e(evt) {
				if (evt.which >= 37 && evt.which <= 40) return;
				$(this).val((index, value) => value.replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ','));
				console.log($(this).val().search(',') > -1);
				if ($(this).val().search(',') > -1) {
					$("#edit-chip").attr("chip-value", $(this).val().replace(',', ''));
				}
			});
		},
		setPour: function (t) {
			$(".option_2_3 .pour").text(gameCommon.moneyFormat(t))
		},
		setBalance: function (e) {
			t.userbalance = e, $(".option_2_3 .balance").text(gameCommon.moneyFormat(e))
		},
		setRecordDate: function (e) {
			t.issue = e,
				$(".game_record_time .record_date").text(t.issue),
				null == t.lastopenissue ? $(".record_qi", ".game_record_time").html("Lượt sổ " + t.issue) : $(".record_qi", ".game_record_time").html("Lượt sổ " + t.lastopenissue)
		},
		setPourTime: function () {
			var e = (new Date(t.opentime.replace("-", "/")) - new Date(t.servertime.replace("-", "/"))) / 1e3;
			if (gameCommon.log("定时器的时间" + e), !(0 > e)) {
				var n;
				n = setInterval(function () {
					e--;
					var i = parseInt(e / 3600),
						o = parseInt((e - 3600 * i) / 60),
						r = parseInt(e % 60),
						a = "";
					10 > i && (i = "0" + i), 10 > o && (o = "0" + o), 10 > r && (r = "0" + r), a = i + ":" + o + ":" + r, 0 == e && t.setFreshOpenNum(), 0 > e ? (clearTimeout(n), gameCommon.initApi(t, function () {
						gameCommon.log("重启计时器"), t.setPourTime(), t.freshAllPanel()
					})) : $(".game_record_time .record_time").text(a)
				}, 1e3)
			}
		},
		setFreshOpenNum: function () {
			gameCommon.getOpenCodeApi(t, t.lastopenissue, function (e) {
				if (e != null) t.showOpenCode(!0, gameCommon.winNumberFormatList(e))
			}, function () {
				t.setFreshOpenNum()
			})
		},
		setOnefreshOpenNum: function () {
			gameCommon.getOpenCodeApi(t, t.lastopenissue, function (e) {
				if (e != null) t.showOpenCode(!0, gameCommon.winNumberFormatList(e))
			}, function () { })
		},
		showOpenCode: function (t, e) {
			if (t) {
				var n = [],
					i = $(".number_box", ".game_record_time");
				$.each(e, function (t, e) {
					n.push('<li class="num' + t + " " + e.type + '" >' + e.num + "</li>")
				}), i.html(n.join("")), i.show(), $(".record_number_loading", ".game_record_time").hide()
			} else $(".number_box", ".game_record_time").hide(),
				$(".record_number_loading", ".game_record_time").show(),
				$(".record_number_loading", ".game_record_time").css("background", "url(../lucky-game/sixmark/recordNum_loading_time.gif)"),
				setTimeout(function () {
					$(".record_number_loading", ".game_record_time").css("background", "url(../lucky-game/sixmark/recordNum_loading.gif)")
				}, 3e3)
		},
		setBonusTip: function (e) {
			$(".Bonus_tip").unbind("click").click(function () {
				var n = [];
				$.each(e, function (t, e) {
					e = e.split(":"),
						n.push('<tr class="title"><td style="height: 30px;width: 280px;text-align: center">' + e[0] + '</td><td style="width: 280px;padding-left: 10px;text-align: center">' + e[1] + "</td></tr>")
				}), t.showHelpPanel("Tỉ lệ", '<table style="height: 700px">' + n.join("") + " </table>")
			})
			$(".Bonus_group").unbind("click").click(function () {
				var n = [];
				$.each(e, function (t, e) {
					e = e.split(":"),
						n.push('<tr class="title"><td style="height: 30px;width: 280px;text-align: center">' + e[0] + '</td><td style="width: 280px;padding-left: 10px;text-align: center">' + e[1] + "</td></tr>")
				}), t.showHelpPanel("Tỉ lệ", '<table style="height: 700px">' + n.join("") + " </table>")
			})
		},
		closeChipBox: function () {
			var t = $(".option_2_2_selectBox"),
				e = $(".option_2_2_selected");
			t.removeClass("open");
			var n = t.children(),
				i = $(n[10]).clone();
			i.addClass("chip-selected"), i.attr("select", !0), e.text(""), e.append(i);
			for (var o = 11; o < n.length; o++) e.append($(n[o]).clone().removeAttr("select"));
			e.toggle(), $(".option_selectBox").toggle(), $(".option_2_2_selected [chip-value]").unbind("click").on("click", function () {
				$(this).siblings().removeClass("chip-selected").removeAttr("select"), $(this).addClass("chip-selected").attr("select", !0)
			})
		},
		setFreshBetRecordInterval: function (e) {
			"number" == typeof t.freshBetRecordInterval && clearTimeout(t.freshBetRecordInterval), t.freshBetRecordInterval = e
		},
		setFreshOpenHistoryInterval: function (e) {
			"number" == typeof t.freshOpenHistoryInterval && clearTimeout(t.freshOpenHistoryInterval), t.freshOpenHistoryInterval = e
		},
		setBonusgroup: function (t) {
			$(".game_new_open .Bonus_group").html(t)
		},
		setMaxbonusMoney: function (t) {
			// $(".game_record .max_money").html(gameCommon.moneyFormat(t))
		},
		showHelpPanel: function (t, e) {
			var n = '<div ><div class="title">' + t + '</div><div class="horizontal-only" id="help_info_panel" ><div class="content"  >' + e + '</div></div><div class="close_btn"></div></div>',
				i = layer.open({
					type: 1,
					shadeClose: !1,
					title: !1,
					closeBtn: 0,
					maxWidth: 606,
					skin: "game_help_message",
					content: n
				});
			$("#help_info_panel").jScrollPane(), $(".game_help_message .close_btn").unbind("click").click(function () {
				layer.close(i)
			})
		},
		showGoNumPanel: function () {
			if (1 == gameCommon.animationSwitch) {
				if (0 == t.pourTotal) return void gameCommon.dialogBoxTip("Vui lòng đặt cược một số tiền nhất định");
				var e = "";
				$.each(t.issueliststr, function (n, i) {
					n += 1, e += '<tr><td class="id ">' + n + '</td><td class="select "><input type="checkbox" class="isGo" ids = ' + i.issue + ' /></td><td class="num ">' + i.issue + '</td><td class="money "><div class="add_btn btn"></div>￥<span class="moneyValue"  >' + t.pourTotal + '</span>.00<div class="minus_btn btn"></div></td><td class="date ">' + i.endtime + "</td></tr>"
				}), t.isssueInfoList = {};
				var n = '<div ><div class="isStopGO"><input type="checkbox" checked = checked  class="" >Dừng lại khi chiến thắng</div><div class="title">Pengaturan nomor pelacakan</div><table class="content"  ><tr><td class="id th">Nomor Periode</td><td class="select th">Pilih</td><td class="num th">Periode mengejar</td><td class="money th">Jumlah</td><td class="date_th th">Thời gian xổ số</td></tr></table><div class="horizontal-only" id="go_info_panel" ><table class="content table-content">' + e + '<table/></div><div class="go_content" >Jumlah total<span class="qi">0</span>Hasil，Tống số cá cược<span class="totalMoney">0</span><span>VNĐ</span></div ><div class="option"><div class="ok_close_box btn" ></div><div class="cancel_close_box btn"></div></div></div>',
					i = layer.open({
						type: 1,
						id: "goNum",
						shadeClose: !1,
						title: !1,
						closeBtn: 0,
						maxWidth: 606,
						skin: "goNum_confirm_bg",
						content: n
					});
				$("#go_info_panel").jScrollPane(), $(".goNum_confirm_bg .add_btn").unbind("click").click(function () {
					var e, n = $(this).parent().find(".moneyValue"),
						i = $(this).parent().parent().find(".isGo"),
						o = i.attr("ids"),
						r = 0,
						a = i.is(":checked");
					n.text(parseFloat(n.text()) + t.pourTotal), a && (e = $(".goNum_confirm_bg .totalMoney"), r = parseFloat(e.text()) + t.pourTotal, e.text(r), t.isssueInfoList[o] = {
						money: parseFloat(n.text())
					})
				}), $(".goNum_confirm_bg .minus_btn").unbind("click").click(function () {
					var e, n = $(this).parent().find(".moneyValue"),
						i = $(this).parent().parent().find(".isGo"),
						o = i.attr("ids"),
						r = 0,
						a = i.is(":checked");
					parseFloat(n.text()) != t.pourTotal && (n.text(parseFloat(n.text()) - t.pourTotal), a && (e = $(".goNum_confirm_bg .totalMoney"), r = parseFloat(e.text()) - t.pourTotal, e.text(r), t.isssueInfoList[o] = {
						money: parseFloat(n.text())
					}))
				}), $(".id,.num,.date", ".goNum_confirm_bg .table-content").unbind("click").click(function () {
					$(this).parent().find(".isGo").click()
				}), $(".goNum_confirm_bg .isGo").unbind("change").change(function () {
					var e = $(this),
						n = e.parent().parent(),
						i = parseFloat(n.find(".moneyValue").text()),
						o = parseFloat($(".goNum_confirm_bg .totalMoney").text()),
						r = parseFloat($(".goNum_confirm_bg .qi").text()),
						a = e.attr("ids");
					e.is(":checked") ? (t.isssueInfoList[a] = {
						money: i
					}, $(".goNum_confirm_bg .qi").text(r + 1), $(".goNum_confirm_bg .totalMoney").text(o + i), n.addClass("selected")) : (delete t.isssueInfoList[a], $(".goNum_confirm_bg .qi").text(r - 1), $(".goNum_confirm_bg .totalMoney").text(o - i), n.removeClass("selected"))
				}), $(".goNum_confirm_bg .ok_close_box").unbind("click").click(function () {
					var e = parseInt($(".goNum_confirm_bg .totalMoney").text()),
						n = parseFloat($(".goNum_confirm_bg .qi").text());
					return 0 == e ? void gameCommon.dialogBoxTip("Silakan centang nomor pelacakan") : t.userbalance - e < 0 ? void gameCommon.dialogBox("Bạn không đủ số dư, Vui lòng kiểm tra lại số theo dõi") : void t.sureBetInfoPanel(e, function () {
						gameCommon.goBetApi(t, e, $(".isStopGO input").is(":checked")), layer.close(i)
					}, n)
				}), $(".goNum_confirm_bg .cancel_close_box").unbind("click").click(function () {
					layer.close(i)
				})
			}
		},
		setRecordListPanel: function (e, n) {
			var i = "";
			$.each(e, function (t, e) {
				i += '<tr><td class="date" >' + e.finishtime.substring(0, 10) + '</td><td  class="state state_' + e.status + '">' + e.statusname + '</td><td  class="betMoney">' + gameCommon.moneyFormat(e.amount) + '</td><td class="getMoney">' + gameCommon.moneyFormat(e.bonus) + '</td><td><div class="see btn" title="Nhấn vào đây để xem chi tiết đặt cược" sid="' + e.id + '" ></div></td></tr>'
			}), $(".game_record_panel .game_record_table").html(i), $(".game_record_table .see").unbind("click").on("click", function () {
				gameCommon.getBetDetailApi(t, $(this).attr("sid"))
			}), $(".game_record_container").jScrollPane();
			var o = setTimeout(function () {
				gameCommon.getBetHistoryApi(t)
			}, 1e3 * n);
			t.setFreshBetRecordInterval(o)
		},
		setBetInfoPanel: function (e) {
			var n = e.betinfo,
				i = "",
				o = [];
			("" != n.winnumber || n.winnumber != null) && (i = gameCommon.winNumberFormatList(n.winnumber), o = [], $.each(i, function (t, e) {
				var n = (0 != t ? "num " : "num te ") + e.type;
				o.push('<div class="' + n + '" >' + e.num + "</div>")
			})), o = o.join("");
			var r = '<div class="game_record_messageBox_body"><div class="title">Xem chi tiết cá cược</div><table ><tr><td class="label" >Tên đăng nhập</td><td>' + e.curloginname + '</td><td class="label" >Kết quả</td><td class="state_' + n.status + '" >' + n.statusname + '</td></tr><tr><td class="label" >Mã số cược</td><td>' + n.id + '</td><td class="label" >Thời gian</td><td>' + n.bettime + '</td></tr><tr><td class="label" >Kiểu chơi</td><td>12 Con Giáp</td><td class="label" >Kỳ mở thưởng</td><td>1 giây</td></tr><tr><td class="label label_large" >Chi tiết cược</td><td colspan="3"  ><div class="label_large horizontal-only" id="game_record_info" >' + n.codes + '</div></td></tr><tr><td class="label" >Số tiền cược</td><td>' + gameCommon.moneyFormat(n.amount) + ' VNĐ</td><td class="label" >Kết quả mở thưởng</td><td>' + o + '</td></tr><tr><td class="label" >Số lần trúng thưởng</td><td>' + n.wintimes + '</td><td class="label" >Thưởng</td><td>' + gameCommon.moneyFormat(n.bonus) + ' VNĐ</td></tr></table><div class="option"><div class="cancel_btn ' + (0 != n.status ? "hide" : "") + '"></div><div class="close_btn btn"></div></div></div>',
				a = layer.open({
					type: 1,
					shadeClose: !1,
					title: !1,
					closeBtn: 0,
					maxWidth: 606,
					skin: "game_record_messageBox",
					content: r
				});
			$("#game_record_info").jScrollPane(), $(".game_record_messageBox_body .cancel_btn").unbind("click").click(function () {
				layer.close(a), gameCommon.cancelBetApi(t, n.id, function () {
					gameCommon.getBetHistoryApi(t)
				})
			}), $(".game_record_messageBox_body .close_btn").unbind("click").click(function () {
				layer.close(a)
			})
		},
		showHistoryPanel: function (t) {
			if (1 == gameCommon.animationSwitch) {
				var e = "";
				$.each(t, function (t, n) {
					e += '<tr><td class="isssue">' + n.issue + '</td><td align="center" class="num" ><div class="hao ' + n.list[0].type + '" >' + n.list[0].num + '</div></td><td align="center" class="num" ><div class="hao ' + n.list[1].type + '" >' + n.list[1].num + '</div></td><td align="center" class="num" ><div class="hao ' + n.list[2].type + '" >' + n.list[2].num + '</div></td><td align="center" class="num" ><div class="hao ' + n.list[3].type + '" >' + n.list[3].num + '</div></td><td align="center" class="num" ><div class="hao ' + n.list[4].type + '" >' + n.list[4].num + '</div></td><td align="center" class="num" ><div class="hao ' + n.list[5].type + '" >' + n.list[5].num + '</div></td><td align="center" class="num" ><div class="hao ' + n.list[6].type + '" >' + n.list[6].num + '</div></td><td align="center" class="date">' + n.openTime + "</td>"
				});
				var n = '<div ><div class="title">Chi tiết xổ số</div><table class="content"  ><tr><td class="isssue th">Số thứ tự</td><td class="num th">ĐB</td><td class="num th">1</td><td class="num th">2</td><td class="num th">3</td><td class="num th">4</td><td class="num th">5</td><td class="num th">6</td><td class="date_th th">Thời gian xổ số</td></tr></table><div class="horizontal-only" id="go_info_panel" ><table class="content table-content"  >' + e + '<table/></div><div class="option"><div class="ok_close_box btn" ></div></div></div>',
					i = layer.open({
						type: 1,
						shadeClose: !1,
						title: !1,
						closeBtn: 0,
						maxWidth: 606,
						skin: "record_history_dialogBox",
						content: n
					});
				$("#go_info_panel").jScrollPane(), $(".record_history_dialogBox .ok_close_box").unbind("click").click(function () {
					layer.close(i)
				})
			}
		},
		setOpenInfoPanel: function (e) {
			var n = "";
			$.each(e, function (t, e) {
				n += '<tr><td class="number"></td><td><div class="' + e.list[0].type + '" >' + e.list[0].num + '</div></td><td><div class="' + e.list[1].type + '" >' + e.list[1].num + '</div></td><td><div class="' + e.list[2].type + '" >' + e.list[2].num + '</div></td><td><div class="' + e.list[3].type + '" >' + e.list[3].num + '</div></td><td><div class="' + e.list[4].type + '" >' + e.list[4].num + '</div></td><td><div class="' + e.list[5].type + '" >' + e.list[5].num + '</div></td><td ><div class="' + e.list[6].type + '" >' + e.list[6].num + '</div></td><td class="date">1 giây - ' + e.openTime + "</td>"
			}),
				$(".game_new_open_panel .game_new_open_table").html(n);
			// var i = setTimeout(function () {
			// 	gameCommon.getHistoryApi(t, 10, t.setOpenInfoPanel)
			// }, 6e4);
			// t.setFreshOpenHistoryInterval(i)
		},
		freshAllPanel: function () {
			var e = "none" != $(".game_record_panel").css("display"),
				n = "none" != $(".game_new_open_panel").css("display");
			e && gameCommon.getBetHistoryApi(t), n && gameCommon.getHistoryApi(t, 10, t.setOpenInfoPanel), $("#sureBetInfo").trigger("refresh")
		},
		sureBetInfoPanel: function (e, n, i) {
			var o = void 0 != i,
				r = o ? "nuôi" + i : t.issue,
				a = o ? "" : "hide",
				l = "Xác nhận cược " + r,
				s = "";
			$.each(t.betCellTotal, function (t, e) {
				var newMoney = gameCommon.moneyFormat(e.money);
				s += isNaN(t) ? "<p>【" + e.text + "】Số tiền：" + newMoney + " VNĐ </p>" : "<p>【" + e.text + "】Lựa chọn：Số " + t + ", Số tiền: " + newMoney + " VNĐ"
			});
			var d = '<div ><div class="title">' + l + '</div></div><table ><tr><td class="label " >Chi tiết cược</td><td class="content" ><div class="horizontal-only" id="game_record_info" >' + s + '</div></td></tr></table><div class="go_content" ><div style="display: inline-block"  class="' + a + '">Jumlah total<span class="qi">' + i + '</span>Số thứ tự，</div>Tống số tiền cược: <span class="totalMoney" >' + gameCommon.moneyFormat(e) + '</span><span>VNĐ</span></div ><div class="option"><div class="ok_close_box btn" ></div><div class="cancel_close_box btn"></div></div></div>',
				c = layer.open({
					type: 1,
					id: "sureBetInfo",
					shadeClose: !1,
					title: !1,
					closeBtn: 0,
					maxWidth: 606,
					skin: "bet_info_bg",
					content: d
				});
			$("#game_record_info").jScrollPane(), $("#Kiểu chơi").unbind("refresh").on("refresh", function () {
				o || (
					r = o ? "nuôi" + i : t.issue,
					l = "Xác nhận cược " + r,
					$(".title", this).html(l)
				)
			}),
				$(".bet_info_bg .ok_close_box").unbind("click").click(function () {
					n(), layer.close(c)
				}),
				$(".bet_info_bg .cancel_close_box").unbind("click").click(function () {
					$(".number_box", ".game_record_time").show(),
						$(".record_number_loading", ".game_record_time").hide(),
						layer.close(c)
				})
		}
	};
	$("#system_menu").sysNav(), gameCommon.initApi(t, function () {
		t.setBalance(t.userbalance), t.setPourTime(), t.initTm1(), t.initTm2(), t.initZm(), t.initEvent()
	})
}), jQuery.fn.extend({
	sysNav: function () {
		function t(t) {
			var n = $('<table menu="' + t.aType + '" class="menu_table" />');
			return $.each(t.items, function (t, i) {
				var o = $("<tr />"),
					r = "",
					a = $("<td />");
				r = i.hasOwnProperty("icon") ? '<td class="title"><div class="icon icon4"></div>' + i.title + "</td>" : '<td class="title">' + i.title + "</td>", o.append(r), $.each(i.actions, function (t, n) {
					a.append(e(n))
				}), o.append(a), n.append(o)
			}), n
		}
		function e(t) {
			var e = $('<ul class="address_container" />');
			return $.each(t, function (t, n) {
				var i = $('<li><a style="' + n.style + '" onclick="' + n.onclick + '" id="' + n.id + '">' + n.label + "</a></li>");
				e.append(i)
			}), e
		}
		function n(t) {
			var e = $('<div class="menu_action" />'),
				n = $('<div class="title">' + t.label + "</div>"),
				i = $('<div class="action_container" />');
			return $.each(t.actions, function (t, e) {
				i.append($('<div class="action_item"><a href="javascript:void(0);" onclick="' + e.onclick + '" id="' + e.id + '"><div class="icon ' + e.icon + '"></div>' + e.label + "</a></div>"));
			}), e.append(n), e.append(i), e
		}
		var i = this,
			o = sysNavConfig.main;
		i.addClass("system_menu");
		var r = '<div class="open_btn system_menu_item" data-label="' + o.label + '"><div class="menu_item ' + o.icon + '"></div></div>',
			a = $('<div class="mfb-zoomin-expend"/>'),
			l = $('<div class="menu_big_info"/>');
		$.each(o.leaf, function (e, n) {
			var i = "";
			n.hasOwnProperty("href") ? i = '<div leaf=false class="system_menu_item system_menu_children" data-label="' + n.label + '"><a href="' + n.href + '"><div class="menu_item ' + n.icon + '" /></a></div>' : (i = "<div index=" + e + ' atype="' + n.aType + '" leaf=true class="system_menu_item system_menu_children" data-label="' + n.label + '"><div class="menu_item ' + n.icon + '" /></div>', l.append(t(n))), a.append($(i))
		}), i.append($(r)), i.append(a), l.append(n(sysNavConfig.fastLink)), i.append(l), $("[leaf=true]", i).on("mouseenter", function () {
			var t = $(this).attr("atype"),
				e = parseInt($(this).attr("index")) + 1,
				n = $(".menu_big_info", i);
			n.prependTo($(this)), n.css("top", -61 * e + "px"), $(".menu_big_info", i).show("slow"), $("[menu]", i).hide(), $("[menu=" + t + "]", i).show()
		}), $(".system_menu_item", i).on("mouseenter", function () {
			$(".menu_item", this).addClass("hover"), $(this).siblings().each(function () {
				$(".menu_item", this).removeClass("hover")
			})
		}), $("[leaf=false]", i).on("mouseenter", function () {
			$(".menu_big_info", i).hide({
				duration: "slow",
				easing: "linear"
			})
		}), $(".open_btn", i).on("mouseenter", function () {
			$(".menu_big_info", i).hide({
				duration: "slow",
				easing: "linear"
			})
		})
	}
});
