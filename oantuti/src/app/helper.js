function toQueryPair(key, value) {
    if (typeof value === 'undefined') {
        return key;
    }
    return key + '=' + encodeURIComponent(value === null ? '' : String(value));
}

function toBodyString(obj) {
    let ret = [];
    for (let key in obj) {
        let values = obj[key];
        if (values && values.constructor === Array) {
            let queryValues = [];
            for (let i = 0, len = values.length, value; i < len; i++) {
                value = values[i];
                queryValues.push(toQueryPair(key + "[]", value));
            }
            ret = ret.concat(queryValues);
        } else if (values && values.constructor === Object) {
            for (let keySub in values) {
                ret.push(toQueryPair(key + "[" + keySub + "]", values[keySub]));
            }
        } else {
            ret.push(toQueryPair(key, values));
        }
    }
    return ret.join('&');
}

export function requestHTTP(data, url) {
    var data = data || {},
        urlAPI = url || '',
        promise;

    promise = new Promise(function (resolve, reject) {
        let bodyParam = toBodyString(data),
            xhr = new window.XMLHttpRequest();

        xhr.open('POST', urlAPI, true);
        xhr.timeout = 10000; // time in milliseconds
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4) {
                if (xhr.status === 200) {
                    resolve(JSON.parse(xhr.responseText));
                } else {
                    reject(xhr);
                }
            }
        };
        xhr.send(bodyParam);
    });
    return promise;
}

export function fireEvent(el, EventName) {
    if (el != null) {
        if (el.fireEvent) {
            el.fireEvent('on' + EventName);
        } else {
            let evObj = document.createEvent('Events');
            evObj.initEvent(EventName, true, false);
            el.dispatchEvent(evObj);
        }
    }
}

export function showSuccessAlert(data, timeHide, funCallback) {
    var timeHide = timeHide || 1500,
        funCallback = funCallback || [];
    if (data) {
        parent.layer.msg(data, {
            shade: 0.3,
            closeBtn: false,
            icon: 1, // 1: icon success, 2: icon error, 3: warning question, 4: lock, 5: face sad, 6: face funny, 7: warning
            time: timeHide
        }, function () {
            for (var i = 0, len = funCallback.length; i < len; i++) {
                funCallback[i] && funCallback[i]();
            }
            //funCallback && funCallback();
        });
    }
}

export function showErrorAlert(data, timeHide, funCallback) {
    var timeHide = timeHide || 1500;
    if (data) {
        parent.layer.msg(data, {
            shade: 0.3,
            closeBtn: false,
            icon: 2, // 1: icon success, 2: icon error, 3: warning question, 4: lock, 5: face sad, 6: face funny, 7: warning
            time: timeHide
        }, function () {
            if (typeof funCallback === "function") funCallback();
        });
    }
}

export function getExaclyEl(el, clsEl) {
    let targetEl = null;
    if (el.classList.contains(clsEl)) {
        targetEl = el;
    } else {
        targetEl = el.closest('.' + clsEl);
    }
    return targetEl;
}

export function isEmptyObject(obj) {
    for (let key in obj) {
        if (obj.hasOwnProperty(key))
            return false;
    }
    return true;
}

export function removeAllEls(els) {
    for (let i = 0; i < els.length; i++) {
        els[i].remove();
    }
}

export function removeClassesAllEls(els, cls) {
    for (let i = 0; i < els.length; i++) {
        els[i].classList.remove(cls);
    }
}

export function addClassesAllEls(els, cls) {
    for (let i = 0; i < els.length; i++) {
        els[i].classList.add(cls);
    }
}

export function addEventEls(eventName, els, clsItem, funCallBack) {
    for (let i = 0; i < els.length; i++) {
        let el = els[i];
        el.removeEventListener(eventName, eventAction);
        el.addEventListener(eventName, eventAction);
        function eventAction(e) {
            e.preventDefault();
            let targetEl = getExaclyEl(e.target, clsItem);
            funCallBack && funCallBack(targetEl, i);
        }
    }
}

/**
 * 0:bau, 1:cua, 2:tom, 3:ca, 4:nai, 5:ga
 * @param valPosi => if null => constructor position, if !null => string 1|5|3
 */
export function randomPosiOverlapping(valPosi) {
    let arrValColor = [];
    const posiItem = [
        [{ x: 100, y: 45 }, { x: 150, y: 8 }, { x: 40, y: 26 }],
        [{ x: 85, y: 0 }, { x: 150, y: 8 }, { x: 7, y: 18 }],
        [{ x: 85, y: 0 }, { x: 150, y: 8 }, { x: 116, y: 44 }],
        [{ x: 13, y: 25 }, { x: 150, y: 8 }, { x: 116, y: 44 }],
        [{ x: 11, y: 11 }, { x: 92, y: 0 }, { x: 116, y: 44 }],
        [{ x: 150, y: 34 }, { x: 92, y: 32 }, { x: 30, y: 30 }],
    ];

    if (valPosi) {
        arrValColor = valPosi;
    } else {
        for (let i = 0; i < 3; i++) {
            arrValColor.push(getRandomInt(1, 6));
        }
    }

    const objRandom = posiItem[getRandomInt(0, posiItem.length - 1)];
    return {
        itemRandom: arrValColor,
        posiRandom: objRandom
    };
}

export function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

export function getPositionXYEl(el) {
    let getRectEl = el.getBoundingClientRect();
    return { x: getRectEl.left, y: getRectEl.top, w: getRectEl.width, h: getRectEl.height };
}

export function removeClassesEffectTimeout(els, clsBefore, clsAfter, timeoutEffect, funCallBack) {
    for (let i = 0; i < els.length; i++) {
        if (els[i].classList.contains(clsBefore)) {
            els[i].classList.remove(clsBefore);
            els[i].classList.add(clsAfter);
            setTimeout(() => {
                els[i].classList.remove(clsAfter);
                funCallBack && funCallBack();
            }, timeoutEffect);
            break;
        }
    }
}

export function numberWithCommas(x) {
    const num = Number(x).toFixed(0);
    return num.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

export function loading(flagRemove, el) {
    const getElLoading = el.querySelector('.lds-spinner');
    if (flagRemove && el) {
        getElLoading.remove();
        return;
    }
    if (getElLoading) return;
    const elLoading = document.createElement('div');
    elLoading.classList.add('lds-spinner');
    elLoading.innerHTML = '<div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div>';
    el.appendChild(elLoading);
}

/**
 * @param dateStr = "2019-30-06 10:25:00"
 * @returns {Date}
 */
export function formatToDate(dateStr) {
    return new Date(dateStr.replace(/-/g, "/"));
}

export function timerCountDown(start, end, item, finished, funCountDown) {
    var timeLeave = 0, timerno;

    if (start == "" || end == "") {
        timeLeave = 0;
    } else {
        timeLeave = (formatToDate(end).getTime() - formatToDate(start).getTime()) / 1000;
    }

    timerno = setInterval(function () {
        if (timeLeave <= 0) {
            clearInterval(timerno);
            finished();
        }
        var oDate = diff(timeLeave--),
            getDate = oDate.day > 0 ? (oDate.day + ' ngày ') : '',
            getHour = (fftime(oDate.hour) + "").substr(0, 1) + (fftime(oDate.hour) + "").substr(1),
            getMinute = (fftime(oDate.minute) + "").substr(0, 1) + (fftime(oDate.minute) + "").substr(1),
            getSecond = (fftime(oDate.second) + "").substr(0, 1) + (fftime(oDate.second) + "").substr(1),
            totalHour = oDate.day > 0 ? (Number(oDate.day) * 24 + Number(getHour)) : getHour;
        funCountDown && funCountDown({ h: totalHour, m: getMinute, s: getSecond });
        item.html("<span><b>" + totalHour + "</b></span>" + ":" + "<span><b>" + getMinute + "</b></span>" + ":" + "<span><b>" + getSecond + "</b></span>");
    }, 1000);

    function fftime(n) {
        return Number(n) < 10 ? "" + 0 + Number(n) : Number(n);
    }
    function diff(t) {
        return t > 0 ? {
            day: Math.floor(t / 86400),
            hour: Math.floor(t % 86400 / 3600),
            minute: Math.floor(t % 3600 / 60),
            second: Math.floor(t % 60)
        } : {
                day: 0,
                hour: 0,
                minute: 0,
                second: 0
            }
    }
}

Object.size = function (obj) {
    let size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};

Array.clean = function (deleteValue) {
    for (let i = 0; i < this.length; i++) {
        if (this[i] === deleteValue) {
            this.splice(i, 1);
            i--;
        }
    }
    return this;
};