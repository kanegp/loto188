import '../scss/style.scss';
import Swiper from 'swiper';
import * as helper from './helper';

function DATA_ITEM_BETTING() {
    this.totalMoney = 0;
    this.itemBetting = '';
    this.chips = [];
}

function DATA_SUBMIT_BETTING() {
    this.flag = 'Bet';
    this.gameid = null;
    this.money = 0;
    this.betdata = {};
}

function OANTUTI() {
    this.LOTTERY_SERVICE = "/LotteryService.aspx";
    this.OANTUTI_SERVICE = "/OanTuTiService.aspx";
    this.els = {
        elUserPlayLeft: document.querySelector('.user-play-left'),
        elUserPlayRight: document.querySelector('.user-play-right'),
        elsUserGirlWin: document.querySelectorAll('.user-girl-win'),
        elsUserBoyWin: document.querySelectorAll('.user-boy-win'),
        elViewHisCurrentBet: document.querySelector('.view-his-current'),
        elChipsBetting: document.getElementById('chipsBetting'),
        elAreaChips: document.querySelector('.area-chips'),
        elContainer: document.getElementById('container'),
        elMoneyBet: document.querySelector('.total-money .money-bet'),
        elActionPlay: document.getElementById('actionPlay'),
        elsUserHandLeft: document.getElementById('hand-left'),
        elsUserHandRight: document.getElementById('hand-right'),
        elBalance: document.getElementById('balance'),
        elMainBalance: document.getElementById('mainBalance'),
        elHistoryResult: document.getElementById('history-result'),
        elMoneyStatuBetEffect: document.getElementById('effectMoneyBet'),
        elRebet: document.getElementById('rebet'),
        elClearBet: document.getElementById('clearBet'),
        elInforQuestion: document.getElementById('infor-question'),
        elTblHistory: document.getElementById('tbl-history'),
        elToggleAudio: document.getElementById('toggleAudio'),
        elContentPopupHis: document.getElementById('historyBetUser'),
    };
    this.numChipShow = 5;
    this.objChips = [
        { chipVal: "5", status: 1 },
        { chipVal: "10", status: 0 },
        { chipVal: "25", status: 0 },
        { chipVal: "50", status: 0 },
        { chipVal: "100", status: 0 },
        { chipVal: "200", status: 0 },
        { chipVal: "300", status: 0 },
        { chipVal: "400", status: 0 },
        { chipVal: "500", status: 0 },
        { chipVal: "custom", status: 0 }
    ];
    this.objChipActive = { chipVal: "", idx: 0, chipPosi: {} };
    this.model = [
        { typebet: 'user-left-keo', userWin: 'result-user1-keo', handWin: 'hand-result-left-keo', maxbet: 5000000, levelnum: 1, ratio: 0.98, key: 'left' },
        { typebet: 'user-left-bua', userWin: 'result-user1-bua', handWin: 'hand-result-left-bua', maxbet: 5000000, levelnum: 1, ratio: 0.98, key: 'left' },
        { typebet: 'user-left-nap', userWin: 'result-user1-nap', handWin: 'hand-result-left-nap', maxbet: 5000000, levelnum: 1, ratio: 0.98, key: 'left' },
        { typebet: 'user-right-keo', userWin: 'result-user2-keo', handWin: 'hand-result-right-keo', maxbet: 5000000, levelnum: 1, ratio: 0.98, key: 'right' },
        { typebet: 'user-right-bua', userWin: 'result-user2-bua', handWin: 'hand-result-right-bua', maxbet: 5000000, levelnum: 1, ratio: 0.98, key: 'right' },
        { typebet: 'user-right-nap', userWin: 'result-user2-nap', handWin: 'hand-result-right-nap', maxbet: 5000000, levelnum: 1, ratio: 0.98, key: 'right' },
        { typebet: 'user-left-win', userWin: 'result-user1-win', handWin: '', maxbet: 5000000, levelnum: 1, ratio: 0.98 },
        { typebet: 'user-right-win', userWin: 'result-user2-win', handWin: '', maxbet: 5000000, levelnum: 1, ratio: 0.98 },
        { typebet: 'user-hoa', userWin: 'result-hoa', handWin: '', maxbet: 5000000, levelnum: 1, ratio: 0.98 }
    ];

    this.statusOpenWin = {
        0: 'Chưa mở thưởng',
        1: 'Đang chờ mở thưởng',
        2: 'Đang chờ tính thưởng',
        3: 'Trượt',
        4: 'Trúng',
        5: 'Hủy nuôi cược',
        6: 'Cá nhân hủy cược',
        7: 'Hủy mở thưởng'
    };

    this.storeDataBetting = [];
    this.maxGroupChip = 5;
    this.dataTypeBet = new DATA_ITEM_BETTING();

    //START audio
    const audioChip = require("../audio/chip.wav");
    const audioClick = require("../audio/click.wav");
    const audioBackgroud = require("../audio/background.mp3");
    const audioOpen = require("../audio/open.wav");
    const audioPlay = require("../audio/play.wav");
    this.objAudioChip = new Audio(audioChip);
    this.objAudioClick = new Audio(audioClick);
    this.objAudioBackground = new Audio(audioBackgroud);
    this.objAudioOpen = new Audio(audioOpen);
    this.objAudioPlay = new Audio(audioPlay);
    //END audio

    this.flagPlayAudioBackground = false;
    this.gameID = null;
    this.storeBalance = 0;
    this.intervalTimeCheckBalance = null;
    this.countRequestBalance = 0;
    this.maxCountRequestBalance = 5;
    this.swiperHis = null;
    this.colHisShow = 8;
    this.timeEndMoveChipWin = 0;
    this.timeEndMoveChipLoss = 0;
    this.storeDataReBet = [];
}

OANTUTI.prototype.init = function () {
    document.addEventListener('click', (event) => {
        if (this.flagPlayAudioBackground) return false;
        this.flagPlayAudioBackground = true;
        this.objAudioBackground.play();
        this.objAudioBackground.volume = 0.15;
        this.objAudioBackground.loop = true;
    });
    this.renderChips();
    this.randomFigure();
    this.chooseItemBetting();

    this.apiGetBalance(data => {
        this.els.elBalance.innerHTML = helper.numberWithCommas(this.storeBalance);
    });
    this.intervalTimeCheckBalance = setInterval(() => {
        this.apiGetBalance(data => {
            this.els.elBalance.innerHTML = helper.numberWithCommas(this.storeBalance);
        });
    }, 10000);

    this.apiInit();

    this.els.elViewHisCurrentBet.addEventListener('click', this.toggerViewHisCurrent.bind(this));
    this.els.elActionPlay.addEventListener('click', this.actionPlay.bind(this));
    this.els.elRebet.addEventListener('click', this.reBetting.bind(this));
    this.els.elClearBet.addEventListener('click', this.resetGame.bind(this));
    this.els.elInforQuestion.addEventListener('click', this.popupInforQuestion.bind(this));
    this.els.elTblHistory.addEventListener('click', this.popupHistory.bind(this));
    this.els.elToggleAudio.addEventListener('click', this.toggleAudio.bind(this));
    document.querySelector('[data-chipkey="chip-custom"] .icon-edit').addEventListener('click', this.editCustomChip.bind(this));
};

OANTUTI.prototype.editCustomChip = function (event) {
    this.objAudioClick.play();
    if (event) event.preventDefault();
    const elEdit = helper.getExaclyEl(event.target, 'icon-edit');
    elEdit.style.display = 'none';
    const elRoot = elEdit.parentElement;
    const elInput = document.createElement('div');
    elInput.classList.add('chip-custom-input');
    elInput.innerHTML = '<div class="wrap-custom-chip"><input type="text" placeholder="Ít nhất 1,000 vnđ" /><div class="btnsChip"><a id="addChipCustom" href="javascript:void(0);">Thêm</a><a id="cancelChipCustom" href="javascript:void(0);">Hủy</a></div></div>';
    elRoot.appendChild(elInput);
    elRoot.querySelector('input').focus();

    elRoot.querySelector('#addChipCustom').addEventListener('click', this.addChipCustom.bind(this));
    elRoot.querySelector('#cancelChipCustom').addEventListener('click', this.cancelChipCustom.bind(this));
    document.querySelector('.wrap-custom-chip input').addEventListener('keyup', function (e) {
        e.preventDefault();
        const valIput = e.target.value.replace(/[,]/g, '');
        e.target.value = helper.numberWithCommas(Number(valIput));
    });
};

OANTUTI.prototype.addChipCustom = function (event) {
    this.objAudioClick.play();
    if (event) event.stopPropagation();
    const elInputMoney = document.querySelector('.wrap-custom-chip input');

    if (elInputMoney) {
        const val = elInputMoney.value.trim();
        if (val === '' || val === '' || /[^0-9,]/g.test(val)) {
            helper.showErrorAlert('Số tiền không hợp lệ!', 2000, function () { });
            return false;
        }
        const getValMoney = Number(val.replace(/[,]/g, '')) / 1000;
        if (getValMoney < 1) {
            helper.showErrorAlert('Số tiền ít nhất 1,000 vnđ!', 2000, function () { });
            return false;
        }
        if (getValMoney > 50000) {
            helper.showErrorAlert('Số tiền nhiều nhất 50,000,000 vnđ!', 2000, function () { });
            return false;
        }
        const checkChipDublicate = this.objChips.find(itemChip => Number(itemChip['chipVal']) === getValMoney);
        if (checkChipDublicate) {
            helper.showErrorAlert('Chip đã tồn tại!', 2000, function () { });
            return false;
        }

        const convertVal = Number(val.replace(/[,]/g, ''));
        const objMoneyAdd = this.convertMoneyStr(convertVal);
        if (objMoneyAdd.str === 'error') {
            helper.showErrorAlert('<p>Số tiền phải là <strong>bội số</strong> cùa <strong>100,000</strong></p><p>VD: 800000 = 800, 1100000 = 1M1, 2200000 = 2M25</p>', 3000, function () { });
            return false;
        }
        const elChipCustom = document.getElementById('chip-custom');
        if (!elChipCustom.querySelector('.chip-highlight')) {
            const elHightLight = document.createElement('span');
            elHightLight.classList.add('chip-highlight', 'icon');
            elChipCustom.appendChild(elHightLight);
        }

        if (elChipCustom.classList.contains('active')) {
            this.objChipActive.chipVal = objMoneyAdd.money;
        }

        elChipCustom.classList.remove('chip-custom');
        elChipCustom.classList.add('chip-custom-icon');
        elChipCustom.querySelector('.chip-custom').style.fontSize = '16px';
        elChipCustom.querySelector('.chip-custom').innerHTML = objMoneyAdd.str;
        elChipCustom.dataset.chipkey = `chip-${objMoneyAdd.money}`;
        this.objChips = this.objChips.map(item => {
            if (item['chipVal'] === 'custom') {
                item['chipVal'] = objMoneyAdd.money.toString();
            }
            return item;
        });
        this.cancelChipCustom();
    } else {
        helper.showErrorAlert('Element không tồn tại!', 2000, function () { });
    }
};

/**
 * @param money = type number
 * @returns {*}
 */
OANTUTI.prototype.convertMoneyStr = function (money) {
    let objMoneyAdd = { str: '', money: 0 };
    const chipExist = this.objChips.find(item => Number(item['chipVal']) * 1000 === money);
    if (!chipExist && money % 100000 > 0) {
        objMoneyAdd = { str: 'error', money: 0 };
    } else if (money < 1000000) {
        objMoneyAdd.str = (money / 1000).toString();
        objMoneyAdd.money = money / 1000;
    } else {
        const convertMilion = (money / 1000000).toString().split('.');
        if (convertMilion.length === 2 && convertMilion[1].length <= 1) {
            objMoneyAdd.str = convertMilion.join('M');
            objMoneyAdd.money = money / 1000;
        } else if (convertMilion.length === 1) {
            objMoneyAdd.str = convertMilion.toString() + 'M';
            objMoneyAdd.money = money / 1000;
        } else {
            objMoneyAdd = { str: 'error', money: 0 };
        }
    }

    return objMoneyAdd;
};

OANTUTI.prototype.cancelChipCustom = function (event) {
    if (event) {
        event.stopPropagation();
        this.objAudioClick.play();
    }
    document.querySelector('#chip-custom .icon-edit').style.display = 'block';
    document.querySelector('.chip-custom-input').remove();
};

OANTUTI.prototype.popupHistory = function (event) {
    this.objAudioClick.play();
    if (event) event.preventDefault();
    const params = { flag: 'Init' };
    helper.requestHTTP(params, this.OANTUTI_SERVICE).then(data => {
        if (data['msg'] === '') {
            this.renderHisPopup(data['history']);
        }
    });
};

/**
 * @param dataHis: [{betcontent: "flag=baucua1sBet&gameid=92...", bettime: "2019-06-02 07:18:22", codes: "[ga]Bet:5,000&&[tom]Bet:5,000...", status: 4, winnumber: "6|4|6", id: "B190605165553100035"}]
 */
OANTUTI.prototype.renderHisPopup = function (dataHis) {
    let convertJson = JSON.parse(dataHis);
    let lenHisApi = convertJson.length;
    let strHis = '<table>';
    if (lenHisApi > 0) {
        for (let i = 0; i < lenHisApi; i++) {
            let item = convertJson[i];
            let arrWin = [];
            let strVi = '';
            arrWin = item['winnumber'].split('|');
            if (arrWin && arrWin.length > 0) {
                for (let j = 0; j < arrWin.length; j++) {
                    const itemMapWin = this.model.find(itemMap => itemMap['typebetMap'] === arrWin[j]);
                    strVi += `<span class="icon ${itemMapWin['typebet']}"></span>`;
                }
            } else {
                strVi = '?';
            }
            strHis += `<tr><td>${item["id"]}</td><td>${item["bettime"]}</td><td>${this.formatBetContent(item['codes'], item['betcontent'])}</td><td><div class="typebet-results">${strVi}</div></td><td class="status_${item["status"]}">${this.statusOpenWin[item["status"]]}</td></tr>`;
        }
    } else {
        strHis += '<tr><td style="text-align:center;">Không có dữ liệu!</td></tr></table>';
    }
    this.els.elContentPopupHis.querySelector('.content').innerHTML = strHis;

    layer.open({
        area: ['929px', '450px'],
        skin: 'inforQuestion',
        type: 1,
        content: $('#historyBetUser'),
        shade: 0.7,
        shadeClose: true,
        scrollbar: true,
        yes: function (index, layero) { },
        cancel: (index, layero) => {
            this.objAudioClick.play();
        },
    });
};

OANTUTI.prototype.formatBetContent = function (codes, betContent) {
    // temp for test
    if (codes === undefined) return '';
    let arrCodes = codes.split('&');
    let strBetContent = betContent.split('&');
    let str = '';
    for (let j = 0; j < arrCodes.length; j++) {
        if (arrCodes[j].trim() === '') continue;
        let itemCode = arrCodes[j].toLowerCase();
        let getPrize = itemCode.split(':');
        let prize = '';
        if (this.gameID === 92) {
            prize = 0;
        }
        if (itemCode.search('prize') >= 0) prize = getPrize[getPrize.length - 1];
        if (itemCode.indexOf('[sciss_left]') >= 0) {
            str += `<span class="detailBetUser"><span>Kéo trái: ${helper.numberWithCommas(getVal(strBetContent, 'sciss_left'))}</span><span class="winMoney">${prize}</span></span>`;
        } else if (itemCode.indexOf('[rock_left]') >= 0) {
            str += `<span class="detailBetUser"><span>Búa trái: ${helper.numberWithCommas(getVal(strBetContent, 'rock_left'))}</span><span class="winMoney">${prize}</span></span>`;
        } else if (itemCode.indexOf('[paper_left]') >= 0) {
            str += `<span class="detailBetUser"><span>Bao trái: ${helper.numberWithCommas(getVal(strBetContent, 'paper_left'))}</span><span class="winMoney">${prize}</span></span>`;
        } else if (itemCode.indexOf('[sciss_right]') >= 0) {
            str += `<span class="detailBetUser"><span>Kéo phải: ${helper.numberWithCommas(getVal(strBetContent, 'sciss_right'))}</span><span class="winMoney">${prize}</span></span>`;
        } else if (itemCode.indexOf('[rock_right]') >= 0) {
            str += `<span class="detailBetUser"><span>Búa phải: ${helper.numberWithCommas(getVal(strBetContent, 'rock_right'))}</span><span class="winMoney">${prize}</span></span>`;
        } else if (itemCode.indexOf('[paper_right]') >= 0) {
            str += `<span class="detailBetUser"><span>Bao phải: ${helper.numberWithCommas(getVal(strBetContent, 'paper_right'))}</span><span class="winMoney">${prize}</span></span>`;
        } else if (itemCode.indexOf('[draw]') >= 0) {
            str += `<span class="detailBetUser"><span>Hòa: ${helper.numberWithCommas(getVal(strBetContent, 'draw'))}</span><span class="winMoney">${prize}</span></span>`;
        } else if (itemCode.indexOf('[left_win]') >= 0) {
            str += `<span class="detailBetUser"><span>Trái Thắng: ${helper.numberWithCommas(getVal(strBetContent, 'left_win'))}</span><span class="winMoney">${prize}</span></span>`;
        } else if (itemCode.indexOf('[right_win]') >= 0) {
            str += `<span class="detailBetUser"><span>Phải Thắng: ${helper.numberWithCommas(getVal(strBetContent, 'right_win'))}</span><span class="winMoney">${prize}</span></span>`;
        }
    }
    return str;

    function getVal(strBetContent, type) {
        let value = 0;
        for (let i = 0; i < strBetContent.length; i++) {
            let valItem = strBetContent[i];
            if (valItem.indexOf('betdata[' + type + ']') >= 0) {
                value = valItem.split('=')[1];
                break;
            }
        }
        return value;
    }
};

OANTUTI.prototype.toggleAudio = function (event) {
    this.objAudioClick.play();
    if (event) event.preventDefault();
    const elClick = helper.getExaclyEl(event.target, 'btn-audio');
    elClick.classList.toggle("toggle-audio");
    if (elClick.classList.contains('toggle-audio')) {
        this.objAudioBackground.pause();
    } else {
        this.objAudioBackground.play();
    }
};

OANTUTI.prototype.popupInforQuestion = function (event) {
    this.objAudioClick.play();
    event.preventDefault();
    layer.open({
        area: ['929px', '450px'],
        skin: 'inforQuestion',
        type: 1,
        content: $('#guiderOanTuTi'),
        shade: 0.7,
        shadeClose: true,
        scrollbar: true,
        yes: function (index, layero) { },
        cancel: (index, layero) => {
            this.objAudioClick.play();
        },
    });
};

OANTUTI.prototype.reBetting = function (event) {
    this.objAudioClick.play();
    event.preventDefault();
    if (this.storeDataReBet.length <= 0) {
        helper.showErrorAlert('Không tồn tại cược trước đó!', 2000, function () { });
        return false;
    }

    const _this = this;

    // start remove all chips added before
    this.storeDataBetting.forEach(itemStore => {
        const elsChipEmbed = document.querySelectorAll(`[data-chip-bet=${itemStore.itemBetting}]`);
        helper.removeAllEls(elsChipEmbed);
    });
    // end remove all chips added before

    this.storeDataBetting = this.storeDataReBet;

    let totalMoneyBet = 0;
    this.storeDataReBet.forEach(itemsChipUI => {
        const chipUIData = itemsChipUI.chips;
        const elDataBet = document.querySelector(`[data-bet=${itemsChipUI.itemBetting}]`);
        const elsChipEmbed = document.querySelectorAll(`[data-chip-bet=${itemsChipUI.itemBetting}]`);
        helper.removeAllEls(elsChipEmbed);
        totalMoneyBet += itemsChipUI.totalMoney;
        chipUIData.forEach((itemChipUI, idx) => {
            if (idx + 1 <= this.maxGroupChip) {
                renderChipReBetting(elDataBet, itemChipUI, false, itemsChipUI.itemBetting);
            } else { // group chip value
                renderChipReBetting(elDataBet, itemChipUI, true, itemsChipUI.itemBetting);
            }
        });
    });

    this.els.elMoneyBet.innerHTML = helper.numberWithCommas(totalMoneyBet * 1000);
    this.historyTypeBet(false);

    function renderChipReBetting(elTypeBetting, dataChipUI, flagGroupChipVal, dataChipBet) {
        let elChipAdd = document.createElement('a');
        elChipAdd.style.position = "absolute";
        elChipAdd.setAttribute('data-chipkey', `chip-${dataChipUI.chipVal}`);
        elChipAdd.style.left = dataChipUI.leftChipMove + 'px';
        elChipAdd.style.top = dataChipUI.topChipMove + 'px';
        elChipAdd.style.zIndex = dataChipUI.zIndex;
        elChipAdd.setAttribute('href', 'javascript:void(0);');
        elChipAdd.setAttribute('data-chip-bet', dataChipBet);
        elChipAdd.classList.add('chip', 'chipEmbed', 'chip-clone-moved');
        if (dataChipUI.flagCheckCustomChip) {
            elChipAdd.classList.add('chip-custom-icon');
        }
        elChipAdd.innerHTML = `<span class="icon chip-clone-${dataChipUI.chipVal}">${_this.convertMoneyStr(Number(dataChipUI.chipVal) * 1000).str}</span>`;
        _this.els.elContainer.appendChild(elChipAdd);
        if (flagGroupChipVal) {
            elChipAdd.classList.add('chipEmbedHideGroup');
            _this.updateTotalMoneyGroupChip(elTypeBetting, 0);
        }
    }
};

OANTUTI.prototype.actionPlay = function (event) {
    this.objAudioPlay.volume = 0.1;
    this.objAudioPlay.play();

    event.preventDefault();
    if (this.storeDataBetting.length <= 0) {
        helper.showErrorAlert('Vui lòng đặt cược!', 2000, function () { });
        return false;
    }
    const paramsBetting = new DATA_SUBMIT_BETTING();
    paramsBetting.gameid = this.gameID;
    if (paramsBetting.gameid === null) {
        helper.showErrorAlert('GameID không hợp lệ!', 2000, function () { });
        return false;
    }

    this.storeDataBetting.forEach(item => {
        paramsBetting.money += item['totalMoney'] * 1000;
        const reMapModel = this.model.find(itemMap => itemMap['typebet'] === item['itemBetting']);
        paramsBetting.betdata[reMapModel['typebetMap']] = item['totalMoney'] * 1000;
    });
    if (paramsBetting.money > this.storeBalance) {
        helper.showErrorAlert('Số dư không đủ!', 2000, function () { });
        return false;
    }
    this.preventTimeout().add();

    this.effectBalance(paramsBetting.money);
    this.els.elMoneyStatuBetEffect.querySelector('.moneyStatusBet').innerHTML = `-${helper.numberWithCommas(paramsBetting.money)}`;
    this.els.elMoneyStatuBetEffect.classList.add('lossMoney', 'showMoneyEffectBet');
    // timeout finish effect loss money: animation: effectMoneyStatusLoss 1s  ease-out; => 1000
    const timeoutEffectMoneyLoss = 1000;
    setTimeout(() => {
        this.els.elMoneyStatuBetEffect.classList.remove('lossMoney', 'showMoneyEffectBet');
        this.submitBetting(paramsBetting);
    }, timeoutEffectMoneyLoss + 20);
};

OANTUTI.prototype.submitBetting = function (paramsBetting) {
    //start reset class element for start
    this.els.elsUserHandRight.classList.remove('hand-result-right-bua', 'hand-result-right-keo', 'hand-result-right-nap');
    this.els.elsUserHandRight.classList.add('hand-start-right');
    this.els.elsUserHandLeft.classList.remove('hand-result-left-bua', 'hand-result-left-keo', 'hand-result-left-nap');
    this.els.elsUserHandLeft.classList.add('hand-start-left');
    //end reset class element for start

    const timeEffectPlay = 350;
    const cssHandRight = `transform-origin: right;animation: rotarHandRight ${timeEffectPlay}ms infinite;animation-iteration-count: infinite;`;
    this.els.elsUserHandRight.setAttribute('style', cssHandRight);
    const cssHandLeft = `transform-origin: left;animation: rotarHandLeft ${timeEffectPlay}ms infinite;animation-iteration-count: infinite;`;
    this.els.elsUserHandLeft.setAttribute('style', cssHandLeft);

    // START call api betting
    let start_time = new Date().getTime();
    helper.requestHTTP(paramsBetting, this.OANTUTI_SERVICE).then(data => {
        const timeWait = new Date().getTime() - start_time;
        if (data['msg'] === '') {
            try {
                const betResult = JSON.parse(data['betresult']);
                this.storeBalance = Number(betResult['data']['balance']);
                this.storeDataReBet = this.storeDataBetting;
                setTimeout(() => {
                    this.els.elsUserHandRight.removeAttribute('style');
                    this.els.elsUserHandLeft.removeAttribute('style');
                    this.els.elsUserHandRight.classList.remove('hand-start-right');
                    this.els.elsUserHandLeft.classList.remove('hand-start-left');
                    this.updateHistory(betResult['data']['result'], betResult['data']['betTime']);
                    this.moveChipWinLoss(betResult['data']['result'], betResult['data']['prize']);
                }, timeWait + timeEffectPlay * 3);
            } catch (error) {
                helper.showErrorAlert('Lỗi kết nối!', 2000, function () { });
            }
        } else {
            helper.showErrorAlert(data['msg'], 2000, function () { });
        }
    });
    // END call api betting
};

OANTUTI.prototype.moveChipWinLoss = function (strResult, prize) {
    const elsChipEmbed = Array.from(document.querySelectorAll(".chipEmbed"));
    const arrResult = strResult.split('|');
    const arrChipEmbedWin = [];
    let arrChipEmbedLoss = [];
    arrResult.forEach(result => {
        const itemWin = this.model.find(itemMap => itemMap['typebetMap'] === result);
        let flag = true;
        let i = 0;
        while (flag) {
            try {
                if (elsChipEmbed.length <= 0) {
                    flag = false;
                    return;
                }
                const getTypeBetChip = elsChipEmbed[i].dataset['chipBet'];
                if (itemWin['typebet'] === getTypeBetChip) {
                    arrChipEmbedWin.push(elsChipEmbed[i]);
                    elsChipEmbed.splice(i, 1);
                } else {
                    i += 1;
                }

                if (i >= elsChipEmbed.length) {
                    flag = false;
                }
            } catch (error) {
                flag = false;
            }
        }
    });
    arrChipEmbedLoss = elsChipEmbed;

    //START group chip embed by typebet win
    const arrObjChipWin = [];
    arrChipEmbedWin.forEach(itemEmbedWin => {
        const getTypeBetChip = itemEmbedWin.dataset['chipBet'];
        const itemChipWin = this.model.find(itemMap => itemMap['typebet'] === getTypeBetChip);
        if (arrObjChipWin[itemChipWin['typebet']] === undefined) {
            arrObjChipWin[itemChipWin['typebet']] = [];
        }
        arrObjChipWin[itemChipWin['typebet']].push(itemEmbedWin);
    });
    //END group chip embed by typebet win

    //START group chip embed by typebet loss
    const arrObjChipLoss = [];
    arrChipEmbedLoss.forEach(itemEmbedLoss => {
        const getTypeBetChip = itemEmbedLoss.dataset['chipBet'];
        const itemChipLoss = this.model.find(itemMap => itemMap['typebet'] === getTypeBetChip);
        if (arrObjChipLoss[itemChipLoss['typebet']] === undefined) {
            arrObjChipLoss[itemChipLoss['typebet']] = [];
        }
        arrObjChipLoss[itemChipLoss['typebet']].push(itemEmbedLoss);
    });
    //END group chip embed by typebet loss

    this.effectTypeBetWin(arrObjChipWin, true);
    let timeWaitEffectBetWin = 0;
    if (Object.size(arrObjChipWin) > 0) {
        timeWaitEffectBetWin = 2000;
    }
    setTimeout(() => {
        this.effectTypeBetWin(arrObjChipWin, false);
        this.moveChipWin(arrObjChipWin);
        this.moveChipLoss(arrObjChipLoss);
        setTimeout(() => {
            this.preventTimeout().remove();
            this.resetGame();

            if (Number(prize) > 0) { // if win and then show effect money win
                this.els.elMoneyStatuBetEffect.querySelector('.moneyStatusBet').innerHTML = `+${helper.numberWithCommas(prize)}`;
                this.els.elMoneyStatuBetEffect.classList.add('winMoney', 'showMoneyEffectBet');
                // timeout finish effect: -webkit-animation: effectMoneyStatusWin 1.5s ease-out; => 1500
                const timeoutEffectMoneyWin = 1500;
                setTimeout(() => {
                    this.els.elMoneyStatuBetEffect.classList.remove('winMoney', 'showMoneyEffectBet');
                }, timeoutEffectMoneyWin + 20);
                this.effectBalance();
            }

        }, Math.max(this.timeEndMoveChipWin, this.timeEndMoveChipLoss));
    }, timeWaitEffectBetWin);
};

OANTUTI.prototype.effectBalance = function (balanceChange = 0) {
    this.els.elBalance.classList.add('effectBalance');
    //timeout effect balance -webkit-animation: effectBalance .3s linear; animation-iteration-count: 2;
    const timeoutEffectBalance = 300;
    const countEffect = 2;
    setTimeout(() => {
        this.els.elBalance.classList.remove('effectBalance');
    }, timeoutEffectBalance * countEffect + 20);
    setTimeout(() => {
        this.els.elBalance.innerHTML = helper.numberWithCommas(this.storeBalance - balanceChange);
    }, timeoutEffectBalance);
};

OANTUTI.prototype.resetGame = function (event) {
    if (event) {
        this.objAudioClick.play();
        event.preventDefault();
    }
    this.storeDataBetting.forEach(itemStore => {
        const elsChipEmbed = document.querySelectorAll(`[data-chip-bet=${itemStore.itemBetting}]`);
        helper.removeAllEls(elsChipEmbed);
    });
    this.historyTypeBet(true);
    this.storeDataBetting = [];
    this.els.elMoneyBet.innerHTML = "0";
    this.dataTypeBet = new DATA_ITEM_BETTING();
};

/**
 * @param arrObjChipWin: type object array chip win
 * @param toggleEffect: false = remove effect, true = add effect
 */
OANTUTI.prototype.effectTypeBetWin = function (arrObjChipWin, toggleEffect) {
    for (let obj in arrObjChipWin) {
        const getTypeWin = document.querySelector(`[data-bet="${obj}"]`);
        if (toggleEffect) {
            getTypeWin.classList.add('effectTypeBetWin');
        } else {
            getTypeWin.classList.remove('effectTypeBetWin');
        }
    }
};

OANTUTI.prototype.moveChipWin = function (arrObjChipWin) {
    const posiBalance = helper.getPositionXYEl(this.els.elMainBalance);
    const areaMoveChipToBalance = {
        x: posiBalance.x + posiBalance.w / 2,
        y: posiBalance.y + posiBalance.h / 2
    };

    let storeTimeDelayMax = 0;
    for (let obj in arrObjChipWin) {
        const listItem = arrObjChipWin[obj].filter(item => !item.classList.contains('chipEmbedHideGroup'));
        let timeDelay = 0.2;
        for (let i = listItem.length - 1; i >= 0; i--) {
            const chip = listItem[i];
            chip.style.top = areaMoveChipToBalance.y + 'px';
            chip.style.left = areaMoveChipToBalance.x - 30 + 'px';
            chip.style.transitionDelay = `${timeDelay}s`;
            chip.classList.add('chipEmbedHideGroup');
            timeDelay += timeDelay / 5;
        }
        if (timeDelay > storeTimeDelayMax) {
            storeTimeDelayMax = timeDelay;
        }
    }

    this.timeEndMoveChipWin = storeTimeDelayMax * 1000 + 320;
    setTimeout(() => {
        for (let obj in arrObjChipWin) {
            const listItem = arrObjChipWin[obj];
            helper.removeAllEls(listItem);
        }
    }, this.timeEndMoveChipWin);
};

OANTUTI.prototype.moveChipLoss = function (arrObjChipLoss) {
    const posiBalance = helper.getPositionXYEl(this.els.elAreaChips);
    const areaMoveChipToBalance = {
        x: posiBalance.x + posiBalance.w / 2,
        y: posiBalance.y - 30
    };

    let storeTimeDelayMax = 0;
    for (let obj in arrObjChipLoss) {
        const listItem = arrObjChipLoss[obj].filter(item => !item.classList.contains('chipEmbedHideGroup'));
        let timeDelay = 0.2;
        for (let i = listItem.length - 1; i >= 0; i--) {
            const chip = listItem[i];
            chip.style.top = areaMoveChipToBalance.y + 'px';
            chip.style.left = areaMoveChipToBalance.x - 30 + 'px';
            chip.style.transitionDelay = `${timeDelay}s`;
            chip.classList.add('chipEmbedHideGroup');
            timeDelay += timeDelay / 5;
        }
        if (timeDelay > storeTimeDelayMax) {
            storeTimeDelayMax = timeDelay;
        }
    }

    this.timeEndMoveChipLoss = storeTimeDelayMax * 1000 + 320;
    setTimeout(() => {
        for (let obj in arrObjChipLoss) {
            const listItem = arrObjChipLoss[obj];
            helper.removeAllEls(listItem);
        }
    }, this.timeEndMoveChipLoss);
};

OANTUTI.prototype.renderChips = function () {
    let strEl = '';
    for (let i = 0; i < this.objChips.length; i++) {
        let objChip = this.objChips[i];
        let statusClass = '';
        if (Number(objChip.status)) {
            statusClass = 'active';
            this.objChipActive.idx = i;
            this.objChipActive.chipVal = objChip.chipVal;
        }
        if (objChip.chipVal === 'custom') {
            strEl +=
                `<div class="swiper-slide">
                    <a id="chip-${objChip.chipVal}" data-chipkey="chip-${objChip.chipVal}" href="javascript:void(0);" class="chip chip-custom">
                        <span class="icon chip-${objChip.chipVal}"></span>
                        <span class="icon icon-edit"></span>
                    </a>
                 </div>`;
        } else {
            strEl +=
                `<div class="swiper-slide">
                    <a data-chipkey="chip-${objChip.chipVal}" href="javascript:void(0);" class="${statusClass} chip">
                        <span class="icon chip-${objChip.chipVal}">${objChip.chipVal}</span>
                        <span class="chip-highlight icon"></span>
                    </a>
                 </div>`;
        }
    }
    this.els.elChipsBetting.innerHTML = strEl;

    this.swiperChip = new Swiper('.area-chips .swiper-container', {
        slidesPerView: this.numChipShow,
        preventClicks: false,
        navigation: {
            nextEl: '.chip-button-next',
            prevEl: '.chip-button-prev',
        },
    });

    this.swiperChip.on('slideChangeTransitionEnd', () => {
        let elChipCur = this.els.elChipsBetting.querySelector('.chip.active');
        this.objChipActive.chipPosi = helper.getPositionXYEl(elChipCur);
        if (this.callbackElChooseBetClick) {
            this.addChipToTypeBetting(this.callbackElChooseBetClick);
            this.callbackElChooseBetClick = null;
        } else if (this.callbackSlideRemove) {
            this.posiChipCheckRemove(this.callbackSlideRemove);
            this.callbackSlideRemove = null;
        }
    });

    // start event click in chip
    let elsChip = this.els.elChipsBetting.querySelectorAll('.chip');
    helper.addEventEls('click', elsChip, 'chip', (targetEl, idxEl) => {
        this.objAudioClick.play();
        if (targetEl.classList.contains('active') || targetEl.classList.contains('chip-custom')) return;
        let getSwiperSlide = targetEl.parentElement;
        let getSwiperItems = getSwiperSlide.parentElement.querySelectorAll('.swiper-slide');
        this.objChipActive.idx = [...getSwiperItems].indexOf(getSwiperSlide);

        // start update status chip
        this.objChips = this.objChips.map((item, idx) => {
            item['status'] = 0;
            if (idx === this.objChipActive.idx) {
                this.objChipActive.chipVal = item['chipVal'];
                item['status'] = 1;
            }
            return item;
        });
        // end update status chip

        helper.removeClassesEffectTimeout(elsChip, 'active', 'hideEffect', 1000);
        targetEl.classList.add('active');
        this.objChipActive.chipPosi = helper.getPositionXYEl(targetEl);
    });
    // end event click in chip
};

OANTUTI.prototype.toggerViewHisCurrent = function (event) {
    this.objAudioClick.play();
    event.preventDefault();
    const elTarget = helper.getExaclyEl(event.target, 'view-his-current');
    if (elTarget.classList.contains('active')) {
        elTarget.classList.remove('active');
    } else {
        elTarget.classList.add('active');
    }
};

OANTUTI.prototype.randomFigure = function () {
    const randGirl = Math.floor(Math.random() * 8) + 1;
    const randBoy = Math.floor(Math.random() * 7) + 1;
    const getClsGirl = `figure-girl-${randGirl}`;
    const getClsBoy = `figure-boy-${randBoy}`;
    this.els.elUserPlayLeft.querySelector('.figure').classList.add(getClsGirl);
    this.els.elUserPlayRight.querySelector('.figure').classList.add(getClsBoy);
    this.els.elsUserGirlWin.forEach(item => {
        item.querySelector('.figure').classList.add(getClsGirl);
    });
    this.els.elsUserBoyWin.forEach(item => {
        item.querySelector('.figure').classList.add(getClsBoy);
    });
};

OANTUTI.prototype.chooseItemBetting = function () {
    const elsChoose = document.querySelectorAll('[data-bet]');
    helper.addEventEls('click', elsChoose, 'typebetting', (el) => {
        let curChipActive = null;
        for (let i = 0; i < this.swiperChip.slides.length; i++) {
            let item = this.swiperChip.slides[i];
            if (item.querySelector('.chip.active')) {
                curChipActive = i;
                break;
            }
        }
        let countCurShowChip = this.swiperChip.activeIndex + this.numChipShow;
        if (countCurShowChip <= curChipActive) { // slide to left
            this.swiperChip.slideTo(curChipActive);
            this.callbackElChooseBetClick = el;
        } else if (countCurShowChip - curChipActive > this.numChipShow) { // slide to right
            this.swiperChip.slideTo(curChipActive);
            this.callbackElChooseBetClick = el;
        } else { // slide normal
            this.addChipToTypeBetting(el);
        }
    });
};

/**
 * @param elTypeBetting: element <div data-bet="ga" class="wraptype">...
 */
OANTUTI.prototype.addChipToTypeBetting = function (elTypeBetting) {
    const getDataChoose = elTypeBetting.dataset.bet;
    const checkTypeBetMaxBet = this.storeDataBetting.find(itemMaxBet => itemMaxBet['itemBetting'] === getDataChoose);
    const getTypeBetModel = this.model.find(model => model['typebet'] === getDataChoose);
    if ((checkTypeBetMaxBet && (checkTypeBetMaxBet.totalMoney + Number(this.objChipActive.chipVal)) * 1000 > getTypeBetModel.maxbet) || Number(this.objChipActive.chipVal) * 1000 > getTypeBetModel.maxbet) {
        helper.showErrorAlert(`Vượt mức giới hạn cược ${helper.numberWithCommas(getTypeBetModel.maxbet)}`, 2000, function () { });
        return;
    }
    this.preventTimeout().add();
    const elTypeBetW = elTypeBetting.clientWidth / 2;
    const elTypeBetH = elTypeBetting.clientHeight / 2;
    const elTypeBetX = elTypeBetting.getBoundingClientRect().left;
    const elTypeBetY = elTypeBetting.getBoundingClientRect().top;

    // start create chip embed to type betting
    let elsChip = this.els.elChipsBetting.querySelectorAll('.chip');
    const elChipAddCurrent = elsChip[this.objChipActive.idx];
    this.objChipActive.chipPosi = helper.getPositionXYEl(elChipAddCurrent);
    let elChipAdd = document.createElement('a');
    elChipAdd.style.position = "absolute";
    elChipAdd.setAttribute('data-chipkey', `chip-${this.objChipActive.chipVal}`);
    elChipAdd.style.left = (this.objChipActive.chipPosi.x + 10) + 'px';
    elChipAdd.style.top = (this.objChipActive.chipPosi.y + 10) + 'px';
    elChipAdd.setAttribute('href', 'javascript:void(0);');
    elChipAdd.setAttribute('data-chip-bet', getDataChoose);
    elChipAdd.classList.add('chip', 'chipEmbed', `chip-${getDataChoose}`);

    let flagCheckCustomChip = false;
    if (elChipAddCurrent.classList.contains('chip-custom-icon')) {
        elChipAdd.classList.add('chip-custom-icon');
        flagCheckCustomChip = true;
    }
    let elChipEmbed = document.querySelectorAll(`.chip-${getDataChoose}`);
    let elChipLast = undefined;
    let zIndexChip = "10";
    if (elChipEmbed && elChipEmbed.length > 0) {
        elChipLast = elChipEmbed[elChipEmbed.length - 1];
        zIndexChip = (Number(elChipLast.style.zIndex) + 1).toString();
    }
    elChipAdd.style.zIndex = zIndexChip;
    elChipAdd.innerHTML = `<span class="icon chip-clone-${this.objChipActive.chipVal}">${this.convertMoneyStr(Number(this.objChipActive.chipVal) * 1000).str}</span>`;
    this.els.elContainer.appendChild(elChipAdd);
    // end create chip embed to type betting

    // start waiting for animation chip
    setTimeout(() => {
        let topChipMove = elTypeBetY + elTypeBetH;
        let leftChipMove = elTypeBetX - elTypeBetW + 30;

        elChipAdd.style.left = leftChipMove + 'px';
        let elsChipEmbedNew = document.querySelectorAll(`.chip-${getDataChoose}`);
        if (elsChipEmbedNew.length > this.maxGroupChip) { // group chip and calculate position top for chip next hidden
            topChipMove = elChipLast.style.top.replace('px', '');
            elChipAdd.classList.add('chipEmbedHideGroup');
            this.updateTotalMoneyGroupChip(elTypeBetting, this.objChipActive.chipVal);
        } else if (elChipLast) { // position top for chip next
            topChipMove = Number(elChipLast.style.top.replace('px', '')) - 5;
        }

        elChipAdd.style.top = topChipMove + 'px';
        setTimeout(() => {
            elChipAdd.classList.add('chip-clone-moved');
        }, 10);

        this.objAudioChip.play();

        // start store chip position move
        const objStorePosiMove = {
            chipVal: this.objChipActive.chipVal,
            leftChipMove: elTypeBetX - elTypeBetW + 30,
            topChipMove: Number(topChipMove),
            zIndex: zIndexChip,
            flagCheckCustomChip: flagCheckCustomChip
        };
        // end store chip position move

        // start check store item betting
        this.dataBetting(1, getDataChoose, null, objStorePosiMove);
        // end check store item betting

        this.preventTimeout().remove();
    }, 100);
    // end waiting for animation chip
};

/**
 * store data each item of type betting selected
 * @param flag => 1: add, 0: remove
 * @param typeBetChoose => string = bau || cua || tom || ca || nai || ga
 * @param chipValRemove => 5,10,25,50
 * @param objStorePosiMove => [null] || [object] {chipVal: 5, leftChipMove: 123, topChipMove: 234, zIndex: 2}
 */
OANTUTI.prototype.dataBetting = function (flag, typeBetChoose, chipValRemove, objStorePosiMove) {
    if (this.storeDataBetting.length <= 0 && flag === 1) { // constructor store data betting
        this.dataTypeBet.itemBetting = typeBetChoose;
        this.dataTypeBet.totalMoney += Number(this.objChipActive.chipVal);
        this.dataTypeBet.chips.push(objStorePosiMove);
        this.storeDataBetting.push(this.dataTypeBet);
    } else if (flag === 0 && chipValRemove) { // remove data store
        this.storeDataBetting = this.storeDataBetting.map((item, idx) => {
            if (item['itemBetting'] === typeBetChoose) { // exist
                item['totalMoney'] -= Number(chipValRemove);
            }
            return item;
        }).filter(itemStore => itemStore['totalMoney'] > 0);
    } else { // update data betting
        let countCheckExist = 0;
        this.storeDataBetting = this.storeDataBetting.map((item, idx) => {
            if (item['itemBetting'] === typeBetChoose) { // exist
                const getMaxBetTypeBet = this.model.find(itemMaxBet => itemMaxBet['typebet'] === item['itemBetting']);
                if (item['totalMoney'] <= getMaxBetTypeBet['maxbet']) {
                    item['totalMoney'] += Number(this.objChipActive.chipVal);
                    item.chips.push(objStorePosiMove);
                }
            } else { // not exist
                countCheckExist += 1;
            }
            return item;
        });

        // constructor store new data betting
        if (countCheckExist === this.storeDataBetting.length) {
            this.dataTypeBet = new DATA_ITEM_BETTING();
            this.dataTypeBet.itemBetting = typeBetChoose;
            this.dataTypeBet.totalMoney += Number(this.objChipActive.chipVal);
            this.dataTypeBet.chips.push(objStorePosiMove);
            this.storeDataBetting.push(this.dataTypeBet);
        }
    }

    // update total money bet
    const totalMoneyBet = this.storeDataBetting.reduce(function (accumulator, currentValue) { return accumulator + currentValue.totalMoney; }, 0);
    this.els.elMoneyBet.innerHTML = helper.numberWithCommas(totalMoneyBet * 1000);
    // end update total money bet

    this.historyTypeBet(false);
};

/**
 * @param flagUpdate: false = update, true = reset
 */
OANTUTI.prototype.historyTypeBet = function (flagUpdate) {
    this.storeDataBetting.forEach(item => {
        const getElHis = document.querySelector(`[data-his-typebet="${item['itemBetting']}"]`);
        const getElMoney = getElHis.querySelector('.his-money');
        if (!flagUpdate) {
            getElMoney.innerHTML = helper.numberWithCommas(item['totalMoney'] * 1000);
        } else {
            getElMoney.innerHTML = "0";
        }
    });
};

/**
 * @param elTypeBet: element <div data-bet="ga" class="wraptype">...
 * @param chipValGroup: [undefined] || string="100"
 */
OANTUTI.prototype.updateTotalMoneyGroupChip = function (elTypeBet, chipValGroup) {
    const getDataChoose = elTypeBet.dataset.bet;
    const elChipEmbed = document.querySelectorAll(`[data-chip-bet=${getDataChoose}]`);
    let getChipValGroup = chipValGroup || 0;
    if (elChipEmbed.length > this.maxGroupChip) {
        const elChipShowMoney = elChipEmbed[this.maxGroupChip - 1];
        const elNumChipShow = elChipShowMoney.querySelector('.icon');
        const getItemMap = this.storeDataBetting.find(item => item['itemBetting'] === getDataChoose);
        if (getItemMap) {
            const total = getItemMap.totalMoney + Number(getChipValGroup);
            const listNumber = (total / 1000).toString().split('.');
            if (total >= 1000) {
                if (listNumber.length === 2) {
                    elNumChipShow.innerHTML = `${listNumber[0]}M${listNumber[1]}`
                } else {
                    elNumChipShow.innerHTML = `${listNumber[0]}M`
                }
            } else {
                elNumChipShow.innerHTML = getItemMap.totalMoney + Number(getChipValGroup);
            }
        }
    }
};

OANTUTI.prototype.preventTimeout = function () {
    return {
        add: () => {
            let elOverlayBG = document.createElement('span');
            elOverlayBG.classList.add('bgoverlay');
            document.querySelector('body').appendChild(elOverlayBG);
            elOverlayBG.addEventListener('contextmenu', e => e.preventDefault());
        },
        remove: () => {
            if (document.querySelector('.bgoverlay')) document.querySelector('.bgoverlay').remove();
        }
    };
};

OANTUTI.prototype.renderHistory = function (dataHistory) {
    const getDataHis = JSON.parse(dataHistory);
    let strHis = '';
    let strSlide = '';
    let idxTemp = 0;
    const lenHis = getDataHis.length;
    if (lenHis > 0) {
        strSlide = '<div class="swiper-slide">';
        for (let i = lenHis - 1; i >= 0; i--) {
            let item = getDataHis[i];
            let strWrapVi = this.createWinElSlide(item['winnumber'], item['bettime'], false).elViResult.outerHTML;
            idxTemp += 1;
            if (idxTemp < this.colHisShow) {
                strSlide += strWrapVi;
            } else {
                strSlide += strWrapVi;
                strHis += strSlide + '</div>';
                idxTemp = 0;
                strSlide = '<div class="swiper-slide">';
            }
            if (i === 0 && idxTemp > 0) {
                strHis += strSlide + '</div>';
            }
        }
    } else {
        strHis = '<div class="swiper-slide no-data"><p>Chưa có dữ liệu!</p></div>';
    }
    this.els.elHistoryResult.querySelector('.swiper-wrapper').innerHTML = strHis;

    this.swiperHis = new Swiper('#history-result .swiper-container', {
        slidesPerView: 1,
        preventClicks: true,
        navigation: {
            nextEl: '.history-button-next',
            prevEl: '.history-button-prev',
        },
    });
    this.swiperHis.slideTo(this.swiperHis.slides.length);
};

/**
 * @param strVi = rock_left|sciss_right|left_win
 * @param titleHover = issue OR time
 */
OANTUTI.prototype.updateHistory = function (strVi, titleHover) {
    let lenSlide = this.swiperHis.slides.length;
    let lastSlide = this.swiperHis.slides[lenSlide - 1];
    let elResults = lastSlide !== undefined ? lastSlide.querySelectorAll('.typebet-results') : [];
    let arrVi = strVi.split('|');
    const objUpdate = this.createWinElSlide(strVi, titleHover, true);
    if (elResults.length > 0 && elResults.length < this.colHisShow) {
        lastSlide.appendChild(objUpdate.elViResult);
    } else if (elResults.length === this.colHisShow || elResults.length === 0) {
        let str = `<div class="swiper-slide">${objUpdate.elViResult.outerHTML}</div>`;
        if (lastSlide.classList.contains('no-data')) {
            this.swiperHis.removeSlide(0);
        }
        this.swiperHis.appendSlide(str);
        this.swiperHis.slideTo(this.swiperHis.slides.length);
    }

    this.objAudioOpen.play();
    this.els.elsUserHandRight.classList.add(objUpdate.dataBottomWin['handWin']);
    this.els.elsUserHandLeft.classList.add(objUpdate.dataTopWin['handWin']);
};

/**
 * @param strResult = rock_left|sciss_right|left_win
 * @param title = issue OR time
 * @param flagEffect => false = no effect, true = effect append
 * @returns {{elViResult: HTMLDivElement, dataTopWin: string, dataBottomWin: string}}
 */
OANTUTI.prototype.createWinElSlide = function (strResult, title, flagEffect) {
    const arrResult = strResult.split('|');
    let elViResult = document.createElement('div');
    let itemTopWin = '';
    let itemBottomWin = '';
    elViResult.classList.add('typebet-results');
    if (flagEffect) { // has effect append
        elViResult.classList.add('appendEffect');
    }
    elViResult.setAttribute('title', title);
    arrResult.forEach(itemWin => {
        const itemMapWin = this.model.find(itemMap => itemMap['typebetMap'] === itemWin);
        if (['user-left-win', 'user-right-win', 'user-hoa'].indexOf(itemMapWin['typebet']) < 0) { // just get result left, right hand
            if (itemMapWin['key'] === 'left') {
                itemTopWin = itemMapWin;
            } else if (itemMapWin['key'] === 'right') {
                itemBottomWin = itemMapWin;
            }
            elViResult.innerHTML = `<span class="icon ${itemTopWin['userWin']}"></span><span class="icon ${itemBottomWin['userWin']}"></span>`;
        }
    });
    return {
        elViResult: elViResult,
        dataTopWin: itemTopWin,
        dataBottomWin: itemBottomWin
    };
};

OANTUTI.prototype.apiGetBalance = function (funCallBack) {
    const params = { flag: 'balance' };
    helper.requestHTTP(params, this.LOTTERY_SERVICE).then(data => {
        if (data['msg'] === '') {
            this.storeBalance = Number(data['balance']);
            this.countRequestBalance = 0;
            funCallBack && funCallBack(data);
        } else {
            this.storeBalance = 0;
            this.countRequestBalance += 1;
            if (this.countRequestBalance >= this.maxCountRequestBalance) {
                clearInterval(this.intervalTimeCheckBalance);
                if (data['msg'] === '请重新登录') {
                    helper.showErrorAlert('Vui lòng đăng nhập!', 2000, function () {
                        window.location.href = '/';
                    });
                } else {
                    helper.showErrorAlert('Lỗi đường truyền!', 2000, function () {
                        window.location.href = '/';
                    });
                }
            }
        }
    });
};

OANTUTI.prototype.apiInit = function () {
    const params = { flag: 'Init' };
    helper.requestHTTP(params, this.OANTUTI_SERVICE).then(data => {
        if (data['msg'] === '') {
            const model = data['model'];
            const history = data['history'];
            this.gameID = data['gameid'];

            let modelMerge = [];
            model.forEach(item => {
                let itemTemp = null;
                if (item['checkflag'] === 'sciss_left') { // keo left
                    itemTemp = this.model.find(itemMap => itemMap['typebet'] === 'user-left-keo');
                } else if (item['checkflag'] === 'rock_left') { // bua left
                    itemTemp = this.model.find(itemMap => itemMap['typebet'] === 'user-left-bua');
                } else if (item['checkflag'] === 'paper_left') { // nap left
                    itemTemp = this.model.find(itemMap => itemMap['typebet'] === 'user-left-nap');
                } else if (item['checkflag'] === 'sciss_right') { // keo right
                    itemTemp = this.model.find(itemMap => itemMap['typebet'] === 'user-right-keo');
                } else if (item['checkflag'] === 'rock_right') { // bua right
                    itemTemp = this.model.find(itemMap => itemMap['typebet'] === 'user-right-bua');
                } else if (item['checkflag'] === 'paper_right') { // nap right
                    itemTemp = this.model.find(itemMap => itemMap['typebet'] === 'user-right-nap');
                } else if (item['checkflag'] === 'left_win') { // left win
                    itemTemp = this.model.find(itemMap => itemMap['typebet'] === 'user-left-win');
                } else if (item['checkflag'] === 'right_win') { // right win
                    itemTemp = this.model.find(itemMap => itemMap['typebet'] === 'user-right-win');
                } else if (item['checkflag'] === 'draw') { // hoa
                    itemTemp = this.model.find(itemMap => itemMap['typebet'] === 'user-hoa');
                }

                if (itemTemp) {
                    itemTemp.typebetMap = item['checkflag'];
                    itemTemp.maxbet = item['maxbet'];
                    itemTemp.levelnum = item['levelnum'];
                    itemTemp.ratio = Number((item['level1'] - item['levelnum']).toFixed(2));
                    modelMerge.push(itemTemp);
                }
            });

            this.model = modelMerge; // map model api

            this.renderHistory(history);
            this.renderRatio();
        } else {
            helper.showErrorAlert(data['msg'], 2000, function () {
                window.location.href = '/';
            });
        }
    });
};

OANTUTI.prototype.renderRatio = function () {
    this.model.forEach(model => {
        let elKeyRatio = model['typebet'];
        if (model['typebet'] === 'user-left-keo' || model['typebet'] === 'user-left-bua' || model['typebet'] === 'user-left-nap') {
            elKeyRatio = 'user-left-ratio';
        } else if (model['typebet'] === 'user-right-keo' || model['typebet'] === 'user-right-bua' || model['typebet'] === 'user-right-nap') {
            elKeyRatio = 'user-right-ratio';
        }
        document.querySelector(`[data-ratio="${elKeyRatio}"]`).innerHTML = `1 : ${model['ratio']}`;
    });
};

window.addEventListener ?
    window.addEventListener('load', function () {
        (new OANTUTI()).init();
    }, false) :
    window.attachEvents && window.attachEvents('onload', function () { });