const path = require('path');
const webpack = require("webpack");
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
    entry: {
        oantuti: './src/app/oantuti.js'
    },
    output: {
        filename: 'assets/js/[name].min.js',
        path: path.resolve(__dirname, './')
    },
    watch: true,
    devServer: {
        host: process.env.HOST, // Defaults to `localhost`
        port: 9999, // Defaults to 8080
        open: true, // Open the page in browser
        overlay: true,
        contentBase: path.join(__dirname, '/src'),
        watchContentBase: true,
        proxy: {
            '/': 'http://localhost:8087'
        }
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: './src/index.html',
            inject: true,
            minify: {
                removeComments: true,
                collapseWhitespace: false
            }
        }),
        new MiniCssExtractPlugin({
            filename: 'assets/css/style.css',
        }),
        new webpack.ProvidePlugin({
            layer: path.join(__dirname, '/src/app/layer.js'),
        }),
    ],
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /(node_modules)/,
                use: {
                    loader: "babel-loader",
                    options: { presets: ["es2015"] },
                }
            },
            {
                test: [/.css$|.scss$/],
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader
                    },
                    "css-loader",
                    "sass-loader"
                ]
            },
            {
                test: /\.html$/,
                use: [
                    {
                        loader: 'html-loader',
                        options: {
                            attrs: ['img:src']
                        }
                    }
                ]
            },
            {
                test: /\.(png|jpg|gif|svg)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                        	// for pro
                            name: '[name].[ext]',
                            publicPath: '../images/'

                            /*// for dev
                            name: 'assets/images/[name].[ext]',
                            publicPath: '/'*/
                        }
                    }
                ]
            },
            {
                test: /\.(wav|mp3)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: 'assets/audio/[name].[ext]'
                        }
                    }
                ]
            }
        ]
    }
};