"use strict";
var arrDomainTest = ["https://loto188.com", "https://loto288.com", "https://loto388.com", "https://loto588.com", "https://loto688.com", "https://loto788.com", "https://loto988.com"];
var $elWrap = $('.moto__testspeed');
var arrSort = [];
var countTemp = 0;

testSpeed();

function reset() {
    arrSort = [];
    countTemp = 0;
    $elWrap.find('.speed__line').remove();
}

$('.reloadTestSpeed').on('click', function (e) {
    e.preventDefault();
    reset();
    testSpeed();
});

function testSpeed() {
    $('#loading__emu').css({
        opacity: 1
    });

    var _loop = function _loop(i, len) {
        var url = arrDomainTest[i];
        var process = 0;
        var objUrlLoad = {};

        htmlLine(url);

        if ( i+1 === len ) {
            var hWrap = $elWrap.outerHeight();
            $elWrap.css({
                height: hWrap+'px'
            });
        }

        ping(url, i).then(function (data) {
            objUrlLoad.idx = i;
            if (+data <= 1000) { // very fast
                objUrlLoad.class = 'speed__1';
            } else if (1000 < +data && +data <= 3000) { // fast
                objUrlLoad.class = 'speed__2';
            } else if (3000 < +data && +data <= 5000) { // normal
                objUrlLoad.class = 'speed__3';
            } else { // slow
                objUrlLoad.class = 'speed__4';
            }

            objUrlLoad.timeLoad = +(data/2)+'MS';
            objUrlLoad.href = url;

            var timeInterVal = +data / 100;
            objUrlLoad.timeOut = timeInterVal;

            var processInterVal = setInterval(function () {
                var flag = true;
                if (process < 100) {
                    process += 1;
                    flag = false;
                }
                objUrlLoad.process = 100 - timeInterVal;
                if (flag) {
                    clearInterval(processInterVal);
                    processLine(objUrlLoad);
                }
            }, timeInterVal);
        }).catch(function (err) {
            debugger;
        });
    };

    for (var i = 0, len = arrDomainTest.length; i < len; i++) {
        _loop(i, len);
    }
}

function processLine(obj){
    countTemp += 1;
    var $elLine = $elWrap.find('.speed__line');
    var timeLoad = obj.timeLoad;
    var classProcess = obj.class;
    var process = obj.process;
    var idx = obj.idx;

    $elLine.eq(idx).find('.numSpeed').html(timeLoad);
    $elLine.eq(idx).attr('data-time', timeLoad);
    $elLine.eq(idx).attr('data-process', process);
    $elLine.eq(idx).addClass(classProcess);

    if ( countTemp === arrDomainTest.length ) {
        $elLine.css({
            opacity: 1
        });

        $elLine.sort(_sortNumTimeLoad);
        $elWrap.append($elLine);

        $elLine.each(function(){
            var _this = $(this),
                time = parseInt(_this.data('time').split('MS')[0]),
                process = _this.data('process');
            setTimeout(function(){
                _this.find('.speed__run').css({
                    width: process+'%'
                });
            },time);
        });
    }
}

function _sortNumTimeLoad(a, b) {
    var a = $(a).data('time').split('MS')[0],
        b = $(b).data('time').split('MS')[0];
    if (a === 'Error' || b === 'Error') {
        return 1;
    }
    return a - b;
}

function htmlLine(href) {
    var html =
        '<div class="speed__line clearfix">\n' +
        '    <button class="btn__speed pull-left">'+href+'</button>\n' +
        '    <div class="pull-left num__speed"><span class="icon__sprites icon__speed"></span><span class="numSpeed"></span></div>\n' +
        '    <div class="speed__process pull-left"><div class="speed__run"></div></div>\n' +
        '    <a href="'+href+'" class="pull-right speed__link btn__speed"><span class="arrow__circle"><span class="icon-triangle-right"></span></span>Truy cập</a>\n' +
        '</div>';
    $elWrap.append($(html));
}

function ping(url, idx) {
    var promise = new Promise(function (resolve, reject) {
        var startTime = new Date().getTime();
        var response = function response() {
            var getTimeLoad = new Date().getTime() - startTime;
            resolve(getTimeLoad);
        };

        request_img(url).then(response).catch(function (err) {
            var objError = {};
            objError.idx = idx;
            objError.href = err;
            objError.timeLoad = 'Error';
            objError.class = 'speed__4';
            objError.process = 100;
            processLine(objError);
        });
    });
    return promise;
}

function request_img(url) {
    var imgName = '/images/speed.gif';
    /*if ( url === 'https://loto188.com' ) {
        imgName = '/images/adfadf.jpg';
    }*/
    var promise = new Promise(function (resolve, reject) {
        var img = new Image();
        img.onload = function () {
            resolve(img);
        };
        img.onerror = function () {
            reject(url);
        };
        img.src = url + imgName + '?clearnCache=' + Math.floor((1 + Math.random()) * 0x10000).toString(16);
    });
    return promise;
}