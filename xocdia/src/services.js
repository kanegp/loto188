function toQueryPair(key, value) {
    if (typeof value === 'undefined') {
        return key;
    }
    return key + '=' + encodeURIComponent(value === null ? '' : String(value));
}

function toBodyString(obj) {
    let ret = [];
    for (let key in obj) {
        let values = obj[key];
        if (values && values.constructor === Array) {
            let queryValues = [];
            for (let i = 0, len = values.length, value; i < len; i++) {
                value = values[i];
                queryValues.push(toQueryPair(key + "[]", value));
            }
            ret = ret.concat(queryValues);
        } else if (values && values.constructor === Object) {
            for (let keySub in values) {
                ret.push(toQueryPair(key + "[" + keySub + "]", values[keySub]));
            }
        } else {
            ret.push(toQueryPair(key, values));
        }
    }
    return ret.join('&');
}

export function requestHTTP(data, url) {
    var data = data || {},
        urlAPI = url || '',
        promise;

    promise = new Promise(function (resolve, reject) {
        let bodyParam = toBodyString(data),
            xhr = new window.XMLHttpRequest();

        xhr.open('POST', urlAPI, true);
        xhr.timeout = 10000; // time in milliseconds
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4) {
                if (xhr.status === 200) {
                    resolve(JSON.parse(xhr.responseText));
                } else {
                    reject(xhr);
                }
            }
        };
        xhr.send(bodyParam);
    });
    return promise;
}

export function fireEvent(el, EventName) {
    if (el != null) {
        if (el.fireEvent) {
            el.fireEvent('on' + EventName);
        } else {
            let evObj = document.createEvent('Events');
            evObj.initEvent(EventName, true, false);
            el.dispatchEvent(evObj);
        }
    }
}

/**
 * @param dateStr = "2019-30-06 10:25:00"
 * @returns {Date}
 */
export function formatToDate(dateStr) {
    return new Date(dateStr.replace(/-/g, "/"));
}

export const configXD = {
    XOCDIA1S_SERVICE: location.origin + "/XDService.aspx",
    LOTTERY_SERVICE: location.origin + "/LotteryService.aspx",
    ELS: {
        elChips: document.getElementById('select-chip'),
        elInnerGame: document.getElementById('inner-game'),
        elCancelBet: document.getElementById('cancelbet'),
        elAgainBet: document.getElementById('rebet'),
        elBalance: document.getElementById('balance'),
        elXocLo: document.getElementById('xocdia'),
        elWrapVi: document.getElementById('items-vi'),
        elTableXocDia: document.getElementById('tableXocDia'),
        elMoneyBet: document.getElementById('money-bet'),
        elMoneyStatusBet: document.getElementById('moneyStatusBet'),
        elGridHistory: document.getElementById('grid-history'),
        elHistoryResult: document.getElementById('history-result'),
        elPopupHis: document.getElementById('tbl-history'),
        elContentPopupHis: document.getElementById('historyBetUser'),
        elPopupInfor: document.getElementById('infor-question'),
        elReloadBalance: document.getElementById('reloadBalance'),
        elTimerIssue: document.getElementById('counttime'),
        elCurrentChipSelected: null,
        elLoadingMoChen: document.getElementById('loadingMoChen'),
        elPopupShowAllIssueHis: document.getElementById('hisAllIssue'),
        elShowAllIssueHis: document.getElementById('showAllHisIssue'),
        elBtnBetSubmit: document.getElementById('betSubmit'),
        elConfirmBet: document.getElementById('confirmBet'),
    },
    chipCurrentData: {},
    idxChipActive: null,
    swiperChip: null,
    numChipShow: 4,
    configChip: [],
    xocdiaID: null,
    model: null,
    betdata: {},
    totalMoneyBet: 0,
    currentBalance: 0,
    swiperChipHis: null,
    getHisAPI: null,
    getHisAllIssueAPI: null,
    hBoxViHis: 0,
    wBoxViHis: 0,
    colHis: 8,
    rowHis: 4,
    wWrapHis: 250,
    objAudioPlay: new Audio('./xocdia/src/assets/audio/xocdia.wav'),
    objAudioOpen: new Audio('./xocdia/src/assets/audio/xocdiaopen.mp3'),
    wChip: 90,
    hChip: 90,
    wWrapViXoc: 150,
    hWrapViXoc: 150,
    particles: [],
    objAlert: {
        errorChipSlect: 'Chọn Chip không hợp lệ!',
        errorBet: 'Vui lòng đặt cược!',
        errorReBet: 'Chưa có cược nào trước đó!',
        errorConnectServer: "Kết nối server bị gián đoạn, vui lòng thử lại trong ít phút! Cảm ơn bạn.",
        errorBalance: 'Số dư hiện tại không đủ!',
    },
    storebetdata: null,
    storeElsReBetting: [],
    getServerTime: null,
    getEndTime: null,
    curIssue: null,
    storeCurIssue: null,
    lastIssue: null,
    numGroup: 5,
    requestMax: 10,
    timeoutRequestApi: 1000,
    statusOpenPlate: false
};

export function XOCDIA() {
    let __this = this;

    function initXD() {
        // reload balance
        configXD.ELS.elReloadBalance.addEventListener('click', __this.loadBalance);
        // end reload balance

        // cancel bet
        configXD.ELS.elCancelBet.addEventListener('click', __this.cancelBet);
        // end cancel bet

        // bet again
        configXD.ELS.elAgainBet.addEventListener('click', __this.reBet);
        // end bet again

        // start popup history
        configXD.ELS.elPopupHis.querySelector('.iconbg').addEventListener('click', __this.openPopupHis);
        // end popup history

        // start popup history
        configXD.ELS.elPopupInfor.querySelector('.iconbg').addEventListener('click', __this.openPopupInfor);
        // end popup history
    }

    this.isEmptyObject = (obj) => {
        for (let key in obj) {
            if (obj.hasOwnProperty(key))
                return false;
        }
        return true;
    };
    this.removeAllEls = (els) => {
        for (let i = 0; i < els.length; i++) {
            els[i].remove();
        }
    };
    this.removeClassesAllEls = (els, cls) => {
        for (let i = 0; i < els.length; i++) {
            els[i].classList.remove(cls);
        }
    };
    this.overlayBG = () => {
        return {
            add: () => {
                let elOverlayBG = document.createElement('span');
                elOverlayBG.classList.add('bgoverlay');
                document.querySelector('body').appendChild(elOverlayBG);
                elOverlayBG.addEventListener('contextmenu', e => e.preventDefault());
            },
            remove: () => {
                document.querySelector('.bgoverlay').remove();
            }
        };
    };
    this.renderPlateWrapVi = () => {
        configXD.ELS.elWrapVi.style.width = configXD.wWrapViXoc + 'px';
        configXD.ELS.elWrapVi.style.height = configXD.hWrapViXoc + 'px';
        configXD.ELS.elWrapVi.style.marginLeft = '-' + configXD.wWrapViXoc / 2 + 'px';
        configXD.ELS.elWrapVi.style.marginTop = '-' + configXD.hWrapViXoc / 2 + 'px';
        this.randomPosiVi();
    };

    /**
     * @param val string 1|0|0|0
     */
    this.randomPosiVi = (val) => {
        let wVi = 25;
        let arrValColor = [];
        let listpos = [];
        let overlapping = false;
        let radius = configXD.wWrapViXoc / 2 - 20;
        if (val) {
            arrValColor = val.split('|');
        } else {
            for (let i = 0; i < 4; i++) {
                arrValColor.push(this.getRandomInt(0, 1));
            }
        }
        while (listpos.length < 4) {
            let pos = {
                x: this.getXY(wVi / 2, radius).x,
                y: this.getXY(wVi / 2, radius).y
            };
            overlapping = false;

            for (let i = 0; i < listpos.length; i++) {
                let existing = listpos[i];
                if (Math.abs(existing.x - pos.x) < 20 && Math.abs(existing.y - pos.y) < 20) {
                    overlapping = true;
                    break;
                }
            }

            if (!overlapping) {
                listpos.push(pos);
            }
        }
        configXD.ELS.elWrapVi.innerHTML = '';
        for (let i = 0; i < 4; i++) {
            let elVi = document.createElement('span');
            elVi.classList.add('vi');
            elVi.classList.add('bet-vi-color' + arrValColor[i]);
            elVi.style.width = wVi + 'px';
            elVi.style.height = wVi + 'px';
            elVi.style.top = listpos[i].y + 'px';
            elVi.style.left = listpos[i].x + 'px';
            configXD.ELS.elWrapVi.appendChild(elVi);
        }
    };
    this.getRandomInt = (min, max) => {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min + 1)) + min;
    };
    this.distance = (x1, y1, x2, y2) => {
        let xDist = x2 - x1;
        let yDist = y2 - y1;
        return Math.sqrt(Math.pow(xDist, 2) + Math.pow(yDist, 2));
    };
    this.getXY = (toppingRadius, radiusPlate) => {
        let posX, posY;
        do {
            posX = Math.floor(Math.random() * ((radiusPlate * 2) - 1));
            posY = Math.floor(Math.random() * ((radiusPlate * 2) - 1));
        } while (Math.sqrt(Math.pow(radiusPlate - posX, 2) + Math.pow(radiusPlate - posY, 2)) > radiusPlate - toppingRadius);

        return { x: posX, y: posY }
    };
    this.setRatio = () => {
        for (let i = 0; i < configXD.model.length; i++) {
            let item = configXD.model[i];
            configXD.ELS.elInnerGame.querySelector('[data-bet=xodia_' + item['methodname'] + '] .ratio-game').innerHTML = item['levelnum'] + ':' + item['ratio'];
        }
    };
    this.errorCheck = (msg) => {
        if (typeof msg !== 'string') msg = 'Lỗi kết nối!';
        //showErrorAlert(msg, 3000, () => {});
    };
    this.loadBalance = (e) => {
        if (e) e.preventDefault();
        configXD.ELS.elReloadBalance.classList.add('active');
        requestHTTP({ flag: 'balance' }, configXD.LOTTERY_SERVICE).then(function (data) {
            if (data['msg'] === '') {
                configXD.ELS.elReloadBalance.classList.remove('active');
                let balance = configXD.currentBalance = data['balance'];
                configXD.ELS.elBalance.querySelector('.txtbalance').innerHTML = numberWithCommas(balance);
            } else {
                __this.errorCheck(data.msg);
            }
        }).catch(function (error) {
            console.log(error);
            __this.errorCheck(configXD.objAlert.errorConnectServer);
        });
    };
    this.openPopupInfor = (e) => {
        e.preventDefault();
        layer.open({
            btnAlign: 'c',
            btn: ['Đóng'],
            area: ['929px'],
            skin: 'hisUserBet',
            type: 1,
            content: $('#xocdiaInfor')
        });
    };
    this.openPopupHis = (e) => {
        e.preventDefault();
        if (configXD.xocdiaID === 91) {
            this.getHisUserBet(() => {
                layer.open({
                    btnAlign: 'c',
                    btn: ['Đóng'],
                    area: ['929px'],
                    skin: 'hisUserBet',
                    type: 1,
                    content: $('#historyBetUser')
                });
            });
            return;
        }
        layer.open({
            btnAlign: 'c',
            btn: ['Đóng'],
            area: ['929px'],
            skin: 'hisUserBet',
            type: 1,
            content: $('#historyBetUser')
        });
    };
    this.getHisUserBet = (callBackFun) => {
        let params = {
            flag: 'xocdia1mHisByUser',
            sid: configXD.xocdiaID,
        };
        requestHTTP(params, configXD.XOCDIA1S_SERVICE).then(data => {
            if (data && !this.isEmptyObject(data) && data['status'] === 'success') {
                let getData = data['data'];
                configXD.getHisAPI = getData['reslist'];
                this.reFormatHisFromApi();
                this.renderHistoryUserBet();
                let arrBtnCancelBet = document.querySelectorAll('.cancelBetItem');
                this.addEventEls('click', arrBtnCancelBet, 'cancelBetItem', (targetEl, i) => {
                    this.cancelBetItem(targetEl.dataset.id);
                });
                callBackFun && callBackFun();
            }
        });
    };
    this.cancelBetItem = (id) => {
        let params = {
            flag: 'xocdia1mCancel',
            sid: id,
        };
        requestHTTP(params, configXD.XOCDIA1S_SERVICE).then(function (data) {
            if (data['status'] === 'success') {
                showSuccessAlert(data['data'], 2000, () => {
                });
                __this.getHisUserBet();
            }
        }).catch(function (error) {
            __this.errorCheck(error);
        });
    };
    this.openAllIssueHis = (e) => {
        e.preventDefault();
        layer.open({
            btnAlign: 'c',
            btn: ['Đóng'],
            area: ['929px'],
            skin: 'hisUserBet',
            type: 1,
            content: $('#hisAllIssue')
        });
    };
    this.renderHistoryUserBet = (objData) => {
        let dataParams = objData || {};
        let lenHisApi = configXD.getHisAPI.length;
        let str = '';
        if (configXD.xocdiaID === 90) {
            str = '<table><tr><th>ID</th><th>Thời gian cược</th><th>Chi tiết</th><th>Kết quả</th><th>Trạng thái</th></tr>';
        } else if (configXD.xocdiaID === 91) {
            str = '<table><tr><th>ID</th><th>Lượt xổ</th><th>Chi tiết</th><th>Cược</th><th>Thắng</th><th>Kết quả</th><th>Trạng thái</th></tr>';
        }
        if (lenHisApi > 0) {
            for (let i = 0; i < lenHisApi; i++) {
                let item = configXD.getHisAPI[i];
                let status = item["status"];
                let strStatus = '';
                if (status === 4) {
                    strStatus = 'Trúng';
                } else if (status === 3) {
                    strStatus = 'Trượt';
                } else if (status === 0) {
                    strStatus = 'Chưa mở thưởng';
                } else if (status === 1) {
                    strStatus = 'Đang chờ mở thưởng';
                } else if (status === 2) {
                    strStatus = 'Đang chờ tính thưởng';
                } else if (status === 5) {
                    strStatus = 'Hủy nuôi số';
                } else if (status === 6) {
                    strStatus = 'Cá nhân hủy cược';
                } else if (status === 7) {
                    strStatus = 'Hủy mở thưởng';
                }
                let arrWin = [];
                let strVi = '';
                if (configXD.xocdiaID === 90) {
                    arrWin = item['winnumber'].split('|');
                    for (let j = 0; j < arrWin.length; j++) {
                        strVi += '<span class="vi bet-vi-color' + arrWin[j] + '" style="height:' + (configXD.hBoxViHis - 5) + 'px;width:' + (configXD.wBoxViHis - 4) + 'px;"></span>';
                    }
                    str += '<tr><td>' + item["id"] + '</td><td>' + item["bettime"] + '</td><td>' + item['betcontent'] + '</td><td>' + strVi + '</td><td class="status_' + status + '">' + strStatus + '</td></tr>';
                } else if (configXD.xocdiaID === 91) {
                    (item.hasOwnProperty('winnumber') && item['winnumber'] !== '') ? arrWin = item['winnumber'].split('') : arrWin = ['-none', '-none', '-none', '-none'];
                    for (let j = 0; j < arrWin.length; j++) {
                        strVi += '<span class="vi bet-vi-color' + arrWin[j] + '" style="height:' + (configXD.hBoxViHis - 5) + 'px;width:' + (configXD.wBoxViHis - 4) + 'px;"></span>';
                    }
                    let btnAction = '';
                    if (item['status'] === 0) {
                        btnAction = '<a class="cancelBetItem" href="javascript:void(0);" data-id="' + item["id"] + '">Hủy</a>';
                        str += '<tr><td>' + item["id"] + '</td><td>' + item["issue"] + '</td><td>' + item['betcontent'] + '</td>' +
                            '<td>' + numberWithCommas(item["amount"]) + '</td><td>' + numberWithCommas(item["bonus"]) + '</td><td class="oneLineVi">' + strVi + '</td><td class="status_' + status + '">' + strStatus + btnAction + '</td></tr>';
                    } else {
                        str += '<tr><td>' + item["id"] + '</td><td>' + item["issue"] + '</td><td>' + item['betcontent'] + '</td>' +
                            '<td>' + numberWithCommas(item["amount"]) + '</td><td>' + numberWithCommas(item["bonus"]) + '</td><td class="oneLineVi">' + strVi + '</td><td class="status_' + status + '">' + strStatus + '</td></tr>';
                    }
                }
            }
        } else {
            str += '<tr><td colspan="10">Không có dữ liệu!</td></tr>';
        }
        str += '</table>';
        configXD.ELS.elContentPopupHis.querySelector('.content').innerHTML = '';
        configXD.ELS.elContentPopupHis.querySelector('.content').innerHTML = str;
    };
    this.renderGridHistory = () => {
        for (let i = 0; i < configXD.colHis * configXD.rowHis; i++) {
            let elGrid = document.createElement('span');
            configXD.hBoxViHis = configXD.wWrapHis / configXD.colHis;
            configXD.wBoxViHis = (configXD.wWrapHis / configXD.colHis) - 1;
            elGrid.classList.add('grid-item');
            elGrid.style.height = configXD.hBoxViHis + 'px';
            elGrid.style.flex = '1 1 ' + configXD.wBoxViHis + 'px';
            configXD.ELS.elGridHistory.style.width = configXD.wWrapHis + 'px';
            configXD.ELS.elHistoryResult.style.width = configXD.wWrapHis + 'px';
            configXD.ELS.elHistoryResult.style.height = configXD.rowHis * (configXD.wWrapHis / configXD.colHis) + 'px';
            configXD.ELS.elGridHistory.appendChild(elGrid);
        }

        let lenHisApi = 0;
        let arrWinNumber = [];
        if (configXD.getHisAllIssueAPI && configXD.getHisAllIssueAPI.length > 0) {//render history all issue 1phut
            lenHisApi = configXD.getHisAllIssueAPI.length;
            arrWinNumber = configXD.getHisAllIssueAPI;
        } else { //render history of user bet 1giay
            lenHisApi = configXD.getHisAPI.length;
            arrWinNumber = configXD.getHisAPI;
        }

        if (lenHisApi > 0) {
            let strSlides = '';
            let strSlide = '<div class="swiper-slide">';
            let idxTemp = 0;
            for (let i = lenHisApi - 1; i >= 0; i--) {
                let item = arrWinNumber[i];
                let arrWin = [];
                let strVi = '';
                if (configXD.xocdiaID === 90) {
                    arrWin = item['winnumber'].split('|');
                } else if (configXD.xocdiaID === 91) {
                    item.hasOwnProperty('winnumber') ? arrWin = item['winnumber'].split('') : arrWin = ['-none', '-none', '-none', '-none'];
                }
                for (let j = 0; j < arrWin.length; j++) {
                    strVi += '<span class="vi bet-vi-color' + arrWin[j] + '" style="height:' + (configXD.hBoxViHis - 5) + 'px;width:' + (configXD.wBoxViHis - 4) + 'px;"></span>';
                }
                let strWrapVi = '<div class="vi-results" style="width:' + (configXD.wBoxViHis + 1) + 'px;">' + strVi + '</div>';
                idxTemp += 1;
                if (idxTemp < configXD.colHis) {
                    strSlide += strWrapVi;
                } else {
                    strSlide += strWrapVi;
                    strSlides += strSlide + '</div>';
                    idxTemp = 0;
                    strSlide = '<div class="swiper-slide">';
                }
                if (i === 0 && idxTemp > 0) {
                    strSlides += strSlide + '</div>';
                }
            }
            configXD.ELS.elHistoryResult.querySelector('.swiper-wrapper').innerHTML = strSlides;
        }
        configXD.swiperChipHis = new Swiper('#history-result .swiper-container', {
            slidesPerView: 1,
            preventClicks: true,
            navigation: {
                nextEl: '.history-button-next',
                prevEl: '.history-button-prev',
            },
        });
        configXD.swiperChipHis.slideTo(configXD.swiperChipHis.slides.length);
        setTimeout(() => {
            configXD.ELS.elHistoryResult.classList.add('show');
        }, 500);
    };
    this.getPositionXYEl = (el) => {
        let getRectEl = el.getBoundingClientRect();
        return { x: getRectEl.left, y: getRectEl.top, w: getRectEl.width, h: getRectEl.height };
    };
    this.addEventEls = (eventName, els, clsItem, funCallBack) => {
        for (let i = 0; i < els.length; i++) {
            let el = els[i];
            el.addEventListener(eventName, (e) => {
                e.preventDefault();
                let targetEl = null;
                if (el.classList.contains(clsItem)) {
                    targetEl = el;
                } else {
                    targetEl = el.parentElement;
                }
                funCallBack && funCallBack(targetEl, i);
            });
        }
    };
    this._removeClassesEffect = (els, clsBefore, clsAfter, timeoutEffect, funCallBack) => {
        for (let i = 0; i < els.length; i++) {
            if (els[i].classList.contains(clsBefore)) {
                els[i].classList.remove(clsBefore);
                els[i].classList.add(clsAfter);
                setTimeout(() => {
                    els[i].classList.remove(clsAfter);
                    funCallBack && funCallBack();
                }, timeoutEffect);
                break;
            }
        }
    };
    this.renderChips = () => {
        let strEl = '';
        let idxElActive;
        for (let i = 0; i < configXD.configChip.length; i++) {
            let statusClass = '';
            if (Number(configXD.configChip[i].status)) {
                statusClass = 'active';
                configXD.chipCurrentData.configChip = configXD.configChip[i];
                idxElActive = i;
            }
            strEl += '<div class="swiper-slide"><a href="javascript:void(0);" class="' + statusClass + ' chip chipItem chip-' + configXD.configChip[i].chipVal + '" style="width:' + configXD.wChip + 'px;height:' + configXD.hChip + 'px;line-height:' + configXD.hChip + 'px"><span class="item"></span><span class="light-around"></span><span class="numchip">' + configXD.configChip[i].chipVal + '</span></a></div>';
        }
        configXD.ELS.elChips.querySelector('.swiper-wrapper').innerHTML = strEl;

        configXD.swiperChip = new Swiper('#select-chip .swiper-container', {
            slidesPerView: configXD.numChipShow,
            preventClicks: false,
        });

        configXD.idxChipActive = configXD.swiperChip.activeIndex;

        configXD.swiperChip.on('slideChangeTransitionEnd', () => {
            let elChipCur = configXD.ELS.elChips.querySelector('.chip.active');
            configXD.chipCurrentData.XY = this.getPositionXYEl(elChipCur);
            if (configXD.ELS.elCurrentChipSelected) {
                this.addChipToBet(configXD.ELS.elCurrentChipSelected);
                configXD.ELS.elCurrentChipSelected = null;
            }
        });

        let arrElChip = configXD.ELS.elChips.querySelectorAll('.chip');
        configXD.chipCurrentData.XY = this.getPositionXYEl(arrElChip[idxElActive]);

        // event click in chip
        this.addEventEls('click', arrElChip, 'chip', (targetEl, idxEl) => {
            if (targetEl.classList.contains('active')) return;
            let getSwiperSlide = targetEl.parentElement;
            let getSwiperItems = getSwiperSlide.parentElement.querySelectorAll('.swiper-slide');
            configXD.idxChipActive = [...getSwiperItems].indexOf(getSwiperSlide);
            this._removeClassesEffect(arrElChip, 'active', 'hideEffect', 1000);
            targetEl.classList.add('active');
            configXD.chipCurrentData.configChip = configXD.configChip[idxEl];
            configXD.chipCurrentData.XY = this.getPositionXYEl(targetEl);
        });
    };
    this.updageTotalMoneyBet = () => {
        configXD.totalMoneyBet = 0;
        for (let data in configXD.betdata) {
            configXD.totalMoneyBet += configXD.betdata[data];
        }
        configXD.ELS.elMoneyBet.innerHTML = numberWithCommas(configXD.totalMoneyBet);
    };
    this.groupChip = (typeBet, elRenderChipSlide, flag) => {
        let elGroup = document.querySelector('[data-bet=' + typeBet + ']');
        let elChipEmbed = elGroup.querySelectorAll('.chipEmbed');
        let len = elChipEmbed.length;
        if (len > configXD.numGroup) {
            let elChipLast = elChipEmbed[configXD.numGroup - 1];
            elChipLast.classList.add('chipOther');
            elRenderChipSlide.style.top = elChipLast.offsetTop + 'px';
            elRenderChipSlide.style.left = elChipLast.offsetLeft + 'px';
            let elNumMoney = elChipLast.querySelector('.numchip');
            let totalMoneyChipGroup = 0;
            for (let i = 0; i < len - 1; i++) {
                let item = elChipEmbed[i];
                let getMoneyItem = Number(JSON.parse(item.dataset.itemchipbet).val) / 1000;
                totalMoneyChipGroup += getMoneyItem;
            }
            let flagMoneyTotal = 0;
            totalMoneyChipGroup < Number(elChipLast.textContent) ? flagMoneyTotal = Number(elChipLast.textContent) : flagMoneyTotal = totalMoneyChipGroup;
            if (flag === 'add') {
                elRenderChipSlide.classList.add('chipEmbedHideGroup');
                elNumMoney.innerHTML = flagMoneyTotal + Number(configXD.chipCurrentData.configChip['chipVal']);
            } else if (flag === 'minus') {
                elRenderChipSlide.classList.remove('chipEmbedHideGroup');
                elNumMoney.innerHTML = flagMoneyTotal - Number(configXD.chipCurrentData.configChip['chipVal']);
            }
        }
    };
    this.addChipToBet = (targetEl) => {
        let flagCheckMaxBet = false;
        let getDataset = targetEl.dataset;
        let getTypeBet = getDataset.bet.split('xodia_');
        if (getTypeBet.length > 1) {
            let betVal = getTypeBet[1];
            if (configXD.betdata.hasOwnProperty(betVal)) {
                let valTemp = configXD.betdata[betVal] + configXD.chipCurrentData.configChip['chipVal'] * 1000;
                for (let i = 0; i < configXD.model.length; i++) {
                    let item = configXD.model[i];
                    let moneyMax = item['maxbet'];
                    if (valTemp > moneyMax) {
                        showErrorAlert('Vượt quá giới hạn cược <strong>[' + numberWithCommas(moneyMax) + ']</strong> !');
                        flagCheckMaxBet = true;
                        break;
                    }
                }
                if (!flagCheckMaxBet) {
                    configXD.betdata[betVal] += configXD.chipCurrentData.configChip['chipVal'] * 1000
                }
            } else {
                configXD.betdata[betVal] = configXD.chipCurrentData.configChip['chipVal'] * 1000;
            }
        } else {
            showErrorAlert(configXD.objAlert.errorChipSlect, 2000);
            return;
        }
        if (flagCheckMaxBet) return;
        this.updageTotalMoneyBet();
        let getItemW = targetEl.clientWidth / 2;
        let getItemH = targetEl.clientHeight / 2;
        let xParent = targetEl.getBoundingClientRect().left;
        let yParent = targetEl.getBoundingClientRect().top;
        let elRenderChipSlide = document.createElement('span');
        let objDataSet = {
            firstX: configXD.chipCurrentData.XY.x,
            firstY: configXD.chipCurrentData.XY.y,
            val: Number(configXD.chipCurrentData.configChip['chipVal']) * 1000
        };

        elRenderChipSlide.innerHTML = '<span class="item"></span><span class="light-around"></span><span class="numchip">' + configXD.chipCurrentData.configChip.chipVal + '</span>';
        elRenderChipSlide.style.position = "absolute";
        elRenderChipSlide.style.top = configXD.chipCurrentData.XY.y + 'px';
        elRenderChipSlide.style.left = configXD.chipCurrentData.XY.x + 'px';
        elRenderChipSlide.style.zIndex = "15";
        elRenderChipSlide.style.width = configXD.wChip + 'px';
        elRenderChipSlide.style.height = configXD.hChip + 'px';
        elRenderChipSlide.style.lineHeight = configXD.hChip + 'px';
        elRenderChipSlide.classList.add('chipEmbed');
        elRenderChipSlide.classList.add('chipItem');
        elRenderChipSlide.classList.add(configXD.chipCurrentData.configChip.chipClass);
        elRenderChipSlide.setAttribute('data-typebet', getDataset.bet);
        targetEl.appendChild(elRenderChipSlide);
        setTimeout(() => {
            let elChips = document.querySelectorAll('[data-typebet=' + getDataset.bet + ']');
            let topItem = (yParent + getItemH - configXD.hChip / 2);
            let leftItem = (xParent + getItemW - configXD.wChip / 2);
            let zIndexItem = 10;
            if (elChips.length > 1) {
                let getZIndexLastOld = Number(elChips[elChips.length - 2].style.zIndex) + 1;
                let getTopLastOld = Number(elChips[elChips.length - 2].style.top.replace(/px/g, '')) - 10;
                elRenderChipSlide.style.top = getTopLastOld + 'px';
                elRenderChipSlide.style.left = leftItem + 'px';
                elRenderChipSlide.style.zIndex = "" + getZIndexLastOld + "";
                objDataSet.lastX = leftItem;
                objDataSet.lastY = getTopLastOld;
                elRenderChipSlide.setAttribute('data-itemchipbet', JSON.stringify(objDataSet));
            } else {
                elRenderChipSlide.style.top = topItem + 'px';
                elRenderChipSlide.style.left = leftItem + 'px';
                elRenderChipSlide.style.zIndex = "" + zIndexItem + "";
                objDataSet.lastX = leftItem;
                objDataSet.lastY = topItem;
                elRenderChipSlide.setAttribute('data-itemchipbet', JSON.stringify(objDataSet));
            }
            this.groupChip(getDataset.bet, elRenderChipSlide, 'add');
            if (!this.isEmptyObject(configXD.betdata) && configXD.ELS.elBtnBetSubmit) {
                configXD.ELS.elBtnBetSubmit.classList.remove('btnBetDisabel');
            }
        }, 0);
    };
    this.removeChipEmbedItem = (e) => {
        this.overlayBG().add();
        let targetEl = null;
        if (e.nodeName) {
            targetEl = e;
        } else {
            e.preventDefault();
            if (e.target.classList.contains('chipEmbed')) {
                targetEl = e.target;
            } else {
                targetEl = e.target.parentElement;
            }
        }
        let getDataSet = JSON.parse(targetEl.dataset.itemchipbet);
        let getTypeBet = targetEl.dataset.typebet.split('xodia_');
        if (getTypeBet.length > 1) {
            let betVal = getTypeBet[1];
            if (configXD.betdata.hasOwnProperty(betVal)) {
                this.groupChip(targetEl.dataset.typebet, targetEl, 'minus');
                configXD.betdata[betVal] -= getDataSet.val;
                if (configXD.betdata[betVal] <= 0) delete configXD.betdata[betVal];
            } else showErrorAlert(configXD.objAlert.errorChipSlect, 2000);
        } else {
            showErrorAlert(configXD.objAlert.errorChipSlect, 2000);
            return;
        }
        this.updageTotalMoneyBet();
        targetEl.style.top = getDataSet.firstY + 'px';
        targetEl.style.left = getDataSet.firstX + 'px';
        setTimeout(() => {
            if (this.isEmptyObject(configXD.betdata) && configXD.ELS.elBtnBetSubmit) {
                configXD.ELS.elBtnBetSubmit.classList.add('btnBetDisabel');
            }
            this.overlayBG().remove();
            targetEl.remove();
        }, 320);
    };
    this.reset = () => {
        configXD.betdata = {};
        if (this.isEmptyObject(configXD.betdata) && configXD.ELS.elBtnBetSubmit) {
            configXD.ELS.elBtnBetSubmit.classList.add('btnBetDisabel');
        }
        this.updageTotalMoneyBet();
    };
    this.eventSelectBet = () => {
        let arrElBet = configXD.ELS.elInnerGame.querySelectorAll('.itemBet');
        this.addEventEls('click', arrElBet, 'itemBet', (targetEl, idxEl) => {
            if (this.isEmptyObject(configXD.chipCurrentData)) {
                showErrorAlert(configXD.objAlert.errorChipSlect, 2000);
                return;
            }
            let curChipActive = null;
            for (let i = 0; i < configXD.swiperChip.slides.length; i++) {
                let item = configXD.swiperChip.slides[i];
                if (item.querySelector('.chip.active')) {
                    curChipActive = i;
                    break;
                }
            }
            let countCurShowChip = configXD.swiperChip.activeIndex + configXD.numChipShow;
            if (countCurShowChip <= curChipActive) { // slide to left
                configXD.swiperChip.slideTo(curChipActive);
                configXD.ELS.elCurrentChipSelected = targetEl;
            } else if (countCurShowChip - curChipActive > configXD.numChipShow) { // slide to right
                configXD.swiperChip.slideTo(curChipActive);
                configXD.ELS.elCurrentChipSelected = targetEl;
            } else { // slide normal
                this.addChipToBet(targetEl);
            }
        });

        this.addEventEls('contextmenu', arrElBet, 'itemBet', (targetEl, idxEl) => {
            let arrChipEmbed = document.querySelectorAll('.chipEmbed[data-typebet=' + targetEl.dataset.bet + ']');
            if (arrChipEmbed.length > 0) this.removeChipEmbedItem(arrChipEmbed[arrChipEmbed.length - 1]);
        });
    };
    this.cancelBet = (e) => {
        e.preventDefault();
        this.reset();
        let arrElChip = document.querySelectorAll('.chipEmbed');
        this.removeAllEls(arrElChip);
    };

    /**
     * @param strVi 0|1|1|2
     */
    this.updateHistory = (strVi) => {
        let lenSlide = configXD.swiperChipHis.slides.length;
        let lastSlide = configXD.swiperChipHis.slides[lenSlide - 1];
        let elViResults = lastSlide !== undefined ? lastSlide.querySelectorAll('.vi-results') : [];
        let arrVi = strVi.split('|');
        if (elViResults.length > 0 && elViResults.length < configXD.colHis) {
            let elViResult = document.createElement('div');
            elViResult.classList.add('vi-results');
            elViResult.style.width = (configXD.wBoxViHis + 1) + 'px';
            elViResult.classList.add('appendEffect');
            for (let i = 0; i < arrVi.length; i++) {
                let item = arrVi[i];
                let elVi = document.createElement('span');
                elVi.classList.add('vi');
                elVi.classList.add('bet-vi-color' + item);
                elVi.style.height = (configXD.hBoxViHis - 5) + 'px';
                elVi.style.width = (configXD.wBoxViHis - 4) + 'px';
                elViResult.appendChild(elVi);
            }
            lastSlide.appendChild(elViResult);
        } else if (elViResults.length === configXD.colHis || elViResults.length === 0) {
            let str = '<div class="swiper-slide"><div class="vi-results appendEffect" style="width:' + (configXD.wBoxViHis + 1) + 'px;">';
            let strVi = '';
            for (let i = 0; i < arrVi.length; i++) {
                let item = arrVi[i];
                strVi += '<span class="vi bet-vi-color' + item + '" style="height:' + (configXD.hBoxViHis - 5) + 'px;width:' + (configXD.wBoxViHis - 4) + 'px;"></span>';
            }
            str += strVi + '</div></div>';
            configXD.swiperChipHis.appendSlide(str);
            configXD.swiperChipHis.slideTo(configXD.swiperChipHis.slides.length);
        }
    };
    this.reBet = (e) => {
        e.preventDefault();
        if (configXD.storeElsReBetting.length <= 0) {
            showErrorAlert(configXD.objAlert.errorReBet, 2000);
            return;
        }
        fireEvent(configXD.ELS.elCancelBet, 'click');
        for (let i = 0; i < configXD.storeElsReBetting.length; i++) {
            let item = configXD.storeElsReBetting[i];
            if (configXD.betdata.hasOwnProperty(item.typeVal)) {
                configXD.betdata[item.typeVal] += item.dataChipBet.val;
            } else {
                configXD.betdata[item.typeVal] = item.dataChipBet.val;
            }
            let elRenderChipSlide = document.createElement('span');
            let objDataSet = {
                lastX: item.dataChipBet.lastX,
                lastY: item.dataChipBet.lastY,
                firstX: item.dataChipBet.firstX,
                firstY: item.dataChipBet.firstY,
                val: item.dataChipBet.val
            };
            elRenderChipSlide.innerHTML = '<span class="item"></span><span class="light-around"></span><span class="numchip">' + item.valMoney + '</span>';
            elRenderChipSlide.style.position = "absolute";
            elRenderChipSlide.style.top = objDataSet.lastY + 'px';
            elRenderChipSlide.style.left = objDataSet.lastX + 'px';
            elRenderChipSlide.style.zIndex = "15";
            elRenderChipSlide.style.width = configXD.wChip + 'px';
            elRenderChipSlide.style.height = configXD.hChip + 'px';
            elRenderChipSlide.style.lineHeight = configXD.hChip + 'px';
            elRenderChipSlide.classList.value = item.clsList;
            elRenderChipSlide.setAttribute('data-typebet', item.dataTypeBet);
            elRenderChipSlide.setAttribute('data-itemchipbet', JSON.stringify(objDataSet));
            configXD.ELS.elInnerGame.querySelector('[data-bet=' + item.dataTypeBet + ']').appendChild(elRenderChipSlide);
        }
        this.updageTotalMoneyBet();
        if (!this.isEmptyObject(configXD.betdata) && configXD.ELS.elBtnBetSubmit) {
            configXD.ELS.elBtnBetSubmit.classList.remove('btnBetDisabel');
        }
    };
    this.effectMoneyWinLoss = (objResult) => {
        let flagWin = false;
        let totalMoneyEachWin = 0;
        let totalMoneyBet = 0;
        for (let item in configXD.betdata) {
            totalMoneyBet += configXD.betdata[item];
            for (let i = 0; i < objResult['details'].length; i++) {
                if (objResult['orderStatus'] === 4 && item === objResult['details'][i]) {
                    flagWin = true;
                    for (let j = 0; j < configXD.model.length; j++) {
                        let itemJ = configXD.model[j];
                        if (itemJ.methodname === item) {
                            totalMoneyEachWin += configXD.betdata[item] * itemJ.ratio + configXD.betdata[item];
                        }
                    }
                }
            }
        }

        clearTimeout(this.timeoutEffectMoneyBox);
        this.effectMoneyBox(Number(objResult['balance']));

        configXD.ELS.elMoneyStatusBet.innerHTML = '-' + numberWithCommas(totalMoneyBet);
        configXD.ELS.elMoneyStatusBet.parentElement.classList.add('active');
        configXD.ELS.elMoneyStatusBet.classList.add('loss');
        configXD.ELS.elMoneyStatusBet.classList.add('active');
        setTimeout(() => {
            configXD.ELS.elMoneyStatusBet.parentElement.classList.remove('active');
            configXD.ELS.elMoneyStatusBet.classList.remove('active');
            configXD.ELS.elMoneyStatusBet.classList.remove('loss');
            setTimeout(() => {
                if (flagWin) {
                    configXD.ELS.elMoneyStatusBet.innerHTML = '+' + numberWithCommas(totalMoneyEachWin);
                    configXD.ELS.elMoneyStatusBet.parentElement.classList.add('active');
                    configXD.ELS.elMoneyStatusBet.classList.add('win');
                    configXD.ELS.elMoneyStatusBet.classList.add('active');
                    setTimeout(() => {
                        configXD.ELS.elMoneyStatusBet.parentElement.classList.remove('active');
                        configXD.ELS.elMoneyStatusBet.classList.remove('active');
                        configXD.ELS.elMoneyStatusBet.classList.remove('win');
                    }, 1500)
                }
            }, 100);
        }, 1500);
    };
    this.xocloFinish = (obj) => {
        configXD.objAudioOpen.play();
        configXD.objAudioOpen.volume = 0.3;
        this.overlayBG().add();
        let objResultNewAppend = {
            id: obj['betId'],
            betcontent: this.formatHis(obj["codes"], obj['betcontent']),
            bettime: obj['betTime'],
            status: obj['orderStatus'],
            winnumber: obj['result'],
        };
        configXD.getHisAPI.unshift(objResultNewAppend);
        this.renderHistoryUserBet(obj);
        let getResultDetail = obj['details'];
        let totalMoneyWin = 0;
        for (let i = 0; i < getResultDetail.length; i++) {
            let item = getResultDetail[i];
            configXD.ELS.elInnerGame.querySelector('[data-bet=xodia_' + item + ']').classList.add('effectResult');
            for (let j in configXD.betdata) {
                if (j === item) {
                    totalMoneyWin += configXD.betdata[j];
                }
            }
        }

        setTimeout(() => {
            this.overlayBG().remove();
            fireEvent(configXD.ELS.elCancelBet, 'click');
            this.removeClassesAllEls(configXD.ELS.elInnerGame.querySelectorAll('.itemBet'), 'effectResult');
        }, 600);
        this.reset();
    };
    this.effectMoneyBox = (money) => {
        configXD.ELS.elBalance.classList.add('reciveMoney');
        configXD.ELS.elBalance.querySelector('.txtbalance').innerHTML = numberWithCommas(money);
        this.timeoutEffectMoneyBox = setTimeout(() => {
            configXD.ELS.elBalance.classList.remove('reciveMoney');
        }, 600);
    };
    this.storeElsReBetting = () => {
        configXD.storeElsReBetting = [];
        let arrChipRebetting = configXD.ELS.elInnerGame.querySelectorAll('.chipEmbed');
        for (let i = 0; i < arrChipRebetting.length; i++) {
            let objItem = {};
            let item = arrChipRebetting[i];
            objItem.dataChipBet = JSON.parse(item['dataset']['itemchipbet']);
            objItem.dataTypeBet = item['dataset']['typebet'];
            objItem.typeVal = objItem.dataTypeBet.split('xodia_')[1];
            objItem.clsList = item.classList.value;
            objItem.valMoney = item.textContent;
            configXD.storeElsReBetting.push(objItem);
        }
    };
    this.plateInteractive = (flag, constructorFlag, callBack) => {
        if (constructorFlag && flag === 'open') {
            configXD.ELS.elXocLo.classList.remove('close');
            configXD.ELS.elXocLo.classList.remove('alreadyOpen');
            configXD.ELS.elXocLo.classList.add('open');
            configXD.statusOpenPlate = true;
        } else if (flag === 'open') {
            if (this.isEmptyObject(configXD.betdata)) {
                showErrorAlert(configXD.objAlert.errorBet, 2000);
                return;
            }
            if (Number(configXD.currentBalance) < configXD.totalMoneyBet) {
                showErrorAlert(configXD.objAlert.errorBalance, 2000);
                return;
            }
            configXD.statusOpenPlate = true;
            // start store rebetting
            if (configXD.xocdiaID === 90) this.storeElsReBetting();
            // end store rebetting

            callBack && callBack();
        } else if (flag === 'close') {
            configXD.ELS.elXocLo.classList.remove('open');
            configXD.ELS.elXocLo.classList.remove('alreadyOpen');
            configXD.ELS.elXocLo.classList.add('close');
        } else if (flag === 'xoc') {
            configXD.ELS.elWrapVi.innerHTML = 'Chưa có kết quả!';
            configXD.statusOpenPlate = false;
            let countVolume = 1;
            configXD.objAudioPlay.play();
            configXD.objAudioPlay.volume = countVolume;
            configXD.ELS.elXocLo.classList.add('active');
            setTimeout(() => {
                let intervalVolum = setInterval(() => {
                    countVolume -= 0.2;
                    if (countVolume <= 0) {
                        clearInterval(intervalVolum);
                        configXD.objAudioPlay.pause();
                        configXD.objAudioPlay.currentTime = 0;
                    } else {
                        configXD.objAudioPlay.volume = countVolume;
                    }
                }, 100);
                if (configXD.xocdiaID === 90) { // 1giay
                    configXD.ELS.elXocLo.classList.add('alreadyOpen');
                }
                configXD.ELS.elXocLo.classList.remove('active');
                this.overlayBG().remove();
                callBack && callBack();
            }, 2500);
        }
    };
    this.xocloAction = (apiCallBack, xocFinishCallBack) => {
        if (configXD.ELS.elXocLo.classList.contains('open')) {
            this.overlayBG().add();
            this.plateInteractive('close', null, null);
            setTimeout(() => {
                this.plateInteractive('xoc', null, xocFinishCallBack);
            }, 300);
        } else if (configXD.ELS.elXocLo.classList.contains('close') && configXD.xocdiaID === 90) {
            this.plateInteractive('open', null, apiCallBack);
        }
    };
    this.countTimer = (finishCallBack, elJQuery) => {
        timerCountDown(configXD.getServerTime, configXD.getEndTime, elJQuery, finishCallBack, (obj) => {
            if (Number(obj['s']) === 7 && configXD.statusOpenPlate) {
                fireEvent(configXD.ELS.elXocLo, 'click');
            }
        });
    };
    this.reFormatHisFromApi = () => {
        configXD.getHisAPI.map(item => {
            item['betcontent'] = this.formatHis(item["codes"], item['betcontent']);
        });
    };
    this.formatHis = (codes, betcontent) => {
        let arrCodes = codes.split('&').clean("");
        let strBetContent = betcontent.split('&');
        let str = '';
        function getVal(strBetContent, type) {
            let value = 0;
            for (let i = 0; i < strBetContent.length; i++) {
                let valItem = strBetContent[i];
                if (valItem.indexOf('betdata[' + type + ']') >= 0) {
                    value = valItem.split('=')[1];
                    break;
                }
            }
            return value;
        }
        for (let j = 0; j < arrCodes.length; j++) {
            let itemCode = arrCodes[j].toLowerCase();
            let getPrize = itemCode.split(':');
            let prize = 0;
            if (itemCode.search('prize') >= 0) prize = getPrize[getPrize.length - 1];
            if (configXD.xocdiaID === 91) prize = '';
            if (itemCode.indexOf('[odd]') >= 0) {
                str += '<span class="detailBetUser"><span>[Lẻ]: ' + numberWithCommas(getVal(strBetContent, 'odd')) + '</span><span class="winMoney">' + prize + '</span></span>';
            } else if (itemCode.indexOf('[even]') >= 0) {
                str += '<span class="detailBetUser"><span>[Chẵn]: ' + numberWithCommas(getVal(strBetContent, 'even')) + '</span><span class="winMoney">' + prize + '</span></span>';
            } else if (itemCode.indexOf('[white]') >= 0) {
                str += '<span class="detailBetUser"><span>[4Xanh]: ' + numberWithCommas(getVal(strBetContent, 'white')) + '</span><span class="winMoney">' + prize + '</span></span>';
            } else if (itemCode.indexOf('[red]') >= 0) {
                str += '<span class="detailBetUser"><span>[4Đỏ]: ' + numberWithCommas(getVal(strBetContent, 'red')) + '</span><span class="winMoney">' + prize + '</span></span>';
            } else if (itemCode.indexOf('[3white1red]') >= 0) {
                str += '<span class="detailBetUser"><span>[3Xanh1Đỏ]: ' + numberWithCommas(getVal(strBetContent, '3white1red')) + '</span><span class="winMoney">' + prize + '</span></span>';
            } else if (itemCode.indexOf('[1white3red]') >= 0) {
                str += '<span class="detailBetUser"><span>[1Xanh3Đỏ]: ' + numberWithCommas(getVal(strBetContent, '1white3red')) + '</span><span class="winMoney">' + prize + '</span></span>';
            }
        }
        return str;
    };
    initXD();
}

Array.prototype.clean = function (deleteValue) {
    for (let i = 0; i < this.length; i++) {
        if (this[i] == deleteValue) {
            this.splice(i, 1);
            i--;
        }
    }
    return this;
};