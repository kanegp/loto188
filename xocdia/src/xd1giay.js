import { requestHTTP, configXD, XOCDIA } from './services';
window.addEventListener ?
    window.addEventListener('load', function () {
        let objXD = new XOCDIA();
        XD1Giay(objXD);
    }, false) :
    window.attachEvents && window.attachEvents('onload', function () {
        let objXD = new XOCDIA();
        XD1Giay(objXD);
    });

function XD1Giay(objXD) {
    initData();

    function initData() {
        layer.load(2, { shade: 0.3 });
        requestHTTP({ flag: 'xocdia1sInit' }, configXD.XOCDIA1S_SERVICE).then(function (data) {
            layer.closeAll('loading');
            if (data['msg'] === '') { // success
                try {
                    configXD.configChip = JSON.parse(data['chips']);
                    configXD.getHisAPI = JSON.parse(data['history']);
                    configXD.xocdiaID = data['gameid'];
                    objXD.reFormatHisFromApi();
                    configXD.model = data['model'];
                    configXD.model = configXD.model.map(item => {
                        return {
                            ratio: item['level1'] - item['levelnum'],
                            levelnum: item['levelnum'],
                            maxbet: item['maxbet'],
                            methodname: item['methodname']
                        };
                    });
                    configXD.configChip.map(item => item['chipClass'] = 'chip-' + item['chipVal']);
                    init1Giay();
                } catch (e) {
                    console.log(e);
                }
            } else { // error
                objXD.errorCheck(data.msg);
            }
        }).catch(function (error) {
            objXD.errorCheck(configXD.objAlert.errorConnectServer);
        });
    }

    function init1Giay() {
        objXD.renderGridHistory();
        objXD.renderHistoryUserBet();
        objXD.setRatio();
        objXD.renderPlateWrapVi();
        setTimeout(() => {
            objXD.plateInteractive('open', true);
        }, 500);
        objXD.loadBalance();
        objXD.renderChips();
        objXD.eventSelectBet();

        // start xoclo action
        configXD.ELS.elXocLo.addEventListener('click', (e) => {
            e.preventDefault();
            objXD.xocloAction(() => {
                objXD.overlayBG().add();
                configXD.ELS.elLoadingMoChen.style.display = 'block';
                configXD.storebetdata = configXD.betdata;
                let params = {
                    flag: 'xocdia1sBet',
                    gameid: 90,
                    money: configXD.totalMoneyBet,
                    betdata: configXD.betdata,
                };
                requestHTTP(params, configXD.XOCDIA1S_SERVICE).then(function (data) {
                    objXD.overlayBG().remove();
                    configXD.ELS.elLoadingMoChen.style.display = 'none';
                    if (data['msg'] === '') {
                        let result = JSON.parse(data['betresult']);
                        let getVi = result['data']['result'];
                        objXD.effectMoneyWinLoss(result['data']);
                        objXD.updateHistory(getVi);
                        objXD.xocloFinish(result['data']);
                        objXD.randomPosiVi(getVi);
                        configXD.ELS.elXocLo.classList.remove('close');
                        configXD.ELS.elXocLo.classList.add('open');
                    } else {
                        objXD.errorCheck(data.msg);
                    }
                }).catch(function (error) {
                    objXD.errorCheck(error);
                });
            });
        });
        // end xoclo action
    }
}