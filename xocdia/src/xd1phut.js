import {requestHTTP, configXD, XOCDIA, fireEvent, formatToDate} from './services';

function XD1GPhut(objXD) {
    initData();

    function initData() {
        console.log('init 1p');
        layer.load(2, {shade: 0.3});
        requestHTTP({ flag: 'xocdia1mInit' }, configXD.XOCDIA1S_SERVICE).then(data => {
            layer.closeAll('loading');
            if (data['msg'] === '') { // success
                try {
                    configXD.getServerTime = data['servertime'];
                    configXD.getEndTime = data['curissue']['endtime'];
                    configXD.curIssue = data['curissue']['issue'];
                    configXD.storeCurIssue = configXD.curIssue;
                    configXD.lastIssue = data['lastopenissue']['issue'];
                    configXD.configChip = JSON.parse(data['chips']);
                    configXD.getHisAPI = JSON.parse(data['history']);
                    configXD.xocdiaID = data['gameid'];
                    objXD.reFormatHisFromApi();
                    configXD.model = data['model'];
                    configXD.model = configXD.model.map(item => {
                        return {
                            ratio: item['level1'] - item['levelnum'],
                            levelnum: item['levelnum'],
                            maxbet: item['maxbet'],
                            methodname: item['methodname']
                        } ;
                    });
                    configXD.configChip.map(item => item['chipClass'] = 'chip-'+item['chipVal']);
                    init1Phut();
                    return getResultsHis(10);
                }catch(e) {
                    console.log(e);
                }
            }else { // error
                objXD.errorCheck(data.msg);
            }
        }).then(data => {
            configXD.getHisAllIssueAPI = data['data'].map(item => {
                return {
                    winnumber: item['winnumber'],
                    opentime: item['opentime'],
                    issue: item['opentime'].trim().substring(0, 10).replace(/[-]/g, '') +'-'+item['issue']
                };
            });
            objXD.renderGridHistory();
            let getLastWin = configXD.getHisAllIssueAPI[0].winnumber.split('').join('|');
            objXD.randomPosiVi(getLastWin);
        }).catch(function(error) {
            objXD.errorCheck(configXD.objAlert.errorConnectServer);
        });
    }

    function init1Phut() {
        constructData();
        objXD.setRatio();
        objXD.renderPlateWrapVi();
        setTimeout(() => {
            objXD.plateInteractive('open', true);
        }, 100);
        objXD.loadBalance();
        objXD.renderChips();
        objXD.eventSelectBet();

        // start xoclo action
        configXD.ELS.elXocLo.addEventListener('click', (e) => {
            e.preventDefault();
            xoloFinishAuto();
        });
        // end xoclo action

        // show popup history all issue
        configXD.ELS.elShowAllIssueHis.addEventListener('click', (e) => {
            e.preventDefault();
            getResultsHis(100).then(data => {
                let getData = data['data'].map(item => {
                    return {
                        winnumber: item['winnumber'],
                        opentime: item['opentime'],
                        issue: item['opentime'].trim().substring(0, 10).replace(/[-]/g, '') +'-'+item['issue']
                    };
                });
                let str = '<table><tr><th>Lượt xổ</th><th>Kết quả</th></tr>';
                for (let i = 0; i < getData.length; i++) {
                    let item = getData[i];
                    let arrWin = item['winnumber'].split('');
                    let strVi = '';
                    for (let j = 0; j < arrWin.length; j++) {
                        strVi += '<span class="vi bet-vi-color'+arrWin[j]+'" style="height:'+(configXD.hBoxViHis-5)+'px;width:'+(configXD.wBoxViHis-4)+'px;"></span>';
                    }
                    str += '<tr><td>'+item['issue']+'</td><td>'+strVi+'</td></tr>'
                }
                str += '</table>';
                configXD.ELS.elPopupShowAllIssueHis.querySelector('.content').innerHTML = '';
                configXD.ELS.elPopupShowAllIssueHis.querySelector('.content').innerHTML = str;
                objXD.openAllIssueHis(e);
            }).catch(function(error) {
                objXD.errorCheck(error);
            });
        });
        // end popup history all issue

        // start bet action
        configXD.ELS.elBtnBetSubmit.addEventListener('click', (e) => {
            e.preventDefault();
            if (Number(configXD.currentBalance) < configXD.totalMoneyBet) {
                showErrorAlert(configXD.objAlert.errorBalance, 2000);
                return;
            }
            if (configXD.ELS.elBtnBetSubmit.classList.contains('btnBetDisabel')) return;
            if (objXD.isEmptyObject(configXD.betdata)) {
                configXD.ELS.elBtnBetSubmit.classList.add('btnBetDisabel');
                showErrorAlert(configXD.objAlert.errorBet, 2000);
                return;
            }
            let str = '';
            for (let key in configXD.betdata) {
                if (key === 'odd') {
                    str += '<span class="detailBetUser">[Lẻ]: '+numberWithCommas(configXD.betdata[key])+'</span>';
                }else if (key === 'even') {
                    str += '<span class="detailBetUser">[Chẵn]: '+numberWithCommas(configXD.betdata[key])+'</span>';
                }else if (key === 'white') {
                    str += '<span class="detailBetUser">[4Xanh]: '+numberWithCommas(configXD.betdata[key])+'</span>';
                }else if (key === 'red') {
                    str += '<span class="detailBetUser">[4Đỏ]: '+numberWithCommas(configXD.betdata[key])+'</span>';
                }else if (key === '3white1red') {
                    str += '<span class="detailBetUser">[3Xanh1Đỏ]: '+numberWithCommas(configXD.betdata[key])+'</span>';
                }else if (key === '1white3red') {
                    str += '<span class="detailBetUser">[1Xanh3Đỏ]: '+numberWithCommas(configXD.betdata[key])+'</span>';
                }
            }
            configXD.ELS.elConfirmBet.querySelector('.detailConfirm').innerHTML = '';
            configXD.ELS.elConfirmBet.querySelector('.detailConfirm').innerHTML = str;
            configXD.ELS.elConfirmBet.querySelector('.issueConfirm').innerHTML = configXD.curIssue;
            configXD.ELS.elConfirmBet.querySelector('.totalMoneyBetNum').innerHTML = numberWithCommas(configXD.totalMoneyBet);
            layer.open({
                btnAlign: 'c',
                btn: ['Xác nhận', 'Đóng'],
                area: ['500px'],
                skin: 'hisUserBet confirmBet',
                type: 1,
                content: $('#confirmBet'),
                yes: function(index, layero){
                    sbBet(index);
                }
            });
        });
        // end bet action
    }

    function xoloFinishAuto() {
        objXD.xocloAction(null, () => {
            if (configXD.storeCurIssue !== configXD.curIssue) return;
            configXD.ELS.elLoadingMoChen.style.display = 'block';
            getCurTime(() => {
                configXD.storeCurIssue = configXD.curIssue;
                configXD.ELS.elLoadingMoChen.style.display = 'none';
                if (configXD.xocdiaID === 91) { // 1phut
                    configXD.ELS.elXocLo.classList.add('plateTimer');
                    objXD.countTimer(() => {
                        configXD.ELS.elXocLo.classList.remove('plateTimer');
                        setTimeout(() => {
                            configXD.ELS.elXocLo.querySelector('.txtXoc').innerHTML = '';
                        }, 0);
                    }, $('#plateup .txtXoc'));
                }
            });
        });
    }

    function sbBet(index) {
        configXD.storebetdata = configXD.betdata;
        let params = {
            flag: 'xocdia1mBet',
            gameid: configXD.xocdiaID,
            trace_if: 0,
            total_nums: 1,
            total_money: configXD.totalMoneyBet,
            betdata: configXD.betdata,
            'buyitem[]': "{'money': "+configXD.totalMoneyBet+", 'nums': 1, 'times': '1'}",
            issue_start: configXD.curIssue
        };
        requestHTTP(params, configXD.XOCDIA1S_SERVICE).then(function(data) {
            layer.close(index);
            if (data['msg'] === '') {
                let getData = JSON.parse(data['betresult']);
                if (getData['success'] === 1 && getData['msg'] !== '') {
                    layer.msg(getData['msg'], {
                        shade : 0.3,
                        closeBtn : true,
                        icon : 1, // 1: icon success, 2: icon error, 3: warning question, 4: lock, 5: face sad, 6: face funny, 7: warning
                        time : 2000
                    }, function() {
                        objXD.storeElsReBetting();
                        objXD.effectMoneyWinLoss(getData['data']);
                        fireEvent(configXD.ELS.elXocLo, 'click');
                        fireEvent(configXD.ELS.elCancelBet, 'click');
                    });
                }
            }else {
                showErrorAlert(data['msg'], 2500, () => {
                    fireEvent(configXD.ELS.elCancelBet, 'click');
                });
            }
        }).catch(function(error) {
            objXD.errorCheck(error);
        });
    }

    function constructData() {
        objXD.countTimer(() => {
            getCurrentIssue();
        }, $('#counttime .timer'));
        configXD.ELS.elTimerIssue.querySelector('.issue').innerHTML = configXD.curIssue;
    }

    function getCurrentIssue(callBack) {
        requestHTTP({flag: 'xocdia1mCurIssue'}, configXD.XOCDIA1S_SERVICE).then(function(data) {
            if (formatToDate(data['endtime']).getTime() > formatToDate(data['servertime']).getTime()) {
                getBetOpen();
                configXD.getServerTime = data['servertime'];
                configXD.getEndTime = data['endtime'];
                configXD.curIssue = data['issue'];
                configXD.lastIssue = data['lastissue'];
                constructData();
                callBack && callBack();
            }else {
                console.log("Request Again!1231231231");
                setTimeout(() => {
                    getCurrentIssue();
                }, configXD.timeoutRequestApi);
            }
        }).catch(function(error) {
            objXD.errorCheck(error);
        });
    }

    let requestMaxGetCurTime = 0;
    function getCurTime(callBack) {
        requestHTTP({flag: 'xocdia1mCurIssue'}, configXD.XOCDIA1S_SERVICE).then(function(data) {
            if (formatToDate(data['endtime']).getTime() > formatToDate(data['servertime']).getTime()) {
                configXD.getServerTime = data['servertime'];
                configXD.getEndTime = data['endtime'];
                configXD.curIssue = data['issue'];
                configXD.lastIssue = data['lastissue'];
                callBack && callBack();
                requestMaxGetCurTime = 0;
            }else {
                console.log("Request Again!");
                requestMaxGetCurTime += 1;
                let timeoutRequest = setTimeout(() => {
                    getCurTime();
                }, configXD.timeoutRequestApi);
                if (requestMaxGetCurTime >= configXD.requestMax) {
                    clearTimeout(timeoutRequest);
                    objXD.errorCheck(configXD.objAlert.errorConnectServer);
                }
            }
        }).catch(function(error) {
            objXD.errorCheck(error);
        });
    }

    let countLoading = 0;
    function getBetOpen() {
        if (countLoading === 0) $('.history-wrap').loading();
        let params = {
            flag: 'xocdia1mBetOpen',
            lotteryid: configXD.xocdiaID,
            issue: configXD.lastIssue
        };

        if (!configXD.ELS.elXocLo.classList.contains('open')) {
            configXD.ELS.elLoadingMoChen.style.display = 'block';
        }
        requestHTTP(params, configXD.XOCDIA1S_SERVICE).then(function(data) {
            if (data['data'].msg.trim().search('未开奖') >= 0) {
                countLoading += 1;
                let timeoutRequest = setTimeout(()=> {
                    getBetOpen();
                }, configXD.timeoutRequestApi);
                if (countLoading >= configXD.requestMax) {
                    clearTimeout(timeoutRequest);
                    objXD.errorCheck(configXD.objAlert.errorConnectServer);
                }
            }else {
                setTimeout(()=>{
                    objXD.loadBalance();
                }, 5500);
                configXD.storeCurIssue = configXD.curIssue;
                configXD.ELS.elLoadingMoChen.style.display = 'none';
                $('.history-wrap').loading({remove: true});
                let getViResult = data['data'].code.join('|');
                countLoading = 0;
                objXD.updateHistory(getViResult);
                objXD.randomPosiVi(getViResult);
                configXD.objAudioOpen.play();
                configXD.objAudioOpen.volume = 0.3;
                objXD.plateInteractive('open', true);
                return getHisByUserBet();
            }
        }).then(data => {
            console.log('data', data);
            if (data && !objXD.isEmptyObject(data) && data['status'] === 'success') {
                let getData = data['data'];
                configXD.getHisAPI = getData['reslist'];
                objXD.reFormatHisFromApi();
                objXD.renderHistoryUserBet();
            }
        }).catch(function(error) {
            objXD.errorCheck(error);
        });
    }

    function getResultsHis(num) {
        let params = {
            flag: 'xocdia1mHis',
            sid: configXD.xocdiaID,
            num: num
        };
        return requestHTTP(params, configXD.XOCDIA1S_SERVICE);
    }

    function getHisByUserBet() {
        let params = {
            flag: 'xocdia1mHisByUser',
            sid: configXD.xocdiaID,
        };
        return requestHTTP(params, configXD.XOCDIA1S_SERVICE);
    }
}

window.addEventListener ?
    window.addEventListener('load', function() {
        let objXD = new XOCDIA();
        XD1GPhut(objXD);
    }, false) :
    window.attachEvents && window.attachEvents('onload', function() {
        let objXD = new XOCDIA();
        XD1GPhut(objXD);
    });