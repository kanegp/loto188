const path = require('path');
module.exports = {
    entry: {
        xd1giay: './src/xd1giay.js',
        xd1phut: './src/xd1phut.js'
    },
    output: {
        filename: '[name].js',
        path: path.resolve(__dirname, 'dist')
    },
    watch: true
};